package com.mindbowser.homeDisplay.interceptor;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.APP_VERSION_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.AUTHTOKEN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.AUTHTOKEN_KEYWORD;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FAIL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FITBIT_NOTIFICATION_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FORGOT_PASSWORD;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.INVALID_USER_TOKEN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.IOS_OS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOADBALANCER_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOGIN_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SIGN_UP_USER_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOCKET_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SQS_URL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SWAGGER_DOC;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_V1;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_BETA;



import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.model.ErrorModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.service.UserService;
import com.mindbowser.homeDisplay.util.ResourceManager;



/*@Provider
@Produces(MediaType.APPLICATION_JSON)*/
public class TokenAuthorizationInterceptor extends
		AbstractPhaseInterceptor<Message> {
	private Logger logger = Logger
			.getLogger(TokenAuthorizationInterceptor.class);
	@Autowired
	private UserService userService;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public TokenAuthorizationInterceptor() {
		super(Phase.PRE_INVOKE); // Put this interceptor in this phase
	}

	public void handleMessage(Message message) throws RuntimeException{
		HttpServletRequest httpRequest = (HttpServletRequest) message
				.get(AbstractHTTPDestination.HTTP_REQUEST);

		String pathInfo = httpRequest.getPathInfo();
		
		logger.info("PATH httpRequest" + pathInfo);
		MDC.put("ipaddress", httpRequest.getRemoteAddr());
		
		if(pathInfo.contains(LOADBALANCER_URL) || pathInfo.equals(DEFAULT_URL) || pathInfo.equals(FITBIT_NOTIFICATION_URL) || (pathInfo.contains(SOCKET_URL)) || (pathInfo.contains(SQS_URL)) || (pathInfo.contains(SWAGGER_DOC)))
		{
         return;		
		}
		
		
		
		String operatingSystem = httpRequest.getHeader("os");
		
		if(!operatingSystem.isEmpty())
			{
				if(operatingSystem.equals(ResourceManager.getProperty(IOS_OS)))
				{
					if(!pathInfo.contains(ResourceManager.getProperty(VERSION_V1)) && !pathInfo.contains(ResourceManager.getProperty(VERSION_BETA)))
					{
						String errorMessage=ResourceManager.getProperty(APP_VERSION_EXCEPTION);						
						ResponseModel responseModel = ResponseModel.getInstance();
						ErrorModel error = new ErrorModel(errorMessage);
						responseModel.setError(error);
						responseModel.setStatus(FAIL);
						String errorResponse = null;
						ObjectMapper mapper = new ObjectMapper();
						try {
							errorResponse = mapper.writeValueAsString(responseModel);
						} catch (Exception e) {
							logger.info(e);
						}
						HttpServletResponse response = (HttpServletResponse) message
								.get(AbstractHTTPDestination.HTTP_RESPONSE);
						response.setStatus(301);
						try {
							response.getWriter().write(errorResponse);
						} catch (IOException e) {
							logger.info(e);
						}
						throw new org.apache.cxf.interceptor.security.AccessDeniedException(ResourceManager.getProperty(APP_VERSION_EXCEPTION));
					}
				}
			}else
			{
				throw new org.apache.cxf.interceptor.security.AccessDeniedException(ResourceManager.getProperty(APP_VERSION_EXCEPTION));
			}	
		
		
		if(!(pathInfo.contains(SIGN_UP_USER_URL)) && !(pathInfo.contains(LOGIN_URL))  && !(pathInfo.contains(FORGOT_PASSWORD)))
		{
			System.out.println("******************** \n"+pathInfo);
			// get the authToken value from header
			String authToken = httpRequest.getHeader(AUTHTOKEN);
			logger.info(AUTHTOKEN_KEYWORD+ authToken);
			
			System.out.println("**********************"+authToken);
			
			UserModel user=null; 
			try{
				user = userService.getUserByAuthToken(authToken);
			}
			catch(Exception ex)
			{
				logger.error(ex.getStackTrace(),ex);
			}
			
			if (user == null) {
				String errorMessage=ResourceManager.getProperty(INVALID_USER_TOKEN);						
				 logger.info(AUTHTOKEN_KEYWORD+ errorMessage);				
				ResponseModel responseModel = ResponseModel.getInstance();
				ErrorModel error = new ErrorModel(errorMessage);
				responseModel.setError(error);
				responseModel.setStatus(FAIL);
				String errorResponse = null;
				ObjectMapper mapper = new ObjectMapper();
				try {
					errorResponse = mapper.writeValueAsString(responseModel);
				} catch (Exception e) {
					logger.info(e);
				}
				HttpServletResponse response = (HttpServletResponse) message
						.get(AbstractHTTPDestination.HTTP_RESPONSE);
				response.setStatus(500);
				try {
					response.getWriter().write(errorResponse);
				} catch (IOException e) {
					logger.info(e);
				}
				throw new org.apache.cxf.interceptor.security.AccessDeniedException(errorMessage);
			}
			
			httpRequest.setAttribute(USER, user);
			httpRequest.setAttribute("authToken", authToken);
			
		}
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
