package com.mindbowser.homeDisplay.Exception;

public class AccessDeniedException extends Exception {

	private static final long serialVersionUID = 5567874767993560081L;

	public AccessDeniedException() {
		super();
		
	}

	public AccessDeniedException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public AccessDeniedException(String message) {
		super(message);
		
	}

	public AccessDeniedException(Throwable cause) {
		super(cause);
		
	}

}
