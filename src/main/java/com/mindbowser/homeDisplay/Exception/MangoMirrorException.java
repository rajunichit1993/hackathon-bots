package com.mindbowser.homeDisplay.Exception;

@SuppressWarnings("serial")
public class MangoMirrorException extends Exception {
	
	public MangoMirrorException()
	{		
	}
	
	public MangoMirrorException(String message)
	{
		super(message);
	}
	
	public MangoMirrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public MangoMirrorException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
