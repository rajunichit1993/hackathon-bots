package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.CALENDER_DATA_SAVE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.CALENDER_FORMAT_LIST_SENT_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.QUOTES_DATA_SAVE_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.STICKYNOTES_UPDATE_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_LIST_SENT_SUCCESS;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.CalendarFormatModel;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.StickyNotesRemoveModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetData;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.service.WidgetService;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;



@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "/widget", description = "Widget specific REST services")
@Path("/{version}/widgets")
public class WidgetWebServiceImpl implements WidgetWebService{

	private static Logger logger = Logger.getLogger(WidgetWebServiceImpl.class);
	
	@Autowired
	private WidgetService widgetService;

	
	@GET
	@ApiOperation(value="WidgetList" , notes="The API returns the List of all available widgets")
	@Override
	public ResponseModel getAllWidgetList(@Context HttpServletRequest request,@PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("<------Widget List------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		List<WidgetModel> widgetList;
		widgetList = widgetService.getAllWidget(request,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(widgetList);
		responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, ResourceManager.getProperty(NOT_FOUND), locale));
		return responseModel;
		
	}

	
	@POST
	@Path("/widgetSetting")
	@ApiOperation(value="widget setting with value" , notes="The API returns the List of all widgets with value for specific user")
	@Override
	public ResponseModel getAllWidgetSetting(@Context HttpServletRequest request,
			WidgetSettingModel widgetSettingModel,@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Widget List with all values like X,Y(axis) and height width------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		List<WidgetData> widgetData= widgetService.getAllWidgetSetting(userModel,widgetSettingModel,version);
	    ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(widgetData);
		responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, ResourceManager.getProperty(NOT_FOUND), locale));
		return responseModel;
	}


	@POST
	@Path("/calendar")
	@ApiOperation(value="calender data" , notes="The API used to save calender data of user")
	@Override
	public ResponseModel saveCalendarData(@Context HttpServletRequest request,
			List<UserCalendarModel> userCalendarModelList,@QueryParam(value = "timeZone") int timeZone,@PathParam("version") String version)throws MangoMirrorException{
		logger.info("<------Save calender event data------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		widgetService.saveCalendarData(request,userCalendarModelList,timeZone,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(CALENDER_DATA_SAVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}


	@GET
	@Path("/calendarFormats")
	@ApiOperation(value="calender update" , notes="The API is used to update calendar setting of user for particular mirror")
	@Override
	public ResponseModel calendarFormatList(@Context HttpServletRequest request,@PathParam("version") String version)throws MangoMirrorException
	{
		logger.info("<------sqs message receiver------>");
		List<CalendarFormatModel> calendarFormatList =  widgetService.calendarFormatList();
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(calendarFormatList);
		responseModel.setMessage(ResourceManager.getMessage(CALENDER_FORMAT_LIST_SENT_SUCCESS, null, ResourceManager.getProperty(NOT_FOUND), null));
		return responseModel;
	}


	@PUT
	@Path("/widgetSetting")
	@ApiOperation(value = "update widget position", notes = "this function is used to update widget (height,width,xPosition,yPosition,deviceHeight,deviceWidth) of requested user")
	@Override
	public ResponseModel updateWidgetSetting(@Context HttpServletRequest request,
			List<GenericWidgetSettingModel> genericWidgetSettingModelList,@PathParam("version") String version)throws MangoMirrorException {
		logger.info("<------Update Widget Setting------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		Map<String, Object> widgets = widgetService.updateWidgetSetting(request, genericWidgetSettingModelList,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(widgets);
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE, null, ResourceManager.getProperty(NOT_FOUND),
				locale));
		return responseModel;
	}

	@PUT
	@Path("/quotes")
	@ApiOperation(value = "update quotes data", notes = "this function is used to update quotes data after every 8 hours")
	@Override
	public ResponseModel saveQuotesData()throws MangoMirrorException{
		logger.info("<------add quotes data------>");
		widgetService.saveQuotesData();
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(QUOTES_DATA_SAVE_SUCCESS_MESSAGE, null, ResourceManager.getProperty(NOT_FOUND),null));
		return responseModel;
	}


	@POST
	@Path("/widgetSettingSocket")
	@ApiOperation(value="widget setting with value" , notes="The API returns the List of all widgets with value for specific user")
	@Override
	public ResponseModel getSocketAllWidgetSetting(@Context HttpServletRequest request, WidgetSettingModel widgetSettingModel,
			@PathParam("version") String version) throws MangoMirrorException {
			logger.info("<------Widget List with all values like X,Y(axis) and height width------>");
			Locale locale = LocaleConverter.getLocaleFromRequest(request);
			UserModel userModel = (UserModel) request.getAttribute(USER);
			List<WidgetData> widgetData= widgetService.getSocketWidgetSetting(userModel,widgetSettingModel,version);
		    ResponseModel responseModel = ResponseModel.getInstance();
			responseModel.setObject(widgetData);
			responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, ResourceManager.getProperty(NOT_FOUND), locale));
			return responseModel;
	}

	@DELETE
	@Path("/stickyNotes")
	@ApiOperation(value="stickyNotes Model" , notes="The API remoove stickyNotes")
	@Override
	public ResponseModel removeStickyNotes(@Context HttpServletRequest request, StickyNotesRemoveModel stickyNotesRemoveModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------remove sticky notes------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		widgetService.removeStickyNotes(request, stickyNotesRemoveModel,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(STICKYNOTES_UPDATE_SUCCESS_MESSAGE, null, ResourceManager.getProperty(NOT_FOUND),locale));
		return responseModel;
	}
	
}
