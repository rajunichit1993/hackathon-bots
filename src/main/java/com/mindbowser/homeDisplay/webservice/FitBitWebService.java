

package com.mindbowser.homeDisplay.webservice;


import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.FitBitNotificationModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;

public interface FitBitWebService {

	
	/**
	 * This method is used to get List of health widget.
	 * @param request
	 * @return
	 */
	ResponseModel getHealthKitWidgetList(HttpServletRequest request,String version) throws MangoMirrorException;
			
	/**
	 * This method is used to save health data.
	 * @param request
	 * @param healthDataList
	 * @return
	 */
	ResponseModel saveHealthKitData(HttpServletRequest request,HealthWidgetModel healthWidgetModel,int timeZone,String version) throws MangoMirrorException;
	
	/**
	 * This method is used to remove health data.
	 * @param request
	 * @param healthDataList
	 * @return
	 */
	ResponseModel removeHealthKitData(HttpServletRequest request, List<HealthDataModel> healthDataList, String version) throws MangoMirrorException;
	
	/**
	 * This method is used to get particular list health widget by providing its master category.
	 * @param request
	 * @param masterCategory
	 * @return
	 */
	ResponseModel fitBitWidgetList(HttpServletRequest request,String healthWidgetMasterCategory,String version) throws MangoMirrorException;
	
	/**
	 * This method is used to update fitbit credentials.
	 * @param request
	 * @param masterCategory
	 * @return
	 */
	ResponseModel fitBitUpdateCredential(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	
	
	/**
	 * This method is used to get updated notification of any users data.
	 * @param request
	 * @return
	 */
	Response getFitBitNotification(HttpServletRequest request,String version,String verify) throws MangoMirrorException;
	
	/**
	 * This method is used to get updated notification of any users data.
	 * @param request
	 * @return
	 */
	Response getFitBitNotificationData(HttpServletRequest request,String version,List<FitBitNotificationModel> notificationList) throws MangoMirrorException;

	ResponseModel getFitbitProfileDetail(HttpServletRequest request,String version,String fitbitUserId)throws MangoMirrorException;
	
	/*ResponseModel refreshFitbitToken(HttpServletRequest request,String version)throws Exception;*/
	
}
