package com.mindbowser.homeDisplay.webservice;


import javax.ws.rs.POST;
import javax.ws.rs.Path;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SQS_PROCESSED_SUCCESS;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.service.SqsService;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;




@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "/sqs", description = "sqs listener")
@Path("/sqs")
public class SqsWebServiceImpl implements SqsWebService{

	private static Logger logger = Logger.getLogger(SqsWebServiceImpl.class);
	
	@Autowired
	private SqsService sqsService;

	
	@POST
	@Path("/sqsMessage")
	@ApiOperation(value="sqs message" , notes="The API used to receive sqs message")
	@Override
	public ResponseModel sqsMessageListner(SocketResponce socketResponce)
			throws MangoMirrorException {
		logger.info("<------sqs message receiver------>");
		sqsService.sqsMessageListner(socketResponce);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(SQS_PROCESSED_SUCCESS, null, "not.found", null));
		return responseModel;
	}
}
