package com.mindbowser.homeDisplay.webservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.ResponseModel;

public interface BetaUsersWebService {
	
	/**
	 * This method is used to get List of health widget.
	 * @param request
	 * @return
	 */
	ResponseModel getHealthKitWidgetList(HttpServletRequest request,String version) throws MangoMirrorException;
	
	/**
	 * this method is used to update widget setting of particular user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 * @throws WidgetSettingException
	 */
	ResponseModel updateWidgetSetting(HttpServletRequest request,List<GenericWidgetSettingModel> genericWidgetSettingModelList,String version)throws MangoMirrorException;

	
	ResponseModel updateFitbitDataFirstTime(HttpServletRequest request,String version)throws MangoMirrorException;
}
