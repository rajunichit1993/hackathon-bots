package com.mindbowser.homeDisplay.webservice;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;

public interface SocketWebService {
	
	
	/**
	 * 
	 * This method is used to check mirror existence  by its major and minor
	 * @param request
	 * @param major
	 * @param minor
	 * @throws Exception
	 */
	ResponseModel checkMirrorByMajorMinor(HttpServletRequest request,int major,int minor) throws MangoMirrorException;
	
	/**
	 * 
	 * This method is used to load general setting on panel
	 * @param request
	 * @param major
	 * @param minor
	 * @throws Exception
	 */
	ResponseModel loadGeneralSetting(HttpServletRequest request,String data)throws MangoMirrorException;

	
	/**
	 * 
	 * This method is used to load general setting on panel
	 * @param request
	 * @param major
	 * @param minor
	 * @throws Exception
	 */
	/*ResponseModel loadGeneralSetting(HttpServletRequest request,SocketRequestModel socketRequestModel)throws Exception;*/

	
	/**
	 * 
	 * This method is used to update widgets from panel
	 * @param request
	 * @param major
	 * @param minor
	 * @throws Exception
	 */
	ResponseModel updateWidgetSetting(HttpServletRequest request,String data)throws MangoMirrorException;

	/**
	 * 
	 * This method is used to make socket connection if its disconnected
	 * @param request
	 * @param mirrorId
	 * @param delayTime
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel reconnectSocket(String data) throws MangoMirrorException;
	
	
	/**
	 * 
	 * This method is used to make socket connection if its disconnected
	 * @param request
	 * @param mirrorId
	 * @param delayTime
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel updateTwitterCredentials(String data) throws MangoMirrorException;

}
