package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_ADDED_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_DELAY_UPDATE_SUCCESS;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.service.SmartMirrorService;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation; 

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "Mirror", description = "mirror specific REST services")
@Path("/{version}/mirrors")

public class SmartMirrorWebServiceImpl implements SmartMirrorWebService {

	private static Logger logger = Logger.getLogger(SmartMirrorWebServiceImpl.class);

	@Autowired
	private SmartMirrorService mirrorService;
	
	
	@Override
	@PUT
	public ResponseModel updateDelayTime(@Context HttpServletRequest request,@QueryParam(value = "mirrorId") int mirrorId,
			@QueryParam(value = "delay") int delay,@PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("<------update mirror delay------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		mirrorService.updateDelayTime(request,mirrorId,delay,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(MIRROR_DELAY_UPDATE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@POST
	@ApiOperation(value = "register new mirror", notes = "This method is used to register new mirror to particular user")
	@Override
	public ResponseModel saveMirror(@Context HttpServletRequest request,
			UserMirrorModel userMirrorModel,@PathParam("version") String version) throws MangoMirrorException {

		logger.info("<------Register new mirror------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		SmartMirrorModel smartMirrorModel = mirrorService.saveMirror(request, userMirrorModel,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(smartMirrorModel);
		responseModel.setMessage(ResourceManager.getMessage(MIRROR_ADDED_SUCCESS_MESSAGE, null, "not.found",locale));
		return responseModel;
	}

	
	@GET
	@Override
	@Path("/restart/{deviceId}")
	@ApiOperation(value = "restart mirror", notes = "This method is used to restart mirror")
	public ResponseModel restartMirror(@Context HttpServletRequest request,
			 @PathParam("deviceId") String deviceId,@PathParam("version") String version) throws MangoMirrorException {

		logger.info("========restart mirror============");
		mirrorService.restartMirror(request,deviceId,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_restart_publish_success"));
		return responseModel;
	}
	
	@GET
	@Override
	@Path("/reset/{deviceId}")
	@ApiOperation(value = "reset mirror", notes = "This method is used to make mirror factory reset")
	public ResponseModel resetMirror(@Context HttpServletRequest request,
			 @PathParam("deviceId") String deviceId,@PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("========reset factory method============");
		mirrorService.resetMirror(request,deviceId,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_reset_factory_success"));
		return responseModel;
	}

	@GET
	@Override
	@Path("/pair/{deviceId}")
	@ApiOperation(value = "mirror pairing mode", notes = "This method is use to put device in pairing mode")
	public ResponseModel mirrorPairingMode(@Context HttpServletRequest request,
			 @PathParam("deviceId") String deviceId,@PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("========mirror pairing mode============");
		mirrorService.mirrorPairingMode(request,deviceId,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_pair_success"));
		return responseModel;
	}
	
	@GET
	@Override
	@Path("/firmwareUpdate/{deviceId}")
	@ApiOperation(value = "mirror pairing mode", notes = "This method is use to put device in pairing mode")
	public ResponseModel firmwareUpdateCheck(@Context HttpServletRequest request,
			 @PathParam("deviceId") String deviceId,@PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("========mirror firmware update============");
		SmartMirrorModel smartMirrorModel =  mirrorService.firmwareUpdateCheck(deviceId);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_firmware_update_success"));
		responseModel.setObject(smartMirrorModel);
		return responseModel;
		
	}

	@GET
	@Override
	@Path("/blerange/{deviceId}/{broadcastRange}")
	@ApiOperation(value = "mirror change broadcast range", notes = "This method is use to update broadcast range of mirror ")
	public ResponseModel updateBleBroadcastRange(@Context HttpServletRequest request,@PathParam("deviceId") String deviceId,
			@PathParam("broadcastRange") int broadcastRange,@PathParam("version") String version)throws MangoMirrorException {

		logger.info("========mirror ble range update============");
		mirrorService.updateBleBroadcastRange(request,deviceId,broadcastRange,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_broadcast_range_update_success"));
		return responseModel;
	}

	@GET
	@Override
	@Path("/bluetooth/{deviceId}/{bluetoothStatus}")
	public ResponseModel updateBluetoothSetting(@Context HttpServletRequest request,@PathParam("deviceId") String deviceId,@PathParam("bluetoothStatus") String bluetoothStatus, @PathParam("version") String version)throws MangoMirrorException
	{
		logger.info("========mirror bluetooth status update============");
		mirrorService.updateBluetoothSetting(request,deviceId,bluetoothStatus,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_bluetooth_status_update_success"));
		return responseModel;
	}

	@GET
	@Override
	@Path("/resetall")
	public ResponseModel resetAllMirror(@Context HttpServletRequest request,@PathParam("version") String version) throws MangoMirrorException {
		logger.info("========reset factory method============");
		mirrorService.resetAllMirror(request,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getProperty("mirror_reset_factory_success"));
		return responseModel;
	}
	
}
