
package com.mindbowser.homeDisplay.webservice;

import javax.servlet.http.HttpServletRequest;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;


public interface SmartMirrorWebService {

	/**
	 * 
	 * This method is used to update time after which mirror will in sleep mode
	 * @param request
	 * @param mirrorId
	 * @param delayTime
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel updateDelayTime(HttpServletRequest request,int mirrorId,int delay,String version) throws MangoMirrorException;
	

	/**
	 * This method is used to save new mirror or allow another user to take ownership of mirror.
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 * @throws MangoMirrorException
	 */
	ResponseModel saveMirror(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
		
	/**
	 * 
	 * This method is used to restart mirror
	 * @param request
	 * @throws Exception
	 */
	ResponseModel restartMirror(HttpServletRequest request,String deviceId,String version) throws MangoMirrorException;
	
	/**
	 * 
	 * This method is used to make reset mirror by assigning it with major and minor 0,0
	 * @param request
	 * @throws Exception
	 */
	ResponseModel resetMirror(HttpServletRequest request,String deviceId,String version) throws MangoMirrorException;
	
	/**
	 * 
	 * This method is used to update current firmware to mirror
	 * @param request
	 * @throws Exception
	 */
	ResponseModel firmwareUpdateCheck(HttpServletRequest request,String deviceId,String version) throws MangoMirrorException;
	
	/**
	 * 
	 * This method is used to set ble broadcast range
	 * @param request
	 * @throws Exception
	 */
	ResponseModel updateBleBroadcastRange(HttpServletRequest request,String deviceId,int range,String version) throws MangoMirrorException;

	/**
	 * 
	 * This method is used to update bluetooth status like (on/off/alwayson)of mirror
	 * @param request
	 * @throws Exception
	 */
	ResponseModel updateBluetoothSetting(HttpServletRequest request, String deviceId, String bluetoothStatus, String version)throws MangoMirrorException;
	
	
	/**
	 * 
	 * This method is used to make mirror in pairing mode
	 * @param request
	 * @throws Exception
	 */
	ResponseModel mirrorPairingMode(HttpServletRequest request, String deviceId, String version) throws MangoMirrorException;
	
	
	//=================temp function===================
	
	ResponseModel resetAllMirror(HttpServletRequest request,String version) throws MangoMirrorException;
	
}
