package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PASSWORD_SENT_SUCCESSFULLY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_DATA_SENT_SUCCESSFULLY_ON_SOCKET;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_LOGGED_IN_SUCCESSFULLY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_LOG_SEND_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_PASSWORD_CHANGE_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_PROFILE_UNIT_UPDATE_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_REGISTERED_SUCCESSFULLY_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_TIMEZONE_SUCCESSFULLY_UPDATED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_UPDATE_SUCCESS_MESSAGE;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.BleInDataModel;
import com.mindbowser.homeDisplay.model.EmailLogDataModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.service.UserService;
import com.mindbowser.homeDisplay.util.EmailSenderThread;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * 
 * @author MB User : Gaurav Tripathi.
 *
 */
@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "/user", description = "User specific REST services")
@Path("/{version}/users")
public class UserWebServiceImpl implements UserWebService {

	private static Logger logger = Logger.getLogger(UserWebServiceImpl.class);

	@Autowired
	private UserService userService;

	@POST
	@Path("/getUserByAuthToken")
	@ApiOperation(value = "Get user details", response = UserModel.class, notes = "This API return specific user detail using authToken.")
	public UserModel getUser(@QueryParam("token") String authToken) throws MangoMirrorException {

		return userService.getUserByAuthToken(authToken);
	}

	@POST
	@Path("/signUp")
	@ApiOperation(value = "register user", notes = "register new user to the system.")
	public ResponseModel signUpUser(@Context HttpServletRequest request, @Context HttpServletResponse response,
			UserModel userModel, @PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------inside signUpUser start------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		ResponseModel responseModel = null;
		UserModel userModel1 = userService.saveUser(request, userModel, version);
		responseModel = ResponseModel.getInstance();
		responseModel.setObject(userModel1);
		responseModel.setMessage(ResourceManager.getMessage(USER_REGISTERED_SUCCESSFULLY_MESSAGE, null, NOT_FOUND, locale));
		return responseModel;
	}

	@PUT
	@Path("/logIn")
	@ApiOperation(value = "login user", response = UserDTO.class, notes = "login  detail.")
	@Override
	public ResponseModel logIn(@Context HttpServletRequest request, @Context HttpServletResponse response,
			UserModel userModel, @PathParam("version") String version) throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		ResponseModel responseModel = null;
		UserModel userResponceModel = userService.logIn(request, userModel, version);
		responseModel = ResponseModel.getInstance();
		responseModel.setObject(userResponceModel);
		responseModel.setMessage(ResourceManager.getMessage(USER_LOGGED_IN_SUCCESSFULLY, null, NOT_FOUND, locale));
		return responseModel;
	}

	@POST
	@Path("/forgotPassword")
	@ApiOperation(value = "forget password", notes = "recovery of password through emailId")
	@Override
	public ResponseModel forgotPassword(@Context HttpServletRequest request, @Context HttpServletResponse response,
			UserModel userModel, @PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Forgot Password start------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.forgotPassword(request, userModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(PASSWORD_SENT_SUCCESSFULLY, null, NOT_FOUND, locale));
		return responseModel;
	}

	@PUT
	@Path("/editProfile")
	@ApiOperation(value = "edit profile", notes = "user can edit his/her profile")
	@Override
	public ResponseModel editProfile(@Context HttpServletRequest request, UserModel userModel,
			@PathParam("version") String version) throws MangoMirrorException {

		logger.info("<------Edit Profile start------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel2 = userService.editProfile(request, userModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(userModel2);
		responseModel.setMessage(ResourceManager.getMessage(USER_UPDATE_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/timeZone")
	@ApiOperation(value = "update timezone", notes = "this method is used to update usertime zone in user table")
	@Override
	public ResponseModel updateUserTimezone(@Context HttpServletRequest request,
			@QueryParam(value = "timeZone") int timeZone, @PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------change user time zone for clockr------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.updateUserTimezone(request, timeZone, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel
				.setMessage(ResourceManager.getMessage(USER_TIMEZONE_SUCCESSFULLY_UPDATED, null, "not.found", locale));
		return responseModel;

	}

	@SuppressWarnings("rawtypes")
	@PUT
	@Path("/mirrorRangeType/{rangeType}")
	@ApiOperation(value = "publish mirror and user info", notes = "publish mirror information like major and minor with userId")
	@Override
	public ResponseModel publishWidgetSetting(@Context HttpServletRequest request, List<UserMirrorModel> userMirrorList,
			@PathParam("rangeType") String rangeType, @PathParam("version") String version)throws MangoMirrorException{

		logger.info("<------publish user and mirror specific info------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		ArrayList socketStatus = userService.publishWidgetSetting(request, userMirrorList, rangeType, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(socketStatus);
		responseModel.setMessage(
				ResourceManager.getMessage(USER_DATA_SENT_SUCCESSFULLY_ON_SOCKET, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/changePassword")
	@ApiOperation(value = "change password", notes = "this method is used to change password of specific user")
	@Override
	public ResponseModel changePassword(@Context HttpServletRequest request, UserModel userModel,
			@PathParam("version") String version) throws MangoMirrorException {

		logger.info("<------publish user and mirror specific info------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.changePassword(request, userModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(
				ResourceManager.getMessage(USER_PASSWORD_CHANGE_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/updateUnit")
	@ApiOperation(value = "update profile data unit", notes = "update unit of healthwidgets")
	@Override
	public ResponseModel updateUnit(@Context HttpServletRequest request, UserModel userModel,
			@PathParam("version") String version)throws MangoMirrorException{
		logger.info("<------ Update health widget units ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.updateUnit(request, userModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(
				ResourceManager.getMessage(USER_PROFILE_UNIT_UPDATE_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	@POST
	@Path("/log")
	@ApiOperation(value = "file", notes = "accept file type")
	@Override
	public ResponseModel sendUserLog(@Context HttpServletRequest request,EmailLogDataModel logData, @PathParam("version") String version)
			throws MangoMirrorException {
		logger.info("<------ send log to user ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		ExecutorService executor = Executors.newFixedThreadPool(2);
	    Runnable taskOne = new EmailSenderThread(logData,version);
	    executor.execute(taskOne);	
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(USER_LOG_SEND_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	
	@POST
	@Path("/mirrorRangeType/{rangeType}")
	@ApiOperation(value = "ble data model", notes = "accept bleIn with calendar and healthData update")
	@Override
	public ResponseModel updateData(@Context HttpServletRequest request, BleInDataModel bleInDataModel,
			@PathParam("rangeType") String rangeType,@QueryParam(value = "timeZone") int timeZone, @PathParam("version") String version)
			throws MangoMirrorException  {
		
		logger.info("<------ bleIn with sata update ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.updateData(request, bleInDataModel, rangeType,timeZone,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(USER_LOG_SEND_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	@GET
	@Path("/updateAllFitbitUserName")
	@ApiOperation(value = "", notes = "update all users fitbit name")
	@Override
	public ResponseModel updateAllFitbitUserName(@Context HttpServletRequest request,@PathParam("version") String version)
			throws MangoMirrorException {
		logger.info("<------ bleIn with sata update ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userService.updateAllFitbitUserName(request,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(USER_LOG_SEND_SUCCESS_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

}