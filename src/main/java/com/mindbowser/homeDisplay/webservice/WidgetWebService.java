package com.mindbowser.homeDisplay.webservice;



import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.StickyNotesRemoveModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;

public interface WidgetWebService {

	
	/**
	 * This method is used to get all widget List.
	 * @param request
	 * @return
	 */
	ResponseModel getAllWidgetList(HttpServletRequest request,String version) throws MangoMirrorException;
	
	/**
	 * This method is used to get all widget setting for specific user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 * @throws WidgetSettingException
	 */
	ResponseModel getAllWidgetSetting(HttpServletRequest request,WidgetSettingModel widgetSettingModel,String version) throws  MangoMirrorException;
	
	
	/**
	 * This method is used to get all widget setting for specific user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 * @throws WidgetSettingException
	 */
	ResponseModel getSocketAllWidgetSetting(HttpServletRequest request,WidgetSettingModel widgetSettingModel,String version) throws  MangoMirrorException;

	/**
	 * this method is used to update widget setting of particular user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 * @throws WidgetSettingException
	 */
	ResponseModel updateWidgetSetting(HttpServletRequest request,List<GenericWidgetSettingModel> genericWidgetSettingModelList,String version)throws MangoMirrorException;
	
	
	/**
	 * This method is used to save calender events for particular user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel saveCalendarData(HttpServletRequest request,List<UserCalendarModel> userCalendarModelList,int timeZone,String version)throws MangoMirrorException;
	
	
	/**
	 * This method is used to get all list of available format.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel calendarFormatList(HttpServletRequest request,String version)throws MangoMirrorException;
	
	/**
	 * This method is used to save calender events for particular user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel saveQuotesData()throws MangoMirrorException;
	
	
	/**
	 * this method is used to remove stickynote widgets of particular user.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 * @throws WidgetSettingException
	 */
	ResponseModel removeStickyNotes(HttpServletRequest request, StickyNotesRemoveModel stickyNotesRemoveModel,String version)throws MangoMirrorException;
	
}
