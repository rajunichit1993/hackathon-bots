package com.mindbowser.homeDisplay.webservice;




import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SocketResponce;

public interface SqsWebService {

		/**
	 * This method is used to receive sqs messages.
	 * @param request
	 * @param widgetSettingModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel sqsMessageListner(SocketResponce socketResponce)throws MangoMirrorException;
	
}
