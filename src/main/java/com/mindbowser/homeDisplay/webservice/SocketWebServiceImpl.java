package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_DETAIL_SENT_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOCKET_DISCONNECT_PUBLISH_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_TWITTER_CREDENTIAL_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_LIST_SENT_SUCCESS;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.service.SmartMirrorService;
import com.mindbowser.homeDisplay.service.SocketService;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;



@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "socket", description = "socket rellated services")
@Path("/socket")
public class SocketWebServiceImpl implements SocketWebService {
	
	@Autowired
	private SmartMirrorService mirrorService;
	
	@Autowired
	private SocketService socketService;
	
	
    @GET
    @Override
	public ResponseModel checkMirrorByMajorMinor(@Context HttpServletRequest request,
			@QueryParam("major") int major,@QueryParam("minor") int minor) throws MangoMirrorException {
	
    	SmartMirrorModel smartMirrorModel =  mirrorService.checkMirrorByMajorMinor(request,major,minor);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(smartMirrorModel);
		responseModel.setMessage(ResourceManager.getMessage(MIRROR_DETAIL_SENT_SUCCESS, null, "not.found", null));
		return responseModel;
	}

    @POST
    @Override
	public ResponseModel loadGeneralSetting(@Context HttpServletRequest request,String data) throws MangoMirrorException {
    	JSONObject jsonObject = new JSONObject(data);
		 Object socketBroadcastMessage = socketService.getWidgetSetting(jsonObject);
		 ResponseModel responseModel = ResponseModel.getInstance();
		 responseModel.setObject(socketBroadcastMessage);
		 responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, "not.found", null));
		 return responseModel;
	}
   
    @PUT
	@Override
	public ResponseModel updateWidgetSetting(@Context HttpServletRequest request,
			String data) throws MangoMirrorException {
    	 JSONObject jsonObject = new JSONObject(data);
		 socketService.updateWidgetSetting(jsonObject);
		 ResponseModel responseModel = ResponseModel.getInstance();
		 responseModel.setMessage(ResourceManager.getMessage(UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE, null, "not.found", null));
		 return responseModel;
	}

    @PUT
    @Path("/reconnectSocket")
	@Override
	public ResponseModel reconnectSocket(String data) throws MangoMirrorException {
       JSONObject jsonObject = new JSONObject(data);
		int major = jsonObject.getInt("major");
		int minor = jsonObject.getInt("minor");
		socketService.reconnectSocket(major,minor);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(SOCKET_DISCONNECT_PUBLISH_SUCCESS_MESSAGE, null, "not.found", null));
		return responseModel;
	}

	@PUT
    @Path("/twitter")
	@Override
	public ResponseModel updateTwitterCredentials(String data) throws MangoMirrorException {
		 JSONObject jsonObject = new JSONObject(data);
		 socketService.updateTwitterCredentials(jsonObject);
		 ResponseModel responseModel = ResponseModel.getInstance();
		 responseModel.setMessage(ResourceManager.getMessage(UPDATE_TWITTER_CREDENTIAL_SUCCESS_MESSAGE, null, "not.found", null));
		 return responseModel;
	}
}
