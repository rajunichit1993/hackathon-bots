package com.mindbowser.homeDisplay.webservice;

public class EnvelopeEncryptedMessage {

	private byte[] encryptedKey;
    private String ciphertext;
	
	
	
	public byte[] getEncryptedKey() {
		return encryptedKey;
	}
	public void setEncryptedKey(byte[] encryptedKey) {
		this.encryptedKey = encryptedKey;
	}
	public String getCiphertext() {
		return ciphertext;
	}
	public void setCiphertext(String ciphertext) {
		this.ciphertext = ciphertext;
	}
	
	
	
	
}
