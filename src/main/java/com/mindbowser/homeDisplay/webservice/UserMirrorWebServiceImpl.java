package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.CALENDER_DATA_SAVE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_DETAILS_UPDATE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_LIST_SENT_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_USER_REMOVE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PREVIEW_START_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PREVIEW_STOP_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_CLOCK_TIMEZONE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_QUOTES_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_TWITTER_CREDENTIAL_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_ADDED_SUCCESSFULLY_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WEATHER_DATA_UPDATE_SUCCESS;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UpdateQuotesModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.service.UserMirrorService;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "userMirror", description = "user and mirror specific REST services")
@Path("/{version}/userMirrors")
public class UserMirrorWebServiceImpl implements UserMirrorWebService {

	private static Logger logger = Logger
			.getLogger(UserMirrorWebServiceImpl.class);

	@Autowired
	private UserMirrorService userMirrorService;


	@GET
	@ApiOperation(value = "get userMirrorList", notes = "The API returns the List of all paired mirror with the specific user")
	@Override
	public ResponseModel getUserMirrorList(@Context HttpServletRequest request,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------mirror list List------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		Map<String, Object> mirrorList = userMirrorService.getUserMirrorList(request, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(mirrorList);
		responseModel.setMessage(ResourceManager.getMessage(
				MIRROR_LIST_SENT_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@ApiOperation(value = "update mirror detail", notes = "This api will update mirror information  for rspective user")
	@Override
	public ResponseModel updateMirrorDetail(@Context HttpServletRequest request, UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Update mirror detail------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userMirrorService.updateMirrorDetail(request, userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(
				MIRROR_DETAILS_UPDATE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@POST
	@ApiOperation(value = "usermirror list by major,minor to check linked user", notes = "This api check if accepted list of major,minor is registered or not.and return that mirror name")
	@Override
	public ResponseModel getUserListByMajorMinor(@Context HttpServletRequest request,List<SmartMirrorModel> mirroList,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Mirror List------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		List<UserMirrorModel> userMirrorList = userMirrorService.getUserListByMajorMinor(request, mirroList, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(userMirrorList);
		responseModel.setMessage(ResourceManager.getMessage(
				MIRROR_LIST_SENT_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/removeUserMirror")
	@ApiOperation(value = "remove user", notes = "This api is used to disable user access of particular mirror")
	@Override
	public ResponseModel removeUserMirror(@Context HttpServletRequest request,
			UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Remove mirror user------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userMirrorService.removeUserMirror(request, userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(
				MIRROR_USER_REMOVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/weatherLocation")
	@ApiOperation(value = "update location", notes = "This api is used to update location of user for particular mirror")
	@Override
	public ResponseModel updateWeatherLocation(
			@Context HttpServletRequest request,
			UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------Update weather location of user------>");
		Map<String, Object> eventsData = userMirrorService.updateWeatherLocation(request,userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(eventsData);
		responseModel.setMessage(ResourceManager.getMessage(
				WEATHER_DATA_UPDATE_SUCCESS, null, "not.found", null));
		return responseModel;
	}

	@SuppressWarnings("rawtypes")
	@PUT
	@Path("/calendarFormatSetting")
	@ApiOperation(value = "calender update", notes = "The API is used to update calendar setting of user for particular mirror")
	@Override
	public ResponseModel updateCalenderFormatSetting(@Context HttpServletRequest request,UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------update Calendar Setting begin------>");
		HashMap eventsData = userMirrorService.updateCalenderFormatSetting(
				request, userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(eventsData);
		responseModel.setMessage(ResourceManager.getMessage(
				CALENDER_DATA_SAVE_SUCCESS, null, "not.found", null));
		return responseModel;
	}

	@PUT
	@Path("/clockTimezone")
	@ApiOperation(value = "update timezone", notes = "The API is used to update clock timezone setting of user for particular mirror")
	@Override
	public ResponseModel updateClockTimezone(@Context HttpServletRequest request,UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException {
		logger.info("<------update Clock timezone------>");
		Map<String, String> data = userMirrorService.updateClockTimezone(
				request, userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(data);
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_CLOCK_TIMEZONE_SUCCESS, null, "not.found", null));
		return responseModel;
	}

	@POST
	@Path("/linkedUser")
	@ApiOperation(value = "register new linked user", notes = "This method is used to register new user to particular mirror as linked user")
	@Override
	public ResponseModel addLinkedUser(@Context HttpServletRequest request,UserMirrorModel userMirrorModel,
			@PathParam("version") String version) throws MangoMirrorException{
		logger.info("<------Register linked user to mirror------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userMirrorService.addLinkedUser(request, userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(
				USER_ADDED_SUCCESSFULLY_MESSAGE, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/twitterCredential")
	@ApiOperation(value = "twitter credential", notes = "This method is used to update user twitter credentials")
	@Override
	public ResponseModel updateTwitterCredentials(@Context HttpServletRequest request, @PathParam("version") String version,
			UserMirrorModel userMirrorModel) throws MangoMirrorException {
		logger.info("<------------update twitter credentials--------------->");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		userMirrorService.updateTwitterCredentials(request, version, userMirrorModel);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_TWITTER_CREDENTIAL_SUCCESS_MESSAGE, null, "not.found",
				locale));
		return responseModel;
	}

	@PUT
	@Path("/preview")
	@ApiOperation(value = "preview mode", notes = "This method is used to make mirror in preview mode")
	@Override
	public ResponseModel updatePreview(@Context HttpServletRequest request, @PathParam("version") String version, SmartMirrorModel smartMirrorModel)
			throws MangoMirrorException {
		logger.info("<------------ start and stop preview --------------->");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		SmartMirrorModel mirrorModel = userMirrorService.updatePreview(request, version, smartMirrorModel);
		ResponseModel responseModel = ResponseModel.getInstance();
		
		if(mirrorModel.getMirrorPreview())
		{
			responseModel.setMessage(ResourceManager.getMessage(PREVIEW_START_SUCCESS_MESSAGE, null, "not.found",locale));	
		}else
		{
			responseModel.setMessage(ResourceManager.getMessage(PREVIEW_STOP_SUCCESS_MESSAGE, null, "not.found",locale));
		}
		return responseModel;
	}

/*	@PUT
	@Path("/quotes")
	@ApiOperation(value = "quotes", notes = "This method is used to update quotes setting")
	@Override
	public ResponseModel updateQuotes(@Context HttpServletRequest request, UserMirrorModel userMirrorModel,@PathParam("version") String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		Map<String, Object> updatedQuotes = userMirrorService.updateQuotes(request,userMirrorModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(updatedQuotes);
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_QUOTES_SUCCESS_MESSAGE, null, "not.found",
				locale));
		return responseModel;
	}*/
	
	@PUT
	@Path("/quotes")
	@ApiOperation(value = "quotes", notes = "This method is used to update quotes setting")
	@Override
	public ResponseModel updateQuotes(@Context HttpServletRequest request, UpdateQuotesModel updateQuotesModel,@PathParam("version") String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		Map<String, Object> updatedQuotes = userMirrorService.updateQuotes(request,updateQuotesModel, version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(updatedQuotes);
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_QUOTES_SUCCESS_MESSAGE, null, "not.found",
				locale));
		return responseModel;
	}

}
