package com.mindbowser.homeDisplay.webservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UpdateQuotesModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;

public interface UserMirrorWebService {

	
	/**
	 * 
	 * This method is used to get list of mirror registered by requested user
	 * @param request
	 * @return
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel getUserMirrorList(HttpServletRequest request,String version) throws MangoMirrorException;
	
	/**
	 * This method is used to register user as a linked user to the mirror.
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 * @throws MangoMirrorException
	 */
	ResponseModel addLinkedUser(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	
	/**
	 * 
	 * this method is used to change/update device orientation (deviceMode). 
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 * 
	 */
	ResponseModel updateMirrorDetail(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	
	
	/**
	 * this method returns all the mirrors which is any how related with requested user either as linkedUser or owner.
	 * 
	 * @param request
	 * @param mirroList
	 * @return
	 * @throws MangoMirrorException
	 * 
	 */
	ResponseModel getUserListByMajorMinor(HttpServletRequest request,List<SmartMirrorModel> mirroList,String version) throws MangoMirrorException;
	
	/**
	 * this method returns all the mirrors which is any how related with requested user either as linkedUser or owner.
	 * 
	 * @param request
	 * @param SmartMirrorModel List
	 * @return
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel removeUserMirror(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	
	/**
	 * this method returns all the mirrors which is any how related with requested user either as linkedUser or owner.
	 * 
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 * @throws Exception
	 */
	ResponseModel updateWeatherLocation(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	
	
	/**
	 * this method returns all the mirrors which is any how related with requested user either as linkedUser or owner.
	 * 
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 * @throws Exception
	 */
	/*ResponseModel updateQuotes(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;*/
	
	ResponseModel updateQuotes(HttpServletRequest request,UpdateQuotesModel	updateQuotesModel,String version) throws MangoMirrorException;
	

	/**
	 * This method is used to update calendar settings.
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel updateCalenderFormatSetting(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	/**
	 * This method is used to update clock timezone for specific mirror.
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel updateClockTimezone(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	

	/**
	 * This method is used to update twitter widget credentials.
	 * @param request
	 * @param twitterModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel updateTwitterCredentials(HttpServletRequest request,String version,UserMirrorModel userMirrorModel)throws MangoMirrorException;
	
	/**
	 * This method is used to make mirror in preview or out of preview mode.
	 * @param request
	 * @param userMirrorModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel updatePreview(HttpServletRequest request,String version,SmartMirrorModel smartMirrorModel)throws MangoMirrorException;
	
}
