package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_URL_ADDRESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SERVER_HEALTH_GOOD;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.util.ResourceManager;

@Component
@Path("/")

public class DefaultPage {

	private static Logger logger = Logger.getLogger(DefaultPage.class);
	
	@GET
	public Response defaultLocation(@Context HttpServletRequest request) 
	{
		URI uri = null;
		try {
			String url = ResourceManager.getProperty(DEFAULT_URL_ADDRESS);
			uri = new URI(url);
		} catch (URISyntaxException e) {
			logger.info(e);
		}
		return Response.temporaryRedirect(uri).build();
	}

	
	@Produces({ MediaType.APPLICATION_JSON })
	@GET
	@Path("/checkHealth")
	public ResponseModel cheackHealthOfServer(@Context HttpServletRequest request) 
	{
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(SERVER_HEALTH_GOOD, null, "not.found", null));
		return responseModel;
	}
	
}
