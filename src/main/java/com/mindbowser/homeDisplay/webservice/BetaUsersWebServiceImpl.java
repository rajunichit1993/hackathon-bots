package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_KIT_DATA_SAVE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.service.BetaUserService;
import com.mindbowser.homeDisplay.util.BetaUserThreadSchedular;
import com.mindbowser.homeDisplay.util.FitbitDataThread;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "Mirror", description = "mirror specific REST services")
@Path("/{version}/betaUsers")
public class BetaUsersWebServiceImpl implements BetaUsersWebService {

	private static Logger logger = Logger.getLogger(FitBitWebServiceImpl.class);

	@Autowired
	private BetaUserService betaUserService;

	@Autowired
	private Mapper dozerMapper;
	
	Map<String, Object> twitterCredentialStatus ;
	
	@GET
	@Override
	@ApiOperation(value = "savehealth data", notes = "This method is used to save health datalist")
	public ResponseModel getHealthKitWidgetList(@Context HttpServletRequest request, @PathParam("version") String version)
			throws MangoMirrorException {
		logger.info("<------ save health data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		UserDTO userDTO = dozerMapper.
				map(userModel, UserDTO.class);
		betaUserService.saveBetaUserData(userDTO);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(HEALTH_KIT_DATA_SAVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	
	@PUT
	@Path("/widgetSetting")
	@ApiOperation(value = "update widget position", notes = "this function is used to update widget (height,width,xPosition,yPosition,deviceHeight,deviceWidth) of requested user")
	@Override
	public ResponseModel updateWidgetSetting(@Context HttpServletRequest request,
			List<GenericWidgetSettingModel> genericWidgetSettingModelList,@PathParam("version") String version)throws MangoMirrorException {
		logger.info("<------Update Widget Setting------>");
		
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		Map<String, Object> widgets = betaUserService.updateWidgetSetting(request, genericWidgetSettingModelList,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(widgets);
		responseModel.setMessage(ResourceManager.getMessage(
				UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE, null, ResourceManager.getProperty(NOT_FOUND),
				locale));
		return responseModel;
	}


	@DELETE
	@Path("/fitbit")
	@Override
	public ResponseModel updateFitbitDataFirstTime(@Context HttpServletRequest request, @PathParam("version") String version)
			throws MangoMirrorException {
		logger.info("<------ save health data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		betaUserService.updateFitbitDataFirstTime();
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(HEALTH_KIT_DATA_SAVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}
}
