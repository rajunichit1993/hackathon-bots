package com.mindbowser.homeDisplay.webservice;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FITBIT_CREDENTIAL_UPDATE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_KIT_DATA_SAVE_SUCCESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_LIST_SENT_SUCCESS;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.cxf.annotations.GZIP;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.FitBitNotificationModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.service.FitBitService;
import com.mindbowser.homeDisplay.util.FitbitDataThread;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;



@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Api(value = "Mirror", description = "mirror specific REST services")
@Path("/{version}/healthData")

public class FitBitWebServiceImpl implements FitBitWebService{

	private static Logger logger = Logger.getLogger(FitBitWebServiceImpl.class);
	
	@Autowired
	private FitBitService fitbitService;
	
	@Autowired
	private Mapper dozerMapper;
	
	@Value("${fitBitEndPointVerificationCode}")
	private String fitBitEndPointVerificationCode;

	@GET
	@Override
	@ApiOperation(value = "getHealth Widget List", notes = "This method is used to get list of all health widget")
	public ResponseModel getHealthKitWidgetList(@Context HttpServletRequest request, @PathParam("version") String version) throws MangoMirrorException {

		logger.info("<------ get widget data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		List<Map<String, Object>> data = fitbitService.getHealthKitWidgetList(version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setObject(data);
		responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, "not.found", locale));
		return responseModel;
	}


	@POST
	@Override
	@ApiOperation(value = "savehealth data", notes = "This method is used to save health datalist")
	public ResponseModel saveHealthKitData(@Context HttpServletRequest request, HealthWidgetModel healthWidgetModel, @QueryParam(value = "timeZone") int timeZone, @PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("<------ save health data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		UserDTO userDTO = dozerMapper.map(userModel, UserDTO.class);
		fitbitService.saveHealthKit(userDTO,healthWidgetModel,timeZone,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(HEALTH_KIT_DATA_SAVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@DELETE
	@Override
	@ApiOperation(value = "removehealth data", notes = "This method is used to remove health data")
	public ResponseModel removeHealthKitData(@Context HttpServletRequest request, List<HealthDataModel> healthDataList, @PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("<------ remove health data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		fitbitService.removeHealthKitData(request,healthDataList,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(HEALTH_KIT_DATA_SAVE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}
	
	@GET
	@Path("/{healthWidgetMasterCategory}")
	@Override
	@ApiOperation(value = "get health widget list", notes = "This method is used to get health widget list by master category")
	public ResponseModel fitBitWidgetList(@Context HttpServletRequest request,@PathParam("healthWidgetMasterCategory") String healthWidgetMasterCategory , @PathParam("version") String version) throws MangoMirrorException {
		
		logger.info("<------ save health data ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		fitbitService.healthWidgetList(healthWidgetMasterCategory,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(WIDGET_LIST_SENT_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

	@PUT
	@Path("/fitbitCredential")
	@Override
	@ApiOperation(value = "update fitbit credentials", notes = "This method is used to update particular mirrors fitbit credentials")
	public ResponseModel fitBitUpdateCredential(@Context HttpServletRequest request, UserMirrorModel userMirrorModel, @PathParam("version") String version)
			throws MangoMirrorException {
		logger.info("<------ update widget credentials ------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		fitbitService.fitBitUpdateCredential(request,userMirrorModel,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(FITBIT_CREDENTIAL_UPDATE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}


	@GET
	@Override
	@Path("/notifications")
	@ApiOperation(value = "fitbit notification", notes = "This method is used to get list of all health widget")
	public Response getFitBitNotification(@Context HttpServletRequest request,@PathParam("version") String version,@QueryParam("verify") String verify) throws MangoMirrorException {

		logger.info("<------ inside fitbit notification verify ------>");
		if(verify.equals(fitBitEndPointVerificationCode))
		{
			logger.info("<------ fitbit subscription verify success------>");
			return Response.status(Status.NO_CONTENT).build();
		}
		logger.info("<------ fitbit subscription verify not applied------>");
		return Response.status(Status.NOT_FOUND).build();
	}
	
	@POST
	@Override
	@Path("/notifications")
	@ApiOperation(value = "fitbit notification", notes = "This method is used to get list of all health widget")
	public Response getFitBitNotificationData(@Context HttpServletRequest request,@PathParam("version") String version,List<FitBitNotificationModel> notificationList) throws MangoMirrorException {
		logger.info("<------inside fitbit notification data ------>");
        ExecutorService executor = Executors.newFixedThreadPool(2);
        Runnable taskOne = new FitbitDataThread(notificationList);
        executor.execute(taskOne);
		return Response.status(Status.NO_CONTENT).build();
	}


	@GET
	@Path("/fitbitProfile/{fitbitUserId}")
	@Override
	public ResponseModel getFitbitProfileDetail(@Context HttpServletRequest request,@PathParam("version") String version,@PathParam("fitbitUserId") String fitbitUserId)
			throws MangoMirrorException {
		logger.info("<------inside fitbit profile data------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		fitbitService.getFitbitProfileDetail(fitbitUserId,version);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(FITBIT_CREDENTIAL_UPDATE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}

/*
	@GET
	@Path("/refreshToken")
	@Override
	public ResponseModel refreshFitbitToken(@Context HttpServletRequest request,@PathParam("version") String version) throws Exception {
		// logger.info("<------inside fitbit profile data------>");
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		FitBitAccountDTO fitBitAccountDTO = new FitBitAccountDTO();
		fitBitAccountDTO.setFitbitRefreshToken("109e0074d7f74d6765d6ff014671272416d8df8fe8b63e8d0194f809674f9c8f");
		fitbitService.getUpdatedFitbitAccessToken(fitBitAccountDTO);
		ResponseModel responseModel = ResponseModel.getInstance();
		responseModel.setMessage(ResourceManager.getMessage(FITBIT_CREDENTIAL_UPDATE_SUCCESS, null, "not.found", locale));
		return responseModel;
	}
	*/	
}
