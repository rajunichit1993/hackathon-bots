
package com.mindbowser.homeDisplay.webservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.BleInDataModel;
import com.mindbowser.homeDisplay.model.EmailLogDataModel;
import com.mindbowser.homeDisplay.model.ResponseModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;

public interface UserWebService {

	/**
	 * this method is used to get user record using authentication token.
	 * 
	 * @param authToken
	 * @return
	 */
	UserModel getUser(String authToken) throws MangoMirrorException;

	/**
	 * this method is used to register new user to the system.
	 * 
	 * @param request
	 * @param response
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel signUpUser(HttpServletRequest request, HttpServletResponse response, UserModel userModel,
			String version) throws MangoMirrorException;

	/**
	 * this api is used to login into the system
	 * 
	 * @param request
	 * @param response
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel logIn(HttpServletRequest request, HttpServletResponse response, UserModel userModel, String version)
			throws MangoMirrorException;

	/**
	 * this method is used to recover the registered user password through
	 * email.
	 * 
	 * @param request
	 * @param response
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel forgotPassword(HttpServletRequest request, HttpServletResponse response, UserModel userModel,
			String version) throws MangoMirrorException;

	/**
	 * This method is used to edit existing user profile.
	 * 
	 * @param request
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel editProfile(HttpServletRequest request, UserModel userModel, String version) throws MangoMirrorException;

	/**
	 * This method is used to update unit in user profile.
	 * 
	 * @param request
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel updateUnit(HttpServletRequest request, UserModel userModel, String version)throws MangoMirrorException;

	/**
	 * This method is used to change password of.
	 * 
	 * @param request
	 * @param userModel
	 * @return
	 * @throws UserException
	 */
	ResponseModel changePassword(HttpServletRequest request, UserModel userModel, String version) throws MangoMirrorException;

	/**
	 * this method is used to update particular user time zone.
	 * 
	 * @param request
	 * @param timezone
	 * @return
	 * @throws UserException
	 */
	ResponseModel updateUserTimezone(HttpServletRequest request, int timeZone, String version) throws MangoMirrorException;

	/**
	 * this method is used to send major minor and userRole
	 * 
	 * @param request
	 * @param timezone
	 * @return
	 * @throws UserException
	 */
	ResponseModel publishWidgetSetting(HttpServletRequest request, List<UserMirrorModel> userMirrorList,
			String rangeType, String version) throws MangoMirrorException;

	/**
	 * this method is used to send major minor and userRole
	 * 
	 * @param request
	 * @param timezone
	 * @return
	 * @throws UserException
	 */

	ResponseModel sendUserLog(HttpServletRequest request,EmailLogDataModel logData,
			String version) throws MangoMirrorException;
	
	
	/**
	 * this method is used to optimize bleIn,bleOut,update calendar and update step data
	 * 
	 * @param request
	 * @param timezone
	 * @return
	 * @throws UserException
	 */

	ResponseModel updateData(HttpServletRequest request,BleInDataModel bleInDataModel,
			String rangeType,int timeZone,String version) throws MangoMirrorException;
	
	
	ResponseModel updateAllFitbitUserName(HttpServletRequest request,String version) throws MangoMirrorException;

}
