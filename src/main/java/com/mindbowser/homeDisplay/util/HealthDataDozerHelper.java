package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.model.HealthDataModel;

public class HealthDataDozerHelper {
	public static  List<HealthDataDTO> MapModelToDto
	(final Mapper mapper, List<HealthDataModel> source, final Class<HealthDataDTO> destType)
	{
		final List<HealthDataDTO> dest = new ArrayList<HealthDataDTO>();
		for (HealthDataModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			HealthDataDTO healthData=mapper.map(element, destType);
			dest.add(healthData);
		}
		// finally remove all null values if any
		List<HealthDataDTO> s1 = new ArrayList<HealthDataDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
	
	public static  List<HealthDataModel> MapDtoToModel
	(final Mapper mapper, List<HealthDataDTO> source, final Class<HealthDataModel> destType)
	{
		final List<HealthDataModel> dest = new ArrayList<HealthDataModel>();
		for (HealthDataDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			HealthDataModel healthData=mapper.map(element, destType);
			dest.add(healthData);
		}
		// finally remove all null values if any
		List<HealthDataModel> s1 = new ArrayList<HealthDataModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
}
