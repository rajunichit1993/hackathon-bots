package com.mindbowser.homeDisplay.util;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EMAIL_IS_NOT_VALID_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NAME_IS_NOT_VALID_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NAME_NO_CHARACTER_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;




public class UserDetailValidation {
	
	
	public void validateEmail(String email) throws MangoMirrorException {
		  try {
		   String EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
		   Boolean b = email.matches(EMAIL_REGEX);
		   if (!b) {
		    throw new MangoMirrorException(
		      ResourceManager.getMessage(EMAIL_IS_NOT_VALID_EXCEPTION, null, NOT_FOUND, null));
		   }
		  } catch (Exception ex) {
		   throw ex;
		  }
	}
	
	
	public void validateName(String name) throws MangoMirrorException {
		  try {
		   String NAME_REGEX = "^[a-zA-Z\\s]*$";
		   Boolean b = name.matches(NAME_REGEX);
			 if(name.length()>50 || name.length()<1)
			 {
				 throw new MangoMirrorException(
					      ResourceManager.getMessage(NAME_NO_CHARACTER_EXCEPTION, null, NOT_FOUND, null));
			 }else
			 {
				 if (!b) {
					    throw new MangoMirrorException(
					      ResourceManager.getMessage(NAME_IS_NOT_VALID_EXCEPTION, null, NOT_FOUND, null));
					   }
					   
			 }
		  } catch (Exception ex) {
		   throw ex;
		  }
	}
	
	
	
	
}