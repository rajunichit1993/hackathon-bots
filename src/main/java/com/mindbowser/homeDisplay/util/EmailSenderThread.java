package com.mindbowser.homeDisplay.util;

import org.apache.log4j.Logger;

import com.mindbowser.homeDisplay.model.EmailLogDataModel;
import com.mindbowser.homeDisplay.service.UserService;

public class EmailSenderThread implements Runnable {
	
	Logger logger = Logger.getLogger(EmailSenderThread.class);
	private EmailLogDataModel logData;
	private String version;

		
   public EmailSenderThread(EmailLogDataModel logData,String version) {
		this.logData = logData;
		this.version = version;
	}

	public void run() {
		try {
			UserService userService = (UserService) ApplicationBeanUtil.getBean("userService");
			userService.sendUserLog(logData,version);
		}catch (Exception e) {
			logger.info(e);
		}
	}

	public void start() {
		Thread thread;
		thread = new Thread(this);
		thread.start();
	}
}
