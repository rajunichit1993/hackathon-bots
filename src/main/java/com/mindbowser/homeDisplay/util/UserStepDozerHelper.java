package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import com.mindbowser.homeDisplay.dto.UserStepDTO;
import com.mindbowser.homeDisplay.model.UserStepModel;

public class UserStepDozerHelper {
	public static  List<UserStepDTO> map
	(final Mapper mapper, List<UserStepModel> source, final Class<UserStepDTO> destType)
	{
		final List<UserStepDTO> dest = new ArrayList<UserStepDTO>();
		for (UserStepModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			UserStepDTO userStep=mapper.map(element, destType);
			dest.add(userStep);
		}
		// finally remove all null values if any
		List<UserStepDTO> s1 = new ArrayList<UserStepDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
}
