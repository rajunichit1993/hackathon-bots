package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mindbowser.homeDisplay.model.FitBitNotificationModel;
import com.mindbowser.homeDisplay.service.FitBitService;

public class FitbitDataThread implements Runnable {

	private static Logger logger = Logger.getLogger(FitbitDataThread.class);
	private Thread thread;
	private List<FitBitNotificationModel> fitBitNotificationModelList;
	
   public FitbitDataThread(List<FitBitNotificationModel> fitBitNotificationModelList) {
		this.fitBitNotificationModelList = fitBitNotificationModelList;
	}

	public void run() {
		try {
			List<FitBitNotificationModel> notificationModels = new ArrayList<>();
			for (FitBitNotificationModel fitBitNotificationModel : fitBitNotificationModelList) {
				notificationModels.add(fitBitNotificationModel);
			}
			FitBitService fitbitService = (FitBitService) ApplicationBeanUtil.getBean("fitbitService");
			fitbitService.saveNotificationFitbitData(notificationModels);
		}catch (Exception e) {
			logger.info(e);
		}
	}

	public void start() {
		thread = new Thread(this);
		thread.start();
	}
}
