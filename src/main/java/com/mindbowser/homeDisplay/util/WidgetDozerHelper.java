package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.WidgetModel;

public class WidgetDozerHelper {
	public static  List<WidgetModel> map
	(final Mapper mapper, List<WidgetDTO> source, final Class<WidgetModel> destType)
	{
		final List<WidgetModel> dest = new ArrayList<WidgetModel>();
		for (WidgetDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			WidgetModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<WidgetModel> s1 = new ArrayList<WidgetModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
	public static  List<WidgetDTO> MapModelToDto
	(final Mapper mapper, List<WidgetModel> source, final Class<WidgetDTO> destType)
	{
		final List<WidgetDTO> dest = new ArrayList<WidgetDTO>();
		for (WidgetModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			WidgetDTO widgetDTO=mapper.map(element, destType);
			dest.add(widgetDTO);
		}
		// finally remove all null values if any
		List<WidgetDTO> s1 = new ArrayList<WidgetDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
}
