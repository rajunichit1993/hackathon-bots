package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.QuotesCategoryDTO;
import com.mindbowser.homeDisplay.model.QuotesCategoryModel;

public class QuotesCategoryDozerHelper {
	public static  List<QuotesCategoryModel> map(final Mapper mapper, List<QuotesCategoryDTO> source, final Class<QuotesCategoryModel> destType)
	{
		final List<QuotesCategoryModel> dest = new ArrayList<QuotesCategoryModel>();
		for (QuotesCategoryDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			QuotesCategoryModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<QuotesCategoryModel> s1 = new ArrayList<QuotesCategoryModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
	
	
	public static  List<QuotesCategoryDTO> QuotesListModelToDto(final Mapper mapper, List<QuotesCategoryModel> source, final Class<QuotesCategoryDTO> destType)
	{
		final List<QuotesCategoryDTO> dest = new ArrayList<QuotesCategoryDTO>();
		for (QuotesCategoryModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			QuotesCategoryDTO notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<QuotesCategoryDTO> s1 = new ArrayList<QuotesCategoryDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
