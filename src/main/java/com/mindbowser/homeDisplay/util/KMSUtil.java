package com.mindbowser.homeDisplay.util;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.services.kms.model.DataKeySpec;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DecryptResult;
import com.amazonaws.services.kms.model.EncryptRequest;
import com.amazonaws.services.kms.model.EncryptResult;
import com.amazonaws.services.kms.model.GenerateDataKeyRequest;
import com.amazonaws.services.kms.model.GenerateDataKeyResult;

public class KMSUtil {
	
	
	@Value("${kms_masterkey}")
	private String kmsMasterKey;
	
	@Autowired
	private KMSClient awskmsClient;
	
	public String decryptKey(String encryptedKey,String emailId){
	       
		  byte[] decodeBase64src = Base64.getDecoder().decode(encryptedKey.getBytes());
    	  ByteBuffer encryptedKeyByteBuffer = ByteBuffer.wrap(decodeBase64src);
          DecryptRequest decryptRequest = new DecryptRequest().withCiphertextBlob(encryptedKeyByteBuffer);
          Map<String, String> encryptionContext = new HashMap<>();
          encryptionContext.put("emailId", emailId);
          decryptRequest.setEncryptionContext(encryptionContext);
          DecryptResult plainTextKey = awskmsClient.getAwskmsClient().decrypt(decryptRequest);
          String plaintTextKey = new String(Base64.getEncoder().encode(plainTextKey.getPlaintext().duplicate().array()));
          System.out.println(plaintTextKey);
		  return plaintTextKey;
	}
	
	public String decryptData(String plainTextKey,String cipherText){
		
		 byte[] decodeBase64src1 = Base64.getDecoder().decode(cipherText.getBytes());
         ByteBuffer encryptedmessage = ByteBuffer.wrap(decodeBase64src1);
         
         HashMap<String, String> map = new HashMap<>();
         map.put("context", plainTextKey);
         
         DecryptRequest decryptRequestMessage = new DecryptRequest().withCiphertextBlob(encryptedmessage);
         decryptRequestMessage.setEncryptionContext(map);
         
         DecryptResult plainTextKey1 = awskmsClient.getAwskmsClient().decrypt(decryptRequestMessage);
         String decryptedTextData = new String(Base64.getDecoder().decode(plainTextKey1.getPlaintext().array()));
         return decryptedTextData;
	}
	
	public String encryptText(String plainTextKey,String plainText){
		
		byte[] decodeBase64src1 = Base64.getEncoder().encode(plainText.getBytes());
        ByteBuffer encryptedMessageByteBuffer = ByteBuffer.wrap(decodeBase64src1);
        
        HashMap<String, String> map = new HashMap<>();
        map.put("context", plainTextKey);
        
        EncryptRequest encryptRequest = new EncryptRequest();
        encryptRequest.setPlaintext(encryptedMessageByteBuffer);
        encryptRequest.setEncryptionContext(map);
        encryptRequest.setKeyId(kmsMasterKey);
        
        EncryptResult encryptResult  = awskmsClient.getAwskmsClient().encrypt(encryptRequest);
        String encryptedString = new String(Base64.getEncoder().encode(encryptResult.getCiphertextBlob().duplicate().array()));

        return encryptedString;
	}
	
	
	public String getEncryptedDataKey(String emailId){
	        GenerateDataKeyRequest generateDataKeyRequest = new GenerateDataKeyRequest();
	        generateDataKeyRequest.setKeyId(kmsMasterKey);
	        Map<String, String> encryptionContext = new HashMap<>();
	        encryptionContext.put("emailId", emailId);
	        generateDataKeyRequest.setEncryptionContext(encryptionContext);
	        generateDataKeyRequest.setKeySpec(DataKeySpec.AES_256);
	        GenerateDataKeyResult dataKeyResult = awskmsClient.getAwskmsClient().generateDataKey(generateDataKeyRequest);
	        String encryptedTextKey = new String(Base64.getEncoder().encode(dataKeyResult.getCiphertextBlob().duplicate().array()));
	        System.out.println(dataKeyResult);
	        
		    return encryptedTextKey;
	}
	
	
	
	

}
