package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;

public class WidgetSettingDozerHelper {
	public static  List<WidgetSettingDTO> MapModelToDto
	(final Mapper mapper, List<WidgetSettingModel> source, final Class<WidgetSettingDTO> destType)
	{
		final List<WidgetSettingDTO> dest = new ArrayList<>();
		for (WidgetSettingModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			WidgetSettingDTO notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<WidgetSettingDTO> s1 = new ArrayList<>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
	public static  List<WidgetSettingModel> MapDtoToModel
	(final Mapper mapper, List<WidgetSettingDTO> source, final Class<WidgetSettingModel> destType)
	{
		final List<WidgetSettingModel> dest = new ArrayList<>();
		for (WidgetSettingDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			WidgetSettingModel widgetSettingModel=mapper.map(element, destType);
			dest.add(widgetSettingModel);
		}
		// finally remove all null values if any
		List<WidgetSettingModel> s1 = new ArrayList<>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
}
