package com.mindbowser.homeDisplay.util;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FORGOT_PASSWORD_EMAIL_PATH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.REGISTRATION_CONFIRMATION_EMAIL_PATH;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;
// Constants
import com.mindbowser.homeDisplay.dto.EmailDTO;
import com.mindbowser.homeDisplay.dto.EmailLogDTO;

public class EmailSender {
	Logger logger = Logger.getLogger(EmailSender.class);
    private JavaMailSender mailSender;
    

	@Autowired
    private VelocityEngine velocityEngine;	
	

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	
	/*
	 * Amazon simple mail service starts
	*/	
	//static final String FROM = "pbmindbowser@gmail.com";   // Replace with your "From" address. This address must be verified.
    //static final String TO = "gaurav.tripathi@mindbowser.com";  // Replace with a "To" address. If your account is still in the 
                                                       // sandbox, this address must be verified.
    
    static final String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
    static final String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";
   
    
	public EmailDTO sendRegistrationConfirmationMail( final EmailDTO email) throws Exception{
		try{
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
		    
				public void prepare(MimeMessage mimeMessage) throws Exception {
		             MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		             message.setTo(email.getTo());
		             message.setSubject(email.getSubject());
		             message.setSentDate(new Date());
		             
		             
		             Map<String, Object> model = new HashMap<String, Object>();	             
		             String text = VelocityEngineUtils.mergeTemplateIntoString(
				                velocityEngine , 
				                String.format(ResourceManager.getMessage(REGISTRATION_CONFIRMATION_EMAIL_PATH,
				                		null, REGISTRATION_CONFIRMATION_EMAIL_PATH, null)) , 
				                "UTF-8", model);
		             text = text.replace("userName" , email.getUsername());
		             message.setText(text, true);
		          }
		       };
		       mailSender.send(preparator);			
		      
		}catch(Exception ex){
			logger.info(ex);
		    throw ex;
		}
		 return email;
	}
	
	public EmailLogDTO sendLogOverMail( final EmailLogDTO emailLogDTO){
		try{
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage) throws Exception {
					 MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
		             message.setTo(emailLogDTO.getTo());
		             message.setCc(emailLogDTO.getCc());
		             message.setSubject(emailLogDTO.getSubject());
		             message.setSentDate(new Date());
		             message.addAttachment("user log", emailLogDTO.getFile());
		             message.setText("",true);
		          }
		       };
		       mailSender.send(preparator);			
		    
		}catch(Exception ex){
			logger.info(ex);
		}
		   return emailLogDTO;
	}
	
	public EmailDTO sendForgotPasswordMail(final EmailDTO email) throws Exception{
		try{
		  MimeMessagePreparator preparator = new MimeMessagePreparator() {
		        //@SuppressWarnings({ "rawtypes" })
				public void prepare(MimeMessage mimeMessage) throws Exception {

		             MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		             message.setTo(email.getTo());
		             message.setSubject("Forgot Password Mail");
		             Map<String, Object> model = new HashMap<>();	             
		             		             
		             String text = VelocityEngineUtils.mergeTemplateIntoString(
				                velocityEngine , 
				                String.format(ResourceManager.getMessage(FORGOT_PASSWORD_EMAIL_PATH,
				                		null, FORGOT_PASSWORD_EMAIL_PATH, null)) , 
				                "UTF-8", model);
		             text = text.replace("[name]", email.getUsername());
		             text = text.replace("[emailId]", email.getTo());
		             text = text.replace("[password]", email.getPassword());
		             message.setText(text, true);
		          }
		       };
		       mailSender.send(preparator);			
		       return email;
		}catch(Exception ex){
			logger.info(ex);
			throw ex;
		}
	}
		
	
	public String readHtmlTemplate(String path) throws Exception
	{
    	StringBuilder contentBuilder = new StringBuilder();
    	FileReader fileReader = null;
    	BufferedReader in = null;
     try {
    	 fileReader = new FileReader(path);
         in = new BufferedReader(fileReader);
         String str;
         while ((str = in.readLine()) != null) {
             contentBuilder.append(str);
         }
     } catch (IOException e) {
    	 logger.info(e);
    	 
     }finally {
    	 if(in!=null)
    	 {
    		 in.close();	 
    	 }
    	 if(fileReader!=null)
    	 {
    		 fileReader.close();	 
    	 }
		
	}
     
     String content = contentBuilder.toString();
	return content;
	}
}
