package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.model.MirrorPageModel;

public class MirrorPageDozerHelper {

	public static  List<MirrorPageDTO> mapModeltoDto
	(final Mapper mapper, List<MirrorPageModel> source, final Class<MirrorPageDTO> destType)
	{
		final List<MirrorPageDTO> dest = new ArrayList<MirrorPageDTO>();
		for (MirrorPageModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			MirrorPageDTO notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<MirrorPageDTO> s1 = new ArrayList<MirrorPageDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
	
	public static  List<MirrorPageModel> mapDtotoModel
	(final Mapper mapper, List<MirrorPageDTO> source, final Class<MirrorPageModel> destType)
	{
		final List<MirrorPageModel> dest = new ArrayList<MirrorPageModel>();
		for (MirrorPageDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			MirrorPageModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<MirrorPageModel> s1 = new ArrayList<MirrorPageModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
