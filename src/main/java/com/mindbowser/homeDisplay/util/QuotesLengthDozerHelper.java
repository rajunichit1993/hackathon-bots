package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.QuotesLengthDTO;
import com.mindbowser.homeDisplay.model.QuotesLengthModel;

public class QuotesLengthDozerHelper {
	public static  List<QuotesLengthModel> map
	(final Mapper mapper, List<QuotesLengthDTO> source, final Class<QuotesLengthModel> destType)
	{
		final List<QuotesLengthModel> dest = new ArrayList<QuotesLengthModel>();
		for (QuotesLengthDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			QuotesLengthModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<QuotesLengthModel> s1 = new ArrayList<QuotesLengthModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
