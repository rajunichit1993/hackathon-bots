
package com.mindbowser.homeDisplay.util;



import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mindbowser.homeDisplay.service.BetaUserService;
import com.mindbowser.homeDisplay.service.BetaUserServiceImpl;
import com.mindbowser.homeDisplay.service.FitBitService;

public class BetaUserThreadSchedular implements Runnable {

	private static Logger logger = Logger.getLogger(BetaUserThreadSchedular.class);
	private Thread thread;
	private String deviceId;
	
	@Autowired
	private BetaUserService betaUserService;
	
	 public BetaUserThreadSchedular(String deviceId) {
			this.deviceId = deviceId;
		}
	
	public void run() {
		
		synchronized (BetaUserServiceImpl.betaMap) {
			try {
				System.out.println(deviceId);
				BetaUserService betaUserService = (BetaUserService) ApplicationBeanUtil.getBean("betaUserService");
				betaUserService.betaUserDefaultSetting(deviceId); 
				
				 Iterator it = BetaUserServiceImpl.betaMap.entrySet().iterator();
					while (it.hasNext())
					{
					   Entry item = (Entry) it.next();
					   if(item.getKey().equals(deviceId)  )
					   {
						   System.out.println(deviceId);
						   BetaUserServiceImpl.betaMap.remove(item.getKey());   
					   }
					}
				
			}catch (Exception e) {
				logger.info(e);
			}	
		}
	}

	public void start() {
		thread = new Thread(this);
		thread.start();
	}
	
}