package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;

public class SmartMirrorDozerHelper {

	public static  List<SmartMirrorDTO> map
	(final Mapper mapper, List<SmartMirrorModel> source, final Class<SmartMirrorDTO> destType)
	{
		final List<SmartMirrorDTO> dest = new ArrayList<>();
		for (SmartMirrorModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			SmartMirrorDTO notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<SmartMirrorDTO> s1 = new ArrayList<>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
