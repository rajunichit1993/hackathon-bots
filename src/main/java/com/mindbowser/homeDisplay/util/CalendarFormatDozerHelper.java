package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.model.CalendarFormatModel;

public class CalendarFormatDozerHelper {
	public static  List<CalendarFormatModel> map
	(final Mapper mapper, List<CalendarFormatDTO> source, final Class<CalendarFormatModel> destType)
	{
		final List<CalendarFormatModel> dest = new ArrayList<CalendarFormatModel>();
		for (CalendarFormatDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			CalendarFormatModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<CalendarFormatModel> s1 = new ArrayList<CalendarFormatModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
