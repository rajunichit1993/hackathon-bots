package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.UserCalendarDTO;
import com.mindbowser.homeDisplay.model.UserCalendarModel;

public class UserCalenderDozerHelper {
	public static  List<UserCalendarDTO> map
	(final Mapper mapper, List<UserCalendarModel> source, final Class<UserCalendarDTO> destType)
	{
		final List<UserCalendarDTO> dest = new ArrayList<UserCalendarDTO>();
		for (UserCalendarModel element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			UserCalendarDTO userCalendar=mapper.map(element, destType);
			dest.add(userCalendar);
		}
		// finally remove all null values if any
		List<UserCalendarDTO> s1 = new ArrayList<UserCalendarDTO>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
}
