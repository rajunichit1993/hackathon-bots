package com.mindbowser.homeDisplay.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.model.UserMirrorModel;

public class UserMirrorDozerHelper {

	public static  List<UserMirrorModel> map
	(final Mapper mapper, List<UserMirrorDTO> source, final Class<UserMirrorModel> destType)
	{
		final List<UserMirrorModel> dest = new ArrayList<UserMirrorModel>();
		for (UserMirrorDTO element : source)
		{
			if (element == null)
			{
				continue;
			}		 
			UserMirrorModel notificationModel=mapper.map(element, destType);
			dest.add(notificationModel);
		}
		// finally remove all null values if any
		List<UserMirrorModel> s1 = new ArrayList<UserMirrorModel>();
		s1.add(null);
		dest.removeAll(s1);
		return dest;
	}
	
}
