package com.mindbowser.homeDisplay.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class CalendarUtcDateTime {

	private static Logger logger = Logger.getLogger(CalendarUtcDateTime.class);
	
	public static final  String UTC_DATE_FORMAT_MILLIS = "yyyy-MM-dd HH:mm:ss:SSS"; 
	public static final  String UTC_DATE_FORMAT_SECOND = "yyyy-MM-dd hh:mm:ss";
	public static final  String UTC_24HOUR_DATE_FORMAT_SECOND = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * This will generate current utc date time
	 * 
	 * @param 
	 * @return utcaDateTime
	 * @throws 
	 */
	public static String getUtcDateTime()
	{
		String currentDate = null;
		try
		{
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat sdf = new SimpleDateFormat(UTC_DATE_FORMAT_SECOND);
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			currentDate = sdf.format(cal.getTime());
			
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
		return currentDate;
	}
	
	
	/**
	 * This will generate current utc date time with milliseconds
	 * 
	 * @param 
	 * @return getUtcDateTimeInMillis
	 * @throws 
	 */
	public static String getUtcDateTimeInMillis()
	{
		String currentDate = null;
		try
		{
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat sdf = new SimpleDateFormat(UTC_DATE_FORMAT_MILLIS);
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			currentDate = sdf.format(cal.getTime());
			
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
		return currentDate;
	}
	
}
