
package com.mindbowser.homeDisplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;

public class AsymmetricCryptography {

	private Cipher cipher;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	
	@Value("${aws_access_key_id}")
	private String accessKeyId;
	
	@Value("${aws_secret_key_id}")
	private String secretKey;
	

	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	public PublicKey getPublicKey() {
		return this.publicKey;
	}
	
	
	public AsymmetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.cipher = Cipher.getInstance("RSA");
	}

	@SuppressWarnings("deprecation")
	public PrivateKey getPrivate(File filename) throws Exception {
		
		File privateKey = new File(System.getProperty("java.io.tmpdir")+"KeyPair/privateKey");
		System.out.println("================================="+privateKey+"=================================");
		KeyFactory kf ;
		PKCS8EncodedKeySpec spec;
		if(!privateKey.exists()){
			AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKey);
		    AmazonS3Client s3Client = new AmazonS3Client(credentials);
			s3Client.getObject(new GetObjectRequest("mangomirrorsshkey", "privateKey"), privateKey);
			byte[] keyBytes = Files.readAllBytes(filename.toPath());
			spec = new PKCS8EncodedKeySpec(keyBytes);
			kf = KeyFactory.getInstance("RSA");
		}else
		{
			byte[] keyBytes = Files.readAllBytes(filename.toPath());
			spec = new PKCS8EncodedKeySpec(keyBytes);
			kf = KeyFactory.getInstance("RSA");	
		}
		
		return kf.generatePrivate(spec);
	}

	@SuppressWarnings("deprecation")
	public PublicKey getPublic(File filename) throws Exception {
		
		File publicKey = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
		System.out.println("================================="+publicKey+"=================================");
		if(!publicKey.exists()){
			AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKey);
		    AmazonS3Client s3Client = new AmazonS3Client(credentials);
	        s3Client.getObject(new GetObjectRequest("mangomirrorsshkey", "publicKey"), publicKey);
		}
		byte[] keyBytes = Files.readAllBytes(filename.toPath());
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}


	public String encryptText(String msg, PrivateKey key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		return   Base64.getEncoder().encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
	}

	public String decryptText(String msg, PublicKey key)
			throws InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(Base64.getDecoder().decode(msg)), "UTF-8");
	}

	public byte[] getFileInBytes(File f) throws IOException {
		FileInputStream fis = new FileInputStream(f);
		byte[] fbytes = new byte[(int) f.length()];
		fis.read(fbytes);
		fis.close();
		return fbytes;
	}

		
		/*AsymmetricCryptography ac1;
		try {
			ac1 = new AsymmetricCryptography(1024);
			ac1.createKeys();
			ac1.writeToFile("KeyPair/publicKey", ac1.getPublicKey().getEncoded());
			ac1.writeToFile("KeyPair/privateKey", ac1.getPrivateKey().getEncoded());
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		
	public void encryptFile(byte[] input, File output, PrivateKey key)
		throws IOException, GeneralSecurityException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		writeToFile(output, this.cipher.doFinal(input));
	}

	public void decryptFile(byte[] input, File output, PublicKey key)
		throws IOException, GeneralSecurityException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		writeToFile(output, this.cipher.doFinal(input));
	}

	private void writeToFile(File output, byte[] toWrite)
			throws IllegalBlockSizeException, BadPaddingException, IOException {
		FileOutputStream fos = new FileOutputStream(output);
		fos.write(toWrite);
		fos.flush();
		fos.close();
	}
	
	
	public AsymmetricCryptography(int keylength) throws NoSuchAlgorithmException, NoSuchProviderException {
		this.keyGen = KeyPairGenerator.getInstance("RSA");
		this.keyGen.initialize(keylength);
	}
	
	
	public void writeToFile(String path, byte[] key) throws IOException {
		File f = new File(path);
		f.getParentFile().mkdirs();

		FileOutputStream fos = new FileOutputStream(f);
		fos.write(key);
		fos.flush();
		fos.close();
	}

	public void createKeys() {
		this.pair = this.keyGen.generateKeyPair();
		this.privateKey = pair.getPrivate();
		this.publicKey = pair.getPublic();
	}
		*/
		
}
