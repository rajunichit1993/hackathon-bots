package com.mindbowser.homeDisplay.service;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.SocketResponce;

public interface SqsService {
	
    void sqsMessageListner(SocketResponce socketResponce)throws MangoMirrorException;
    
    
}
