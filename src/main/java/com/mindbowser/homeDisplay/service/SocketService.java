package com.mindbowser.homeDisplay.service;

import java.util.ArrayList;
import org.json.JSONObject;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.SocketBroadcastMessage;


public interface  SocketService {
	
	public SocketBroadcastMessage  getWidgetSetting(JSONObject jsonObject)throws MangoMirrorException;
	
	public void  updateWidgetSetting(JSONObject jsonObject)throws MangoMirrorException;
	
	@SuppressWarnings("rawtypes")
	public ArrayList broadcast(String data) throws MangoMirrorException;
	
	public void  reconnectSocket(int major,int minor)throws MangoMirrorException;
	
	 public void updateTwitterCredentials(JSONObject jsonObject) throws MangoMirrorException;
}
