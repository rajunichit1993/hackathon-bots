package com.mindbowser.homeDisplay.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UpdateQuotesModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;

public interface UserMirrorService {
	public Map<String, Object> getUserMirrorList(HttpServletRequest request,String version)throws MangoMirrorException;
	
	void updateMirrorDetail(HttpServletRequest request ,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	List<UserMirrorModel> getUserListByMajorMinor(HttpServletRequest request,List<SmartMirrorModel> mirroList,String version)throws MangoMirrorException;
	
	public void removeUserMirror(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
 	
	public Map<String, Object> updateWeatherLocation(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	@SuppressWarnings("rawtypes")
	public HashMap updateCalenderFormatSetting(HttpServletRequest request,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	public Map<String, String> updateClockTimezone(HttpServletRequest request ,UserMirrorModel userMirrorModel,String version)throws MangoMirrorException;
	
	public void addLinkedUser(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	   
	public void updateTwitterCredentials(HttpServletRequest request,String version,UserMirrorModel userMirrorModel) throws MangoMirrorException;
	
	public SmartMirrorModel updatePreview(HttpServletRequest request,String version,SmartMirrorModel smartMirrorModel) throws MangoMirrorException;
	
	/*public Map<String, Object> updateQuotes(HttpServletRequest request, UserMirrorModel userMirrorModel, String version)throws MangoMirrorException;*/
	public Map<String, Object> updateQuotes(HttpServletRequest request, UpdateQuotesModel updateQuotesModel, String version)throws MangoMirrorException;
	
}
