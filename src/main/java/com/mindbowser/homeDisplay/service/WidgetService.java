
package com.mindbowser.homeDisplay.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import com.github.dvdme.ForecastIOLib.ForecastIO;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.CalendarFormatModel;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthDataTimeZoneModel;
import com.mindbowser.homeDisplay.model.QuotesCategoryModel;
import com.mindbowser.homeDisplay.model.QuotesLengthModel;
import com.mindbowser.homeDisplay.model.StickyNotesRemoveModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetData;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;


public interface WidgetService {
	
	List<WidgetModel> getAllWidget(HttpServletRequest request,String version) throws MangoMirrorException; 
    List<WidgetData> getAllWidgetSetting(UserModel userModel,WidgetSettingModel widgetSettingData,String version)throws MangoMirrorException;
    List<WidgetData> getSocketWidgetSetting(UserModel userModel,WidgetSettingModel widgetSettingData,String version)throws MangoMirrorException;
    
    public Map<String, Object> updateWidgetSetting(HttpServletRequest request,List<GenericWidgetSettingModel> mirrorPageList,String version) throws MangoMirrorException;
    public void socketUpdateWidgetSetting(UserModel userModel,List<WidgetSettingModel> widgetSetting) throws MangoMirrorException;
    public Map<String, Object> getHealthData(HealthDataModel healthDataModel,Double conversionValue,HealthDataTimeZoneModel timeZoneModel,WidgetDTO widgetDTO,String selectedunit)throws MangoMirrorException;
    
    void saveCalendarData(HttpServletRequest request,List<UserCalendarModel> userCalendarModelList,int timeZone,String version)throws MangoMirrorException;
    public ForecastIO getWeatherInfo(String latitude,String longitude)throws MangoMirrorException;
    public void saveWeatherData(JSONObject dailyTemperature, LocationDTO locationDTO, String weatherType)throws MangoMirrorException;
    public Map<String, Object> getDailyWeatherData(UserMirrorDTO userMirrorDTO,String iconFormat,String weatherType)throws MangoMirrorException ;
    public Map<String, Object> getHourlyWeatherData(UserMirrorDTO userMirrorDTO,String iconFormat,String weatherType)throws MangoMirrorException ;
    public Map<String, Object> getCurrentWeatherData(UserMirrorDTO userMirrorDTO,String iconFormat)throws MangoMirrorException;
    public LocationDTO addWeatherData(String latitude,String longitude,String locationName)throws MangoMirrorException;
    
    public Map<String, String> getQuotesData(UserMirrorDTO userMirrorDTO)throws MangoMirrorException;
    public List<QuotesCategoryModel> getQuotesCategoryList()throws MangoMirrorException;
    public List<QuotesLengthModel> getQuotesCategoryLengthList()throws MangoMirrorException;
    
    public Map<String, List<Map<String, Object>>> getCalenderData(int userId,UserMirrorDTO userMirrorDTO)throws MangoMirrorException;
    public List<CalendarFormatModel> calendarFormatList()throws MangoMirrorException;
    public void saveQuotesData()throws MangoMirrorException; 
    public void removeStickyNotes(HttpServletRequest request,StickyNotesRemoveModel stickyNotesRemoveModel,String version)throws MangoMirrorException;
    public void checkAndUpdateWeatherData(LocationDTO locationDTO)throws MangoMirrorException;
    public ArrayList addPinnedWidget(WidgetData pinnedWidget,ArrayList groups);
    public List<WidgetDTO> widgetListByVersion(String version);
    
}
