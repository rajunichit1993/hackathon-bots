package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_DATA;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_DEVICE_ID;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_MAC_ADDRESS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_MOTION_TYPE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MAX_BLEIN_STATUS_TIME_LIMIT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_NOT_REGISTER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MOTION_STOP;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.SmartMirrorDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDaoImpl;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.util.CalendarUtcDateTime;
import com.mindbowser.homeDisplay.util.ResourceManager;

public class SqsServiceImpl implements SqsService {
	
	private static Logger logger = Logger.getLogger(SqsServiceImpl.class);
	
	@Autowired
	private UserMirrorDaoImpl userMirrorDao;
	
	@Autowired
	private SmartMirrorDao smartMirrorDao;

	
	@Autowired
	private SocketService socketService;
	
	@Value("${foreCastApiKey}")
	private String ForeCastApiKey;
	
	@Value("${s3BasePath}")
	private String s3BasePath;
	
	
	@Override
	public void sqsMessageListner(SocketResponce socketResponce)throws MangoMirrorException {
		String data = socketResponce.getData();
		JSONObject jsonData = new JSONObject(data);
		
		String deviceId =   jsonData.getString(ResourceManager.getProperty(KEY_MAC_ADDRESS));
		String motionType = jsonData.getString(ResourceManager.getProperty(KEY_MOTION_TYPE));
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		
		try
		{
			if(smartMirrorDTO!=null)
			{
				if(motionType.equals(ResourceManager.getProperty(MOTION_STOP)))
				{
					  String stopTime = CalendarUtcDateTime.getUtcDateTimeInMillis();
					  smartMirrorDTO.setMotionStopTime(stopTime);
				}else
				{
						JSONArray array = new JSONArray();
						JSONObject jsonObject = new JSONObject();
						ObjectMapper objectMapper= new ObjectMapper();

						if(!smartMirrorDTO.getMotionStopTime().isEmpty())
						{
							String datetime = smartMirrorDTO.getMotionStopTime();
							Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
							SimpleDateFormat sdf = new SimpleDateFormat(CalendarUtcDateTime.UTC_DATE_FORMAT_MILLIS);
							sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
							String currentDateStr = sdf.format(cal.getTime());
							Date date = sdf.parse(datetime);
							Date currentDate  =sdf.parse(currentDateStr);
							long diffInMin = TimeUnit.MILLISECONDS.toSeconds(currentDate.getTime() - date.getTime());
							if(diffInMin > Integer.parseInt(ResourceManager.getProperty(MAX_BLEIN_STATUS_TIME_LIMIT)))
							{
								userMirrorDao.setOutOfRange(smartMirrorDTO.getId());
								if(smartMirrorDTO.getMirrorPreview() == false)
								{
									jsonObject.put(ResourceManager.getProperty(KEY_DEVICE_ID), deviceId);
									jsonObject.put(ResourceManager.getProperty(KEY_DATA), objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketService.broadcast(array.toString());
								}
							}
							
						}else
						{
							jsonObject.put(ResourceManager.getProperty(KEY_DEVICE_ID), deviceId);
							jsonObject.put(ResourceManager.getProperty(KEY_DATA), objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
							socketService.broadcast(array.toString());
						}
						
				}		
	        }else
	        {
	        	throw new MangoMirrorException(
					      ResourceManager.getMessage(MIRROR_NOT_REGISTER, null, NOT_FOUND, null));
	        }			
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		

	}
}

	
