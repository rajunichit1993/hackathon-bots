package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.iotdata.AWSIotDataClient;
import com.amazonaws.services.iotdata.model.GetThingShadowRequest;
import com.amazonaws.services.iotdata.model.GetThingShadowResult;
import com.amazonaws.services.iotdata.model.PublishRequest;
import com.amazonaws.services.iotdata.model.ResourceNotFoundException;
import com.amazonaws.services.iotdata.model.UpdateThingShadowRequest;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.FitBitDao;
import com.mindbowser.homeDisplay.dao.LogDao;
import com.mindbowser.homeDisplay.dao.SmartMirrorDao;
import com.mindbowser.homeDisplay.dao.UserDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDao;
import com.mindbowser.homeDisplay.dao.WidgetDao;
import com.mindbowser.homeDisplay.dto.BetaUserSmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.LogDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.StickyNotesDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.BetaUserMirrorList;
import com.mindbowser.homeDisplay.model.MirrorPageModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.util.AsymmetricCryptography;
import com.mindbowser.homeDisplay.util.CalendarUtcDateTime;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.WidgetSettingDozerHelper;

import scala.util.parsing.combinator.testing.Str;

public class SmartMirrorServiceImpl implements SmartMirrorService {

	private static Logger logger = Logger.getLogger(SmartMirrorServiceImpl.class);

	@Autowired
	private UserMirrorDao userMirrorDao;

	@Autowired
	private SmartMirrorDao smartMirrorDao;

	@Autowired
	private WidgetDao widgetDao;

	@Autowired
	private LogDao logDao;

	@Autowired
	private FitBitDao fitbitDao;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	private AsymmetricCryptography asymmetricCryptography;

	@Value("${aws_access_key_id}")
	private String accessKeyId;

	@Value("${aws_secret_key_id}")
	private String secretAccessKey;

	@Value("${s3_bucket_name}")
	private String bucketName;

	@Value("${s3_file_path}")
	private String s3FilePath;

	@Value("${aws_region}")
	private String awsRegion;
	
	@Value("${s3_betauser_bucket_name}")
	private String betaUserBucketName;

	@Value("${s3_betausermirror_jsonfile}")
	private String betauserFileName;

	@Autowired
	private UserDao userDao;

	@Autowired
	private WidgetService widgetService;

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public void updateDelayTime(HttpServletRequest request, int mirrorId, int delay, String version)
			throws MangoMirrorException {

		UserModel userModel = (UserModel) request.getAttribute(USER);
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorFileds(userModel.getId(), mirrorId,
				ResourceManager.getProperty(USER_GENERAL), Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		if (userMirrorDTO != null) {
			try {
				if (delay >= 0) {
					SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorById(mirrorId);
					smartMirrorDTO.setDelay(delay);
					AWSIotDataClient awsIotClient = new AWSIotDataClient(
							new BasicAWSCredentials(accessKeyId, secretAccessKey));
					UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
					updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
					JSONObject payloadJson = new JSONObject();
					Map<String, String> reportData = new HashMap<>();
					reportData.put(ResourceManager.getProperty(DISPLAY_SLEEP_TIME), Integer.toString(delay));
					reportData.put(ResourceManager.getProperty(IBEACON), ResourceManager.getProperty(BEACON_ON));
					Map<String, Map> reported = new HashMap<String, Map>();
					reported.put(ResourceManager.getProperty(DESIRED), reportData);
					payloadJson.put(ResourceManager.getProperty(STATE), reported);
					String payload = payloadJson.toString();
					byte[] b = payload.getBytes(StandardCharsets.UTF_8);
					PublishRequest publishRequest = new PublishRequest();
					publishRequest
							.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
					publishRequest.setPayload(ByteBuffer.wrap(b));
					awsIotClient.publish(publishRequest);
				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_OWNER_EXCEPTION, null, NOT_FOUND, null));
				}

			} catch (MangoMirrorException exception) {
				logger.info(exception);
				throw exception;
			} catch (Exception exception) {
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
		}
	}

	@Override
	public SmartMirrorModel checkMirrorByMajorMinor(HttpServletRequest request, int major, int minor)
			throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.checkMirror(major, minor,
				Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		SmartMirrorModel smartMirrorModel = null;
		if (smartMirrorDTO != null) {
			smartMirrorModel = dozerMapper.map(smartMirrorDTO, SmartMirrorModel.class);
		}
		return smartMirrorModel;
	}

	@Override
	public List<WidgetSettingDTO> InitializeWidgets(UserMirrorDTO userMirrorDTO, MirrorPageDTO mirrorPageDTO) {

		ArrayList<String> widgetList = new ArrayList<>();

		widgetList.add(ResourceManager.getProperty(WIDGET_CALENDAR));
		widgetList.add(ResourceManager.getProperty(WIDGET_CLOCK));
		widgetList.add(ResourceManager.getProperty(WIDGET_WEATHER));
		widgetList.add(ResourceManager.getProperty(WIDGET_TWITTER));
		widgetList.add(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST));
		widgetList.add(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST));
		widgetList.add(ResourceManager.getProperty(WIDGET_QUOTES));
		widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL));

		widgetList.add(ResourceManager.getProperty(WIDGET_STEP));
		widgetList.add(ResourceManager.getProperty(WIDGET_DISTANCE));
		widgetList.add(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES));
		widgetList.add(ResourceManager.getProperty(WIDGET_CALORIES));
		widgetList.add(ResourceManager.getProperty(WIDGET_CALORIESBMR));

		widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE));
		widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED));

		widgetList.add(ResourceManager.getProperty(WIDGET_CARB));
		widgetList.add(ResourceManager.getProperty(WIDGET_FAT));
		widgetList.add(ResourceManager.getProperty(WIDGET_FIBER));

		widgetList.add(ResourceManager.getProperty(WIDGET_PROTEIN));
		widgetList.add(ResourceManager.getProperty(WIDGET_SODIUM));
		widgetList.add(ResourceManager.getProperty(WIDGET_WATER));
		widgetList.add(ResourceManager.getProperty(WIDGET_BMI));
		widgetList.add(ResourceManager.getProperty(WIDGET_WEIGHT));
		widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE));
		widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED));

		widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC));
		widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC));
		widgetList.add(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB));
		widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL));
		widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR));
		widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE));
		widgetList.add(ResourceManager.getProperty(WIDGET_HEART_RATE));
		widgetList.add(ResourceManager.getProperty(WIDGET_STICKYNOTES));

		List<WidgetDTO> widgets = widgetDao.getAllWidget(widgetList);
		List<WidgetSettingDTO> widgetSettingList = new ArrayList<WidgetSettingDTO>();

		/* initialize widgets with defauult size and positions */
		for (WidgetDTO widgetDTO : widgets) {
			WidgetDTO widgetDTO1 = new WidgetDTO();
			WidgetSettingDTO widgetSettingDTO = new WidgetSettingDTO();
			widgetDTO1.setId(widgetDTO.getId());
			widgetSettingDTO.setWidget(widgetDTO1);
			widgetSettingDTO
					.setDeviceWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));
			widgetSettingDTO
					.setDeviceHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT)));
			widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));
			widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));

			widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_XPOS)));
			widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_YPOS)));
			widgetSettingDTO.setPinned(false);

			widgetSettingDTO.setUserMirror(userMirrorDTO);
			if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CLOCK))) {
				widgetSettingDTO.setStatus(ResourceManager.getProperty(STATUS_ON));
				/*
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CLOCK_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CLOCK_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CLOCK_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CLOCK_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_CLOCK_MINHEIGHT)
				 * ));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_CLOCK_MINWIDTH)));
				 */

				widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				widgetSettingDTO
						.setMinHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_TEXT));
				widgetSettingDTO.setMirrorPage(mirrorPageDTO);
			} else {

				widgetSettingDTO.setStatus(ResourceManager.getProperty(STATUS_OFF));
				/*
				 * if (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_CALENDAR))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CELENDAR_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CELENDAR_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CELENDAR_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_CELENDAR_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_CELENDAR_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_CELENDAR_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); } else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_WEATHER))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_WEATHER_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_WEATHER_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); } else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_TWITTER))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_TWITTER_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_TWITTER_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_TWITTER_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_TWITTER_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_TWITTER_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_TWITTER_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); } else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_5DAY_WEATHER_FORECAST))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_WEATHER_DAILY_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_WEATHER_DAILY_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); } else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_24HOUR_WEATHER_FORECAST))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_HEIGHT)))
				 * ; widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_WEATHER_HOURLEY_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_WEATHER_HOURLEY_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); }
				 * 
				 * else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_QUOTES))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_QUOTES_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_QUOTES_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_QUOTES_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_QUOTES_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_QUOTES_MINHEIGHT
				 * )));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_QUOTES_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); }
				 * 
				 * else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_STICKYNOTES))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(
				 * PORTRAIT_DEFAULT_STICKYNOTES_MINHEIGHT)));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_STICKYNOTES_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_HEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
				 * widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_TEXT));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); }
				 * 
				 * else if
				 * (widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_STEP)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_FAT)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_CALORIES)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_DISTANCE)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_ACTIVITY_CALORIES)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_CALORIESBMR)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_CARB)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_FIBER)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_PROTEIN)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_SODIUM)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_DIATRY_FATTOTAL)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_BLOOD_PRESSURE_SYSTOLIC)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_DIATRY_SUGAR)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_FLIGHT_CLIMB)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_DIATRY_CHOLESTEROL)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_WATER))
				 * ||widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_TOTAL_SLEEPMINUTE)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_BMI))
				 * ||widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_WEIGHT))
				 * ||widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_BLOOD_GLUCOSE)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_BLOOD_PRESSURE_DIASTOLIC))
				 * ||widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_HEART_RATE)) ||
				 * widgetDTO.getType().equals(ResourceManager.getProperty(
				 * WIDGET_TOTAL_TIMEINBED))) {
				 * widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STEPS_WIDGET_XPOS)));
				 * widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STEPS_WIDGET_YPOS)));
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STEPS_WIDGET_HEIGHT)));
				 * widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_STEPS_WIDGET_WIDTH)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_STEPS_MINHEIGHT)
				 * ));
				 * widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager
				 * .getProperty(PORTRAIT_DEFAULT_STEPS_MINWIDTH)));
				 * 
				 * widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.
				 * getProperty(PORTRAIT_DEFAULT_GRAPHHEIGHT)));
				 * widgetSettingDTO.setMinHeight(Integer.parseInt(
				 * ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHMINHEIGHT))
				 * ); widgetSettingDTO.setViewType(ResourceManager.getProperty(
				 * WIDGET_VIEW_TYPE_GRAPH));
				 * widgetSettingDTO.setMirrorPage(mirrorPageDTO); }
				 */

				if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_STEP))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CARB))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FIBER))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_PROTEIN))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_SODIUM))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_HEART_RATE))
						|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {

					widgetSettingDTO
							.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHHEIGHT)));
					widgetSettingDTO.setMinHeight(
							Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHMINHEIGHT)));
					widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_GRAPH));
					widgetSettingDTO.setMirrorPage(mirrorPageDTO);
				} else {
					widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_HEIGHT)));
					widgetSettingDTO
							.setMinHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
					widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_TEXT));
					widgetSettingDTO.setMirrorPage(mirrorPageDTO);
				}

			}
			widgetSettingList.add(widgetSettingDTO);
		}

		return widgetSettingList;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void restartMirror(HttpServletRequest request, String deviceId, String version) throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		Locale locale = LocaleConverter.getLocaleFromRequest(request);

		if (smartMirrorDTO != null) {
			AWSIotDataClient awsIotClient = new AWSIotDataClient(new BasicAWSCredentials(accessKeyId, secretAccessKey));
			UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
			updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
			JSONObject payloadJson = new JSONObject();
			Map<String, String> reportData = new HashMap<>();
			reportData.put(ResourceManager.getProperty("mirror_restart"),
					ResourceManager.getProperty("mirror_message_yes"));
			Map<String, Map<String, String>> reported = new HashMap<>();
			reported.put(ResourceManager.getProperty(DESIRED), reportData);
			payloadJson.put(ResourceManager.getProperty(STATE), reported);
			String payload = payloadJson.toString();
			byte[] b = payload.getBytes(StandardCharsets.UTF_8);
			PublishRequest publishRequest = new PublishRequest();
			publishRequest.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
			publishRequest.setPayload(ByteBuffer.wrap(b));
			awsIotClient.publish(publishRequest);
			reported.clear();
			reportData.clear();
		} else {
			throw new MangoMirrorException(ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
		}
	}

	@Override
	public void resetMirror(HttpServletRequest request, String deviceId, String version) throws MangoMirrorException {

		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		Locale locale = LocaleConverter.getLocaleFromRequest(request);

		try {
			if (smartMirrorDTO != null) {
				/*
				 * AWSIotDataClient awsIotClient = new AWSIotDataClient(new
				 * BasicAWSCredentials(accessKeyId,secretAccessKey));
				 * UpdateThingShadowRequest updateThingShadowRequest = new
				 * UpdateThingShadowRequest();
				 * updateThingShadowRequest.setThingName("MangoMirror-"+
				 * smartMirrorDTO.getDeviceId()); JSONObject payloadJson = new
				 * JSONObject(); Map<String, String> reportData = new
				 * HashMap<>(); reportData.put(ResourceManager.getProperty(
				 * "mirror_factory_reset"),ResourceManager.getProperty(
				 * "mirror_message_yes")); Map<String, Map<String, String>>
				 * reported = new HashMap<>();
				 * reported.put(ResourceManager.getProperty(DESIRED),
				 * reportData);
				 * payloadJson.put(ResourceManager.getProperty(STATE),
				 * reported); String payload = payloadJson.toString(); byte[] b
				 * = payload.getBytes(StandardCharsets.UTF_8); PublishRequest
				 * publishRequest = new PublishRequest();
				 * publishRequest.setTopic("$aws/things/"+
				 * updateThingShadowRequest.getThingName()+"/shadow/update");
				 * publishRequest.setPayload(ByteBuffer.wrap(b));
				 * awsIotClient.publish(publishRequest); reported.clear();
				 * reportData.clear();
				 */

			/*	if (version.equals(ResourceManager.getProperty(VERSION_BETA))) {
					smartMirrorDao.updateBetaMirrorstatus(smartMirrorDTO.getDeviceId(),
							Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
				}*/
				smartMirrorDao.updateMirrorstatus(smartMirrorDTO.getDeviceId(),
						Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
				userMirrorDao.updateUserMirrorStatus(smartMirrorDTO.getId(),
						Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));

			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
			}

		} catch (MangoMirrorException ex) {
			logger.info(ex);
			throw ex;
		} catch (Exception ex) {
			logger.info(ex);
			throw new MangoMirrorException(
					ResourceManager.getMessage(SOMETHING_WENTWRONG_EXCEPTION, null, NOT_FOUND, locale));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void mirrorPairingMode(HttpServletRequest request, String deviceId, String version)
			throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		Locale locale = LocaleConverter.getLocaleFromRequest(request);

		if (smartMirrorDTO != null) {
			AWSIotDataClient awsIotClient = new AWSIotDataClient(new BasicAWSCredentials(accessKeyId, secretAccessKey));
			UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
			updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
			JSONObject payloadJson = new JSONObject();
			Map<String, String> reportData = new HashMap<>();
			reportData.put(ResourceManager.getProperty("mirror_pairing"), ResourceManager.getProperty("status.on"));
			Map<String, Map<String, String>> reported = new HashMap<>();
			reported.put(ResourceManager.getProperty(DESIRED), reportData);
			payloadJson.put(ResourceManager.getProperty(STATE), reported);
			String payload = payloadJson.toString();
			byte[] b = payload.getBytes(StandardCharsets.UTF_8);
			PublishRequest publishRequest = new PublishRequest();
			publishRequest.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
			publishRequest.setPayload(ByteBuffer.wrap(b));
			awsIotClient.publish(publishRequest);
			reported.clear();
			reportData.clear();
		} else {
			throw new MangoMirrorException(ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public SmartMirrorModel saveMirror(HttpServletRequest request, UserMirrorModel userMirrorModel, String version)
			throws MangoMirrorException, MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserDTO userDTO = new UserDTO();
		UserModel userModel = (UserModel) request.getAttribute(USER);
		// register mirror with new major and minor
		SmartMirrorDTO smartMirrorDTO3 = smartMirrorDao.getMajorMinor();
		String encrypted_EmailId = null;
		SmartMirrorDTO smartMirrorDTO = dozerMapper.map(userMirrorModel.getMirror(), SmartMirrorDTO.class);
		SmartMirrorDTO mirrorDTO = smartMirrorDao.getMirrorByDeviceId(smartMirrorDTO.getDeviceId());
		smartMirrorDao.updateMirrorstatus(smartMirrorDTO.getDeviceId(),
				Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
		/*
		 * if(version.equals(ResourceManager.getProperty(VERSION_BETA))) {
		 * smartMirrorDao.updateBetaMirrorstatus(smartMirrorDTO.getDeviceId(),
		 * Integer.parseInt(ResourceManager.getProperty(FLAG_ONE))); }
		 */
		if (mirrorDTO != null) {
			userMirrorDao.updateUserMirrorStatus(mirrorDTO.getId(),
					Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
		}

		try {
			if (smartMirrorDTO3 == null) {
				smartMirrorDTO.setMajor(Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
				smartMirrorDTO.setMinor(Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
			} else {
				if (smartMirrorDTO3.getMinor() < Integer.parseInt(ResourceManager.getProperty(MINORLIMIT))) {
					smartMirrorDTO.setMajor(smartMirrorDTO3.getMajor());
					smartMirrorDTO.setMinor(
							smartMirrorDTO3.getMinor() + Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
				} else {
					smartMirrorDTO.setMajor(
							smartMirrorDTO3.getMajor() + Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
					smartMirrorDTO.setMinor(Integer.parseInt(FLAG_ZERO));
				}
				if ((smartMirrorDTO3.getMinor() >= Integer.parseInt(ResourceManager.getProperty(MINORLIMIT)))
						&& smartMirrorDTO3.getMajor() >= Integer.parseInt(ResourceManager.getProperty(MAJORLIMIT))) {
					throw new MangoMirrorException(
							ResourceManager.getMessage(BLE_LIMIT_EXCEPTION, null, NOT_FOUND, locale));
				}
			}

		} catch (MangoMirrorException ex) {
			logger.info(ex);
			throw new MangoMirrorException(ResourceManager.getMessage(BLE_LIMIT_EXCEPTION, null, NOT_FOUND, locale));
		}
		smartMirrorDTO.setMirrorStatus(Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		smartMirrorDTO.setMirrorPreview(false);
		smartMirrorDTO.setAutoUpdate(true);
		smartMirrorDTO.setFirmwareUpdateTime(ResourceManager.getProperty(DEFAULT_FIRMWARE_UPDATE_TIME));
		smartMirrorDTO.setBluetoothRange(Integer.parseInt(ResourceManager.getProperty(DEFAULT_BLUETOOTH_RANGE)));

		if (userMirrorModel.getMirror().getMultiUser() == null) {
			smartMirrorDTO.setMultiUser(true);
		}

		Boolean defaultFirmwareVersion = false;
		try {
			AWSIotDataClient awsIotClient = new AWSIotDataClient(new BasicAWSCredentials(accessKeyId, secretAccessKey));
			GetThingShadowRequest getThingShadowRequest = new GetThingShadowRequest();
			getThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());

			GetThingShadowResult data1 = awsIotClient.getThingShadow(getThingShadowRequest);
			String data = new String(data1.getPayload().array());
			JSONObject thingJson = new JSONObject(data);
			JSONObject thingStateJson = thingJson.getJSONObject(ResourceManager.getProperty(STATE));

			if (thingStateJson.has(ResourceManager.getProperty(REPORTED))) {
				JSONObject ThingReportJson = thingStateJson.getJSONObject(ResourceManager.getProperty(REPORTED));
				if (ThingReportJson.has(ResourceManager.getProperty(CURRENT_FW_VERSION))) {
					double firmwareVersion = Double
							.parseDouble((String) ThingReportJson.get(ResourceManager.getProperty(CURRENT_FW_VERSION)));
					smartMirrorDTO.setCurrentFirmwareVersion(firmwareVersion);
				} else {
					defaultFirmwareVersion = true;
				}
			} else {
				defaultFirmwareVersion = true;
			}

		} catch (ResourceNotFoundException exception) {
			logger.info(exception);
			defaultFirmwareVersion = true;
		} catch (Exception exception) {
			logger.info(exception);
			defaultFirmwareVersion = true;
		}

		if (defaultFirmwareVersion) {
			double firmwareVersion = Double.parseDouble(ResourceManager.getProperty(FIRMWARE_VERSION));
			smartMirrorDTO.setCurrentFirmwareVersion(firmwareVersion);
		}

		SmartMirrorDTO smartMirrorDTO1 = smartMirrorDao.saveMirror(smartMirrorDTO);
		try {
			updateDelayTime(request, smartMirrorDTO1.getId(), userMirrorModel.getMirror().getDelay(), version);

			File privatefilepath = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privatefilepath);
			 encrypted_EmailId =asymmetricCryptography.encryptText(ResourceManager.getProperty(BETA_USER_DEFAULT_EMAILID),privateKey);
		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(exception);
		}

		SmartMirrorDTO smartMirrorDTO2 = new SmartMirrorDTO();
		CalendarFormatDTO calendarFormatDTO = new CalendarFormatDTO();
		calendarFormatDTO.setId(userMirrorModel.getCalendarFormat().getId());
		smartMirrorDTO2.setId(smartMirrorDTO1.getId());
		LocationDTO locationDTO = widgetDao.checkLocation(userMirrorModel.getLocation().getLatitude(),
				userMirrorModel.getLocation().getLongitude());

		if (locationDTO == null) {
			locationDTO = widgetService.addWeatherData(userMirrorModel.getLocation().getLatitude(),
					userMirrorModel.getLocation().getLongitude(), userMirrorModel.getLocation().getLocationName());

		}
		QuotesDTO quote = new QuotesDTO();
		quote.setId(Integer.parseInt(ResourceManager.getProperty(DEFAULT_QUOTES_VALUE)));

		UserMirrorDTO userMirrorDTO = new UserMirrorDTO();
		userDTO.setId(userModel.getId());
		userMirrorDTO.setMirror(smartMirrorDTO2);
		userMirrorDTO.setUser(userDTO);
		userMirrorDTO.setTimeZone(userMirrorModel.getTimeZone());
		userMirrorDTO.setLocation(locationDTO);
		userMirrorDTO.setClockMessageStatus(true);
		userMirrorDTO.setMirrorName(userMirrorModel.getMirrorName());
		userMirrorDTO.setCalendarFormat(calendarFormatDTO);
		userMirrorDTO.setUserStatusFlag(Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		userMirrorDTO.setCalendarFlag(false);
		userMirrorDTO.setReminderFlag(false);
		List<QuotesDTO> quotes = new ArrayList<>();
		quotes.add(quote);

		if (smartMirrorDTO1.getMultiUser()) {
			userMirrorDTO.setUserRole(ResourceManager.getProperty(USER_OWNER));
			UserMirrorDTO userMirrorDTO1 = userMirrorDao.setUserMirror(userMirrorDTO);
			widgetDao.addQuotesForUser(userMirrorDTO1, quotes);
			MirrorPageDTO mirrorPageDTO = new MirrorPageDTO();
			mirrorPageDTO.setPageNumber(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGENUMBER)));
			mirrorPageDTO.setDelay(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGE_TRANSITION_DELAY)));
			mirrorPageDTO.setUserMirror(userMirrorDTO1);
			mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
			List<WidgetSettingDTO> widgetSettingList = InitializeWidgets(userMirrorDTO1, mirrorPageDTO);
			widgetDao.saveWidgetSetting(widgetSettingList);
		}

		userMirrorDTO.setUserRole(ResourceManager.getProperty(USER_GENERAL));
		UserMirrorDTO userMirrorDTO2;

		/* for beta users and general setting only */

		if (version.equals(ResourceManager.getProperty(VERSION_BETA)) && !smartMirrorDTO1.getDeviceId().equals(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID))) 
		{                    
	        userMirrorDTO2 = userMirrorDao.setUserMirror(userMirrorDTO);
			
	        FitBitAccountDTO fitBitAccountDTO = fitbitDao
			.getFitBitAccountByUserId(ResourceManager.getProperty(BETA_USER_FITBIT_ID));
	        
			userMirrorDTO2.setFitbitAccount(fitBitAccountDTO);

			SmartMirrorDTO mirrorDTO2 = smartMirrorDao.getMirrorByDeviceId(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID));
			UserDTO userDTO2 = userDao.checkUser(encrypted_EmailId);
			UserMirrorDTO defaultUserMirrorDto = userMirrorDao.getUserMirror(userDTO2.getId(), mirrorDTO2.getId(),
			ResourceManager.getProperty(USER_GENERAL), 0);
	
			Boolean calendarFlag = defaultUserMirrorDto.getCalendarFlag();
			Boolean remindarFlag = defaultUserMirrorDto.getReminderFlag();
			userMirrorDao.updatecalendar(userMirrorDTO2.getId(),calendarFlag,remindarFlag,defaultUserMirrorDto.getCalendarFormat().getId());
			
			List<WidgetDTO> widgetDTOList = widgetService
					.widgetListByVersion(ResourceManager.getProperty(VERSION_BETA));
			List<WidgetSettingDTO> widgetSettingDTOs = widgetDao.getWidgetSetting(defaultUserMirrorDto, widgetDTOList);
			List<WidgetSettingModel> widgetSettingModels = WidgetSettingDozerHelper.MapDtoToModel(dozerMapper,
					widgetSettingDTOs, WidgetSettingModel.class);
			List<WidgetSettingDTO> updatedWidgetSettingDto = WidgetSettingDozerHelper.MapModelToDto(dozerMapper,
					widgetSettingModels, WidgetSettingDTO.class);

			writeBetaMirrorFile(smartMirrorDTO1.getDeviceId());

			Map<Integer, MirrorPageDTO> pagelist = new HashMap<>();
			for (WidgetSettingDTO widgetSettingDTO : updatedWidgetSettingDto) {

				widgetSettingDTO.setId(0);
				widgetSettingDTO.setUserMirror(userMirrorDTO2);
				if (pagelist.containsKey(widgetSettingDTO.getMirrorPage().getPageNumber())) {
					MirrorPageDTO mirrorPageDTO = pagelist.get(widgetSettingDTO.getMirrorPage().getPageNumber());
					widgetSettingDTO.setMirrorPage(mirrorPageDTO);
				} else {
					MirrorPageDTO mirrorPageDTO = new MirrorPageDTO();
					mirrorPageDTO.setPageNumber(widgetSettingDTO.getMirrorPage().getPageNumber());
					mirrorPageDTO.setDelay(widgetSettingDTO.getMirrorPage().getDelay());
					mirrorPageDTO.setUserMirror(userMirrorDTO2);
					mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
					pagelist.put(widgetSettingDTO.getMirrorPage().getPageNumber(), mirrorPageDTO);
					widgetSettingDTO.setMirrorPage(mirrorPageDTO);
				}

				if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))
						&& widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON))) {
					List<StickyNotesDTO> notesDTO = widgetSettingDTO.getStickyNotes();
					notesDTO.get(0).setId(0);
					widgetSettingDTO.setStickyNotes(notesDTO);
				}
			}
			widgetDao.addQuotesForUser(userMirrorDTO2, quotes);
			widgetDao.saveWidgetSetting(updatedWidgetSettingDto);
		} else {
			userMirrorDTO2 = userMirrorDao.setUserMirror(userMirrorDTO);
			widgetDao.addQuotesForUser(userMirrorDTO2, quotes);
			MirrorPageDTO mirrorPageDTO = new MirrorPageDTO();
			mirrorPageDTO.setPageNumber(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGENUMBER)));
			mirrorPageDTO.setDelay(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGE_TRANSITION_DELAY)));
			mirrorPageDTO.setUserMirror(userMirrorDTO2);
			mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
			List<WidgetSettingDTO> widgetSettingList = InitializeWidgets(userMirrorDTO2, mirrorPageDTO);
			widgetDao.saveWidgetSetting(widgetSettingList);
		}

		updateBleBroadcastRange(request, smartMirrorDTO.getDeviceId(),
				Integer.parseInt(ResourceManager.getProperty(DEFAULT_BLUETOOTH_RANGE)), version);
		SmartMirrorModel smartMirrorModel = dozerMapper.map(smartMirrorDTO, SmartMirrorModel.class);
		String logTime = CalendarUtcDateTime.getUtcDateTime();

		LogDTO logDTO = new LogDTO();
		logDTO.setUserId(userModel.getId());
		logDTO.setTime(logTime);
		logDTO.setMirrorId(smartMirrorModel.getId());
		logDTO.setEvent(ResourceManager.getProperty(LOG_USER_CONFIGURE_NEWMIRROR));
		logDao.saveLog(logDTO);
		return smartMirrorModel;
	}

	public void writeBetaMirrorFile(String deviceId) {

		AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
		AmazonS3Client s3Client = new AmazonS3Client(credentials);

		try {
			
			S3Object s3object = s3Client.getObject(new GetObjectRequest(betaUserBucketName, betauserFileName));
			//S3Object s3object = s3Client.getObject(new GetObjectRequest("betamirrors", "betamirror.json"));
			BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
			String line;

			String jsonString="";
			while ((line = reader.readLine()) != null) {
				jsonString += line;
			}
			File uploadFilePath = new File(System.getProperty("java.io.tmpdir")+"/betamirror.json");
			uploadFilePath.createNewFile();
			FileOutputStream fos = new FileOutputStream(uploadFilePath,false);
			
			ObjectMapper mapper = new ObjectMapper();
			byte[] jsonBetaMirrorData = jsonString.getBytes();
			BetaUserMirrorList betaMirrorList = mapper.readValue(jsonBetaMirrorData, BetaUserMirrorList.class);
			List<String> data = betaMirrorList.getBetaUserMirrorList();
			if(!data.contains(deviceId))
			{
				data.add(deviceId);
				betaMirrorList.setBetaUserMirrorList(data);
				
				byte[] updatedData = mapper.writeValueAsBytes(betaMirrorList);
				fos.write(updatedData);
				fos.flush();
				fos.close();
				//	s3Client.putObject(new PutObjectRequest("betamirrors", "betamirror.json", uploadFilePath));
				s3Client.putObject(new PutObjectRequest(betaUserBucketName, betauserFileName, uploadFilePath));	
			}
		} catch (Exception exception) {
			logger.info(exception);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void firmwareUpdateSQSMessage(String deviceId, String version) throws MangoMirrorException {

		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);

		if (smartMirrorDTO != null) {
			AWSIotDataClient awsIotClient = new AWSIotDataClient(new BasicAWSCredentials(accessKeyId, secretAccessKey));
			UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
			updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
			JSONObject payloadJson = new JSONObject();
			Map<String, String> reportData = new HashMap<>();
			reportData.put(ResourceManager.getProperty("device_id"), ResourceManager.getProperty("device_id_value")
					+ "-" + smartMirrorDTO.getMajor() + "-" + smartMirrorDTO.getMinor());
			reportData.put(ResourceManager.getProperty(FW_VERSION), version);
			Map<String, Map<String, String>> reported = new HashMap<>();
			reported.put(ResourceManager.getProperty(DESIRED), reportData);
			payloadJson.put(ResourceManager.getProperty(STATE), reported);
			payloadJson.put(ResourceManager.getProperty("clientToken_label"), smartMirrorDTO.getDeviceId());
			String payload = payloadJson.toString();
			byte[] b = payload.getBytes(StandardCharsets.UTF_8);
			PublishRequest publishRequest = new PublishRequest();
			publishRequest.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
			publishRequest.setPayload(ByteBuffer.wrap(b));
			awsIotClient.publish(publishRequest);
			reported.clear();
			reportData.clear();
		} else {
			throw new MangoMirrorException(ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, null));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void updateBleBroadcastRange(HttpServletRequest request, String deviceId, int broadcastRange, String version)
			throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		Locale locale = LocaleConverter.getLocaleFromRequest(request);

		try {
			if (smartMirrorDTO != null) {
				AWSIotDataClient awsIotClient = new AWSIotDataClient(
						new BasicAWSCredentials(accessKeyId, secretAccessKey));
				UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
				updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
				JSONObject payloadJson = new JSONObject();
				Map<String, Integer> reportData = new HashMap<>();
				reportData.put(ResourceManager.getProperty("mirror_range_broadcast_key"), broadcastRange);
				Map<String, Map<String, Integer>> reported = new HashMap<>();
				reported.put(ResourceManager.getProperty(DESIRED), reportData);
				payloadJson.put(ResourceManager.getProperty(STATE), reported);
				String payload = payloadJson.toString();
				byte[] b = payload.getBytes(StandardCharsets.UTF_8);
				PublishRequest publishRequest = new PublishRequest();
				publishRequest.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
				publishRequest.setPayload(ByteBuffer.wrap(b));
				awsIotClient.publish(publishRequest);
				reported.clear();
				reportData.clear();
				smartMirrorDTO.setBluetoothRange(broadcastRange);

			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
			}
		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(BLERANGE_UPDATE_EXCEPTION) + " " + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(BLERANGE_UPDATE_EXCEPTION));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void updateBluetoothSetting(HttpServletRequest request, String deviceId, String bluetoothStatus,
			String version) throws MangoMirrorException {

		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		try {
			if (smartMirrorDTO != null) {
				smartMirrorDTO.setBluetoothStatus(bluetoothStatus);
				AWSIotDataClient awsIotClient = new AWSIotDataClient(
						new BasicAWSCredentials(accessKeyId, secretAccessKey));
				UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
				updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
				JSONObject payloadJson = new JSONObject();
				Map<String, String> reportData = new HashMap<>();
				reportData.put(ResourceManager.getProperty(IBEACON), bluetoothStatus);
				Map<String, Map<String, String>> reported = new HashMap<>();
				reported.put(ResourceManager.getProperty(DESIRED), reportData);
				payloadJson.put(ResourceManager.getProperty(STATE), reported);
				String payload = payloadJson.toString();
				byte[] b = payload.getBytes(StandardCharsets.UTF_8);
				PublishRequest publishRequest = new PublishRequest();
				publishRequest.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
				publishRequest.setPayload(ByteBuffer.wrap(b));
				awsIotClient.publish(publishRequest);
				reported.clear();
				reportData.clear();
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
			}
		} catch (MangoMirrorException exception) {
			logger.info(ResourceManager.getProperty(FIRMWARE_UPDATE_EXCEPTION) + " " + exception);
			throw exception;
		}
	}

	@Override
	public void checkForFirmwareUpdate() throws MangoMirrorException {

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String startDateTime = sdf.format(calendar.getTime());

		calendar.add(Calendar.MINUTE, 30);
		String endDateTime = sdf.format(calendar.getTime());
		List<SmartMirrorDTO> smartMirrorList = smartMirrorDao.getMirrorListByMirrorUpdateDateTime(startDateTime,
				endDateTime);

		try {
			double currentVersion = checkCurrentFirmwareVersionS3();
			if (!smartMirrorList.isEmpty()) {
				for (SmartMirrorDTO smartMirrorDTO : smartMirrorList) {

					if (smartMirrorDTO.getCurrentFirmwareVersion() < currentVersion && smartMirrorDTO.getAutoUpdate()) {
						firmwareUpdateSQSMessage(smartMirrorDTO.getDeviceId(), Double.toString(currentVersion));
						smartMirrorDTO.setCurrentFirmwareVersion(currentVersion);
					}
				}
			}

		} catch (MangoMirrorException exception) {
			logger.info(ResourceManager.getProperty(FIRMWARE_UPDATE_EXCEPTION) + " " + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(FIRMWARE_UPDATE_EXCEPTION));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public double checkCurrentFirmwareVersionS3() throws MangoMirrorException {

		double currentVersion = 0;
		BufferedReader reader = null;
		InputStreamReader inputreader = null;
		String line;
		try {
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
			AmazonS3Client amazonS3Client = new AmazonS3Client(awsCredentials);
			amazonS3Client.setRegion(Region.getRegion(Regions.fromName(awsRegion)));

			S3Object s3object = amazonS3Client.getObject(new GetObjectRequest(bucketName, s3FilePath));
			inputreader = new InputStreamReader(s3object.getObjectContent());
			reader = new BufferedReader(inputreader);
			while ((line = reader.readLine()) != null) {
				currentVersion = new Double(line);
			}
		} catch (IOException exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + " " + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputreader != null) {
					inputreader.close();
				}
			} catch (IOException exception) {
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + " " + exception);
			}
		}
		return currentVersion;
	}

	@Override
	public SmartMirrorModel firmwareUpdateCheck(String deviceId) throws MangoMirrorException {

		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
		try {
			double currentVersion = checkCurrentFirmwareVersionS3();
			if (smartMirrorDTO != null) {
				if (smartMirrorDTO.getCurrentFirmwareVersion() < currentVersion && smartMirrorDTO.getAutoUpdate()) {
					firmwareUpdateSQSMessage(smartMirrorDTO.getDeviceId(), Double.toString(currentVersion));
					smartMirrorDTO.setCurrentFirmwareVersion(currentVersion);
				}
			}
		} catch (Exception exception) {
			logger.info(exception);
			throw new MangoMirrorException(
					ResourceManager.getMessage(MIRROR_UPDATE_UNSUCCESSFUL, null, NOT_FOUND, null));
		}

		return dozerMapper.map(smartMirrorDTO, SmartMirrorModel.class);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void resetAllMirror(HttpServletRequest request, String version) throws MangoMirrorException {
		List<SmartMirrorDTO> smartMirrorList = smartMirrorDao.getallmirrors();
		Locale locale = LocaleConverter.getLocaleFromRequest(request);

		try {
			if (!smartMirrorList.isEmpty()) {
				for (SmartMirrorDTO smartMirrorDTO : smartMirrorList) {
					AWSIotDataClient awsIotClient = new AWSIotDataClient(
							new BasicAWSCredentials(accessKeyId, secretAccessKey));
					UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
					updateThingShadowRequest.setThingName("MangoMirror-" + smartMirrorDTO.getDeviceId());
					JSONObject payloadJson = new JSONObject();
					Map<String, String> reportData = new HashMap<>();
					reportData.put(ResourceManager.getProperty("mirror_factory_reset"),
							ResourceManager.getProperty("mirror_message_yes"));
					Map<String, Map<String, String>> reported = new HashMap<>();
					reported.put(ResourceManager.getProperty(DESIRED), reportData);
					payloadJson.put(ResourceManager.getProperty(STATE), reported);
					String payload = payloadJson.toString();
					byte[] b = payload.getBytes(StandardCharsets.UTF_8);
					PublishRequest publishRequest = new PublishRequest();
					publishRequest
							.setTopic("$aws/things/" + updateThingShadowRequest.getThingName() + "/shadow/update");
					publishRequest.setPayload(ByteBuffer.wrap(b));
					awsIotClient.publish(publishRequest);
					reported.clear();
					reportData.clear();
				}

			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
			}

		} catch (MangoMirrorException ex) {
			logger.info(ex);
			throw ex;
		} catch (Exception ex) {
			logger.info(ex);
			throw new MangoMirrorException(
					ResourceManager.getMessage(SOMETHING_WENTWRONG_EXCEPTION, null, NOT_FOUND, locale));
		}

	}

}
