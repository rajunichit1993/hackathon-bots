package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DESIRED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_HEIGHT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_MAJOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_MINOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_USER_ID;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_USER_ROLE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_WIDTH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_NOT_REGISTER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.REST_URL_SOCKET;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOCKET_DISCONNECT_STATUS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOCKET_STATUS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.STATE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_MIRROR_NOT_REGISTERED_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_NOT_REGISTER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_V1;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.iotdata.AWSIotDataClient;
import com.amazonaws.services.iotdata.model.PublishRequest;
import com.amazonaws.services.iotdata.model.UpdateThingShadowRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.SmartMirrorDao;
import com.mindbowser.homeDisplay.dao.UserDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDao;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.SocketBroadcastMessage;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.TwitterModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetData;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.util.ResourceManager;



public class SocketServiceImpl implements SocketService {

	
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private UserMirrorDao userMirrorDao;

	@Autowired
	private SmartMirrorDao smartMirrorDao;
	
	@Autowired
	private WidgetService widgetService;

	@Autowired
	private Mapper dozerMapper;

	@Value("${accessKeyId}")
	private String accessKeyId;
	
	@Value("${secretAccessKey}")
	private String secretAccessKey;
	
	private static Logger logger = Logger.getLogger(SocketServiceImpl.class);
	JSONObject jsonObject3 = new JSONObject();
	
	@Override
	public SocketBroadcastMessage getWidgetSetting(JSONObject jsonObject) throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO = null;
		SocketBroadcastMessage socketBroadcastMessage = new SocketBroadcastMessage();
		ObjectMapper objectMapper = new ObjectMapper();
		SocketResponce socketResponce = new SocketResponce();
		// TODO socket responce and object mapper added 
		try {
			String userRole = jsonObject.getString(ResourceManager.getProperty(KEY_USER_ROLE));
			int major = Integer.parseInt(jsonObject.getString("major"));
			int minor = Integer.parseInt(jsonObject.getString("minor"));
			int deviceheight = jsonObject.getInt("height");
			int deviceWidth = jsonObject.getInt("width");
			String userIdValue=jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID));
			int userId;
			
			smartMirrorDTO = smartMirrorDao.checkMirror(major, minor,Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
			if (smartMirrorDTO == null) {
				socketResponce.setType(ResourceManager.getProperty(EXCEPTION));
				socketResponce.setMessage(ResourceManager.getProperty(MIRROR_NOT_REGISTER));
				socketBroadcastMessage.setData(objectMapper.writeValueAsString(socketResponce));
				return socketBroadcastMessage;
			} 
			
			if(jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)) == null ||  userIdValue.isEmpty())
			{
				userId=0;
			}else
			{
				userId= Integer.parseInt(jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)));	
			}
			
			if(jsonObject.getString(ResourceManager.getProperty(KEY_USER_ROLE)) == null ||  userRole.isEmpty())
			{
				userRole="";
			}

			UserMirrorModel userMirrorModel = new UserMirrorModel();
			UserModel userModel = null;
			if(userRole.isEmpty())
			{
				if(smartMirrorDTO.getMirrorPreview() == false)
				{
					UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorByBleRangeStatusTime(smartMirrorDTO.getId());
					if (userMirrorDTO != null) {
						userMirrorModel = dozerMapper.map(userMirrorDTO,UserMirrorModel.class);
						  userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
					     } 
					else {
						  UserMirrorDTO userMirrorDTO1 = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
						  if (userMirrorDTO1 != null) 
						    {
							  userMirrorModel = dozerMapper.map(userMirrorDTO1,UserMirrorModel.class);
							  userModel = dozerMapper.map(userMirrorDTO1.getUser(), UserModel.class);
						    }
					    }	
				}else
				{
					  UserMirrorDTO userMirrorDTO1 = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
					  if (userMirrorDTO1 != null) 
					    {
						  userMirrorModel = dozerMapper.map(userMirrorDTO1,UserMirrorModel.class);
						  userModel = dozerMapper.map(userMirrorDTO1.getUser(), UserModel.class);
					    }
				}
			}else
			{
				if(userId != 0)
				{
					UserDTO userDTO = userDao.getUserById(userId);
					if (userDTO != null) {
						userModel = dozerMapper.map(userDTO, UserModel.class);
					} else {
						socketResponce.setType(ResourceManager.getProperty(EXCEPTION));
						socketResponce.setMessage(ResourceManager.getProperty(USER_NOT_REGISTER));
						socketBroadcastMessage.setData(objectMapper.writeValueAsString(socketResponce));
						return socketBroadcastMessage;
					}
					UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorFileds(userModel.getId(), smartMirrorDTO.getId(), userRole,Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
					if (userMirrorDTO != null) {
						userMirrorModel = dozerMapper.map(userMirrorDTO,UserMirrorModel.class);
					} else {
						socketResponce.setType(ResourceManager.getProperty(EXCEPTION));
						socketResponce.setMessage(ResourceManager.getProperty(USER_MIRROR_NOT_REGISTERED_EXCEPTION));
						socketBroadcastMessage.setData(objectMapper.writeValueAsString(socketResponce));
						return socketBroadcastMessage;
					}
				}else
				{
					UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
					if (userMirrorDTO != null) {
						userMirrorModel = dozerMapper.map(userMirrorDTO,UserMirrorModel.class);
						userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
					} else {
						socketResponce.setType(ResourceManager.getProperty(EXCEPTION));
						socketResponce.setMessage(ResourceManager.getProperty(USER_MIRROR_NOT_REGISTERED_EXCEPTION));
						socketBroadcastMessage.setData(objectMapper.writeValueAsString(socketResponce));
						return socketBroadcastMessage;
					}
				}
			}

			WidgetSettingModel widgetSettingModel = new WidgetSettingModel();
			widgetSettingModel.setUserMirror(userMirrorModel);
			widgetSettingModel.setDeviceHeight(deviceheight);
			widgetSettingModel.setDeviceWidth(deviceWidth);
            String iconFormat = "svg";
            widgetSettingModel.setIconFormat(iconFormat);
			List<WidgetData> widgetData = widgetService.getSocketWidgetSetting(userModel, widgetSettingModel,ResourceManager.getProperty(VERSION_V1));
			ObjectMapper mapper = new ObjectMapper();
			String widgets = mapper.writeValueAsString(widgetData);
			jsonObject3.put("type", "widgetList");
			jsonObject3.put("data", widgets);
			if(userModel!=null)
			{
				jsonObject3.put("userId", Integer.toString(userModel.getId()));	
			}
			jsonObject3.put("userRole", userMirrorModel.getUserRole());
			String deviceId = smartMirrorDTO.getDeviceId();
			socketBroadcastMessage.setDeviceId(deviceId);
			socketBroadcastMessage.setData(jsonObject3.toString());
			 		
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return socketBroadcastMessage;

	}

	@Override
	public void updateWidgetSetting(JSONObject jsonObject)throws MangoMirrorException {
		String userRole = jsonObject.getString(ResourceManager.getProperty(KEY_USER_ROLE));
		int userId;
		String userIdValue=jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID));
		if( jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)) == null ||  userIdValue.isEmpty())
		{
			userId=0;
		}else
		{
			userId= Integer.parseInt(jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)));	
		}
		
		int major = Integer.parseInt(jsonObject.getString(ResourceManager.getProperty(KEY_MAJOR)));
		int minor = Integer.parseInt(jsonObject.getString(ResourceManager.getProperty(KEY_MINOR)));
		int deviceheight = jsonObject.getInt(ResourceManager.getProperty(KEY_HEIGHT));
		int deviceWidth = jsonObject.getInt(ResourceManager.getProperty(KEY_WIDTH));

		JSONObject jsonObject2 = jsonObject.getJSONObject("value");
		UserModel userModel = null;
		try
		{
			SmartMirrorDTO smartMirrorDTO = smartMirrorDao.checkMirror(major, minor,Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
			WidgetSettingModel widgetSettingModel = new WidgetSettingModel();
			WidgetModel widgetModel = new WidgetModel();
			UserMirrorModel userMirrorModel = new UserMirrorModel();
			if (smartMirrorDTO != null)
			{
				SmartMirrorModel smartMirrorModel = dozerMapper.map(smartMirrorDTO,SmartMirrorModel.class);
				widgetModel.setId(jsonObject2.getInt("contentId"));
				
				if(userId != 0)
				{
					UserDTO userDTO = userDao.getUserById(userId);
					UserMirrorDTO userMirrorDTO = null;
					if (userDTO != null) {
						userModel = dozerMapper.map(userDTO, UserModel.class);
						userMirrorDTO = userMirrorDao.getUserMirrorFileds(userModel.getId(), smartMirrorDTO.getId(), userRole,Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
					} 
					if (userMirrorDTO != null) {
						userMirrorModel = dozerMapper.map(userMirrorDTO,UserMirrorModel.class);
					}
					userMirrorModel.setMirror(smartMirrorModel);
					userMirrorModel.setUserRole(userRole);
				}else
				{
					UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
					if (userMirrorDTO != null) {
						userMirrorModel = dozerMapper.map(userMirrorDTO,
								UserMirrorModel.class);
						userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
					} 
				}
			}

			widgetSettingModel.setWidget(widgetModel);
			widgetSettingModel.setUserMirror(userMirrorModel);
			widgetSettingModel.setxPos(jsonObject2.getInt("xPos"));
			widgetSettingModel.setyPos(jsonObject2.getInt("yPos"));
			widgetSettingModel.setStatus(jsonObject2.getString("status"));
			widgetSettingModel.setDeviceHeight(deviceheight);
			widgetSettingModel.setDeviceWidth(deviceWidth);
			widgetSettingModel.setMinHeight(jsonObject2.getInt("minHeight"));
			widgetSettingModel.setMinWidth(jsonObject2.getInt("minWidth"));
			widgetSettingModel.setHeight(jsonObject2.getInt("height"));
			widgetSettingModel.setWidth(jsonObject2.getInt("width"));
			widgetSettingModel.setId(jsonObject2.getInt("widgetSettingId"));
			List<WidgetSettingModel> widgetSettingList = new ArrayList<WidgetSettingModel>();
			widgetSettingList.add(widgetSettingModel);
			widgetService.socketUpdateWidgetSetting(userModel, widgetSettingList);

		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@SuppressWarnings("rawtypes")
	public ArrayList broadcast(String data) throws MangoMirrorException {
		try
		{
			final String uri = ResourceManager.getProperty(REST_URL_SOCKET);
		 	HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<>(data,headers);
			RestTemplate restTemplate = new RestTemplate();
			String updatedUri = uri.concat("socket");
			return restTemplate.postForObject(updatedUri,entity, ArrayList.class);	
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public void reconnectSocket(int major, int minor) throws MangoMirrorException {
		SmartMirrorDTO smartMirrorDTO=smartMirrorDao.checkMirror(major, minor,Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		try
		{
			if(smartMirrorDTO!=null)
			{
				JSONObject payloadJson = new JSONObject();
				AWSIotDataClient awsIotClient = new AWSIotDataClient(new BasicAWSCredentials(accessKeyId,secretAccessKey));
				UpdateThingShadowRequest updateThingShadowRequest = new UpdateThingShadowRequest();
				updateThingShadowRequest.setThingName("MangoMirror-"+smartMirrorDTO.getDeviceId());
				Map<String, String> reportData = new HashMap<>();
				reportData.put(ResourceManager.getProperty(SOCKET_STATUS),ResourceManager.getProperty(SOCKET_DISCONNECT_STATUS) );
				Map<String, Map> reported = new HashMap<>();
				reported.put(ResourceManager.getProperty(DESIRED), reportData);
				payloadJson.put(ResourceManager.getProperty(STATE), reported);
	            String payload = payloadJson.toString();
				byte[] b = payload.getBytes(StandardCharsets.UTF_8);
				PublishRequest publishRequest = new PublishRequest();
				publishRequest.setTopic("$aws/things/"+updateThingShadowRequest.getThingName()+"/shadow/update");
				publishRequest.setPayload(ByteBuffer.wrap(b));
				awsIotClient.publish(publishRequest);
			}	
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@Override
	public void updateTwitterCredentials(JSONObject jsonObject)
			throws MangoMirrorException {
		
			int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			try
			{
				String userRole = jsonObject.getString(ResourceManager.getProperty(KEY_USER_ROLE));
				int major = Integer.parseInt(jsonObject.getString("major"));
				int minor = Integer.parseInt(jsonObject.getString("minor"));
				String twitterAuthStatus = jsonObject.getString("twitterAuthStatus");

				int userId;
				
				SmartMirrorDTO smartMirrorDTO = smartMirrorDao.checkMirror(major, minor,userStatusFlag);
				String userIdValue=jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID));

				if( jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)) == null ||  userIdValue.isEmpty())
				{
					userId=0;
				}else
				{
					userId= Integer.parseInt(jsonObject.getString(ResourceManager.getProperty(KEY_USER_ID)));	
				}
				
				UserMirrorDTO userMirrorDTO = null;
				
				if (smartMirrorDTO != null)
				{
					if(userId != 0)
					{
						
						userMirrorDTO = userMirrorDao.getUserMirrorFileds(userId, smartMirrorDTO.getId(), userRole,userStatusFlag);
					}else
					{
						userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
					}
				}
				
				UserMirrorModel userMirrorModel = dozerMapper.map(userMirrorDTO, UserMirrorModel.class);
				TwitterModel twitterModel=new TwitterModel();
				twitterModel.setTwitterCredentialStatus(twitterAuthStatus);
				userMirrorModel.setTwitter(twitterModel);
				if(userMirrorDTO!=null)
					{
					 if(userMirrorDTO.getTwitter()!=null)
						{
							userMirrorDao.updateTwitterCredentials(userMirrorModel,userMirrorDTO.getTwitter().getId());	
						}
					}else
					{
						throw new MangoMirrorException(ResourceManager.getMessage(
								USER_MIRROR_NOT_REGISTERED_EXCEPTION, null, NOT_FOUND, null));
					}
				
			}catch(Exception exception)
			{
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
	}


}
