
package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dvdme.ForecastIOLib.ForecastIO;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.UserDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDaoImpl;
import com.mindbowser.homeDisplay.dao.WidgetDao;
import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.QuotesCategoryDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.QuotesLengthDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserCalendarDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WeatherDataDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.CalendarFormatModel;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthDataTimeZoneModel;
import com.mindbowser.homeDisplay.model.QuotesCategoryModel;
import com.mindbowser.homeDisplay.model.QuotesLengthModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.StickyNotesRemoveModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.UserStepModel;
import com.mindbowser.homeDisplay.model.WidgetData;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.util.AsymmetricCryptography;
import com.mindbowser.homeDisplay.util.CalendarFormatDozerHelper;
import com.mindbowser.homeDisplay.util.CalendarUtcDateTime;
import com.mindbowser.homeDisplay.util.KMSUtil;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.QuotesCategoryDozerHelper;
import com.mindbowser.homeDisplay.util.QuotesLengthDozerHelper;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.UserCalenderDozerHelper;
import com.mindbowser.homeDisplay.util.WidgetDozerHelper;
import com.mindbowser.homeDisplay.util.WidgetSettingDozerHelper;


public class WidgetServiceImpl implements WidgetService {

	private static Logger logger = Logger.getLogger(WidgetServiceImpl.class);

	@Autowired
	private WidgetDao widgetDao;

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private BetaUserService betaUserService;

	@Autowired
	private UserMirrorDaoImpl userMirrorDao;

	@Autowired
	private SocketService socketService;
	
	private Random randomGenerator;

	@Autowired
	private Mapper dozerMapper;

	@Value("${foreCastApiKey}")
	private String foreCastApiKey;
	
	@Value("${foreCastApiUrl}")
	private String foreCastApiUrl;

	@Value("${s3BasePath}")
	private String s3BasePath;

	@Value("${quotesCategoryBasePath}")
	private String categoryBasePath;

	@Value("${quotesBasePath}")
	private String quotesBasePath;

	@Value("${quotesApiKey}")
	private String quotesApiKey;
	
	@Autowired
	private KMSUtil kmsUtil;
		
	@Autowired
	private AsymmetricCryptography asymmetricCryptography;
	
	
	@Autowired
	private FitBitService fitbitService;

	@Override
	public List<WidgetModel> getAllWidget(HttpServletRequest request, String version) throws MangoMirrorException {

		ArrayList<String> widgetList = new ArrayList<>();
		widgetList.add(ResourceManager.getProperty(WIDGET_CALENDAR));
		widgetList.add(ResourceManager.getProperty(WIDGET_CLOCK));
		widgetList.add(ResourceManager.getProperty(WIDGET_STEP));
		widgetList.add(ResourceManager.getProperty(WIDGET_WEATHER));

		List<WidgetDTO> widgets = widgetDao.getAllWidget(widgetList);
		List<WidgetModel> widgetModelList = null;
		try {
			if (widgetList.isEmpty()) {
				throw new MangoMirrorException(
						ResourceManager.getMessage(NO_WIDGET_AVAILABLE_EXCEPTION, null, NOT_FOUND, null));
			} else {
				widgetModelList = WidgetDozerHelper.map(dozerMapper, widgets, WidgetModel.class);
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return widgetModelList;
	}

	@Override
	public List<WidgetData> getAllWidgetSetting(UserModel userModel, WidgetSettingModel widgetSetting, String version)
			throws MangoMirrorException {

		List<WidgetData> newWidgetSetting = new ArrayList<>();
		try {
			/* check whether mirror related info is provide or not in request */
			if (widgetSetting.getUserMirror().getMirror() != null) {
				int mirrorId = widgetSetting.getUserMirror().getMirror().getId();
				if (mirrorId > 0) {
					if (widgetSetting.getUserMirror().getUserRole() != null) {
						/*
						 * get usermirror data related with the requested user
						 * with specified userRole on mirror
						 */
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), mirrorId,
								widgetSetting.getUserMirror().getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
						if (userMirrorDTO != null) {
							List<WidgetDTO> widgetList = widgetDao.getNewWidgetList(userMirrorDTO);
							if (!widgetList.isEmpty()) {
								List<MirrorPageDTO> mirrorPageList = widgetDao.getMirrorPage(userMirrorDTO.getId());
								MirrorPageDTO mirrorPageDTO = mirrorPageList.get(0);
								addNewWidgets(widgetList, mirrorPageDTO, userMirrorDTO);
							}
							List<WidgetDTO> widgetDtoList = widgetListByVersion(version);
							// Get all widgets setting in the database with
							// respect to the user in the request
							List<WidgetSettingDTO> WidgetSettingData = widgetDao.getWidgetSetting(userMirrorDTO,
									widgetDtoList);
							// Calculate widget size and position as per
							// requested device height and width
							newWidgetSetting = addWidgetData(WidgetSettingData, widgetSetting.getDeviceWidth(),
									widgetSetting.getDeviceHeight(), userMirrorDTO, widgetSetting.getIconFormat(),
									version);
						} else {
							throw new MangoMirrorException(ResourceManager
									.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION, null, NOT_FOUND, null));
						}
					} else {

						throw new MangoMirrorException(
								ResourceManager.getMessage(USER_ROLE_REGISTRATION_EXCEPTION, null, NOT_FOUND, null));
					}
				} else {
					throw new MangoMirrorException (
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return newWidgetSetting;
	}

	@Override
	public List<WidgetDTO> widgetListByVersion(String version) {

		ArrayList<String> widgetList = new ArrayList<>();
		if (version.equals(ResourceManager.getProperty(VERSION_V1)) || version.equals(ResourceManager.getProperty(VERSION_BETA))) {
			widgetList.add(ResourceManager.getProperty(WIDGET_CALENDAR));
			widgetList.add(ResourceManager.getProperty(WIDGET_CLOCK));
			widgetList.add(ResourceManager.getProperty(WIDGET_STEP));
			widgetList.add(ResourceManager.getProperty(WIDGET_WEATHER));
		
			widgetList.add(ResourceManager.getProperty(WIDGET_TWITTER));
			widgetList.add(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST));
			widgetList.add(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST));
			widgetList.add(ResourceManager.getProperty(WIDGET_QUOTES));
			widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL));

			widgetList.add(ResourceManager.getProperty(WIDGET_DISTANCE));
			widgetList.add(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES));
			widgetList.add(ResourceManager.getProperty(WIDGET_CALORIES));
			widgetList.add(ResourceManager.getProperty(WIDGET_CALORIESBMR));

			widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE));
			widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED));

			widgetList.add(ResourceManager.getProperty(WIDGET_CARB));
			widgetList.add(ResourceManager.getProperty(WIDGET_FAT));
			widgetList.add(ResourceManager.getProperty(WIDGET_FIBER));

			widgetList.add(ResourceManager.getProperty(WIDGET_PROTEIN));
			widgetList.add(ResourceManager.getProperty(WIDGET_SODIUM));
			widgetList.add(ResourceManager.getProperty(WIDGET_WATER));
			widgetList.add(ResourceManager.getProperty(WIDGET_BMI));
			widgetList.add(ResourceManager.getProperty(WIDGET_WEIGHT));
			widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE));
			widgetList.add(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED));

			widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC));
			widgetList.add(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB));
			widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL));
			widgetList.add(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR));

			widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE));
			widgetList.add(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC));
			widgetList.add(ResourceManager.getProperty(WIDGET_HEART_RATE));
			widgetList.add(ResourceManager.getProperty(WIDGET_STICKYNOTES));
			
		}
		return widgetDao.getAllWidget(widgetList);
	}

	public void addNewWidgets(List<WidgetDTO> widgetDTOList, MirrorPageDTO mirrorPageDTO, UserMirrorDTO userMirrorDTO) throws MangoMirrorException {
		List<WidgetSettingDTO> widgetSettingList = new ArrayList<>();
		
		try
		{
			for (WidgetDTO widgetDTO : widgetDTOList) {
				WidgetSettingDTO widgetSettingDTO = new WidgetSettingDTO();
				widgetSettingDTO.setWidget(widgetDTO);
				widgetSettingDTO.setDeviceWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));
				widgetSettingDTO.setDeviceHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT)));
				widgetSettingDTO.setWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));
				widgetSettingDTO.setMinWidth(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_WIDTH)));
				widgetSettingDTO.setxPos(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_XPOS)));
				widgetSettingDTO.setyPos(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_YPOS)));
				widgetSettingDTO.setPinned(false);
				
				widgetSettingDTO.setUserMirror(userMirrorDTO);
				widgetSettingDTO.setMirrorPage(mirrorPageDTO);
				if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CLOCK))) {
					widgetSettingDTO.setStatus(ResourceManager.getProperty(STATUS_ON));
					widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_HEIGHT)));
					widgetSettingDTO.setMinHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
					widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_TEXT));

				} else {
					widgetSettingDTO.setStatus(ResourceManager.getProperty(STATUS_OFF));
					if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_STEP))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CARB))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FIBER))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_PROTEIN))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_SODIUM))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_HEART_RATE))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))
							|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {
						
						widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHHEIGHT)));
						widgetSettingDTO.setMinHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHMINHEIGHT)));
						widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_GRAPH));
					       }
					else
					{
						widgetSettingDTO.setHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_HEIGHT)));
						widgetSettingDTO.setMinHeight(Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)));
						widgetSettingDTO.setViewType(ResourceManager.getProperty(WIDGET_VIEW_TYPE_TEXT));
					}
				
				}
				widgetSettingList.add(widgetSettingDTO);
			}
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		widgetDao.saveWidgetSetting(widgetSettingList);
	}

	/**
	 * This method is used to update existing widget setting of particular user
	 * for particular mirror
	 * 
	 * @param request
	 * @param widgetSettingModelList
	 * @param userMirrorDTO
	 * @throws UserException
	 * @throws WidgetSettingException
	 */

	@Override
	public Map<String, Object> updateWidgetSetting(HttpServletRequest request,
			List<GenericWidgetSettingModel> genericWidgetSettingModelList, String version)
			throws MangoMirrorException, MangoMirrorException {

		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		HashMap<String, Object> updatedStatus = new HashMap<>();
		try {
			if (genericWidgetSettingModelList.get(0).getUserMirror().getMirror() != null) {
				int mirrorId = genericWidgetSettingModelList.get(0).getUserMirror().getMirror().getId();
				if (mirrorId > 0) {
					if (genericWidgetSettingModelList.get(0).getUserMirror().getUserRole() != null) {
						/* Check user relation with mirror is existed or not */
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), mirrorId,
								genericWidgetSettingModelList.get(0).getUserMirror().getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));

						if (genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus() != null && 
								userMirrorDTO.getClockMessageStatus() != genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus()) 
						{
							userMirrorDTO.setClockMessageStatus(genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus());
						}
						if (userMirrorDTO != null) {
							widgetDao.deleteMirrorPage(userMirrorDTO.getId());
							widgetDao.deleteStickyNotesByUserMirror(userMirrorDTO.getId());

							for (GenericWidgetSettingModel genericWidgetSettingModel : genericWidgetSettingModelList) {
								MirrorPageDTO mirrorPageDTO = dozerMapper.map(genericWidgetSettingModel.getMirrorPage(),
										MirrorPageDTO.class);
								mirrorPageDTO.setUserMirror(userMirrorDTO);
								mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
								
								if (!genericWidgetSettingModel.getWidgets().isEmpty()) {
									List<WidgetSettingModel> widgetSetting = genericWidgetSettingModel.getWidgets();
									List<WidgetSettingDTO> widgetSettingList = WidgetSettingDozerHelper.MapModelToDto(dozerMapper, widgetSetting, WidgetSettingDTO.class); 
									widgetDao.newUpdateWidgetSetting(widgetSettingList, userMirrorDTO, mirrorPageDTO);
								}
							}

							WidgetSettingModel widgetSettingModel = new WidgetSettingModel();
							widgetSettingModel.setDeviceHeight(genericWidgetSettingModelList.get(0).getWidgets().get(0).getDeviceHeight());
							widgetSettingModel.setDeviceWidth(genericWidgetSettingModelList.get(0).getWidgets().get(0).getDeviceWidth());
							widgetSettingModel.setUserMirror(genericWidgetSettingModelList.get(0).getUserMirror());
						    List<WidgetData> widgetDataList = getAllWidgetSetting(userModel,widgetSettingModel, version);
						    
							updatedStatus.put("mirrorPreview", userMirrorDTO.getMirror().getMirrorPreview());
							updatedStatus.put("widgets", widgetDataList);
							
							UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(mirrorId);
							SocketResponce socketResponce = new SocketResponce();
							ObjectMapper objectMapper = new ObjectMapper();
							Map<String, String> user = new HashMap<>();
							socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
							socketResponce.setMessage(ResourceManager.getProperty(WIDGET_REFRESH));
							JSONArray array = new JSONArray();
							JSONObject jsonObject = new JSONObject();

							if (userMirrorDTO.getMirror().getMirrorPreview()) {
								if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
									user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
									user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO.getUserRole());
									socketResponce.setData(objectMapper.writeValueAsString(user));
									jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
									jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketService.broadcast(array.toString());
								}
							} else {
								if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)) && userMirrorDTO1 == null) {
										user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
										user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO.getUserRole());
										socketResponce.setData(objectMapper.writeValueAsString(user));
										jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
										jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
										array.put(jsonObject);
										socketService.broadcast(array.toString());
									

								} else {
									if (userMirrorDTO1 != null && userMirrorDTO1.getUser().getId() == userModel.getId()) {
											user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
											user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO1.getUserRole());
											socketResponce.setData(objectMapper.writeValueAsString(user));
											jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
											jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
											array.put(jsonObject);
											socketService.broadcast(array.toString());
										}
									
								}
							}

							if (userMirrorDTO.getTwitter() != null) {
								updatedStatus.put(ResourceManager.getProperty(TWITTERCREDENTIALSTATUS_LABEL),
										userMirrorDTO.getTwitter().getTwitterCredentialStatus());
							}
							betaUserService.updateAllBetaMirrorLayout(ResourceManager.getProperty(VERSION_BETA));
						} else {
							throw new MangoMirrorException(ResourceManager.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION,
									null, NOT_FOUND, locale));
						}
					} else {
						throw new MangoMirrorException(
								ResourceManager.getMessage(USER_ROLE_NOT_SPECIFIED_EXCEPTION, null, NOT_FOUND, locale));
					}
				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, locale));
				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, locale));
			}

		} 
		catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return updatedStatus;
	}

	/**
	 * this method is used to accept request from web portal through socket to
	 * update widgets
	 */

	@Override
	public void socketUpdateWidgetSetting(UserModel userModel, List<WidgetSettingModel> widgetSetting)
			throws MangoMirrorException, MangoMirrorException {

		try {
			if (widgetSetting.get(0).getUserMirror().getMirror() != null) {
				int mirrorId = widgetSetting.get(0).getUserMirror().getMirror().getId();
				if (mirrorId > 0) {
					if (widgetSetting.get(0).getUserMirror().getUserRole() != null) {
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorFileds(userModel.getId(), mirrorId,
								widgetSetting.get(0).getUserMirror().getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
						if (userMirrorDTO != null) {
							widgetDao.socketUpdateWidgetSetting(widgetSetting, userMirrorDTO);
						} else {
							throw new MangoMirrorException(ResourceManager.getMessage(MODIFICATION_ACCESS_DENIED_EXCEPTION,
									null, NOT_FOUND, null));
						}
					} else {
						throw new MangoMirrorException(ResourceManager.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION, null,
								NOT_FOUND, null));
					}
				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
			}
		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		}

	}

	@Override
	public ForecastIO getWeatherInfo(String latitude, String longitude) {
		
		ForecastIO fio = null;
		try
		{
			fio = new ForecastIO(foreCastApiKey);
			fio.setUnits(ForecastIO.UNITS_SI);
			fio.setLang(ForecastIO.LANG_ENGLISH);
			fio.setExcludeURL("alerts,flags,minutely");
			fio.getForecast(latitude, longitude);
		}catch(Exception exception)
		{
			logger.info(exception);
		}
		  return fio;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<WidgetData> addWidgetData(List<WidgetSettingDTO> widgetSettingData, int deviceWidth, int deviceHeight,
			UserMirrorDTO userMirrorDTO, String iconFormat, String version)throws MangoMirrorException {
		int userId = widgetSettingData.get(0).getUserMirror().getUser().getId();
		double oldDeviceHeight;
		double minHeight;
		double xPos;
		double yPos;
		double widgetHeight;
		double newXPos;
		double newYPos;
		double newWidgetHeight;
		double newMinHeight;
		int defaultheight;
		int defaultMinHeight;
		
		
		
		ArrayList groups = new ArrayList();
		List<MirrorPageDTO> mirrorPageDtoList = widgetDao.getMirrorPage(userMirrorDTO.getId());
		LocationDTO locationDTO = userMirrorDTO.getLocation();
	
		try
		{
			if (locationDTO != null) {
				checkAndUpdateWeatherData(locationDTO);
			}
			for (MirrorPageDTO mirrorPageDTO : mirrorPageDtoList) {
				Map<String, Object> pages = new HashMap<>();

				pages.put("pageNumber", mirrorPageDTO.getPageNumber());
				pages.put("pageId", mirrorPageDTO.getId());
				ArrayList widgetList = new ArrayList();

				for (WidgetSettingDTO widgetSettingDTO : widgetSettingData) {
					widgetHeight = widgetSettingDTO.getHeight();
					xPos = widgetSettingDTO.getxPos();
					yPos = widgetSettingDTO.getyPos();
					minHeight = widgetSettingDTO.getMinHeight();
					oldDeviceHeight = widgetSettingDTO.getDeviceHeight();
					newXPos = xPos;
					newYPos = (yPos * deviceHeight) / oldDeviceHeight;
					newWidgetHeight = (widgetHeight * deviceHeight) / oldDeviceHeight;
					newMinHeight = (minHeight * deviceHeight) / oldDeviceHeight;
					
					if (widgetSettingDTO.getViewType().equals(ResourceManager.getProperty(WIDGET_VIEW_TYPE_GRAPH))) {
						defaultheight = Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHHEIGHT)) * deviceHeight/Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT));
						defaultMinHeight= Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_GRAPHMINHEIGHT)) * deviceHeight/Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT));

					}else
					{
						defaultheight = Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_HEIGHT)) * deviceHeight/Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT));
						defaultMinHeight= Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_MINHEIGHT)) * deviceHeight/Integer.parseInt(ResourceManager.getProperty(PORTRAIT_DEFAULT_DEVICE_HEIGHT));
					}
					
					WidgetData widgetData = new WidgetData();
					widgetData.setDefaultHeight(defaultheight);
					widgetData.setDefaultMinHeight(defaultMinHeight);
					widgetData.setPinned(widgetSettingDTO.getPinned());
					widgetData.setWidth(deviceWidth);
					widgetData.setMinWidth(deviceWidth);
					
					widgetData.setDeviceId(userMirrorDTO.getMirror().getDeviceId());
					widgetData.setContentId(widgetSettingDTO.getWidget().getId());
					widgetData.setContentType(widgetSettingDTO.getWidget().getType());
					widgetData.setHeight(newWidgetHeight);
					widgetData.setxPos(newXPos);
					widgetData.setyPos(newYPos);
					widgetData.setStatus(widgetSettingDTO.getStatus());
					widgetData.setMinHeight(newMinHeight);
					widgetData.setViewType(widgetSettingDTO.getViewType());
					widgetData.setWidgetMasterCategory(widgetSettingDTO.getWidget().getMasterCategory());
					widgetData.setWidgetSubCategory(widgetSettingDTO.getWidget().getSubCategory());
					if (!widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
						widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName());	
					}
					widgetData.setGraphType(widgetSettingDTO.getWidget().getGraphType());
					widgetData.setWidgetSettingId(widgetSettingDTO.getId());

					if (widgetSettingDTO.getMirrorPage().getId() == mirrorPageDTO.getId()) {
						if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CLOCK))) {

							int minutes = userMirrorDTO.getTimeZone();
							int offMilisecond = minutes * 60 * 1000;
							Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
							long time = cal.getTimeInMillis() + offMilisecond;
							SimpleDateFormat sdf = new SimpleDateFormat("HH");
							cal.add(Calendar.MINUTE, minutes);
							sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
							String Hours = sdf.format(cal.getTime());
							int hours = Integer.parseInt(Hours);
							String message = "";
							String userName = "";

							Map<String, Object> data = new HashMap<>();
						/*	if (userMirrorDTO.getUser().getName() != null) {
								userName = userMirrorDTO.getUser().getName();
							}*/
							if (hours >= 4 && hours < 12) {
								message = message.concat(ResourceManager.getProperty(MORNING_WISH));
							}
							if (hours >= 12 && hours < 16) {
								message = message.concat(ResourceManager.getProperty(AFTERNOON_WISH));
							}
							if (hours >= 16 && hours < 20) {
								message = message.concat(ResourceManager.getProperty(EVENING_WISH));
							}
							if (hours >= 20 || hours < 4) {
								message = message.concat(ResourceManager.getProperty(HELLO_WISH));
							}

							if (!userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
								if (userMirrorDTO.getUser().getName() != null) {
										File publicKeyFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
										PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
										String emailId = asymmetricCryptography.decryptText(userMirrorDTO.getUser().getEmailId(), publicKey);
										String plainTextKey = kmsUtil.decryptKey(userMirrorDTO.getUser().getEncryptedPlainText(), emailId);
										userName  = kmsUtil.decryptData(plainTextKey, userMirrorDTO.getUser().getName());
										data.put("userName", userName);
								}		
								message = message.concat(", " + userName);
							}

							
							data.put("utcMiliSecond", time);
							data.put("message", message);
							data.put("clockMessageStatus", userMirrorDTO.getClockMessageStatus().toString());
							widgetData.setData(data);
						}

						else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_WEATHER))) {
							Map<String, Object> data = getCurrentWeatherData(userMirrorDTO, iconFormat);
							widgetData.setData(data);
						}

						else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_CALENDAR))) {

							
							HashMap event = new HashMap();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								Map<String, List<Map<String, Object>>> events = getCalenderData(userId, userMirrorDTO);
								event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
							}
							List<CalendarFormatModel> calendarFormatList = calendarFormatList();
							Map<String, Boolean>  options= new HashMap<>();
							options.put("reminderFlag", userMirrorDTO.getReminderFlag());
							options.put("calendarFlag", userMirrorDTO.getCalendarFlag());
							event.put("visibleOptions", options);
							event.put("calendarFormatList", calendarFormatList);
							Map<String, String> calendarTitle = new HashMap<>();
							calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
							calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO.getCalendarFormat().getLabel());
							List<Map<String, String>> Title = new ArrayList<>();
							Title.add(calendarTitle);
							event.put(ResourceManager.getProperty(TITLE_LABEL), Title);
							widgetData.setData(event);

						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST))) {

							Map<String, Object> data =  getDailyWeatherData(userMirrorDTO, iconFormat,
									ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_TWITTER))) {
							Map<String, Object> twitter = new HashMap<>();
							TwitterDTO twitterDTO = userMirrorDTO.getTwitter();
							if (twitterDTO != null) {
								twitter.put("id", twitterDTO.getId());
								twitter.put("screenName", twitterDTO.getScreenName());
								twitter.put("timeLine", twitterDTO.getTimeLine());
								twitter.put(ResourceManager.getProperty(TWITTERCREDENTIALSTATUS_LABEL), twitterDTO.getTwitterCredentialStatus());
								twitter.put("twitterFollowingUser", twitterDTO.getTwitterFollowingUser());
								twitter.put("twitterOauthToken", twitterDTO.getTwitterOauthToken());
								twitter.put("twitterOauthTokenSecret", twitterDTO.getTwitterOauthTokenSecret());
							}
							widgetData.setData(twitter);

						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))) {

							Map<String, Object> data = getHourlyWeatherData(userMirrorDTO, iconFormat,
									ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
							widgetData.setData(data);

						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_QUOTES))) {

							Map<String, Object> data = new HashMap<>();
							HashMap quotesData = new HashMap();
							List<QuotesCategoryModel> quotesCategoryDtoList = getQuotesCategoryList();
							List<QuotesLengthModel> quotesLengthDtoList = getQuotesCategoryLengthList();
							quotesData.put(ResourceManager.getProperty(QUOTES_CATEGORY_LABEL), quotesCategoryDtoList);
							quotesData.put(ResourceManager.getProperty(QUOTES_LENGTH_LABEL), quotesLengthDtoList);
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)) )
							{
								List<QuotesDTO> quotesList = widgetDao.getQuotesByUserMrrrorId(userMirrorDTO.getId());
								if(!quotesList.isEmpty())
								{
									data.put("selectedQuoteLength", quotesList.get(0).getQuotesLength().getId());
									List<Integer> quotesCategoryId = new ArrayList<>();
									for (QuotesDTO quotesDTO : quotesList) {
										quotesCategoryId.add(quotesDTO.getQuotesCategory().getId());
									}
									data.put("selectedQuoteCategories", quotesCategoryId);
								}
							}
							quotesData.put(ResourceManager.getProperty(QUOTES_LABEL), data);
							widgetData.setData(quotesData);
						}
						else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
							
						  Map<String, Object> data = new HashMap<>();
						  if(widgetSettingDTO.getStickyNotes().size() > 0)
							{
								data.put("id", widgetSettingDTO.getStickyNotes().get(0).getId());
								data.put(ResourceManager.getProperty(DATA_LABEL), widgetSettingDTO.getStickyNotes().get(0).getData());
								data.put("name", widgetSettingDTO.getStickyNotes().get(0).getName());
								widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName()+ ":" +widgetSettingDTO.getStickyNotes().get(0).getName());
								widgetData.setData(data);
							}else
							{
								widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName());
							}
						}
						
						widgetList.add(widgetData);
					}
				}
				pages.put("widgets", widgetList);
				pages.put("delay", mirrorPageDTO.getDelay());
				pages.put(ResourceManager.getProperty(PREVIEWSTATUS_LABEL), userMirrorDTO.getMirror().getMirrorPreview());
				if (userMirrorDTO.getFitbitAccount() != null) {
					
					FitBitAccountModel fitBitAccountModel = dozerMapper.map(userMirrorDTO.getFitbitAccount(), FitBitAccountModel.class);
				//	PublicKey publicKey = asymmetricCryptography.getPublic(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
					File publicKeyFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
					PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
					String fullname = asymmetricCryptography.decryptText(fitBitAccountModel.getFitbitUserName(), publicKey);
					fitBitAccountModel.setFitbitUserName(fullname);
					pages.put("fitBitDetail", fitBitAccountModel);
				}
				
				List<Map<String, Object>> healthWidgetList = null;
				try {
					healthWidgetList = fitbitService.getHealthKitWidgetList(version);
					pages.put("healthWidget", healthWidgetList);
				} catch (Exception e) {
					logger.info(e);
				}
				
				pages.put(ResourceManager.getProperty(PREVIEWSTATUS_LABEL), userMirrorDTO.getMirror().getMirrorPreview());
				pages.put("currentFirmwareVersion", userMirrorDTO.getMirror().getCurrentFirmwareVersion());
				groups.add(pages);
			}
			
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
			return groups;
	}

	@Override
	public Map<String, List<Map<String, Object>>> getCalenderData(int userId, UserMirrorDTO userMirrorDTO) {

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat sdf = new SimpleDateFormat(CalendarUtcDateTime.UTC_24HOUR_DATE_FORMAT_SECOND);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.MINUTE, userMirrorDTO.getUser().getUserCurrentTimeZone());
		String fromDate = sdf.format(cal.getTime());
		List<UserCalendarModel> CalendarList = null;
		Boolean calendarFlag = userMirrorDTO.getCalendarFlag();
		Boolean reminderFlag = userMirrorDTO.getReminderFlag();
		
		int userCurrentTimeZone = userMirrorDTO.getUser().getUserCurrentTimeZone();
		int limit = userMirrorDTO.getCalendarFormat().getCount();
		CalendarList = widgetDao.getCalendarDataByLimit(userId, fromDate, userCurrentTimeZone, limit,calendarFlag,reminderFlag);
		
		Map<String, List<Map<String, Object>>> events = new java.util.LinkedHashMap<>();
		String eventDateString, currentDateString;

		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat(CalendarUtcDateTime.UTC_24HOUR_DATE_FORMAT_SECOND);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMMM dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("UTC"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		dateTimeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

		for (int calenderCount = 0; calenderCount < CalendarList.size(); calenderCount++) {
			Map<String, Object> eventData = new HashMap<>();

			Date startTime = null, currentDate = null, endTime = null, eventTime = null;
			try {
				startTime = dateTimeFormatter.parse(CalendarList.get(calenderCount).getStartDate());
				if(CalendarList.get(calenderCount).getEndDate()!=null)
				{
					endTime = dateTimeFormatter.parse(CalendarList.get(calenderCount).getEndDate());	
				}
				eventDateString = dateFormat.format(startTime);
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				eventTime = (Date) formatter.parse(eventDateString);
				currentDateString = dateFormat.format(cal.getTime());
				currentDate = (Date) formatter.parse(currentDateString);
			} catch (ParseException e) {
				logger.info(e);
			}

			DateTime dateTime1 = new DateTime(eventTime);
			DateTime dateTime2 = new DateTime(currentDate);
			int dif = Days.daysBetween(dateTime2, dateTime1).getDays();
			SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat(CalendarUtcDateTime.UTC_24HOUR_DATE_FORMAT_SECOND);
			eventData.put("id", Integer.toString(CalendarList.get(calenderCount).getId()));
			eventData.put("startTime", timeFormat.format(startTime));
			eventData.put("startDate", dateTimeFormat.format(startTime));
			if(endTime !=null)
			{
				eventData.put("endTime", timeFormat.format(endTime));	
			}
			
			eventData.put("event", CalendarList.get(calenderCount).getEvent());
			eventData.put("location", CalendarList.get(calenderCount).getLocation());

			String eventDate;
			if (dif == 0) {
				eventDate = "Today";
			} else if (dif == 1) {
				eventDate = "Tomorrow";
			} else {
				eventDate = dateFormat1.format(startTime);
			}
			if (events.containsKey(eventDate)) {
				List<Map<String, Object>> eventList = events.get(eventDate);
				eventList.add(eventData);
				events.put(eventDate, eventList);
			} else {
				List<Map<String, Object>> newEventList = new ArrayList<Map<String, Object>>();
				newEventList.add(eventData);
				events.put(eventDate, newEventList);
			}
		}
		return events;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, Object> getHealthData(HealthDataModel requestHealthDataModel, Double conversionValue,
			HealthDataTimeZoneModel timeZoneModel, WidgetDTO widgetDTO, String selectedunit) throws MangoMirrorException {

		Date date;
		String label;
		Map stepValues = new HashMap();
		/* create one map of last 7 days with default value "0" */
		Map<Date, Object> healthValue = new HashMap<>();
		double maxValue = 0;
		Double minValue = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate;
		Calendar cal;

		cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.MINUTE, timeZoneModel.getUseratimeZone());
		startDate = sdf.format(cal.getTime());
		cal.add(Calendar.DAY_OF_YEAR, -7);
		Double defaultValue = Double.valueOf(0);
		for (int i = 0; i < 7; i++) {
			cal.add(Calendar.DAY_OF_YEAR, 1);
			String datevalue = sdf.format(cal.getTime());
			try {
				date = sdf.parse(datevalue);
				healthValue.put(date, defaultValue);
			} catch (ParseException e) {
				logger.info(e);
			}
		}

		/* sort map by date */
		Map<Date, Object> healthValue1 =  new TreeMap();
		List<Object> healthList = new ArrayList<>();
		
		List<String> labelArray = new ArrayList<String>();
		List healthData = new ArrayList();

		double goalValueDouble = 0;
		double initialMinDouble = 0;
		
		/* fetch step data of specific user */
		List<HealthDataModel> healthDataList;
		if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))
			||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
			||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
			||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
			||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
			||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
		{
			 healthDataList = widgetDao.getHealthDataByLimit(requestHealthDataModel, conversionValue, widgetDTO);
			 if (!healthDataList.isEmpty()) {
					goalValueDouble = healthDataList.get(0).getGoalValue();
					initialMinDouble = healthDataList.get(0).getGoalValue();
				
			 }
		}else
		{
			 healthValue1 = new TreeMap(healthValue);
			 healthDataList = widgetDao.getHealthData(requestHealthDataModel, conversionValue,startDate, widgetDTO);
			 if (!healthDataList.isEmpty()) {
					goalValueDouble = healthDataList.get(healthDataList.size()-1).getGoalValue();
					initialMinDouble = healthDataList.get(healthDataList.size()-1).getGoalValue();
			 }
		}
		

		/*
		 * itterate the healthdata and check if that data is available in map or
		 * not if exist then replace default value
		 */
		
		if (!healthDataList.isEmpty()) {
			maxValue = initialMinDouble;
			
			if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {
				int totalMinutes = Double.valueOf(goalValueDouble).intValue();
				int hours = totalMinutes / 60;
				int minutes = totalMinutes - hours * 60;
				double totalTime = hours + ((double) minutes / 100);
				stepValues.put(ResourceManager.getProperty(GOALVALUE_LABEL), Double.valueOf(totalTime));
			} else {
				stepValues.put(ResourceManager.getProperty(GOALVALUE_LABEL), goalValueDouble);
			}
			
			for (HealthDataModel healthDataModel : healthDataList) {
				
				String time = healthDataModel.getHealthTime();
				try {
					if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))
							||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
							||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
							||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
							||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
							||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
						{
						Date date2 = sdf.parse(time);
						Double healthValueDouble = healthDataModel.getHealthValue();
						if (maxValue < healthValueDouble) {
							maxValue = healthValueDouble;
						}
						List<Object> data =new ArrayList<>();
						data.add(date2);
						data.add(healthValueDouble);
						healthList.add(data);
					}
					else
					{
							Date date2 = sdf.parse(time);
							if (healthValue.containsKey(date2)) {
								Double healthValueDouble = healthDataModel.getHealthValue();
						
								if (maxValue < healthValueDouble) {
									maxValue = healthValueDouble;
								}
								
								if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
										|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {
									int totalMinutes = Double.valueOf(healthValueDouble).intValue();
									int hours = totalMinutes / 60;
									int minutes = totalMinutes - hours * 60;
									double totalTime = hours + ((double) minutes / 100);
									healthValue1.put(date2, totalTime);
								} else {
									healthValue1.put(date2, healthValueDouble);
								}
							}	
					}
				}catch(Exception exception)
				{
					logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
					throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
				}
			}

			if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {
				int maxTimeInMinutes = Double.valueOf(maxValue).intValue();
				int maxTimeInHours = maxTimeInMinutes / 60;
				int remainMaxTimeInMinutes = maxTimeInMinutes - maxTimeInHours * 60;
				double maxTime = maxTimeInHours + ((double) remainMaxTimeInMinutes / 100);
				stepValues.put(ResourceManager.getProperty(MAXVALUE_LABEL), maxTime);
			   } else {
				stepValues.put(ResourceManager.getProperty(MAXVALUE_LABEL), maxValue);
			}
		} else {
			stepValues.put(ResourceManager.getProperty(GOALVALUE_LABEL), goalValueDouble);
			stepValues.put(ResourceManager.getProperty(MAXVALUE_LABEL), maxValue);
		}
		/* change the format of date field like "MON,TUE" */

		int healthListIteration = 0;
		double value;
		if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
			{
			
			if(!healthDataList.isEmpty())
			{
				for (Object entry : healthList) {
					
					ArrayList<Object> data = (ArrayList<Object>) entry;
					data.get(0);
					
					healthData.add(data.get(1));
					label = new SimpleDateFormat("MMM d").format(data.get(0));
					
					value = (double) data.get(1);
					
					if(healthListIteration == 0)
					{
						minValue = value;
					}else
					{
						if (minValue > value) {
							minValue = value;
						}	
					}
					labelArray.add(label);
					healthListIteration = healthListIteration +1;
				}			
			}
		 }else
		 {
			 for (Map.Entry<Date, Object> entry : healthValue1.entrySet()) {
					healthData.add(entry.getValue());
					
					label = new SimpleDateFormat("EEE").format(entry.getKey());
					value = (double) entry.getValue();
					
					if(healthListIteration == 0)
					{
						minValue = value;
					}else
					{
						if (minValue > value) {
							minValue = value;
						}	
					}
					labelArray.add(label);
					healthListIteration = healthListIteration +1;
				}			
		 }
		
		stepValues.put(ResourceManager.getProperty(DATA_LABEL), healthData);
		stepValues.put(ResourceManager.getProperty(LABEL), labelArray);
		if(minValue == null)
		{
			minValue = (double) 0;
			stepValues.put(ResourceManager.getProperty(MINVALUE_LABEL), minValue);
		}else
		{
			stepValues.put(ResourceManager.getProperty(MINVALUE_LABEL), minValue);	
		}
			
		return convertDataToUnit(stepValues, widgetDTO, selectedunit);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> convertDataToUnit(Map<String, Object> stepValues, WidgetDTO widgetDTO, String currentUnit) throws MangoMirrorException
	   {
		List healthData = (List) stepValues.get(ResourceManager.getProperty(DATA_LABEL));
		List<String> labelArray = (List<String>) stepValues.get(ResourceManager.getProperty(LABEL));
		double goalValue =  (double) stepValues.get(ResourceManager.getProperty(GOALVALUE_LABEL));
		double maxValue =   (double) stepValues.get(ResourceManager.getProperty(MAXVALUE_LABEL));
		double minValue =   (double) stepValues.get(ResourceManager.getProperty(MINVALUE_LABEL));
		double updatedGoalValue= 0;
		double updatedMaxValue = 0;
		double updatedMinValue = 0;
		
		Map<String, Object> data = new HashMap<>();
		List updatedHealthData = new ArrayList();
		int counter = 0;
		Boolean conversionNeeded = false;
		String conversionUnit = null;

		if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT))
				&& widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CARB))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FIBER))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_PROTEIN))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR))) {
			
			for (counter = 0; counter < healthData.size(); counter++) {
				Double healthValue = (Double) healthData.get(counter);
				//if current value is > 1000 gram
				if (healthValue >= 1000) {
					conversionNeeded = true;
					break;
				} else {
					conversionNeeded = false;
					conversionUnit = ResourceManager.getProperty(GRAM_UNIT);
				}
			}
			if (conversionNeeded) {
				for (counter = 0; counter < healthData.size(); counter++) {
					Double healthValue = (Double) healthData.get(counter);
					// Convert gm to kg
					if(currentUnit == null){
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue/1000)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue/1000));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue/1000));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue/1000));
						
						conversionUnit = ResourceManager.getProperty(KILOGRAM_UNIT);
					}else{
						//throw exception;
					}
				}
			}else
			{
				updatedHealthData = healthData;
				updatedGoalValue = goalValue;
				updatedMaxValue = maxValue;
				updatedMinValue = minValue;
			}

		} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_SODIUM))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL))) 
		{
			for (counter = 0; counter < healthData.size(); counter++) {
				Double healthValue = (Double) healthData.get(counter);
				//if current value is > 1000 mg 
				if (healthValue >= 1000) {
					conversionNeeded = true;
					break;
				} else {
					conversionNeeded = false;
					conversionUnit = ResourceManager.getProperty(MG_UNIT);
				}
			}

			if (conversionNeeded) {
				for (counter = 0; counter < healthData.size(); counter++) {
					Double healthValue = (Double) healthData.get(counter);
					// Convert mg to gm
					if(currentUnit == null){
						
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue/1000)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue/1000));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue/1000));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue/1000));
						conversionUnit = ResourceManager.getProperty(GRAM_UNIT);
					}
				}
			}else
			{
				updatedHealthData = healthData;
				updatedGoalValue = goalValue;
				updatedMaxValue = maxValue;
				updatedMinValue = minValue;
			}
			
		} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))) {
			for (counter = 0; counter < healthData.size(); counter++) {
				Double healthValue = (Double) healthData.get(counter);
				//if current value is > 1 Km or 1 mile
				if (healthValue >= 1) {
					conversionNeeded = false;
					break;
				} else {
					conversionNeeded = true;
				}
			}
			if (conversionNeeded) {
				for (counter = 0; counter < healthData.size(); counter++) {
					Double healthValue = (Double) healthData.get(counter);
					// Convert Km to meter or mile to yard
					if(currentUnit.equals(ResourceManager.getProperty(HEALTH_DISTANCE_KILOMETER))){
						
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue*1000)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue*1000));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue*1000));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue*1000));
						conversionUnit = ResourceManager.getProperty(METER_UNIT);
					}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_DISTANCE_MILES))){
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue*1760)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue*1760));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue*1760));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue*1760));
						conversionUnit = ResourceManager.getProperty(YARDS_UNIT);
					}
				}
			}else
			{
				updatedHealthData = healthData;
				updatedGoalValue = goalValue;
				updatedMaxValue = maxValue;
				updatedMinValue = minValue;
				if(currentUnit.equals(ResourceManager.getProperty(HEALTH_DISTANCE_KILOMETER))){
					conversionUnit = ResourceManager.getProperty(KILOMETER_UNIT);
				}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_DISTANCE_MILES))){
					conversionUnit = ResourceManager.getProperty(MILES_UNIT);
				}
			}
			
		} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))) {
			
			for (counter = 0; counter < healthData.size(); counter++) {
				Double healthValue = (Double) healthData.get(counter);
				//if current value is > 1000 ml or floz
				
				if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_MILLILITER)))
				{
					if (healthValue >= 1000) {
						conversionNeeded = true;
						break;
					} else {
						conversionNeeded = false;
						conversionUnit = ResourceManager.getProperty(MILILITER_UNIT);
					}	
				}else if (currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ)))
				{
					if (healthValue >= 128) {
						conversionNeeded = true;
						break;
					} else {
						conversionNeeded = false;
						conversionUnit = ResourceManager.getProperty(FLOZ_UNIT);
					}
				}
			}
			
			if (conversionNeeded) {
				for (counter = 0; counter < healthData.size(); counter++) {
					double healthValue = (Double) healthData.get(counter);
					// Convert Km to meter or mile to yard
					if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_MILLILITER))){
						
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue/1000)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue/1000));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue/1000));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue/1000));
						conversionUnit = ResourceManager.getProperty(LITER_UNIT);
					}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ))){
						

						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue/128)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue/128));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue/128));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue/128));
						conversionUnit = ResourceManager.getProperty(GALLONS_UNIT);
					}
				}
			}else
			{
				updatedHealthData = healthData;
				updatedGoalValue = goalValue;
				updatedMaxValue = maxValue;
				updatedMinValue = minValue;
				if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_MILLILITER))){
					conversionUnit = ResourceManager.getProperty(MILILITER_UNIT);
				}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ))){
					conversionUnit = ResourceManager.getProperty(FLOZ_UNIT);
				}
			}

		} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
			
			for (counter = 0; counter < healthData.size(); counter++) {
				Double healthValue = (Double) healthData.get(counter);
				//if current value is > 1 kg or floz
				
				if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WEIGHT_KILOGRAM)))
				{
					if (healthValue > 1) {
						conversionNeeded = false;
						break;
					} else {
						conversionNeeded = true;
					}	
				}
			}
			if (conversionNeeded) {
				for (counter = 0; counter < healthData.size(); counter++) {
					Double healthValue = (Double) healthData.get(counter);
					// Convert Km to meter or mile to yard
					if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WEIGHT_KILOGRAM))){
						

						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue*1000)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue*1000));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue*1000));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue*1000));
						
						conversionUnit = ResourceManager.getProperty(GRAM_UNIT);
					}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WEIGHT_POUND))){

						
						updatedHealthData.add(Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), healthValue*16)));
						updatedGoalValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), goalValue*16));
						updatedMaxValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), maxValue*16));
						updatedMinValue = Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), minValue*16));
						conversionUnit = ResourceManager.getProperty(OUNCES_UNIT);
					}
				}
			}else
			{
				if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WEIGHT_KILOGRAM))){
					conversionUnit = ResourceManager.getProperty(KILOGRAM_UNIT);
				}else if(currentUnit.equals(ResourceManager.getProperty(HEALTH_WEIGHT_POUND))){
					conversionUnit = ResourceManager.getProperty(POUND_UNIT);
				}
				updatedHealthData = healthData;
				updatedGoalValue = goalValue;
				updatedMaxValue = maxValue;
				updatedMinValue = minValue;
			}

		}else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_STEP))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_HEART_RATE))
				||widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
				|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED)))
		{
			updatedHealthData = healthData;
			updatedGoalValue = goalValue;
			updatedMaxValue = maxValue;
			updatedMinValue = minValue;
			
			if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_STEP))) {
				conversionUnit = ResourceManager.getProperty(STEPS_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))) {
				conversionUnit = ResourceManager.getProperty(FLIGHTCLIMB_UNIT);
			}else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI))) {
				conversionUnit = ResourceManager.getProperty(BMI_UNIT);
			} 
			else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))) {
				conversionUnit = ResourceManager.getProperty(CALORIES_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))) {
					conversionUnit = ResourceManager.getProperty(BODYWEIGHT_FAT_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))) {
				conversionUnit = ResourceManager.getProperty(MMHG_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))) {
				conversionUnit = ResourceManager.getProperty(VITAL_BLOOD_GLUCOSE_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_HEART_RATE))) {
				conversionUnit = ResourceManager.getProperty(VITAL_HEART_RATE_UNIT);
			} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
					|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))) {
				conversionUnit = ResourceManager.getProperty(SLEEP_UNIT);
			}
			// throw exception of unknown category
		}
		
		data.put(ResourceManager.getProperty(DATA_LABEL), updatedHealthData);
		data.put("unit", conversionUnit);
		data.put(ResourceManager.getProperty(GOALVALUE_LABEL), updatedGoalValue);
		data.put(ResourceManager.getProperty(MAXVALUE_LABEL), updatedMaxValue);
		data.put(ResourceManager.getProperty(MINVALUE_LABEL), updatedMinValue);
		data.put(ResourceManager.getProperty(LABEL), labelArray);
		return data;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<String, List> getHealthKitData(int userId, int userCurrentTimeZone) throws MangoMirrorException {

		Date date;
		HashMap stepValues = new HashMap();
		/* create one map of last 7 days with default value "0" */
		Map<Date, BigDecimal> stepValue = new HashMap<>();

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.MINUTE, userCurrentTimeZone);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String startDate = sdf.format(cal.getTime());

		try
		{
			BigDecimal defaultValue = new BigDecimal(0);
			for (int i = 0; i < 7; i++) {
				cal.setTime(new Date());
				cal.add(Calendar.DATE, -i);
				String datevalue = sdf.format(cal.getTime());
				try {
					date = sdf.parse(datevalue);
					stepValue.put(date, defaultValue);
				} catch (ParseException e) {
					logger.info(e);
				}
			}

			/* sort map by date */
			Map<Date, Double> stepvalue1 = new TreeMap(stepValue);
			List<String> labelArray = new ArrayList<>();
			List stepData = new ArrayList();

			/* fetch step data of specific user */
			List<UserStepModel> userStepModels = widgetDao.getStepData(userId, startDate, userCurrentTimeZone);

			/*
			 * itterate the stepdata and check if that data is available in map or
			 * not if exist then replace default value
			 */
			for (UserStepModel user : userStepModels) {
				String time = user.getSteptime();
				try {
					Date date2 = sdf.parse(time);
					if (stepvalue1.containsKey(date2)) {
						stepvalue1.put(date2, user.getTotalSteps());
					}
				} catch (ParseException e) {
					logger.info(e);
				}
			}

			/* change the format of date field like "MON,TUE" */
			for (Map.Entry<Date, Double> entry : stepvalue1.entrySet()) {
				stepData.add(entry.getValue());
				String label = new SimpleDateFormat("EEE").format(entry.getKey());
				labelArray.add(label);
			}
			stepValues.put(ResourceManager.getProperty(DATA_LABEL), stepData);
			stepValues.put(ResourceManager.getProperty(LABEL), labelArray);	
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return stepValues;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void saveCalendarData(HttpServletRequest request, List<UserCalendarModel> userCalendarModelList,
			int timeZone, String version)throws MangoMirrorException{
		
		UserModel userModel = (UserModel) request.getAttribute(USER);
		List<UserCalendarDTO> userCalendarList = UserCalenderDozerHelper.map(dozerMapper, userCalendarModelList,UserCalendarDTO.class);
		try
		{
			userDao.updateUserTimezone(userModel.getId(), timeZone);
			userDao.updateClockTimezone(userModel.getId(), timeZone);
			widgetDao.deleteCalenderData(userModel.getId());
			widgetDao.saveCalenderData(userModel.getId(), userCalendarList);
			SocketResponce socketResponce = new SocketResponce();
			socketResponce.setType("refreshCalenderData");
			socketResponce.setMessage("refreshCalenderData");
			ObjectMapper objectMapper = new ObjectMapper();

			String linkedUser = ResourceManager.getProperty(USER_LINKED);
			String generalUser = ResourceManager.getProperty(USER_GENERAL);
			int mirrorStatus = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			HashMap event = new HashMap();
			List<UserMirrorDTO> userMirrorDTOList1 = userMirrorDao.getUserMirrorList(userModel.getId(), generalUser,
					linkedUser, mirrorStatus, userStatusFlag);
			Map<Integer, Integer> mirrorIdList = new HashMap<>();
			if (!userMirrorDTOList1.isEmpty()) {
				for (UserMirrorDTO userMirrorDTO : userMirrorDTOList1) {
					mirrorIdList.put(userMirrorDTO.getMirror().getId(), userMirrorDTO.getMirror().getId());
				}
			}
			JSONArray array = new JSONArray();
			List<UserMirrorDTO> userMirrorDTOList = userMirrorDao.getConnectedUser(userModel.getId());
			if (!userMirrorDTOList.isEmpty()) {
				for (UserMirrorDTO userMirrorDTO : userMirrorDTOList) {
					if (userMirrorDTO.getMirror().getMirrorPreview() == false) {
						if(userMirrorDTO.getCalendarFlag() || userMirrorDTO.getReminderFlag())
						{
							Map<String, List<Map<String, Object>>> events = getCalenderData(userModel.getId(), userMirrorDTO);
							event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
							Map<String, String> calendarTitle = new HashMap<String, String>();
							calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
							calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO.getCalendarFormat().getLabel());
							List<Map<String, String>> title = new ArrayList<>();
							title.add(calendarTitle);
							event.put(ResourceManager.getProperty(TITLE_LABEL), title);
							socketResponce.setData(objectMapper.writeValueAsString(event));
							JSONObject jsonObject = new JSONObject();
							jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
						}
						int id = mirrorIdList.get(userMirrorDTO.getMirror().getId());
						mirrorIdList.remove(id);
					}else
					{
						UserMirrorDTO userMirrorDTO1 = userMirrorDao.getOwnerDetail(userMirrorDTO.getMirror().getId());
						if (userMirrorDTO1.getUser().getId() == userMirrorDTO.getUser().getId()) {
							if(userMirrorDTO1.getCalendarFlag() || userMirrorDTO1.getReminderFlag())
							{
								Map<String, List<Map<String, Object>>> events = getCalenderData(userModel.getId(), userMirrorDTO1);
								event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
								Map<String, String> calendarTitle = new HashMap<>();
								calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO1.getCalendarFormat().getId()));
								calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO1.getCalendarFormat().getLabel());
								List<Map<String, String>> title = new ArrayList<>();
								title.add(calendarTitle);
								event.put(ResourceManager.getProperty(TITLE_LABEL), title);
								socketResponce.setData(objectMapper.writeValueAsString(event));
								JSONObject jsonObject = new JSONObject();
								jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO1.getMirror().getDeviceId());
								jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
								array.put(jsonObject);
							}
							int id = mirrorIdList.get(userMirrorDTO1.getMirror().getId());
							mirrorIdList.remove(id);	
					}
				  }
				}
			}
			for (Map.Entry<Integer, Integer> entry : mirrorIdList.entrySet()) {
				
					UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(entry.getValue());
					if (userMirrorDTO != null) {
						
					if(userMirrorDTO.getUser().getId() == userModel.getId())
					{
						if(userMirrorDTO.getCalendarFlag() || userMirrorDTO.getReminderFlag())
						{
							Map<String, List<Map<String, Object>>> events = getCalenderData(userModel.getId(), userMirrorDTO);
							event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
							Map<String, String> calendarTitle = new HashMap<>();
							calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
							calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO.getCalendarFormat().getLabel());
							List<Map<String, String>> title = new ArrayList<>();
							title.add(calendarTitle);
							event.put(ResourceManager.getProperty(TITLE_LABEL), title);
							socketResponce.setData(objectMapper.writeValueAsString(event));
							JSONObject jsonObject = new JSONObject();
							jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);	
						}
					}
				}
			}
			if(array.length()>0)
			{
				socketService.broadcast(array.toString());
	        }	
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@Override
	public List<CalendarFormatModel> calendarFormatList()throws MangoMirrorException{
		List<CalendarFormatDTO> calendarFormatData = widgetDao.calendarFormatList();
		List<CalendarFormatModel> calendarFormatList = null;
		try
		{
			if(!calendarFormatData.isEmpty())
			{
				calendarFormatList = CalendarFormatDozerHelper.map(dozerMapper, calendarFormatData,
						CalendarFormatModel.class);	
			}	
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		
		return calendarFormatList;
	}

	@Override
	public LocationDTO addWeatherData(String latitude, String longitude, String locationName)throws MangoMirrorException {

		Date date = new Date();
		long currentTime = date.getTime();
		Timestamp updatedCurrentTime = new Timestamp(currentTime);
		ForecastIO weatherData = getWeatherInfo(latitude, longitude);
		String timeZone = weatherData.getTimezone();
		LocationDTO locationDTO = new LocationDTO();
			try
			{
				locationDTO.setLatitude(latitude);
				locationDTO.setLongitude(longitude);
				locationDTO.setLastAccessTime(updatedCurrentTime);
				locationDTO.setLocationName(locationName);
				locationDTO.setTimeZone(timeZone);
				locationDTO = widgetDao.saveLocationData(locationDTO);
			
				JSONObject dailyTemperature = new JSONObject(weatherData.getDaily().toString());
				JSONObject hourleyTemperature = new JSONObject(weatherData.getHourly().toString());
				saveWeatherData(dailyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
				saveWeatherData(hourleyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
			}catch(Exception exception)
			{
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
			return locationDTO;
	}

	public void saveWeatherData(JSONObject dailyTemperature, LocationDTO locationDTO, String weatherType) {
		JSONArray weeklyJsonArray = dailyTemperature.getJSONArray(ResourceManager.getProperty(DATA_LABEL));
		int minTemperature = 0;
		int maxTemperature = 0;
		int temperature = 0;

		List<WeatherDataDTO> weatherDataDtoList = new ArrayList<>();
		for (int i = 0; i < weeklyJsonArray.length(); i++) {
			WeatherDataDTO weatherDataDTO = new WeatherDataDTO();
			weatherDataDTO.setLocation(locationDTO);
			if (weatherType.equals(ResourceManager.getProperty(WIDGET_DAILY_WEATHER))) {
				Object minTemp = weeklyJsonArray.getJSONObject(i).get("temperatureMin");
				Object maxTemp = weeklyJsonArray.getJSONObject(i).get("temperatureMax");
				if (minTemp instanceof Double) {
					minTemperature = (int) Math.round((Double) minTemp);
				} else {
					if (minTemp instanceof Integer) {
						minTemperature = (int) minTemp;
					}
				}
				if (maxTemp instanceof Double) {
					maxTemperature = (int) Math.round((Double) maxTemp);
				} else {
					if (maxTemp instanceof Integer) {
						maxTemperature = (int) maxTemp;
					}
				}
				weatherDataDTO.setMaxTemperature(maxTemperature);
				weatherDataDTO.setMinTemperature(minTemperature);
			} else {
				Object currTemp = weeklyJsonArray.getJSONObject(i).get(ResourceManager.getProperty(TEMPERATURE_LABEL));

				if (currTemp instanceof Double) {
					temperature = (int) Math.round((Double) currTemp);
				} else {
					if (currTemp instanceof Integer) {
						temperature = (int) currTemp;
					}
				}
				weatherDataDTO.setCurrentTemperature(temperature);
			}
			weatherDataDTO.setWeatherType(weatherType);
			Integer timeInSeconds = (Integer) weeklyJsonArray.getJSONObject(i).get("time");
			DateFormat formatter = new SimpleDateFormat(CalendarUtcDateTime.UTC_24HOUR_DATE_FORMAT_SECOND);
			long timeInMilliSeconds = Long.parseLong(Integer.toString(timeInSeconds) + "000");

			TimeZone tx = TimeZone.getTimeZone(locationDTO.getTimeZone());
			Calendar calendar = Calendar.getInstance(tx);
			formatter.setTimeZone(tx);
			calendar.setTimeInMillis(timeInMilliSeconds);
			String weatherDescription = (String) weeklyJsonArray.getJSONObject(i).get("summary");
			String tempIcon = (String) weeklyJsonArray.getJSONObject(i).get("icon");
			weatherDataDTO.setIcon(tempIcon);
			weatherDataDTO.setDescription(weatherDescription);
			weatherDataDTO.setWeatherTime(formatter.format(calendar.getTime()));
			weatherDataDtoList.add(weatherDataDTO);
		}
		widgetDao.saveWeatherDataList(weatherDataDtoList);
	}

	@Override
	public Map<String, Object> getHourlyWeatherData(UserMirrorDTO userMirrorDTO, String iconFormat,
			String weatherType) {

		ArrayList<Map<String, Object>> weatherDataList = new ArrayList<>();
		Map<String, Object> weatherData = new HashMap<>();
		ArrayList<String> dayDescription = new ArrayList<>();
		dayDescription.add("morn");
		dayDescription.add("aft");
		dayDescription.add("eve");
		dayDescription.add("night");

		TimeZone tx = TimeZone.getTimeZone(userMirrorDTO.getLocation().getTimeZone());
		Calendar cal = Calendar.getInstance(tx);
		Calendar cal1 = Calendar.getInstance(tx);
		Calendar cal2 = Calendar.getInstance(tx);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(tx);

		Date date1 = cal.getTime();
		sdf.applyPattern("HH");
		int hours = Integer.parseInt(sdf.format(date1));
		ArrayList<String> dates = new ArrayList<>();

		cal1.add(Calendar.DATE, 1);
		cal2.add(Calendar.DATE, 2);
		sdf.applyPattern("yyyy-MM-dd");
		int flag = 0;

		if (hours >= 4 && hours < 12) {
			if (Integer.toString(hours).length() <= 1) {
				dates.add((sdf.format(cal.getTime()) + " 0" + hours).toString());
			} else {
				dates.add((sdf.format(cal.getTime()) + " " + hours).toString());
			}
			dates.add((sdf.format(cal.getTime()) + " " + "12").toString());
			dates.add((sdf.format(cal.getTime()) + " " + "18").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "00").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "06").toString());
			flag = 0;
		} else if (hours >= 12 && hours < 17) {
			if (Integer.toString(hours).length() <= 1) {
				dates.add((sdf.format(cal.getTime()) + " 0" + hours).toString());
			} else {
				dates.add((sdf.format(cal.getTime()) + " " + hours).toString());
			}
			dates.add((sdf.format(cal.getTime()) + " " + "18").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "00").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "06").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "12").toString());
			flag = 1;
		} else if (hours >= 17 && hours < 20) {
			if (Integer.toString(hours).length() <= 1) {
				dates.add((sdf.format(cal.getTime()) + " 0" + hours).toString());
			} else {
				dates.add((sdf.format(cal.getTime()) + " " + hours).toString());
			}
			dates.add((sdf.format(cal1.getTime()) + " " + "00").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "06").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "12").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "18").toString());
			flag = 2;
		} else if (hours >= 20 || hours < 4) {
			
			if(hours>=20)
			{
				dates.add((sdf.format(cal.getTime()) + " " + hours).toString());
			}else if(hours<4)
			{
				if (Integer.toString(hours).length() <= 1) {
					dates.add((sdf.format(cal1.getTime()) + " 0" + hours).toString());
				} else {
					dates.add((sdf.format(cal1.getTime()) + " " + hours).toString());
				}	
			}
			dates.add((sdf.format(cal1.getTime()) + " " + "06").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "12").toString());
			dates.add((sdf.format(cal1.getTime()) + " " + "18").toString());
			dates.add((sdf.format(cal2.getTime()) + " " + "00").toString());
			flag = 3;
		}
		List<WeatherDataDTO> weatherDataDTOList = widgetDao.getHourleyWeatherData(userMirrorDTO.getLocation().getId(),
				dates, userMirrorDTO.getUser().getTemperatureUnit());

		try {
			int index = 0;
			for (WeatherDataDTO weatherDataDTO : weatherDataDTOList) {
				Map<String, Object> data = new HashMap<>();
				data.put(ResourceManager.getProperty(TEMPERATURE_LABEL), weatherDataDTO.getCurrentTemperature());
				data.put("description", weatherDataDTO.getDescription());
				String tempIcon = weatherDataDTO.getIcon();

				if (tempIcon.equals(ResourceManager.getProperty(ICON_RAIN)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_DAY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_NIGHT))||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLOUDY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_DAY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_NIGHT)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_SLEET)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_SNOW)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_WIND)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_FOG))) 
				{
					data.put("icon", s3BasePath + tempIcon + "." + iconFormat);
				} else {
					data.put("icon", s3BasePath + ResourceManager.getProperty(ICON_DEFAULT) + "." + iconFormat);
				}

				if (index == 0) {
					data.put("dayDesc", "now");
				} else {
					if (flag == dayDescription.size()) {
						flag = 0;
					}
					data.put("dayDesc", dayDescription.get(flag));
				}
				index++;
				flag++;
				weatherDataList.add(data);
			}
			weatherData.put("hourlyData", weatherDataList);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw exception;
		}
		return weatherData;
	}

	@Override
	public Map<String, Object> getCurrentWeatherData(UserMirrorDTO userMirrorDTO, String iconFormat) {

		TimeZone tx = TimeZone.getTimeZone(userMirrorDTO.getLocation().getTimeZone());
		Calendar cal = Calendar.getInstance(tx);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(tx);

		Date date = cal.getTime();
		String dailyDate = sdf.format(date);

		sdf.applyPattern("yyyy-MM-dd HH");
		String hourleyDate = sdf.format(date);

		List<WeatherDataDTO> weatherDataDTOList = widgetDao.getCurrentWeatherData(userMirrorDTO.getLocation().getId(),
				dailyDate, hourleyDate, userMirrorDTO.getUser().getTemperatureUnit());
		Map<String, Object> weatherData = new HashMap<>();
		try {
			Map<String, Object> data = new HashMap<>();
			data.put("minTemperature", weatherDataDTOList.get(0).getMinTemperature());
			data.put("maxTemperature", weatherDataDTOList.get(0).getMaxTemperature());
			data.put(ResourceManager.getProperty(TEMPERATURE_LABEL), weatherDataDTOList.get(1).getCurrentTemperature());
			data.put("description", weatherDataDTOList.get(1).getDescription());
			data.put("locationName", userMirrorDTO.getLocation().getLocationName());
			data.put("temperatureFormat", userMirrorDTO.getUser().getTemperatureUnit());
			String tempIcon = weatherDataDTOList.get(1).getIcon();

			if (tempIcon.equals(ResourceManager.getProperty(ICON_RAIN)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_DAY)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_NIGHT))||
					tempIcon.equals(ResourceManager.getProperty(ICON_CLOUDY)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_DAY)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_NIGHT)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_SLEET)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_SNOW)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_WIND)) ||
					tempIcon.equals(ResourceManager.getProperty(ICON_FOG))) 
			{
				data.put("icon", s3BasePath + tempIcon + "." + iconFormat);
			} else {
				data.put("icon", s3BasePath + ResourceManager.getProperty(ICON_DEFAULT) + "." + iconFormat);
			}
			weatherData.put("currentData", data);

		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw exception;
		}
		return weatherData;
	}

	@Override
	public Map<String, Object> getDailyWeatherData(UserMirrorDTO userMirrorDTO, String iconFormat, String weatherType) {

		List<WeatherDataDTO> weatherDataDTOList = widgetDao.getWeatherData(userMirrorDTO.getLocation().getId(),
				weatherType, userMirrorDTO.getUser().getTemperatureUnit());
		ArrayList<Map<String, Object>> weatherDataList = new ArrayList<>();
		Map<String, Object> weatherData = new HashMap<>();
		String time;
		Date parsedTime;

		try {
			for (WeatherDataDTO weatherDataDTO : weatherDataDTOList) {
				Map<String, Object> data = new HashMap<>();
				SimpleDateFormat formatter = new SimpleDateFormat(CalendarUtcDateTime.UTC_24HOUR_DATE_FORMAT_SECOND);
				TimeZone tx = TimeZone.getTimeZone(userMirrorDTO.getLocation().getTimeZone());
				formatter.setTimeZone(tx);
				data.put("minTemperature", weatherDataDTO.getMinTemperature());
				data.put("maxTemperature", weatherDataDTO.getMaxTemperature());
				time = weatherDataDTO.getWeatherTime();
				parsedTime = formatter.parse(time);
				formatter.applyPattern("EEE");
				data.put("day", formatter.format(parsedTime));
				String tempIcon = weatherDataDTO.getIcon();

				if (tempIcon.equals(ResourceManager.getProperty(ICON_RAIN)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_DAY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLEAR_NIGHT))||
						tempIcon.equals(ResourceManager.getProperty(ICON_CLOUDY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_DAY)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_PARTLY_CLOUDY_NIGHT)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_SLEET)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_SNOW)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_WIND)) ||
						tempIcon.equals(ResourceManager.getProperty(ICON_FOG))) 
				{
					data.put("icon", s3BasePath + tempIcon + "." + iconFormat);
				}else {
					data.put("icon", s3BasePath + ResourceManager.getProperty(ICON_DEFAULT) + "." + iconFormat);
				}
				weatherDataList.add(data);
			}
			weatherData.put("dailyData", weatherDataList);

		} catch (Exception exception) {
			logger.info(exception);
			/*throw exception; */
		}
		return weatherData;
	}

	@Override
	public Map<String, String> getQuotesData(UserMirrorDTO userMirrorDTO) {

		Map<String, String> data = new HashMap<>();
		QuotesDTO quotes = widgetDao.getQuotesData(userMirrorDTO.getQuotes().getId());
		data.put(ResourceManager.getProperty(AUTHOR_LABEL), quotes.getAuthor());
		data.put(ResourceManager.getProperty(QUOTES_LABEL), quotes.getQuotes());
		return null;
	}

	@Override
	public List<QuotesCategoryModel> getQuotesCategoryList() {
		List<QuotesCategoryDTO> quotesCategoryDtoList = widgetDao.quotesCategoryList();

		List<QuotesCategoryModel> quotesCategoryList = new ArrayList<>();
		if (!quotesCategoryDtoList.isEmpty()) {
			quotesCategoryList = QuotesCategoryDozerHelper.map(dozerMapper, quotesCategoryDtoList,
					QuotesCategoryModel.class);
		}

		return quotesCategoryList;
	}

	@Override
	public List<QuotesLengthModel> getQuotesCategoryLengthList() {
		List<QuotesLengthDTO> quotesLengthDtoList = widgetDao.quotesCategoryLengthList();
		List<QuotesLengthModel> quotesLengthList = new ArrayList<>();
		if (!quotesLengthDtoList.isEmpty()) {
			quotesLengthList = QuotesLengthDozerHelper.map(dozerMapper, quotesLengthDtoList, QuotesLengthModel.class);
		}
		return quotesLengthList;
	}

	@Override
	public void saveQuotesData() {

		String minlength = "minlength=";
		String maxlength = "maxlength=";
		String category = "category=";
		String key;
		String value;
		List<QuotesDTO> updateQuotesList = new ArrayList<>();
		List<QuotesDTO> addQuotesList = new ArrayList<>();

		try {
			URL categoryUrl = new URL(categoryBasePath);
			HttpURLConnection con = (HttpURLConnection) categoryUrl.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("X-TheySaidSo-Api-Secret", quotesApiKey);
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			JSONObject categoryList = json.getJSONObject("contents").getJSONObject("categories");
			List<QuotesCategoryDTO> categoryDTOList1 = widgetDao.quotesCategoryList();
			List<String> existingKey = new ArrayList<>();
			List<Integer> updateObjectList = new ArrayList<>();

			if (!categoryDTOList1.isEmpty()) {
				for (QuotesCategoryDTO quotesCategoryDTO : categoryDTOList1) {
					if (!categoryList.has(quotesCategoryDTO.getCategoryType())) {
						quotesCategoryDTO.setCategoryStatus(false);
					} else {
						quotesCategoryDTO.setCategoryStatus(true);
						updateObjectList.add(quotesCategoryDTO.getId());
						existingKey.add(quotesCategoryDTO.getCategoryType());
					}
				}
				categoryList.keySet().removeAll(existingKey);
			}
			Iterator<String> iter = categoryList.keys();
			List<QuotesCategoryDTO> categoryDTOList = new ArrayList<>();

			while (iter.hasNext()) {
				key = (String) iter.next();
				value = categoryList.getString(key);
				QuotesCategoryDTO categoryDTO = new QuotesCategoryDTO();

				categoryDTO.setCategoryType(key);
				categoryDTO.setCategoryDesscription(value);
				categoryDTO.setCategoryStatus(true);
				categoryDTOList.add(categoryDTO);
			}
			if(!categoryDTOList.isEmpty())
			{
				widgetDao.saveQuotesCategoryList(categoryDTOList);	
			}
			

			List<QuotesCategoryDTO> categoryDTOList2 = widgetDao.quotesCategoryList();
			List<QuotesLengthDTO> categoryLengthDTOList = widgetDao.quotesCategoryLengthList();

			for (QuotesCategoryDTO quotesCategoryDTO : categoryDTOList2) {
				for (QuotesLengthDTO quotesLengthDTO : categoryLengthDTOList) {
					String quotesPath;
					if (quotesLengthDTO.getMinLength() == 0) {
						quotesPath = quotesBasePath.concat(category).concat(quotesCategoryDTO.getCategoryType());
					} else {
						quotesPath = quotesBasePath.concat(minlength)
								.concat(Integer.toString(quotesLengthDTO.getMinLength())).concat("&").concat(maxlength)
								.concat(Integer.toString(quotesLengthDTO.getMaxLength()).concat("&").concat(category)
										.concat(quotesCategoryDTO.getCategoryType()));
					}

					URL quotesUrl = new URL(quotesPath);
					HttpURLConnection quotesconnection = (HttpURLConnection) quotesUrl.openConnection();
					quotesconnection.setRequestProperty("Accept-Charset", Charset.forName("UTF-8").toString());
					quotesconnection.setRequestMethod("GET");
					quotesconnection.setRequestProperty("X-TheySaidSo-Api-Secret", quotesApiKey);

					logger.info("they said so api called for quotes data");
					logger.info(quotesconnection);

					rd = new BufferedReader(
							new InputStreamReader(quotesconnection.getInputStream(), Charset.forName("UTF-8")));
					jsonText = readAll(rd);
					json = new JSONObject(jsonText);
					String quotes = json.getJSONObject("contents").getString("quote");
					String author = json.getJSONObject("contents").getString(ResourceManager.getProperty(AUTHOR_LABEL));
					QuotesDTO categoryLengthDTO = new QuotesDTO();

					categoryLengthDTO.setQuotesLength(quotesLengthDTO);
					categoryLengthDTO.setQuotesCategory(quotesCategoryDTO);
					categoryLengthDTO.setQuotes(quotes);
					categoryLengthDTO.setAuthor(author);

					if (!existingKey.isEmpty()) {
						if (existingKey.contains(quotesCategoryDTO.getCategoryType())) {
							updateQuotesList.add(categoryLengthDTO);
						} else {
							addQuotesList.add(categoryLengthDTO);
						}
					} else {
						addQuotesList.add(categoryLengthDTO);
					}
				}
			}

			if (!updateQuotesList.isEmpty()) {
				widgetDao.updateQuotesData(updateQuotesList);
			}

			if (!addQuotesList.isEmpty()) {
				widgetDao.saveQuotesData(addQuotesList);
			}

		} catch (Exception exception) {
			logger.info(exception);
			
		}

	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	@Override
	public List<WidgetData> getSocketWidgetSetting(UserModel userModel, WidgetSettingModel widgetSetting,
			String version) throws MangoMirrorException {
		List<WidgetData> newWidgetSetting = new ArrayList<>();
		try {
			/* check whether mirror related info is provide or not in request */
			if (widgetSetting.getUserMirror().getMirror() != null) {
				int mirrorId = widgetSetting.getUserMirror().getMirror().getId();
				if (mirrorId > 0) {
					List<WidgetSettingDTO> oldWidgetSettingData;

					if (widgetSetting.getUserMirror().getUserRole() != null) {
						/*
						 * get usermirror data related with the requested user
						 * with specified userRole on mirror
						 */
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), mirrorId,
								widgetSetting.getUserMirror().getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
						if (userMirrorDTO != null) {

							List<WidgetDTO> widgetList = widgetDao.getNewWidgetList(userMirrorDTO);
							if (!widgetList.isEmpty()) {
								List<MirrorPageDTO> mirrorPageList = widgetDao.getMirrorPage(userMirrorDTO.getId());
								MirrorPageDTO mirrorPageDTO = mirrorPageList.get(0);
/*								addNewWidgets(widgetList, mirrorPageDTO, userMirrorDTO);*/
							}
							
							/*
							 * Get all widgets by speecific version
							 */
							List<WidgetDTO> widgetDtoList = widgetListByVersion(version);

							/*
							 * Get all widgets setting in the database with
							 * respect to the user in the request
							 */
							oldWidgetSettingData = widgetDao.getWidgetSetting(userMirrorDTO, widgetDtoList);
							/*
							 * Calculate widget size and position as per
							 * requested device height and width
							 */
							newWidgetSetting = addSocketWidgetData(oldWidgetSettingData, widgetSetting.getDeviceWidth(),
									widgetSetting.getDeviceHeight(), userMirrorDTO, widgetSetting.getIconFormat(),
									version);
						} else {
							throw new MangoMirrorException(ResourceManager
									.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION, null, NOT_FOUND, null));
						}

					} else {

						throw new MangoMirrorException(
								ResourceManager.getMessage(USER_ROLE_REGISTRATION_EXCEPTION, null, NOT_FOUND, null));
					}

				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, null));
			}
		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		}
		return newWidgetSetting;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<WidgetData> addSocketWidgetData(List<WidgetSettingDTO> widgetSettingData, int deviceWidth, int deviceHeight,
			UserMirrorDTO userMirrorDTO, String iconFormat, String version)throws MangoMirrorException {
		int userId = widgetSettingData.get(0).getUserMirror().getUser().getId();
		double oldDeviceHeight;
		double oldDeviceWidth;
		double minHeight;
		double xPos;
		double yPos;
		double widgetWidth;
		double widgetHeight;
		double newXPos;
		double newYPos;
		double newWidgetWidth;
		double newWidgetHeight;
		double newMinHeight;
		double newMinWidth;
		Boolean pinnedWidgetStatus = false;
		Boolean checkPinnedStatus = false;
		
		WidgetData pinnedWidget = new WidgetData();
		ArrayList groups = new ArrayList();
		List<MirrorPageDTO> mirrorPageDtoList = widgetDao.getMirrorPage(userMirrorDTO.getId());
		LocationDTO locationDTO = userMirrorDTO.getLocation();
		FitBitAccountModel fitBitAccountModel = null;
		if (userMirrorDTO.getFitbitAccount() != null) {
			fitBitAccountModel = dozerMapper.map(userMirrorDTO.getFitbitAccount(), FitBitAccountModel.class);
		}
		UserModel userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
		try
		{
			if (locationDTO != null) {
				checkAndUpdateWeatherData(locationDTO);
			}

			for (MirrorPageDTO mirrorPageDTO : mirrorPageDtoList) {
				Map<String, Object> pages = new HashMap<>();
				pages.put("pageNumber", mirrorPageDTO.getPageNumber());
				pages.put("pageId", mirrorPageDTO.getId());
				ArrayList widgetList = new ArrayList();

				for (WidgetSettingDTO widgetSettingDTO : widgetSettingData) {
					widgetHeight = widgetSettingDTO.getHeight();
					widgetWidth = widgetSettingDTO.getWidth();
					xPos = 1;
					yPos = widgetSettingDTO.getyPos();
					minHeight = widgetSettingDTO.getMinHeight();
					oldDeviceHeight = widgetSettingDTO.getDeviceHeight();
					oldDeviceWidth = widgetSettingDTO.getDeviceWidth();
					newXPos = (xPos * deviceWidth) / oldDeviceWidth;
					newYPos = (yPos * deviceHeight) / oldDeviceHeight;
					newWidgetWidth = (widgetWidth * deviceWidth) / oldDeviceWidth;
					newWidgetHeight = (widgetHeight * deviceHeight) / oldDeviceHeight;
					newMinHeight = (minHeight * deviceHeight) / oldDeviceHeight;
					newMinWidth = deviceWidth;
					newMinWidth = deviceWidth;
					WidgetData widgetData = new WidgetData();

					if (widgetSettingDTO.getMirrorPage().getId() == mirrorPageDTO.getId()) {

						widgetData.setWidgetSettingId(widgetSettingDTO.getId());
						widgetData.setDeviceId(userMirrorDTO.getMirror().getDeviceId());
						widgetData.setContentId(widgetSettingDTO.getWidget().getId());
						widgetData.setContentType(widgetSettingDTO.getWidget().getType());
						widgetData.setHeight(newWidgetHeight);
						widgetData.setWidth(newWidgetWidth);
						widgetData.setxPos(newXPos);
						widgetData.setyPos(newYPos);
						widgetData.setStatus(widgetSettingDTO.getStatus());
						widgetData.setMinHeight(newMinHeight);
						widgetData.setMinWidth(newMinWidth);
						widgetData.setViewType(widgetSettingDTO.getViewType());
						widgetData.setWidgetMasterCategory(widgetSettingDTO.getWidget().getMasterCategory());
						widgetData.setWidgetSubCategory(widgetSettingDTO.getWidget().getSubCategory());
						
						if (!widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
							widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName());	
						}

						widgetData.setGraphType(widgetSettingDTO.getWidget().getGraphType());
						if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CLOCK))) {
							int minutes = userMirrorDTO.getTimeZone();
							int offMilisecond = minutes * 60 * 1000;
							Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
							long time = cal.getTimeInMillis() + offMilisecond;
							SimpleDateFormat sdf = new SimpleDateFormat("HH");
							cal.add(Calendar.MINUTE, minutes);
							sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
							String Hours = sdf.format(cal.getTime());
							int hours = Integer.parseInt(Hours);
							String greeting = "";
							String userName = "";
							Map<String, Object> data = new HashMap<>();
							
							if (hours >= 4 && hours < 12) {
								greeting = greeting.concat(ResourceManager.getProperty(MORNING_WISH));
							}
							if (hours >= 12 && hours < 16) {
								greeting = greeting.concat(ResourceManager.getProperty(AFTERNOON_WISH));
							}
							if (hours >= 16 && hours < 22) {
								greeting = greeting.concat(ResourceManager.getProperty(EVENING_WISH));
							}
							if (hours >= 22 || hours < 4) {
								greeting = greeting.concat(ResourceManager.getProperty(HELLO_WISH));
							}
							if (!userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
								if (userMirrorDTO.getUser().getName() != null) {
									
										File publicKeyFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
										PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
										String emailId = asymmetricCryptography.decryptText(userMirrorDTO.getUser().getEmailId(), publicKey);
										String plainTextKey = kmsUtil.decryptKey(userMirrorDTO.getUser().getEncryptedPlainText(), emailId);
										userName  = kmsUtil.decryptData(plainTextKey, userMirrorDTO.getUser().getName());
										data.put("userName", userName);
								}
							}
							
							data.put("utcMiliSecond", Long.toString(time));
							data.put("greeting", greeting);
							data.put("clockMessageStatus", userMirrorDTO.getClockMessageStatus().toString());
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_TWITTER))) {
							Map<String, Object> data = new HashMap<String, Object>();
							if (userMirrorDTO.getTwitter() != null) {
								data.put("id", Integer.toString(userMirrorDTO.getTwitter().getId()));
								data.put("twitterOauthToken", userMirrorDTO.getTwitter().getTwitterOauthToken());
								data.put("twitterOauthTokenSecret",
										userMirrorDTO.getTwitter().getTwitterOauthTokenSecret());
								data.put(ResourceManager.getProperty(TWITTERCREDENTIALSTATUS_LABEL),
										userMirrorDTO.getTwitter().getTwitterOauthTokenSecret());
								data.put("timeLine", userMirrorDTO.getTwitter().getTimeLine());
								data.put("twitterFollowingUser", userMirrorDTO.getTwitter().getTwitterFollowingUser());
								data.put("screenName", userMirrorDTO.getTwitter().getScreenName());
							}
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_WEATHER))) {
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = getCurrentWeatherData(userMirrorDTO, iconFormat);	
							}
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STEP))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_FAT))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_CALORIES))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DISTANCE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CARB))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_FIBER))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_PROTEIN))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_SODIUM))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_BMI))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_WATER))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WALKINGRUNNING_DISTANCE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_HEART_RATE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
							
							
							List healthData = new ArrayList();
							Object goalValue = null;
							Object maxValue = null;
							Object minValue = null;
							Double multiplier = Double.parseDouble(ResourceManager.getProperty(DEFAULT_MULTIPLIER));
							String unit = null;
							String unitName = null;
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								HealthDataModel healthDataModel = new HealthDataModel();
								HealthDataTimeZoneModel timeZoneModel = new HealthDataTimeZoneModel();

								if (widgetSettingDTO.getWidget().getMasterCategory().equals(ResourceManager.getProperty(FITBIT_MASTERCATEGORY))) {
									if (fitBitAccountModel != null) {
										healthDataModel.setFitbit(fitBitAccountModel);
									} else {
										healthDataModel.setUser(userModel);
									}
								} else {
									healthDataModel.setUser(userModel);
								}

								timeZoneModel.setUseratimeZone(userMirrorDTO.getUser().getUserCurrentTimeZone());
								if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DISTANCE))) {
									if (userMirrorDTO.getUser().getDistanceUnit()
											.equals(ResourceManager.getProperty(HEALTH_DISTANCE_MILES))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(KM_TO_MILES));
										unitName = userMirrorDTO.getUser().getDistanceUnit();
									} else if (userMirrorDTO.getUser().getDistanceUnit()
											.equals(ResourceManager.getProperty(HEALTH_DISTANCE_KILOMETER))) {
										unitName = userMirrorDTO.getUser().getDistanceUnit();
									}
								} else if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
									unitName = userMirrorDTO.getUser().getWeightUnit();
									if (userMirrorDTO.getUser().getWeightUnit()
											.equals(ResourceManager.getProperty(HEALTH_WEIGHT_POUND))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(KG_TO_POUND));
									}
								} else if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WATER))) {
									unitName = userMirrorDTO.getUser().getWaterUnit();
									if (userMirrorDTO.getUser().getWaterUnit()
											.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(ML_TO_FLOZ));
									}
								} 
								List<String> labelArray = new ArrayList<>();
								try {

									Map<String, Object> healthDataValues = getHealthData(healthDataModel, multiplier,
											timeZoneModel, widgetSettingDTO.getWidget(), unitName);
									if (!healthDataValues.isEmpty()) {
										healthData = (List) healthDataValues.get(ResourceManager.getProperty(DATA_LABEL));
										labelArray = (List<String>) healthDataValues.get(ResourceManager.getProperty(LABEL));
										goalValue = healthDataValues.get(ResourceManager.getProperty(GOALVALUE_LABEL));
										maxValue = healthDataValues.get(ResourceManager.getProperty(MAXVALUE_LABEL));
										minValue = healthDataValues.get(ResourceManager.getProperty(MINVALUE_LABEL));
										unit = (String) healthDataValues.get("unit");
									}
								} catch (Exception exception) {
									logger.info(exception);
								}
								
								HashMap datasetObject = new HashMap();
								// barGraph property

								datasetObject.put(ResourceManager.getProperty(KEY_DATA), healthData);
								datasetObject.put(ResourceManager.getProperty(LABEL), "step data");

								if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(46,204,113,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(46,204,113,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(46,204,113,1)");

								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(52,152,219,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(52,152,219,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(52,152,219,1)");

								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(231,76,60,0)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(231,76,60,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(231,76,60,1)");
								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(155,89,182,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(155,89,182,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(155,89,182,1)");
								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(VITAL_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(192,57,43,0)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(192,57,43,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(192,57,43,1)");
								}
								List datasetObjectValue = new ArrayList();
								datasetObjectValue.add(datasetObject);

								/* new code as per new design */

								int divider = 1000;
								String currentDateData = null;

								if(widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE)))
								{
	 								   String totalTime = String.valueOf(healthData.get(6));
									   String hours = totalTime.substring(0,totalTime.indexOf('.'));
								       String minutes = totalTime.substring(totalTime.indexOf('.')+1);
								        if(hours.length() ==1)
								        { 
								        	StringBuilder hourStringBuilder = new StringBuilder();
								        	hours = hourStringBuilder.append("0").append(hours).toString(); 
								        }
								        if(minutes.length() ==1)
								        {  
								        	StringBuilder minStringBuilder = new StringBuilder();
								        	minutes = minStringBuilder.append(minutes).append("0").toString(); 
								        }
								        currentDateData = hours+"h : "+minutes+"m";
								}else
								{
									int index = healthData.size();
									
									if(index > 0)
									{
										if (healthData.get(index-1) instanceof Double) 
										{
											double currentDateValue =  (double) healthData.get(index-1);
											if (currentDateValue >= divider && (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
													|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
													|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR)))) 
												{
												double newCaloriesValue = currentDateValue / 1000;
												int newCaloriesValueInteger = Double.valueOf(newCaloriesValue).intValue();
												if (newCaloriesValue == newCaloriesValueInteger) {
													currentDateData =  Integer.toString(newCaloriesValueInteger);
											    } else {
											    	currentDateData =  Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), (newCaloriesValue))).toString();
											    }
												unit = "k".concat(unit);
											} else {
												int currentDateDataInInteger = Double.valueOf(currentDateValue).intValue();
												if (currentDateValue == currentDateDataInInteger) {
													currentDateData = Integer.toString(currentDateDataInInteger);
											    } else {
											    	currentDateData =  Double.toString(currentDateValue);
											    }
												
											}
										}
									}else
									{
										int currentDateDataInDouble = 0;
									    currentDateData = Double.toString(currentDateDataInDouble);
									}
								}
								
								HashMap<String, Object> data = new HashMap();
								data.put("datasets", datasetObjectValue);
								data.put("labels", labelArray);
								data.put("unit", unit);
								data.put("todaysData", currentDateData);
								data.put(ResourceManager.getProperty(GOALVALUE_LABEL), goalValue);
								data.put(ResourceManager.getProperty(MAXVALUE_LABEL), maxValue);
								data.put(ResourceManager.getProperty(MINVALUE_LABEL), minValue);
								widgetData.setData(data);	
							}
							
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_CALENDAR))) {

							HashMap event = new HashMap();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								Map<String, List<Map<String, Object>>> events = getCalenderData(userId, userMirrorDTO);
								List<CalendarFormatModel> calendarFormatList = calendarFormatList();
								event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
								event.put("calendarFormatList", calendarFormatList);
								Map<String, String> calendarTitle = new HashMap<>();
								calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
								calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO.getCalendarFormat().getLabel());
								List<Map<String, String>> Title = new ArrayList<>();
								Title.add(calendarTitle);
								event.put(ResourceManager.getProperty(TITLE_LABEL), Title);	
							}
							widgetData.setData(event);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST))) {
							
							Map<String, Object> data = new HashMap<>();
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = getDailyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
									
							}
							widgetData.setData(data);
							
						}else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))) {
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = getHourlyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
									
							}
							widgetData.setData(data);
							
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_QUOTES))) {
							Map<String, Object> data = new HashMap<>();
							HashMap quotesData = new HashMap();
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								List<QuotesCategoryModel> quotesCategoryDtoList = getQuotesCategoryList();
								List<QuotesLengthModel> quotesLengthDtoList = getQuotesCategoryLengthList();
								quotesData.put(ResourceManager.getProperty(QUOTES_CATEGORY_LABEL), quotesCategoryDtoList);
								quotesData.put(ResourceManager.getProperty(QUOTES_LENGTH_LABEL), quotesLengthDtoList);
								
								List<QuotesDTO> quotesList = widgetDao.getQuotesByUserMrrrorId(userMirrorDTO.getId());
								if(!quotesList.isEmpty())
								{
									randomGenerator = new Random();
									int index = randomGenerator.nextInt(quotesList.size());
							        QuotesDTO quotesDTO = quotesList.get(index);
									if (quotesDTO != null) {
										data.put(ResourceManager.getProperty(AUTHOR_LABEL), quotesDTO.getAuthor());
										data.put(ResourceManager.getProperty(QUOTES_LABEL), quotesDTO.getQuotes());
										data.put(ResourceManager.getProperty(QUOTES_LENGTH_LABEL), quotesDTO.getQuotesLength().getId());
										data.put(ResourceManager.getProperty(QUOTES_CATEGORY_LABEL), quotesDTO.getQuotesCategory().getId());
									}	
								}
							}
							quotesData.put(ResourceManager.getProperty(QUOTES_LABEL), data);
							widgetData.setData(quotesData);
						}
						
						else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
							
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data.put("name", widgetSettingDTO.getWidget().getDisplayName()+ " : " +widgetSettingDTO.getStickyNotes().get(0).getName());
								data.put("htmlContent", widgetSettingDTO.getStickyNotes().get(0).getData());
								widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName()+ " : " +widgetSettingDTO.getStickyNotes().get(0).getName());
								widgetData.setData(data);	
							}
						}
						
						if(widgetSettingDTO.getPinned())
						{
							pinnedWidget = widgetData;
							pinnedWidgetStatus = true;
							checkPinnedStatus = true;
						}
						widgetList.add(widgetData);
					}
				}
				pages.put("widgets", widgetList);
				pages.put("delay", mirrorPageDTO.getDelay());
				pages.put(ResourceManager.getProperty(PREVIEWSTATUS_LABEL), userMirrorDTO.getMirror().getMirrorPreview());
				pages.put("pinned", pinnedWidgetStatus);
				groups.add(pages);
				pinnedWidgetStatus = false;
			}			
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		
		if(checkPinnedStatus)
		{
			groups = addPinnedWidget(pinnedWidget,groups);
		}
		
		return groups;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ArrayList addPinnedWidget(WidgetData pinnedWidget,ArrayList groups){
			
		for(int i=0;i<groups.size();i++)
		{
			HashMap<String, Object> data =   (HashMap<String, Object>) groups.get(i);
			Boolean checkPinned =  (Boolean) data.get("pinned");
			data.put("pinnedWidegtId", pinnedWidget.getContentId());
			if(!checkPinned)
			{
				ArrayList widget =   (ArrayList) data.get("widgets");
				widget.add(pinnedWidget);				
			}
		}
		return groups;
	}
	
	
	@Override
	public void removeStickyNotes(HttpServletRequest request, StickyNotesRemoveModel stickyNotesRemoveModel,
			String version) throws MangoMirrorException {
		
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		
		UserMirrorModel userMirrorModel = stickyNotesRemoveModel.getUserMirror();
		List<WidgetSettingModel> widgetSettingModels = stickyNotesRemoveModel.getWidgetSetting();
		try {
			if (!widgetSettingModels.isEmpty()) {
				int mirrorId = userMirrorModel.getMirror().getId();
				if (mirrorId > 0) {
					if (userMirrorModel.getUserRole() != null) {
						/* Check user relation with mirror is existed or not */
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), mirrorId,userMirrorModel.getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));

						if (userMirrorDTO != null) {
							List<WidgetSettingDTO> widgetSettingList = WidgetSettingDozerHelper.MapModelToDto(dozerMapper, widgetSettingModels, WidgetSettingDTO.class);
							widgetDao.removeStickyNotes(widgetSettingList);

							UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(mirrorId);
							SocketResponce socketResponce = new SocketResponce();
							ObjectMapper objectMapper = new ObjectMapper();
							
							socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
							socketResponce.setMessage(ResourceManager.getProperty(WIDGET_REFRESH));
							
							JSONArray array = new JSONArray();
							JSONObject jsonObject = new JSONObject();
							Map<String, String> user = new HashMap<>();
							
							if (userMirrorDTO.getMirror().getMirrorPreview()) {
								if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
									user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
									user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO.getUserRole());
									socketResponce.setData(objectMapper.writeValueAsString(user));
									jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
									jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketService.broadcast(array.toString());
								}
							} else {
								if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
									if (userMirrorDTO1 == null) {
										user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
										user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO.getUserRole());
										socketResponce.setData(objectMapper.writeValueAsString(user));
										jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
										jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
										array.put(jsonObject);
										socketService.broadcast(array.toString());
									}
								} else {
									if (userMirrorDTO1 != null && userMirrorDTO1.getUser().getId() == userModel.getId()) {
											user.put(ResourceManager.getProperty(USERID_LABEL), Integer.toString(userMirrorDTO.getUser().getId()));
											user.put(ResourceManager.getProperty(USERROLE_LABEL), userMirrorDTO.getUserRole());
											socketResponce.setData(objectMapper.writeValueAsString(user));
											jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
											jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
											array.put(jsonObject);
											socketService.broadcast(array.toString());
										}
								}
							}
							
						} else {
							throw new MangoMirrorException(ResourceManager.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION,
									null, NOT_FOUND, locale));
						}
					} else {
						throw new MangoMirrorException(
								ResourceManager.getMessage(USER_ROLE_NOT_SPECIFIED_EXCEPTION, null, NOT_FOUND, locale));
					}
				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, locale));
				}
			} 
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		}catch (Exception exception) {
			logger.info(exception);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void checkAndUpdateWeatherData(LocationDTO locationDTO)throws MangoMirrorException {
		
		Date date = new Date();
		long currentTime = date.getTime();
		try
		{
			Timestamp lastAccessTime = locationDTO.getLastAccessTime();
			SimpleDateFormat sdf = new SimpleDateFormat("dd");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			if (currentTime - lastAccessTime.getTime() > 300000
					|| Integer.parseInt(sdf.format(date)) > lastAccessTime.getDate()) {
				Timestamp updatedCurrentTime = new Timestamp(currentTime);
				ForecastIO weatherData = getWeatherInfo(locationDTO.getLatitude(), locationDTO.getLongitude());
				if(weatherData.getRawResponse()!=null)
				{
					JSONObject dailyTemperature = new JSONObject(weatherData.getDaily().toString());
					JSONObject hourleyTemperature = new JSONObject(weatherData.getHourly().toString());
					widgetDao.deleteWeatherData(locationDTO.getId());
					locationDTO.setLastAccessTime(updatedCurrentTime);
					saveWeatherData(dailyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
					saveWeatherData(hourleyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));	
				}
			}
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
    }
}