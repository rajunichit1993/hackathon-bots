package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BLE_IN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BLE_OUT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_MISSING_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EMAIL_LOG_SUBJECT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EMAIL_SUBJECT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.INVALID_USER_TOKEN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_BLEIN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_BLEOUT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_CHANGE_PASSWORD;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_EDIT_PROFILE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_LOGIN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_RECOVER_PASSWORD;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_SIGNUP;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PARSING_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PASSWORD_AND_CONFIRMATION_PASSWORD_DOES_NOT_MATCH_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PASSWORD_LENGTH_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PASSWORD_MISMATCH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_ALREADY_EXIST_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_GENERAL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_LINKED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_NOT_REGISTERED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_OWNER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_24HOUR_WEATHER_FORECAST;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_5DAY_WEATHER_FORECAST;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DAILY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DISTANCE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_HOURLEY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_REFRESH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WATER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WEIGHT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WITHINRANGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WRONG_OLD_PASSWORD_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.REFRESH_HEALTHKIT_SOCKET_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.REFRESH_WEATHER_SOCKET_MESSAGE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_BETA;


import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.log4j.Logger;
import org.aspectj.util.FileUtil;

import javax.servlet.http.HttpServletRequest;
import org.dozer.Mapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.*;
import com.mindbowser.homeDisplay.dto.EmailDTO;
import com.mindbowser.homeDisplay.dto.EmailLogDTO;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.LogDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.BleInDataModel;
import com.mindbowser.homeDisplay.model.EmailLogDataModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetHealthDataModel;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.service.UserService;
import com.mindbowser.homeDisplay.util.ApplicationBeanUtil;
import com.mindbowser.homeDisplay.util.AsymmetricCryptography;
import com.mindbowser.homeDisplay.util.CalendarUtcDateTime;
import com.mindbowser.homeDisplay.util.EmailSender;
import com.mindbowser.homeDisplay.util.KMSUtil;
import com.mindbowser.homeDisplay.util.UserDetailValidation;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.TokenGenerator;

public class UserServiceImpl implements UserService {

	private static Logger logger = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private SmartMirrorDao smartMirrorDao;

	@Autowired
	private WidgetDao widgetDao;

	@Autowired
	private LogDao logDao;
	
	@Autowired
	private BetaUserService betaUserService;

	@Autowired
	private FitBitService fitbitService;

	@Autowired
	private UserMirrorDao userMirrorDao;

	@Autowired
	private WidgetService widgetService;

	@Autowired
	private KMSUtil kmsUtil;

	@Autowired
	private AsymmetricCryptography asymmetricCryptography;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	private SocketService socketService;

	@Value("${log.cc.mail}")
	private String logEmailCC;

	@Value("${log.mail}")
	private String logEmailTo;

	@Value("${email.filepath}")
	private String emailFilePath;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setDozerMapper(Mapper dozerMapper) {
		this.dozerMapper = dozerMapper;
	}

	/**
	 * This method is used to fetch user related data using authentication token
	 * 
	 * @param userModel
	 * @param request
	 * @throws UserException
	 */
	@Override
	public UserModel getUserByAuthToken(String authToken) throws MangoMirrorException {
		UserModel userModel;
		UserDTO userDtos = userDao.getUserByAuthToken(authToken);
		if (userDtos != null) {
			userModel = dozerMapper.map(userDtos, UserModel.class);
		} else {
			logger.error(ResourceManager.getMessage(INVALID_USER_TOKEN, null, NOT_FOUND, null));
			throw new MangoMirrorException(ResourceManager.getMessage(INVALID_USER_TOKEN, null, NOT_FOUND, null));
		}

		return userModel;
	}

	/**
	 * This method saves user details in database
	 * 
	 * @param userModel
	 * @param request
	 * @throws UserException
	 */
	@Override
	public UserModel saveUser(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {

		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		EmailDTO emailDTO = new EmailDTO();
		UserModel userModel2 = null;
		String authToken = null;
		try {
			UserDTO userDTO = dozerMapper.map(userModel, UserDTO.class);
			// To check whether the email ID is already exist or not.

			if (userModel.getEmailId() == null || userModel.getName() == null || userModel.getGender() == null
					|| userModel.getPassword() == null) {
				// data not proper
				logger.error(ResourceManager.getMessage(DATA_MISSING_EXCEPTION, null, NOT_FOUND, locale));
				throw new MangoMirrorException(
						ResourceManager.getMessage(DATA_MISSING_EXCEPTION, null, NOT_FOUND, locale));
			}

			if (userModel.getPassword().length() <= 5 || userModel.getPassword().length() >= 20) {
				logger.error(ResourceManager.getMessage(USER_NOT_REGISTERED, null, NOT_FOUND, locale));
				throw new MangoMirrorException(
						ResourceManager.getMessage(PASSWORD_LENGTH_EXCEPTION, null, NOT_FOUND, locale));
			} else {
				if (!userModel.getPassword().equals(userModel.getConfirmPassword())) {
					logger.error(ResourceManager.getMessage(USER_NOT_REGISTERED, null, NOT_FOUND, locale));
					throw new MangoMirrorException(ResourceManager.getMessage(
							PASSWORD_AND_CONFIRMATION_PASSWORD_DOES_NOT_MATCH_EXCEPTION, null, NOT_FOUND, locale));
				}
			}

			UserDetailValidation userValidation = new UserDetailValidation();
			userValidation.validateEmail(userModel.getEmailId());
			userValidation.validateName(userModel.getName());

			// To save updated time
			String logTime = CalendarUtcDateTime.getUtcDateTime();

			File privatefilepath = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privatefilepath);
			String encrypted_EmailId = asymmetricCryptography.encryptText(userModel.getEmailId(), privateKey);

			UserDTO userDTO2 = userDao.checkUser(encrypted_EmailId);
			if (userDTO2 != null) {
				// already exist
				logger.error(ResourceManager.getMessage(USER_ALREADY_EXIST_EXCEPTION, null, NOT_FOUND, locale));
				throw new MangoMirrorException(
						ResourceManager.getMessage(USER_ALREADY_EXIST_EXCEPTION, null, NOT_FOUND, locale));

			}

			userDTO.setEmailId(encrypted_EmailId);

			authToken = TokenGenerator.generateToken(userModel.getEmailId());
			userDTO.setAuthToken(authToken);

			// To save update Time
			userDTO.setUpdatedAt(logTime);

			// To save created Time
			userDTO.setCreatedAt(logTime);
			String encryptedPlainTextKey;
			String plainTextDataKey;

			if (userDTO.getEncryptedPlainText() == null) {
				encryptedPlainTextKey = kmsUtil.getEncryptedDataKey(userModel.getEmailId());
				userDTO.setEncryptedPlainText(encryptedPlainTextKey);
				plainTextDataKey = kmsUtil.decryptKey(encryptedPlainTextKey, userModel.getEmailId());
			} else {
				plainTextDataKey = kmsUtil.decryptKey(userDTO.getEncryptedPlainText(), userModel.getEmailId());
			}

			String encryptedName = kmsUtil.encryptText(plainTextDataKey, userModel.getName()); 
			userDTO.setName(encryptedName);
			
			String encryptedPassword = kmsUtil.encryptText(plainTextDataKey, userModel.getPassword());
			userDTO.setPassword(encryptedPassword);

			UserDTO userDTO3 = userDao.saveUser(userDTO);
			if (userDTO3 != null) {
				userModel2 = dozerMapper.map(userDTO3, UserModel.class);
				userModel2.setName(userModel.getName());
				userModel2.setEmailId(userModel.getEmailId());
				if(version.equals(ResourceManager.getProperty(VERSION_BETA)))
				{
					betaUserService.saveBetaUserData(userDTO3);
				}
			}
			
			// set data to email dto
			emailDTO.setTo(userModel.getEmailId());
			emailDTO.setPassword(userModel.getPassword());
			emailDTO.setUsername(userModel.getName());
			emailDTO.setSubject(ResourceManager.getProperty(EMAIL_SUBJECT) + " " + userModel.getName());

			try {
				/* send confirmation email to the user */
				EmailSender emailSender = (EmailSender) ApplicationBeanUtil.getApplicationContext().getBean("mail");
				emailSender.sendRegistrationConfirmationMail(emailDTO);
			} catch (Exception ex) {
				logger.error(ex.getStackTrace(), ex);
			}
			LogDTO logDTO = new LogDTO();
			logDTO.setUserId(userDTO3.getId());
			logDTO.setTime(logTime);
			logDTO.setEvent(ResourceManager.getProperty(LOG_USER_SIGNUP));
			logDao.saveLog(logDTO);

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

		userModel2.setPassword(null);
		userModel2.setConfirmPassword(null);
		return userModel2;
	}

	/**
	 * This method checks whether user registered with application or not and if
	 * exist then allows user to login with valid credentials
	 * 
	 * @param request
	 *            containing user credentials
	 * @param userModel
	 *            containing user details.
	 * @return userModel containing user object
	 * @throws UserException
	 */
	@Override
	public UserModel logIn(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {

		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel1 = null;
		String emailId = null;
		String name = null;

		/* check user email id is registered or not */
		try {

			File privatefilepath = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privatefilepath);
			String encrypted_EmailId = asymmetricCryptography.encryptText(userModel.getEmailId(), privateKey);

			UserDTO user = userDao.checkUser(encrypted_EmailId);
			if (user != null) {

				String encryptedDataKey = user.getEncryptedPlainText();
				String plainTextKey = kmsUtil.decryptKey(encryptedDataKey, userModel.getEmailId());

				String decryptedPassword = kmsUtil.decryptData(plainTextKey, user.getPassword());

				if (!decryptedPassword.equals(userModel.getPassword())) {
					throw new MangoMirrorException(
							ResourceManager.getMessage(PASSWORD_MISMATCH, null, "not.found", locale));
				} else {
					if(version.equals(ResourceManager.getProperty(VERSION_BETA)))
					{
						userModel1 = dozerMapper.map(user, UserModel.class);
					}else
					{
						String authToken = TokenGenerator.generateToken(userModel.getEmailId());
						user.setAuthToken(authToken);
						userModel1 = dozerMapper.map(user, UserModel.class);
					}
					
					String authToken = TokenGenerator.generateToken(userModel.getEmailId());
					user.setAuthToken(authToken);
					userModel1 = dozerMapper.map(user, UserModel.class);

					// PublicKey publicKey =
					// asymmetricCryptography.getPublic(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
					File publicKeyFile = new File(System.getProperty("java.io.tmpdir") + "KeyPair/publicKey");
					PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
					emailId = asymmetricCryptography.decryptText(user.getEmailId(), publicKey);
					name = kmsUtil.decryptData(plainTextKey, user.getName());

					String logTime = CalendarUtcDateTime.getUtcDateTime();
					LogDTO logDTO = new LogDTO();
					logDTO.setUserId(user.getId());
					logDTO.setTime(logTime);
					logDTO.setEvent(ResourceManager.getProperty(LOG_USER_LOGIN));
					logDao.saveLog(logDTO);

					userModel1.setName(name);
					userModel1.setEmailId(emailId);

				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(USER_NOT_REGISTERED, null, "not.found", locale));
			}

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

		userModel1.setCreatedAt(null);
		userModel1.setUpdatedAt(null);
		userModel1.setPassword(null);
		userModel1.setConfirmPassword(null);
		userModel1.setEncryptedPlainText(null);
		return userModel1;
	}

	/**
	 * This method sends an mail containing user's password.
	 * 
	 * @param request
	 * @param email
	 * @return message containing password sent to user or not
	 * @throws UserException
	 */
	@Override
	public void forgotPassword(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		int newPasssword = 0;
		String encryptedPassword;
		String plainTextDataKey;
		String emailId;
		EmailDTO emailDTO = new EmailDTO();
		try {

			File privatefilepath = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privatefilepath);
			emailId = asymmetricCryptography.encryptText(userModel.getEmailId(), privateKey);

			UserDTO retrievedUser = userDao.checkUser(emailId);
			if (retrievedUser == null) {
				logger.error(ResourceManager.getMessage(USER_NOT_REGISTERED, null, NOT_FOUND, locale));
				throw new MangoMirrorException(
						ResourceManager.getMessage(USER_NOT_REGISTERED, null, NOT_FOUND, locale));
			}

			newPasssword = (int) ((Math.random() * 9000000) + 1000000);
			plainTextDataKey = kmsUtil.decryptKey(retrievedUser.getEncryptedPlainText(), userModel.getEmailId());
			encryptedPassword = kmsUtil.encryptText(plainTextDataKey, Integer.toString(newPasssword));
			retrievedUser.setPassword(encryptedPassword);

			String userName = kmsUtil.decryptData(plainTextDataKey, retrievedUser.getName()); 
			
			emailDTO.setTo(userModel.getEmailId());
			emailDTO.setEmailAsUserName(emailId);
			emailDTO.setPassword(Integer.toString(newPasssword));
			emailDTO.setUsername(userName);

			// Working code using velocity dependancies
			EmailSender emailSender = (EmailSender) ApplicationBeanUtil.getApplicationContext().getBean("mail");
			emailSender.sendForgotPasswordMail(emailDTO);

			String logTime = CalendarUtcDateTime.getUtcDateTime();

			LogDTO logDTO = new LogDTO();
			logDTO.setUserId(retrievedUser.getId());
			logDTO.setTime(logTime);
			logDTO.setEvent(ResourceManager.getProperty(LOG_USER_RECOVER_PASSWORD));
			logDao.saveLog(logDTO);

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

	}

	/**
	 * This method is used to edit current user's profile.
	 * 
	 * @param request
	 *            containing user credentials
	 * @param userModel
	 *            containing user details.
	 * @return userModel containing user object
	 * @throws UserException
	 */
	@Override
	public UserModel editProfile(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {

		UserModel userModel1 = (UserModel) request.getAttribute(USER);
		UserModel userResponceModel = null;

		try {
			UserDTO userDTO = userDao.getUserById(userModel1.getId());
			if (userDTO != null) {
				if (userModel.getName() != null && !userModel.getName().isEmpty()) {
					
					File publicKeyFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
					PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
					String emailId = asymmetricCryptography.decryptText(userDTO.getEmailId(), publicKey);
					String plainTextDataKey = kmsUtil.decryptKey(userDTO.getEncryptedPlainText(), emailId);
				    String encryptedName = kmsUtil.encryptText(plainTextDataKey, userModel.getName()); 
			    	userDTO.setName(encryptedName);
				}
				if (userModel.getGender() != null) {
					userDTO.setGender(userModel.getGender());
				}
				String logTime = CalendarUtcDateTime.getUtcDateTime();
				userDTO.setUpdatedAt(logTime);
				userResponceModel = dozerMapper.map(userDTO, UserModel.class);

				LogDTO logDTO = new LogDTO();
				logDTO.setUserId(userResponceModel.getId());
				logDTO.setTime(logTime);
				logDTO.setEvent(ResourceManager.getProperty(LOG_USER_EDIT_PROFILE));
				logDao.saveLog(logDTO);

				userResponceModel.setAuthToken(null);
				userResponceModel.setPassword(null);
			} else {
				throw new MangoMirrorException(ResourceManager.getProperty(USER_NOT_REGISTERED));
			}

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

		return userResponceModel;
	}

	@Override
	public void updateUserTimezone(HttpServletRequest request, int timeZone, String version)
			throws MangoMirrorException {
		try {
			UserModel userModel = (UserModel) request.getAttribute(USER);
			if (userModel.getUserCurrentTimeZone() != timeZone) {
				userDao.updateUserTimezone(userModel.getId(), timeZone);
				userDao.updateClockTimezone(userModel.getId(), timeZone);
			}

		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public ArrayList publishWidgetSetting(HttpServletRequest request, List<UserMirrorModel> userMirrorList,
			String rangeType, String version) throws MangoMirrorException {

		UserModel userModel = (UserModel) request.getAttribute(USER);
		SocketResponce socketResponce = new SocketResponce();
		socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
		socketResponce.setMessage(rangeType);
		ObjectMapper objectMapper = new ObjectMapper();
		String general = ResourceManager.getProperty(USER_GENERAL);
		String personal = ResourceManager.getProperty(USER_OWNER);
		String linkedUser = ResourceManager.getProperty(USER_LINKED);
		Map<String, String> userMap = new HashMap<>();
		String sessionStartDate = CalendarUtcDateTime.getUtcDateTimeInMillis();
		JSONArray array = new JSONArray();
		ArrayList socketStatus = null;

		try {
			for (UserMirrorModel userMirrorModel : userMirrorList) {
				SmartMirrorDTO smartMirrorDTO = smartMirrorDao.checkMirror(userMirrorModel.getMirror().getMajor(),
						userMirrorModel.getMirror().getMinor(),
						Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));

				UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorData(userModel.getId(), smartMirrorDTO.getId(),
						personal, linkedUser, Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));

				LogDTO logDTO = new LogDTO();
				logDTO.setUserId(userModel.getId());
				logDTO.setTime(sessionStartDate);
				logDTO.setMirrorId(smartMirrorDTO.getId());

				if (rangeType.equals(ResourceManager.getProperty(WITHINRANGE)) && smartMirrorDTO.getMultiUser()) {
					if (!smartMirrorDTO.getMirrorPreview()) {
						UserMirrorDTO userMirrorDTO1 = userMirrorDao
								.getUserMirrorByBleRangeStatusTime(smartMirrorDTO.getId());
						if (userMirrorDTO1 == null) {
							if (userMirrorDTO != null) {
								userMirrorDTO.setBleRangeStatusTime(sessionStartDate);
								userMirrorDTO.setBleRangeStatus(ResourceManager.getProperty(BLE_IN));
								/*userMirrorDao.updateBleStatus(userMirrorDTO.getId(), ResourceManager.getProperty(BLE_IN), sessionStartDate);*/
								userMap.put("userId", Integer.toString(userModel.getId()));
								userMap.put("userRole", userMirrorDTO.getUserRole());
								socketResponce.setData(objectMapper.writeValueAsString(userMap));
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
								jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
								array.put(jsonObject);
								socketStatus = socketService.broadcast(array.toString());
							}
						} else {
							/*userMirrorDao.updateBleStatus(userMirrorDTO.getId(), ResourceManager.getProperty(BLE_IN), sessionStartDate);*/
							userMirrorDTO.setBleRangeStatus(ResourceManager.getProperty(BLE_IN));
							userMirrorDTO.setBleRangeStatusTime(sessionStartDate);
						}
					} else {
						/*userMirrorDao.updateBleStatus(userMirrorDTO.getId(), ResourceManager.getProperty(BLE_IN), sessionStartDate);*/
						userMirrorDTO.setBleRangeStatus(ResourceManager.getProperty(BLE_IN));
						userMirrorDTO.setBleRangeStatusTime(sessionStartDate);
					}
					logDTO.setEvent(ResourceManager.getProperty(LOG_USER_BLEIN));
				} else {

					if (smartMirrorDTO.getMultiUser()) {
						if (!smartMirrorDTO.getMirrorPreview()) {
							if (userMirrorDTO != null) {
								userMirrorDTO.setBleRangeStatus(ResourceManager.getProperty(BLE_OUT));
								UserMirrorDTO userMirrorDTO1 = userMirrorDao
										.getUserMirrorByBleRangeStatusTime(smartMirrorDTO.getId());
								if (userMirrorDTO1 != null) {
									userMap.put("userId", Integer.toString(userMirrorDTO1.getUser().getId()));
									userMap.put("userRole", userMirrorDTO1.getUserRole());
									socketResponce.setData(objectMapper.writeValueAsString(userMap));
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
									jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketStatus = socketService.broadcast(array.toString());
								} else {
									userMap.put("userId", "");
									userMap.put("userRole", general);
									socketResponce.setData(objectMapper.writeValueAsString(userMap));
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
									jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketStatus = socketService.broadcast(array.toString());
								}
							}
						} else {
							if (userMirrorDTO != null) {
								userMirrorDTO.setBleRangeStatus(ResourceManager.getProperty(BLE_OUT));
							}
						}
					}
					logDTO.setEvent(ResourceManager.getProperty(LOG_USER_BLEOUT));
				}
				logDao.saveLog(logDTO);
			}
		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return socketStatus;
	}

	@Override
	public void changePassword(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {

		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel user = (UserModel) request.getAttribute(USER);
		UserDTO userDTO = userDao.getUserUsingAuthToken(user.getAuthToken());

		try {
			if (userDTO != null) {
				File publicKeyFile = new File(System.getProperty("java.io.tmpdir") + "KeyPair/publicKey");
				PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
				String emailId = asymmetricCryptography.decryptText(userDTO.getEmailId(), publicKey);

				String plainTextDataKey = kmsUtil.decryptKey(userDTO.getEncryptedPlainText(), emailId);
				String decryptedPassword = kmsUtil.decryptData(plainTextDataKey, userDTO.getPassword());

				if (decryptedPassword.equals(userModel.getOldPassword())) {
					if (userModel.getNewPassword().equals(userModel.getConfirmPassword())) {

						String encryptedPassword = kmsUtil.encryptText(plainTextDataKey, userModel.getNewPassword());
						userDTO.setPassword(encryptedPassword);

						String logTime = CalendarUtcDateTime.getUtcDateTime();
						userDTO.setUpdatedAt(logTime);

						LogDTO logDTO = new LogDTO();
						logDTO.setUserId(user.getId());
						logDTO.setTime(logTime);
						logDTO.setEvent(ResourceManager.getProperty(LOG_USER_CHANGE_PASSWORD));
						logDao.saveLog(logDTO);

					} else {
						logger.error(ResourceManager.getMessage(
								PASSWORD_AND_CONFIRMATION_PASSWORD_DOES_NOT_MATCH_EXCEPTION, null, NOT_FOUND, locale));
						throw new MangoMirrorException(ResourceManager.getMessage(
								PASSWORD_AND_CONFIRMATION_PASSWORD_DOES_NOT_MATCH_EXCEPTION, null, NOT_FOUND, locale));
					}
				} else {
					logger.error(ResourceManager.getMessage(WRONG_OLD_PASSWORD_EXCEPTION, null, NOT_FOUND, locale));
					throw new MangoMirrorException(
							ResourceManager.getMessage(WRONG_OLD_PASSWORD_EXCEPTION, null, NOT_FOUND, locale));
				}

			}

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

	}

	@Override
	public void updateUnit(HttpServletRequest request, UserModel userModel, String version)
			throws MangoMirrorException {
		UserModel user = (UserModel) request.getAttribute(USER);
		userModel.setId(user.getId());
		int userUpdateFlag = 0;
		try {
			List<String> widgetTypeList = new ArrayList<>();
			UserModel userModel1 = new UserModel();
			if (user.getDistanceUnit() != userModel.getDistanceUnit()) {
				userModel1.setDistanceUnit(userModel.getDistanceUnit());
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_DISTANCE));
				userUpdateFlag = 1;
			}
			if (user.getWaterUnit() != userModel.getWaterUnit()) {
				userModel1.setWaterUnit(userModel.getWaterUnit());
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_WATER));
				userUpdateFlag = 1;
			}
			if (user.getWeightUnit() != userModel.getWeightUnit()) {
				userModel1.setWeightUnit(userModel.getWeightUnit());
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_WEIGHT));
				userUpdateFlag = 1;
			}
			if (user.getTemperatureUnit() != userModel.getTemperatureUnit()) {
				userModel1.setTemperatureUnit(userModel.getTemperatureUnit());
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST));
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST));
				widgetTypeList.add(ResourceManager.getProperty(WIDGET_WEATHER));
				userUpdateFlag = 1;
			}

			Map<String, List<WidgetHealthDataModel>> data = new HashMap<>();
			int weatherUpdateFlag = 0;
			if (userUpdateFlag == 1) {
				userModel1.setId(user.getId());
				userDao.updateUnit(userModel);
				List<WidgetModel> widgetModels = new ArrayList<>();
				List<WidgetDTO> widgetModelList = widgetDao.getAllWidget(widgetTypeList);

				String linkedUser = ResourceManager.getProperty(USER_LINKED);
				String generalUser = ResourceManager.getProperty(USER_GENERAL);
				int mirrorStatus = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
				int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));

				List<UserMirrorDTO> userMirrorDTOList1 = userMirrorDao.getUserMirrorList(userModel.getId(), generalUser,
						linkedUser, mirrorStatus, userStatusFlag);
				Map<Integer, Integer> mirrorIdList = new HashMap<>();
				if (!userMirrorDTOList1.isEmpty()) {
					for (UserMirrorDTO userMirrorDTO : userMirrorDTOList1) {
						mirrorIdList.put(userMirrorDTO.getMirror().getId(), userMirrorDTO.getMirror().getId());
					}
				}

				List<UserMirrorDTO> userMirrorDTOList = userMirrorDao.getConnectedUser(userModel.getId());
				if (!userMirrorDTOList.isEmpty()) {
					for (UserMirrorDTO userMirrorDTO : userMirrorDTOList) {
						if (!userMirrorDTO.getMirror().getMirrorPreview()) {
							List<WidgetDTO> widgetList = widgetDao.getWidgetSettingByStatusAndWidgetList(userMirrorDTO,
									widgetModelList);
							for (WidgetDTO widgetDTO : widgetList) {
								if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))
										|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))
										|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
									widgetModels.add(dozerMapper.map(widgetDTO, WidgetModel.class));
								} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEATHER))
										|| widgetDTO.getType()
												.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))
										|| widgetDTO.getType()
												.equals(ResourceManager.getProperty(WIDGET_DAILY_WEATHER))) {
									weatherUpdateFlag = 1;
								}
							}

							if (!widgetModels.isEmpty()) {
								ObjectMapper objectMapper = new ObjectMapper();
								JSONArray array = new JSONArray();
								SocketResponce socketResponce = new SocketResponce();
								socketResponce.setType(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
								socketResponce
										.setMessage(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
								List<WidgetHealthDataModel> widgetDataList = fitbitService
										.getHealthDataByUserMirror(userMirrorDTO, widgetModels);
								data.put("data", widgetDataList);
								socketResponce.setData(objectMapper.writeValueAsString(data));
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
								jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
								array.put(jsonObject);
								socketService.broadcast(array.toString());
							}

							if (weatherUpdateFlag == 1) {
								LocationDTO locationDTO = userMirrorDTO.getLocation();
								if (locationDTO != null) {
									widgetService.checkAndUpdateWeatherData(locationDTO);
								}

								String iconFormat = "svg";
								ObjectMapper objectMapper = new ObjectMapper();
								ArrayList<Map<String, Object>> weatherData = new ArrayList<>();

								weatherData.add(widgetService.getCurrentWeatherData(userMirrorDTO, iconFormat));
								weatherData.add(widgetService.getDailyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_DAILY_WEATHER)));
								weatherData.add(widgetService.getHourlyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)));

								SocketResponce socketResponce = new SocketResponce();
								socketResponce.setType(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
								socketResponce.setMessage(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
								socketResponce.setData(objectMapper.writeValueAsString(weatherData));

								JSONArray array = new JSONArray();
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
								jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
								array.put(jsonObject);
								socketService.broadcast(array.toString());
							}
							int id = mirrorIdList.get(userMirrorDTO.getMirror().getId());
							mirrorIdList.remove(id);
						} else {
							UserMirrorDTO userMirrorDTO1 = userMirrorDao
									.getOwnerDetail(userMirrorDTO.getMirror().getId());
							if (userMirrorDTO1.getUser().getId() == userMirrorDTO.getUser().getId()) {

								List<WidgetDTO> widgetList = widgetDao
										.getWidgetSettingByStatusAndWidgetList(userMirrorDTO1, widgetModelList);
								for (WidgetDTO widgetDTO : widgetList) {
									if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))
											|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))
											|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
										widgetModels.add(dozerMapper.map(widgetDTO, WidgetModel.class));
									} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEATHER))
											|| widgetDTO.getType()
													.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))
											|| widgetDTO.getType()
													.equals(ResourceManager.getProperty(WIDGET_DAILY_WEATHER))) {
										weatherUpdateFlag = 1;
									}
								}

								if (!widgetModels.isEmpty()) {
									ObjectMapper objectMapper = new ObjectMapper();
									JSONArray array = new JSONArray();
									SocketResponce socketResponce = new SocketResponce();
									socketResponce
											.setType(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
									socketResponce
											.setMessage(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
									List<WidgetHealthDataModel> widgetDataList = fitbitService
											.getHealthDataByUserMirror(userMirrorDTO1, widgetModels);
									data.put("data", widgetDataList);
									socketResponce.setData(objectMapper.writeValueAsString(data));
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("deviceId", userMirrorDTO1.getMirror().getDeviceId());
									jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketService.broadcast(array.toString());
								}

								if (weatherUpdateFlag == 1) {
									LocationDTO locationDTO = userMirrorDTO1.getLocation();
									if (locationDTO != null) {
										widgetService.checkAndUpdateWeatherData(locationDTO);
									}
									String iconFormat = "svg";

									ObjectMapper objectMapper = new ObjectMapper();
									ArrayList<Map<String, Object>> weatherData = new ArrayList<>();

									weatherData.add(widgetService.getCurrentWeatherData(userMirrorDTO1, iconFormat));
									weatherData.add(widgetService.getDailyWeatherData(userMirrorDTO1, iconFormat,
											ResourceManager.getProperty(WIDGET_DAILY_WEATHER)));
									weatherData.add(widgetService.getHourlyWeatherData(userMirrorDTO1, iconFormat,
											ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)));

									SocketResponce socketResponce = new SocketResponce();
									socketResponce.setType(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
									socketResponce
											.setMessage(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
									socketResponce.setData(objectMapper.writeValueAsString(weatherData));

									JSONArray array = new JSONArray();
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("deviceId", userMirrorDTO1.getMirror().getDeviceId());
									jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									socketService.broadcast(array.toString());
								}
								int id = mirrorIdList.get(userMirrorDTO1.getMirror().getId());
								mirrorIdList.remove(id);

							}
						}
					}
				}

				for (Map.Entry<Integer, Integer> entry : mirrorIdList.entrySet()) {

					UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(entry.getValue());
					if (userMirrorDTO != null && userMirrorDTO.getUser().getId() == userModel.getId()) {
						List<WidgetDTO> widgetList = widgetDao.getWidgetSettingByStatusAndWidgetList(userMirrorDTO,
								widgetModelList);
						for (WidgetDTO widgetDTO : widgetList) {
							if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE))
									|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER))
									|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
								widgetModels.add(dozerMapper.map(widgetDTO, WidgetModel.class));
							} else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEATHER))
									|| widgetDTO.getType()
											.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))
									|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DAILY_WEATHER))) {
								weatherUpdateFlag = 1;
							}
						}

						if (!widgetModels.isEmpty()) {
							ObjectMapper objectMapper = new ObjectMapper();
							JSONArray array = new JSONArray();
							SocketResponce socketResponce = new SocketResponce();
							socketResponce.setType(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
							socketResponce.setMessage(ResourceManager.getProperty(REFRESH_HEALTHKIT_SOCKET_MESSAGE));
							List<WidgetHealthDataModel> widgetDataList = fitbitService
									.getHealthDataByUserMirror(userMirrorDTO, widgetModels);
							data.put("data", widgetDataList);
							socketResponce.setData(objectMapper.writeValueAsString(data));
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
							socketService.broadcast(array.toString());
						}

						if (weatherUpdateFlag == 1) {
							LocationDTO locationDTO = userMirrorDTO.getLocation();
							if (locationDTO != null) {
								widgetService.checkAndUpdateWeatherData(locationDTO);
							}
							String iconFormat = "svg";

							ObjectMapper objectMapper = new ObjectMapper();
							ArrayList<Map<String, Object>> weatherData = new ArrayList<>();

							Map<String, Object> currentWeatherdata = widgetService.getCurrentWeatherData(userMirrorDTO,
									iconFormat);
							weatherData.add(currentWeatherdata);

							Map<String, Object> dailyWeatherdata = widgetService.getDailyWeatherData(userMirrorDTO,
									iconFormat, ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
							weatherData.add(dailyWeatherdata);

							Map<String, Object> hourleyWeatherdata = widgetService.getHourlyWeatherData(userMirrorDTO,
									iconFormat, ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
							weatherData.add(hourleyWeatherdata);

							SocketResponce socketResponce = new SocketResponce();
							socketResponce.setType(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
							socketResponce.setMessage(ResourceManager.getProperty(REFRESH_WEATHER_SOCKET_MESSAGE));
							socketResponce.setData(objectMapper.writeValueAsString(weatherData));

							JSONArray array = new JSONArray();
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
							socketService.broadcast(array.toString());
						}

					}
				}
			}

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@Override
	public void sendUserLog(EmailLogDataModel logData, String version) throws MangoMirrorException {

		String inputData = logData.getData();
		String[] emailIdCCList = logEmailCC.split(" ");
		String[] emailIdToList = logEmailTo.split(" ");
		EmailLogDTO logDTO = new EmailLogDTO();
		logDTO.setCc(emailIdCCList);
		logDTO.setTo(emailIdToList);
		File file = null;
		File dir = new File(emailFilePath);
		try {
			file = File.createTempFile("userLog", ".text", dir);
			FileUtil.writeAsString(file, inputData);
			logDTO.setFile(file);
			logDTO.setSubject(ResourceManager.getProperty(EMAIL_LOG_SUBJECT));
			EmailSender emailSender = (EmailSender) ApplicationBeanUtil.getApplicationContext().getBean("mail");
			emailSender.sendLogOverMail(logDTO);
			/* file.deleteOnExit(); */
		} catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@Override
	public void updateData(HttpServletRequest request, BleInDataModel bleInDataModel, String rangeType, int timeZone,
			String version) throws MangoMirrorException {

		UserModel userModel = (UserModel) request.getAttribute(USER);

		try {
			if (bleInDataModel.getUserMirrorList() != null) {
				List<UserMirrorModel> userMirrorList = bleInDataModel.getUserMirrorList();
				if (!userMirrorList.isEmpty()) {
					publishWidgetSetting(request, userMirrorList, rangeType, version);
				}
			}

			HealthWidgetModel healthWidgetModel = bleInDataModel.getHealthWidgetModel();
			if (healthWidgetModel != null) {

				UserDTO userDTO = dozerMapper.map(userModel, UserDTO.class);
				fitbitService.saveHealthKit(userDTO, healthWidgetModel, timeZone, version);
			}

			List<UserCalendarModel> userCalendarModelList = bleInDataModel.getUserCalendarModelList();
			if (!userCalendarModelList.isEmpty()) {
				widgetService.saveCalendarData(request, userCalendarModelList, timeZone, version);
			}

		} catch (MangoMirrorException exception) {
			logger.info(exception);
			throw exception;
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@Override
	public void updateAllFitbitUserName(HttpServletRequest request, String version) throws MangoMirrorException {

		try {
			List<FitBitAccountDTO> fitBitAccountList = userMirrorDao.getFitbitAccounts();
			File privateKeyaFile = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privateKeyaFile);

			for (FitBitAccountDTO fitBitAccountDTO : fitBitAccountList) {
				String fullname = fitBitAccountDTO.getFitbitUserName();
				if (fullname != null) {
					fullname = asymmetricCryptography.encryptText(fullname, privateKey);
					fitBitAccountDTO.setFitbitUserName(fullname);
				}
			}
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) + "" + exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}

	}
}