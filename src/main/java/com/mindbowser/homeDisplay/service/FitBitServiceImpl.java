
package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.ACTIVITIES_NOTIFICATION_KEY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.ACTIVITY_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BODY_AND_WEIGHT_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BODY_NOTIFICATION_KEY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_FILLCOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_POINTCOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_POINTSTROKECOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_STROKECOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_MULTIPLIER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FITBIT_MASTERCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FOODS_NOTIFICATION_KEY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTHKIT_MASTERCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_DISTANCE_KILOMETER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_DISTANCE_MILES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_WATER_FLOZ;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_WEIGHT_POUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.INVALID_FITBIT_TOKEN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_DATA;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KG_TO_POUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KM_TO_MILES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MINUTEASSLEEP;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.ML_TO_FLOZ;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NO_WIDGET_AVAILABLE_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NUTRITIONS_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PROFILE_NOTIFICATION_KEY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SLEEP_NOTIFICATION_KEY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SLEEP_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TIMEINBED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_GENERAL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_LINKED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_V1;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VITAL_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_ACTIVITY_CALORIES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_BMI;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALORIES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALORIESBMR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CARB;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DISTANCE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_FAT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_FIBER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_PROTEIN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_SODIUM;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_STEP;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_TOTAL_SLEEPMINUTE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_TOTAL_TIMEINBED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WATER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WEIGHT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.JSONFORMAT_EXTENSION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TWO_DECIMAL_FLOATING_POINT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEVICEID_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;

import java.io.File;
import java.security.PrivateKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.FitBitDao;
import com.mindbowser.homeDisplay.dao.UserDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDao;
import com.mindbowser.homeDisplay.dao.WidgetDao;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.FitBitNotificationModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthDataTimeZoneModel;
import com.mindbowser.homeDisplay.model.HealthKitModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetHealthDataModel;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.util.AsymmetricCryptography;
import com.mindbowser.homeDisplay.util.HealthDataDozerHelper;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.WidgetDozerHelper;


public class FitBitServiceImpl implements FitBitService {

	private static Logger logger = Logger.getLogger(FitBitServiceImpl.class);

	@Autowired
	private WidgetDao widgetDao;
	
	@Autowired
	private FitBitDao fitbitDao;
	
	@Autowired
	private UserMirrorDao userMirrorDao;
	
	@Autowired
	private WidgetService widgetService;
	
	@Autowired
	private SocketService socketService;
	
	@Autowired
	private UserDao userDao;
		
	@Autowired
	private AsymmetricCryptography asymmetricCryptography;
	
	@Autowired
	private Mapper dozerMapper;
	
	@Value("${fitBitTokenUrl}")
	private String fitBitTokenUrl;
	
	@Value("${fitBitBaseUrl}")
	private String fitBitBaseUrl;
	
	@Value("${fitBitBasicToken}")
	private String fitBitBasicToken;
	
	@Value("${fitBitSubscriberId}")
	private String fitBitSubscriberId;
	
	

	@Override
	public List<Map<String, Object>> getHealthKitWidgetList(String version) throws MangoMirrorException {

        List<String> categoryList = new ArrayList<>();
        categoryList.add(ResourceManager.getProperty(HEALTHKIT_MASTERCATEGORY));
        categoryList.add(ResourceManager.getProperty(FITBIT_MASTERCATEGORY));
		List<WidgetDTO> widgets = widgetDao.getHealthKitWidgetListByMasterCategory(categoryList);
		List<Map<String, Object>> widgetList = new ArrayList<>();
		
		Map<String, Object> healthKitMap = new HashMap<>();
		HealthKitModel healthKit = new HealthKitModel();
		HealthKitModel fitbit = new HealthKitModel();
		
		List<WidgetModel> fitbitActivityList = new ArrayList<>();
		List<WidgetModel> fitbitNeutritionList = new ArrayList<>();
		List<WidgetModel> fitbitBodyAndWeightList = new ArrayList<>();
		List<WidgetModel> fitbitSleepList = new ArrayList<>();
		
		
		List<WidgetModel> healthkitActivityList = new ArrayList<>();
		List<WidgetModel> healthkitNeutritionList = new ArrayList<>();
		List<WidgetModel> healthkitBodyAndWeightList = new ArrayList<>();
		List<WidgetModel> healthkitSleepList = new ArrayList<>();
		List<WidgetModel> healthkitVitalList = new ArrayList<>();
		
		try {
			if (widgets.isEmpty()) {
				throw new MangoMirrorException(
						ResourceManager.getMessage(NO_WIDGET_AVAILABLE_EXCEPTION, null, NOT_FOUND, null));
			} else {
                 for (WidgetDTO widgetDTO : widgets) {
                	WidgetModel widgetModel = dozerMapper.map(widgetDTO, WidgetModel.class);
                	 if(widgetDTO.getMasterCategory().equals(ResourceManager.getProperty(HEALTHKIT_MASTERCATEGORY)))
                	 {
                        if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
                        {
                         healthkitActivityList.add(widgetModel);
                        }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
                        {
                         healthkitNeutritionList.add(widgetModel);
                        }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY)))
                        {
                        	healthkitSleepList.add(widgetModel);
                        }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
                        {
                        	healthkitBodyAndWeightList.add(widgetModel);
                        }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(VITAL_SUBCATEGORY)))
                        {
                        	healthkitVitalList.add(widgetModel);
                        }
                	 }else if(widgetDTO.getMasterCategory().equals(ResourceManager.getProperty(FITBIT_MASTERCATEGORY)))
                	 {
                		  if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
                          {
                           fitbitActivityList.add(widgetModel);
                          }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
                          {
                           fitbitNeutritionList.add(widgetModel);
                          }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY)))
                          {
                              fitbitSleepList.add(widgetModel);
                          }else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
                          {
                              fitbitBodyAndWeightList.add(widgetModel);
                          }
                	 }
   	 			}
                 healthKit.setActivity(healthkitActivityList);
                 healthKit.setNutrition(healthkitNeutritionList);
                 healthKit.setSleep(healthkitSleepList);
                 healthKit.setBodyAndWeight(healthkitBodyAndWeightList);
                 healthKit.setVital(healthkitVitalList);
                 fitbit.setActivity(fitbitActivityList);
                 fitbit.setNutrition(fitbitNeutritionList);
                 fitbit.setBodyAndWeight(fitbitBodyAndWeightList);
                 fitbit.setSleep(fitbitSleepList);
                 
           /*  	 healthKitMap.put(ResourceManager.getProperty(HEALTHKIT_MASTERCATEGORY), healthKit);
            	 healthKitMap.put(ResourceManager.getProperty(FITBIT_MASTERCATEGORY), fitbit);*/
           
            	 healthKitMap.put("HealthKit", healthKit);
            	 healthKitMap.put("Fitbit", fitbit);
            	 widgetList.add(healthKitMap);
			}
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw exception;
		}
		return widgetList;
	}

	@Override
	public void saveHealthKitData(HealthDataDTO healthDataDTO,HealthWidgetModel healthWidgetModel,String version) throws MangoMirrorException{	
		   
		    List<HealthDataModel> healthDataList = healthWidgetModel.getHealthDataList();
		    List<WidgetModel> widgetModelList = healthWidgetModel.getWidgetList();		
		    List<HealthDataDTO> healthDataDtoList = HealthDataDozerHelper.MapModelToDto(dozerMapper, healthDataList, HealthDataDTO.class);
		    if(!healthDataDtoList.isEmpty())
		    {
		    	fitbitDao.saveHealthData(healthDataDTO, healthDataDtoList);	
		    }
		    List<WidgetHealthDataModel> widgetList;
            
            String generalUser = ResourceManager.getProperty(USER_GENERAL);
			String linkedUser = ResourceManager.getProperty(USER_LINKED);
			int mirrorStatus = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));

			List<UserMirrorDTO> userMirrorDTOList1;
			if(healthDataDTO.getUser()!=null)
			{
				userMirrorDTOList1 = userMirrorDao.getUserMirrorList(healthDataDTO.getUser().getId(), generalUser,linkedUser, mirrorStatus, userStatusFlag);	
			}else
			{
				userMirrorDTOList1 = userMirrorDao.getUserMirrorListByFitBitId(healthDataDTO.getFitbit().getId(), generalUser,linkedUser, mirrorStatus, userStatusFlag);	
			}

			ObjectMapper objectMapper = new ObjectMapper();
			SocketResponce socketResponce = new SocketResponce();
			socketResponce.setType("refreshHealthKitData");
			socketResponce.setMessage("refreshHealthKitData");
			Map<String, List<WidgetHealthDataModel>> data = new HashMap<>();
			Boolean sendDataToSocket = false;
			JSONArray array = new JSONArray();
			
			try {
				
				if (!userMirrorDTOList1.isEmpty()) {
					for (UserMirrorDTO userMirrorDTO : userMirrorDTOList1) {
						if(userMirrorDTO.getMirror().getMirrorPreview() == false)
						{
							UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorDTO.getMirror().getId());
							if (userMirrorDTO1 == null) {
								
								if (userMirrorDTO.getUserRole().equals(generalUser))
								   {
									widgetList = getHealthDataByUserMirror(userMirrorDTO, widgetModelList);
									if(!widgetList.isEmpty())
									{
										data.put(ResourceManager.getProperty(DATA_LABEL), widgetList);
										socketResponce.setData(objectMapper.writeValueAsString(data));
										JSONObject jsonObject = new JSONObject();
										jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
										jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
										array.put(jsonObject);
										sendDataToSocket =  true;
									}
								}
							}else
							{
								if (userMirrorDTO.getUser().getId() == userMirrorDTO1.getUser().getId())
								{
									widgetList = getHealthDataByUserMirror(userMirrorDTO1, widgetModelList);
									if(!widgetList.isEmpty())
									{
										data.put(ResourceManager.getProperty(DATA_LABEL), widgetList);
										socketResponce.setData(objectMapper.writeValueAsString(data));
										JSONObject jsonObject = new JSONObject();
										jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO1.getMirror().getDeviceId());
										jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
										array.put(jsonObject);
										sendDataToSocket =  true;
									}
								}
							}
						}else
						{
							if (userMirrorDTO.getUserRole().equals(generalUser))
							{
								widgetList = getHealthDataByUserMirror(userMirrorDTO, widgetModelList);
								if(!widgetList.isEmpty())
								{
									data.put(ResourceManager.getProperty(DATA_LABEL), widgetList);
									socketResponce.setData(objectMapper.writeValueAsString(data));
									JSONObject jsonObject = new JSONObject();
									jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
									jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
									array.put(jsonObject);
									sendDataToSocket =  true;
								}
							}
						}
					}
				}
				if(sendDataToSocket)
				{
					socketService.broadcast(array.toString());	
				}			
			}catch (Exception exception) {
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+" "+exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<WidgetHealthDataModel> getHealthDataByUserMirror(UserMirrorDTO userMirrorDTO,List<WidgetModel> widgetModelList)
	{
		FitBitAccountModel fitBitAccountModel = null;
		List<WidgetHealthDataModel> widgetList = new ArrayList<>();
        List<String> widgetsNameList = new ArrayList<>();		
		if(!widgetModelList.isEmpty())
		{
			for (WidgetModel widgetModel : widgetModelList) {
				widgetsNameList.add(widgetModel.getType());
			}
			List<WidgetDTO> widgetDTOList = widgetDao.getAllWidget(widgetsNameList);
			List<WidgetDTO>	widgetDTOs = widgetDao.getWidgetSettingByStatusAndWidgetList(userMirrorDTO, widgetDTOList);
		
			if(!widgetDTOs.isEmpty())
			{
				if(userMirrorDTO.getFitbitAccount()!=null)
				{
					fitBitAccountModel = dozerMapper.map(userMirrorDTO.getFitbitAccount(), FitBitAccountModel.class);	
				}
				UserModel userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
				
				 for (WidgetDTO widgetDTO : widgetDTOs) {
			    	    WidgetHealthDataModel widgetData = new WidgetHealthDataModel();
						List healthData = new ArrayList();
						Object goalValue = null;
						Object maxValue = null;
						Double multiplier = Double.parseDouble(ResourceManager.getProperty(DEFAULT_MULTIPLIER));
						String unit = null;
						String unitName = null;
						Object minValue = null;

						HealthDataModel healthDataModel = new HealthDataModel();
						HealthDataTimeZoneModel timeZoneModel = new HealthDataTimeZoneModel();

						if (widgetDTO.getMasterCategory()
								.equals(ResourceManager.getProperty(FITBIT_MASTERCATEGORY))) {
							if (fitBitAccountModel != null) {
								healthDataModel.setFitbit(fitBitAccountModel);
							} else {
								healthDataModel.setUser(userModel);
							}
						} else {
							healthDataModel.setUser(userModel);
						}

						timeZoneModel.setUseratimeZone(userMirrorDTO.getUser().getUserCurrentTimeZone());

						if (widgetDTO.getType()
								.equals(ResourceManager.getProperty(WIDGET_DISTANCE))) {
							if (userMirrorDTO.getUser().getDistanceUnit()
									.equals(ResourceManager.getProperty(HEALTH_DISTANCE_MILES))) {
								multiplier = Double.parseDouble(ResourceManager.getProperty(KM_TO_MILES));
								unitName = userMirrorDTO.getUser().getDistanceUnit();
							} else if (userMirrorDTO.getUser().getDistanceUnit()
									.equals(ResourceManager.getProperty(HEALTH_DISTANCE_KILOMETER))) {
								unitName = userMirrorDTO.getUser().getDistanceUnit();
							}
						} else if (widgetDTO.getType()
								.equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
							unitName = userMirrorDTO.getUser().getWeightUnit();
							if (userMirrorDTO.getUser().getWeightUnit()
									.equals(ResourceManager.getProperty(HEALTH_WEIGHT_POUND))) {
								multiplier = Double.parseDouble(ResourceManager.getProperty(KG_TO_POUND));
							}
						} else if (widgetDTO.getType()
								.equals(ResourceManager.getProperty(WIDGET_WATER))) {
							unitName = userMirrorDTO.getUser().getWaterUnit();
							if (userMirrorDTO.getUser().getWaterUnit()
									.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ))) {
								multiplier = Double.parseDouble(ResourceManager.getProperty(ML_TO_FLOZ));
							}
						} 
						List<String> labelArray = new ArrayList<String>();
						try {

							Map<String, Object> healthDataValues = widgetService.getHealthData(healthDataModel, multiplier,timeZoneModel, widgetDTO, unitName);
							if (!healthDataValues.isEmpty()) {
								healthData = (List) healthDataValues.get(ResourceManager.getProperty(DATA_LABEL));
								labelArray = (List<String>) healthDataValues.get("label");
								goalValue = healthDataValues.get("goalValue");
								maxValue = healthDataValues.get("maxValue");
								minValue = healthDataValues.get("minValue");
								unit = (String) healthDataValues.get("unit");
							}
						} catch (Exception exception) {
							logger.info(exception);
						}

						HashMap datasetObject = new HashMap();
						// barGraph property
						datasetObject.put(ResourceManager.getProperty(KEY_DATA), healthData);
						datasetObject.put("label", "step data");
						if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
						{
							datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
							datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(46,204,113,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(46,204,113,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(46,204,113,1)");
							
						}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY)))
						{
							datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
							datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(52,152,219,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(52,152,219,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(52,152,219,1)");
							
						}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
						{
							datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
							datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(231,76,60,0)");
							datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(231,76,60,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(231,76,60,1)");
						}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
							datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(155,89,182,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(155,89,182,1)");
							datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(155,89,182,1)");
						}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(VITAL_SUBCATEGORY)))
						{
						datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
						datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(192,57,43,0)");
						datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(192,57,43,1)");
						datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(192,57,43,1)");
						}

		              /*new code as per new design*/				
						
						int divider = 1000;
						String currentDateData = null;
						/*StringBuilder hours = new StringBuilder();
						StringBuilder minutes = new StringBuilder();
*/						
						if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))
								|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE)))
								{
						/*		   String totalTime = String.valueOf(healthData.get(6));
								   hours.append(totalTime.substring(0,totalTime.indexOf('.')));
								   minutes.append(totalTime.substring(totalTime.indexOf('.')+1));
							        if(hours.length() ==1)
							        {
							        	hours = hours.append("0").append(hours);
							        }
							        if(minutes.length() ==1)
							        {
							        	minutes = minutes.append(hours).append("0");
							        }
							        currentDateData = hours+"h : "+minutes+"m";*/
							        
							        String totalTime = String.valueOf(healthData.get(6));
									   String hours = totalTime.substring(0,totalTime.indexOf('.'));
								       String minutes = totalTime.substring(totalTime.indexOf('.')+1);
								        if(hours.length() ==1)
								        { 
								        	StringBuilder hourStringBuilder = new StringBuilder();
								        	hours = hourStringBuilder.append("0").append(hours).toString(); 
								        }
								        if(minutes.length() ==1)
								        {  
								        	StringBuilder minStringBuilder = new StringBuilder();
								        	minutes = minStringBuilder.append(minutes).append("0").toString(); 
								        }
								        currentDateData = hours+"h : "+minutes+"m";
							        
							        
								}else
								{
									int index = healthData.size();
									
									if(index > 0)
									{
										if (healthData.get(index-1) instanceof Double) 
										{
											double currentDateValue =  (double) healthData.get(index-1);
											if (currentDateValue >= divider && (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
													|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
													|| widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR)))) 
												{
												double newCaloriesValue = currentDateValue / 1000;
												int newCaloriesValueInteger = Double.valueOf(newCaloriesValue).intValue();
												if (newCaloriesValue == newCaloriesValueInteger) {
													currentDateData =  Integer.toString(newCaloriesValueInteger);
											    } else {
											    	currentDateData =  Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), (newCaloriesValue))).toString();
											    }
												unit = "k".concat(unit);
											} else {
												int currentDateDataInInteger = Double.valueOf(currentDateValue).intValue();
												if (currentDateValue == currentDateDataInInteger) {
													currentDateData =  Integer.toString(currentDateDataInInteger);
											    } else {
											    	currentDateData = Double.toString(currentDateValue);
											    }
											}
										}
									}else
									{
										int currentDateDataInDouble = 0;
									    currentDateData = Double.toString(currentDateDataInDouble);
									}
								}
						
						List<Map<String, Object>> datasetObjectValue = new ArrayList<>();
						datasetObjectValue.add(datasetObject);
						HashMap<String, Object> data = new HashMap();
						data.put("datasets", datasetObjectValue);
						data.put("labels", labelArray);
						data.put("unit", unit);
						data.put("todaysData", currentDateData);
						data.put("goalValue", goalValue);
						data.put("maxValue", maxValue);
						data.put("minValue", minValue);
						widgetData.setData(data);
						widgetData.setWidget(dozerMapper.map(widgetDTO, WidgetModel.class));
					    widgetList.add(widgetData);
				}				
			}
		}
		 return widgetList;
	}
	
	
	
	
	@Override
	public void saveHealthKit(UserDTO userDTO, HealthWidgetModel healthWidgetModel,int timeZone,String version)
			throws MangoMirrorException{
		
		userDao.updateUserTimezone(userDTO.getId(), timeZone);
		List<WidgetModel> widgetModelList = healthWidgetModel.getWidgetList();
		try {
			List<WidgetDTO> widgetDTOList =  WidgetDozerHelper.MapModelToDto(dozerMapper, widgetModelList, WidgetDTO.class);
			
			 List<Integer> widgetIdlist = new ArrayList<>();
			    
			    if(!widgetDTOList.isEmpty())
			    {
			    	for (WidgetDTO widgetDTO : widgetDTOList) {
			    		widgetIdlist.add(widgetDTO.getId());
					}
			    }
			
			fitbitDao.removeHealthKitData(dozerMapper.map(userDTO, UserModel.class), widgetIdlist);
			widgetDTOList = widgetDao.getWidgetListByWidgetId(widgetIdlist);
			widgetModelList= WidgetDozerHelper.map(dozerMapper, widgetDTOList, WidgetModel.class);
			healthWidgetModel.setWidgetList(widgetModelList);
			HealthDataDTO healthDataDTO = new HealthDataDTO();
			healthDataDTO.setUser(userDTO);
			saveHealthKitData(healthDataDTO,healthWidgetModel,version);
			
		}catch(Exception exception)
		{
			logger.error(exception);
		}
	}

	@Override
	public List<WidgetModel> healthWidgetList(String healthWidgetMasterCategory, String version)
			throws MangoMirrorException {

		List<String> categoryList = new ArrayList<>();
        categoryList.add(healthWidgetMasterCategory);
        List<WidgetDTO> widgetDtoList = widgetDao.getHealthKitWidgetListByMasterCategory(categoryList);
        List<WidgetModel> widgets =  WidgetDozerHelper.map(dozerMapper, widgetDtoList, WidgetModel.class);
		try {
			if (widgets.isEmpty()) {
				throw new MangoMirrorException(
						ResourceManager.getMessage(NO_WIDGET_AVAILABLE_EXCEPTION, null, NOT_FOUND, null));
			   } 
		}catch (Exception exception) {
			logger.error(exception.getMessage());
			throw exception;
		}
		return widgets;
	}


	@Override
	public void removeHealthKitData(HttpServletRequest request, List<HealthDataModel> healthDataList, String version)
			throws MangoMirrorException {
		  
		    UserModel userModel = (UserModel) request.getAttribute(USER);
		    List<Integer> list = new ArrayList<>();
		    
		    if(!healthDataList.isEmpty())
		    {
		    	for (HealthDataModel healthDataModel : healthDataList) {
					list.add(healthDataModel.getWidget().getId());
				}
		    }
		    fitbitDao.removeHealthKitData(userModel, list);
	}

	@Override
	public void fitBitUpdateCredential(HttpServletRequest request, UserMirrorModel userMirrorModel, String version)
			throws MangoMirrorException {
		 UserModel userModel = (UserModel) request.getAttribute(USER);
		 UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), userMirrorModel.getMirror().getId(),
					userMirrorModel.getUserRole(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		 
		 int flag=0;
		 try
		 {
			 if(userMirrorDTO != null)
			    {
			    	
				 if(userMirrorModel.getFitbitAccount() == null)
				 {
					 userMirrorDTO.setFitbitAccount(null);
				 }else
				 {
					 if(userMirrorDTO.getFitbitAccount()!=null)
				    	{
				    		if(userMirrorDTO.getFitbitAccount().getFitbitUserId().equals(userMirrorModel.getFitbitAccount().getFitbitUserId()))
				    		{
				    			fitbitDao.updateFitBitCredential(userMirrorModel.getFitbitAccount(),userMirrorDTO.getFitbitAccount().getId());	
				    		}else
				    		{
				    			flag = 1;
				    		}
				    	}
				    	else
				    	{
					      flag = 1;	
				    	}
				 }

			    	if(flag == 1 && !userMirrorModel.getFitbitAccount().getFitbitUserId().isEmpty() && userMirrorModel.getFitbitAccount().getFitbitUserId()!=null) 
				    	{
				    		FitBitAccountDTO fitBitAccountDTO = fitbitDao.getFitBitAccountByUserId(userMirrorModel.getFitbitAccount().getFitbitUserId());
				    		if(fitBitAccountDTO!=null)
				    		{
				    			fitbitDao.updateFitBitCredential(userMirrorModel.getFitbitAccount(),fitBitAccountDTO.getId());
				    			userMirrorDao.updateFitbitAccount(fitBitAccountDTO, userMirrorDTO);
				    		}else
				    		{
				    			FitBitAccountDTO fitBitAccountDTO1 = dozerMapper.map(userMirrorModel.getFitbitAccount(), FitBitAccountDTO.class);
					    		ResponseEntity<String> dataResponse;
					    		HttpHeaders headers1 = new HttpHeaders();
								headers1.set("Content-Type", MediaType.APPLICATION_JSON);
								headers1.set("Authorization", "Bearer  " +fitBitAccountDTO1.getFitbitAccessToken());
								HttpEntity<String> entity1 = new HttpEntity<>(headers1);
								RestTemplate restTemplate = new RestTemplate();
								String profileUrl = fitBitBaseUrl.concat("profile.json");
								dataResponse = restTemplate.exchange(profileUrl, HttpMethod.GET, entity1, String.class);
								JSONObject jsonObject = new JSONObject(dataResponse.getBody());	
								JSONObject userProfileDetail = jsonObject.getJSONObject("user");
								String fullname = jsonObject.getJSONObject("user").getString("fullName");
								File privateKeyaFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/privateKey");
								PrivateKey privateKey = asymmetricCryptography.getPrivate(privateKeyaFile);
								fullname = asymmetricCryptography.encryptText(fullname, privateKey);
								fitBitAccountDTO1.setFitbitUserName(fullname);
								
								fitBitAccountDTO1.setFitbitUserTimeZone(userProfileDetail.getString("timezone"));
								FitBitAccountDTO fitBitAccountDTO2 = fitbitDao.saveFitBitAccount(fitBitAccountDTO1);
								userMirrorDTO.setFitbitAccount(fitBitAccountDTO2);
					    		getFitBitApiData(fitBitAccountDTO2);
					    		HttpHeaders headers = new HttpHeaders();
					    		headers.set("X-Fitbit-Subscriber-Id", fitBitSubscriberId);
								headers.set("Authorization", "Bearer  " +userMirrorDTO.getFitbitAccount().getFitbitAccessToken());
								HttpEntity<String> entity = new HttpEntity<>(headers);
						    	String subscriptionUrl = fitBitBaseUrl.concat("apiSubscriptions/"+userMirrorDTO.getFitbitAccount().getId()+ResourceManager.getProperty(JSONFORMAT_EXTENSION));
						    	restTemplate.exchange(subscriptionUrl, HttpMethod.POST, entity, String.class);
				    		}
			    	}
				 	
			    }			 
		 }catch(HttpClientErrorException httpClientErrorException)
			{
					logger.info(httpClientErrorException);
					
					if( Integer.parseInt(httpClientErrorException.getStatusCode().toString()) ==  409)
					{
						throw new MangoMirrorException(ResourceManager.getMessage(INVALID_FITBIT_TOKEN, null, NOT_FOUND, null));	
					}
			}
			catch(Exception exception)
			{
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
	}

	@Override
	public List<HealthDataDTO> getFitBitApiData(FitBitAccountDTO fitBitAccountDTO) throws MangoMirrorException {
	
	     List<WidgetDTO> neutritionsWidget = new ArrayList<>();
		 List<WidgetDTO> activityWidget = new ArrayList<>();
		 List<WidgetDTO> sleepWidget = new ArrayList<>();
		 List<WidgetDTO> bodyAndWeightWidget = new ArrayList<>();
		 List<HealthDataDTO> healthDataDTOList  = new ArrayList<>();
		 List<HealthDataDTO> neutritionsHealthData = null ;
		 List<HealthDataDTO> activityHealthData = null ;
		 List<HealthDataDTO> sleepHealthData = null ;
		 List<HealthDataDTO> bodyWeightHealthData = null ;
	  try
		{
		        List<String> subCategoryList = new ArrayList<>();
	    		subCategoryList.add(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY));
				subCategoryList.add(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY));
				subCategoryList.add(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY));
				subCategoryList.add(ResourceManager.getProperty(SLEEP_SUBCATEGORY));
		
          List<WidgetDTO> widgets = widgetDao.getWidgetBySubCategory(subCategoryList,ResourceManager.getProperty(FITBIT_MASTERCATEGORY));
		
	        for (WidgetDTO widgetDTO : widgets) 
	        {
				if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
				{
					   neutritionsWidget.add(widgetDTO);
				}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				{
					activityWidget.add(widgetDTO);
				}
				else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY)))
				{
					sleepWidget.add(widgetDTO);
				}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
				{
					bodyAndWeightWidget.add(widgetDTO);
				}
			}
			if(!neutritionsWidget.isEmpty())
			{
				neutritionsHealthData = getNeutritionData(neutritionsWidget,fitBitAccountDTO);
				healthDataDTOList.addAll(neutritionsHealthData) ;
			}
			if(!activityWidget.isEmpty())
			{
				activityHealthData = getActivityData(activityWidget,fitBitAccountDTO);
				healthDataDTOList.addAll(activityHealthData) ;
			}
			if(!sleepWidget.isEmpty())
			{
				sleepHealthData = getSleepData(sleepWidget,fitBitAccountDTO);
				healthDataDTOList.addAll(sleepHealthData) ;
			}
			if(!bodyAndWeightWidget.isEmpty())
			{
				bodyWeightHealthData = getBodyAndWeightData(bodyAndWeightWidget,fitBitAccountDTO);
				healthDataDTOList.addAll(bodyWeightHealthData) ;
			}
					
			List<HealthDataModel> healthDataList = HealthDataDozerHelper.MapDtoToModel(dozerMapper,healthDataDTOList , HealthDataModel.class);
			if(!healthDataList.isEmpty())
			{
				fitbitDao.removeHealthKitDataByFitBitId(fitBitAccountDTO.getId());
				List<WidgetModel> widgetModels = WidgetDozerHelper.map(dozerMapper, widgets, WidgetModel.class);
				HealthWidgetModel healthWidgetModel = new HealthWidgetModel();
				healthWidgetModel.setWidgetList(widgetModels);
				healthWidgetModel.setHealthDataList(healthDataList);
				HealthDataDTO healthDataDTO = new HealthDataDTO();
				healthDataDTO.setFitbit(fitBitAccountDTO);
				saveHealthKitData(healthDataDTO, healthWidgetModel, ResourceManager.getProperty(VERSION_V1));
			}
		    
		}catch(HttpClientErrorException httpClientErrorException)
		{
			if(httpClientErrorException.getStatusCode().value()==401)
			{
				getUpdatedFitbitAccessToken(fitBitAccountDTO); 
				getFitBitApiData(fitBitAccountDTO);
			}else
			{
				logger.error(httpClientErrorException.getMessage());
				throw httpClientErrorException;
			}
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) +" "+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return healthDataDTOList;
	}
  


	@SuppressWarnings("rawtypes")
	@Override
	public void getUpdatedFitbitAccessToken(FitBitAccountDTO fitBitAccountDTO)
			throws MangoMirrorException {
		
		HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
		HttpMessageConverter<?> stringHttpMessageConverter = new StringHttpMessageConverter();
		List<HttpMessageConverter> msgConverters = new ArrayList<>();
		msgConverters.add(formHttpMessageConverter);
		msgConverters.add(stringHttpMessageConverter);

		// Prepare acceptable media type
		MultiValueMap<String, String> data = new LinkedMultiValueMap<>();
		data.add("grant_type", "refresh_token");
		data.add("refresh_token", fitBitAccountDTO.getFitbitRefreshToken());
		
		// Prepare header
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic " +fitBitBasicToken);
		HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(data,headers);
		
		try{
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.exchange(fitBitTokenUrl, HttpMethod.POST, httpEntity, String.class);
			JSONObject jsonObject = new JSONObject(response.getBody());
			FitBitAccountModel fitBitAccountModel = new FitBitAccountModel();
			fitBitAccountModel.setFitbitAccessToken(jsonObject.getString("access_token"));
			fitBitAccountModel.setFitbitRefreshToken(jsonObject.getString("refresh_token"));
			fitBitAccountModel.setFitbitUserId(jsonObject.getString("user_id"));
			fitbitDao.updateFitBitCredential(fitBitAccountModel,fitBitAccountDTO.getId());
		}
		catch(HttpClientErrorException httpClientErrorException)
		{
			logger.info(httpClientErrorException);
			throw httpClientErrorException;
		}
		
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) +" "+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	
	@Override
	public List<HealthDataDTO> saveNotificationFitbitData(List<FitBitNotificationModel> fitbitNotificationData)
			throws MangoMirrorException {
		
		Set<String> subCategorySet = new HashSet<>();
		List<String> subCategoryList = new ArrayList<>();
		List<WidgetDTO> neutritionsWidget = new ArrayList<>();
		List<WidgetDTO> activityWidget = new ArrayList<>();
		List<WidgetDTO> sleepWidget = new ArrayList<>();
		List<WidgetDTO> bodyAndWeightWidget = new ArrayList<>();
		List<HealthDataDTO> healthDataDTOList  = new ArrayList<>();
		String fitBitUserId = null;
		FitBitAccountDTO fitBitAccountDTO = null;
		FitBitAccountDTO fitBitAccountDTO1 = null;
		
	  try
		{
		 if(!fitbitNotificationData.isEmpty())
			{
				fitBitUserId = fitbitNotificationData.get(0).getOwnerId();
			}
		  fitBitAccountDTO = fitbitDao.getFitBitAccountByUserId(fitBitUserId);
		  FitBitAccountModel fitBitAccountModel = null;
		  int profileFlag = 0;
		  int checkForTimeZone = 0;
		  
		  if(fitBitAccountDTO!=null)
				{
				  for (FitBitNotificationModel fitBitNotificationModel : fitbitNotificationData) {
						subCategorySet.add(fitBitNotificationModel.getCollectionType());
					}
					for (String subCategory : subCategorySet) {
						if(subCategory.equals(ResourceManager.getProperty(FOODS_NOTIFICATION_KEY)))
						{
							subCategoryList.add(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY));
						}else if(subCategory.equals(ResourceManager.getProperty(BODY_NOTIFICATION_KEY)))
						{
							subCategoryList.add(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY));
						}else if(subCategory.equals(ResourceManager.getProperty(ACTIVITIES_NOTIFICATION_KEY)))
						{
							subCategoryList.add(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY));
						}else if(subCategory.equals(ResourceManager.getProperty(SLEEP_NOTIFICATION_KEY)))
						{
							subCategoryList.add(ResourceManager.getProperty(SLEEP_SUBCATEGORY));
						}else if(subCategory.equals(ResourceManager.getProperty(PROFILE_NOTIFICATION_KEY)))
						{
                           profileFlag = 1; 							
						}
					}
					
				if(profileFlag == 1)
				{
				 fitBitAccountModel = getFitbitProfileDetail(fitBitAccountDTO.getFitbitUserId(), ResourceManager.getProperty(VERSION_V1));
				 fitBitAccountDTO1 = fitBitAccountDTO;
				 fitBitAccountDTO = fitbitDao.getFitBitAccountByUserId(fitBitUserId);
				 if(!fitBitAccountDTO1.getFitbitUserTimeZone().equals(fitBitAccountModel.getFitbitUserTimeZone())) 
                 {
                	 getFitBitApiData(fitBitAccountDTO);
                	 checkForTimeZone = 1;
                 }         
				}
				
				if(checkForTimeZone == 0)
				{
					if(!subCategoryList.isEmpty())
					 {
						 List<WidgetDTO> widgets = widgetDao.getWidgetBySubCategory(subCategoryList,ResourceManager.getProperty(FITBIT_MASTERCATEGORY)); 
							for (WidgetDTO widgetDTO : widgets) {
								
								if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
								{
									   neutritionsWidget.add(widgetDTO);
								}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
								{
									activityWidget.add(widgetDTO);
								}
								else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY)))
								{
									sleepWidget.add(widgetDTO);
								}else if(widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY)))
								{
									bodyAndWeightWidget.add(widgetDTO);
								}
							}
							
							if(!neutritionsWidget.isEmpty())
							{
								List<HealthDataDTO> neutritionsHealthData = getNeutritionData(neutritionsWidget,fitBitAccountDTO);
								healthDataDTOList.addAll(neutritionsHealthData) ;
							}
							if(!activityWidget.isEmpty())
							{
								List<HealthDataDTO> activityHealthData = getActivityData(activityWidget,fitBitAccountDTO);
								healthDataDTOList.addAll(activityHealthData) ;
							}
							if(!sleepWidget.isEmpty())
							{
								List<HealthDataDTO> sleepHealthData = getSleepData(sleepWidget,fitBitAccountDTO);
								healthDataDTOList.addAll(sleepHealthData) ;
							}
							if(!bodyAndWeightWidget.isEmpty())
							{
								List<HealthDataDTO> bodyWeightHealthData = getBodyAndWeightData(bodyAndWeightWidget,fitBitAccountDTO);
								List<HealthDataDTO> updatedBodyWeightHealthData = new ArrayList<>();
								for (HealthDataDTO healthDataDTO : bodyWeightHealthData) {
								
									if(healthDataDTO.getHealthValue() > 0)
									{
										updatedBodyWeightHealthData.add(healthDataDTO);      									
									}
								}
								if(!updatedBodyWeightHealthData.isEmpty())
								{
									healthDataDTOList.addAll(updatedBodyWeightHealthData) ;	
								}
							}
							List<HealthDataModel> healthDataList = HealthDataDozerHelper.MapDtoToModel(dozerMapper,healthDataDTOList , HealthDataModel.class);
						    List<WidgetModel> widgetModels = WidgetDozerHelper.map(dozerMapper, widgets, WidgetModel.class);
							HealthWidgetModel healthWidgetModel = new HealthWidgetModel();
							healthWidgetModel.setWidgetList(widgetModels);
							healthWidgetModel.setHealthDataList(healthDataList);
							HealthDataDTO healthDataDTO = new HealthDataDTO();
							healthDataDTO.setFitbit(fitBitAccountDTO);
							saveHealthKitData(healthDataDTO, healthWidgetModel, ResourceManager.getProperty(VERSION_V1));						 
					 	}
					}
				}
		}catch(HttpClientErrorException httpClientErrorException)
		{
			if(httpClientErrorException.getStatusCode().value()==401)
			{
				getUpdatedFitbitAccessToken(fitBitAccountDTO); 
				saveNotificationFitbitData(fitbitNotificationData);
			}else
			{
				logger.info(httpClientErrorException);
				throw httpClientErrorException;
			}
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) +" "+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return healthDataDTOList;	
	}
	
	
	 public List<HealthDataDTO> getBodyAndWeightData(List<WidgetDTO> widgetDtoList,FitBitAccountDTO fitBitAccountDTO) throws ParseException 
	 	{
		    String bodyWeightDataUrl = "" ;
		    String bodyWeightGoalUrl = "" ;
		    
	 	    HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer  " +fitBitAccountDTO.getFitbitAccessToken());
			HttpEntity<String> entity = new HttpEntity<>(headers);
			ResponseEntity<String> dataResponse;
			
			Double goalValue = Double.valueOf(0);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setTimeZone(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
			String currentDate = sdf.format(cal.getTime());
			
			String daysJson = ".json";
			String jsonObjectKey = "";
            String lastAccessDate="";
			int flag = 0;
			
			List<HealthDataDTO> healthDataDtoList = new ArrayList<>();
			try
			{
				for (WidgetDTO widgetDTO : widgetDtoList) {
					Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
					Double value;
					RestTemplate restTemplate = new RestTemplate();
					
					if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_BMI)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))) {
						jsonObjectKey = "body-bmi";
						flag = 0;
					}else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))){
						jsonObjectKey = "body-fat";
						flag = 1;
					}else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))) {
						jsonObjectKey = "body-weight";
						flag = 1;
					}
					bodyWeightGoalUrl = fitBitBaseUrl.concat("body/log/"+widgetDTO.getType()+"/goal.json");
					HealthDataDTO healthDataDTO1 = fitbitDao.getFitBitLastAccessDate(widgetDTO,fitBitAccountDTO);
					if(healthDataDTO1!=null)
					{
						lastAccessDate = healthDataDTO1.getHealthTime();
						Date todaysDate = sdf.parse(currentDate);
						Date date = sdf.parse(lastAccessDate);
						long diffInMilies= todaysDate.getTime()-date.getTime();
						long days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);
						List<WidgetDTO> widgetDTOList = new ArrayList<>();
						if(days<=6)
						{
							widgetDTOList.add(widgetDTO);
							fitbitDao.removeHealthKitDataByDate(fitBitAccountDTO, widgetDTOList, lastAccessDate);
							bodyWeightDataUrl = fitBitBaseUrl.concat("body/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);	
						}else
						{
							cal1.add(Calendar.DATE, -6);
							lastAccessDate = sdf.format(cal1.getTime());
							bodyWeightDataUrl = fitBitBaseUrl.concat("body/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);
						}
					}else
					{
						cal1.add(Calendar.DATE, -6);
						lastAccessDate = sdf.format(cal1.getTime());
						bodyWeightDataUrl = fitBitBaseUrl.concat("body/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);
					}
					
					
					if(flag == 1)
					{
						ResponseEntity<String> goalResponse = restTemplate.exchange(bodyWeightGoalUrl, HttpMethod.GET, entity, String.class);
						JSONObject jsonObject1 = new JSONObject(goalResponse.getBody());
						
						if(jsonObject1.getJSONObject("goal").length()>0)
						{
							if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)))
							{
							  if(jsonObject1.getJSONObject("goal").has("fat"))
								{
								  goalValue = jsonObject1.getJSONObject("goal").getDouble("fat");
								}		
							}

							if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WEIGHT)))
							{
							   if(jsonObject1.getJSONObject("goal").has("weight"))
								{
								  goalValue = jsonObject1.getJSONObject("goal").getDouble("weight");
							    }		
							}
						}
					}
					
					dataResponse = restTemplate.exchange(bodyWeightDataUrl, HttpMethod.GET, entity, String.class);
					JSONObject jsonObject = new JSONObject(dataResponse.getBody());
					JSONArray data = jsonObject.getJSONArray(jsonObjectKey);
					
					for (int i = 0; i < data.length(); i++) {
	                   HealthDataDTO healthDataDTO = new HealthDataDTO();
	                   healthDataDTO.setHealthTime(data.getJSONObject(i).getString("dateTime"));
	                   value  = data.getJSONObject(i).getDouble("value");
	                   healthDataDTO.setHealthValue(value);
	                   healthDataDTO.setGoalValue(goalValue);
	                   healthDataDTO.setWidget(widgetDTO);
	                   healthDataDtoList.add(healthDataDTO);
					}
				}
			
			}
			catch(Exception exception)
			{  
				logger.error(exception.getMessage());
				throw exception;
			}
			return healthDataDtoList;
	 	}


	 public List<HealthDataDTO> getSleepData(List<WidgetDTO> widgetDtoList,FitBitAccountDTO fiBitAccountDTO) throws ParseException
	 	{
		    String sleepDataUrl;
		    String daysJson = ".json";
		    String jsonObjectKey = null;
		    String lastAccessDate ; 
	 	    HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer  " +fiBitAccountDTO.getFitbitAccessToken());
			HttpEntity<String> entity = new HttpEntity<>(headers);
			ResponseEntity<String> dataResponse;
			RestTemplate restTemplate = new RestTemplate();
			
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(fiBitAccountDTO.getFitbitUserTimeZone()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setTimeZone(TimeZone.getTimeZone(fiBitAccountDTO.getFitbitUserTimeZone()));
			String currentDate = sdf.format(cal.getTime());

			Double value;
			Double goalValue = Double.valueOf(0);
			String sourcePath = null;
			
			String sleepGoalUrl = fitBitBaseUrl.concat("sleep/goal.json");
			ResponseEntity<String> goalResponse = restTemplate.exchange(sleepGoalUrl, HttpMethod.GET, entity, String.class);
			JSONObject jsonObject1 = new JSONObject(goalResponse.getBody());
			if(jsonObject1.getJSONObject("goal").has("minDuration"))
			{
				Double minutes = jsonObject1.getJSONObject("goal").getDouble("minDuration");
				goalValue = minutes;
			}
			
			List<HealthDataDTO> healthDataDtoList = new ArrayList<>();
			try
			{
				for (WidgetDTO widgetDTO : widgetDtoList) {
					
					Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(fiBitAccountDTO.getFitbitUserTimeZone()));
					
					if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY))){
						sourcePath = ResourceManager.getProperty(MINUTEASSLEEP);
						jsonObjectKey = "sleep-minutesAsleep";
					}else if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY))) {
						sourcePath = ResourceManager.getProperty(TIMEINBED);
						jsonObjectKey = "sleep-timeInBed";
					}
					
					HealthDataDTO healthDataDTO1 = fitbitDao.getFitBitLastAccessDate(widgetDTO,fiBitAccountDTO);
					if(healthDataDTO1!=null){
						
						lastAccessDate = healthDataDTO1.getHealthTime();
						Date todaysDate = sdf.parse(currentDate);
						Date date = sdf.parse(lastAccessDate);
						long diffInMilies= todaysDate.getTime()-date.getTime();
						long days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);
				    if(days<=6)
						{
							fitbitDao.removeHealthKitDataByDate(fiBitAccountDTO, widgetDtoList, lastAccessDate);
							sleepDataUrl = fitBitBaseUrl.concat("sleep/"+sourcePath+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);	
						}else
						{
							cal1.add(Calendar.DATE, -6);
							lastAccessDate = sdf.format(cal1.getTime());
							sleepDataUrl = fitBitBaseUrl.concat("sleep/"+sourcePath+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);
						}
					}else{
						cal1.add(Calendar.DATE, -6);
						lastAccessDate = sdf.format(cal1.getTime());
						sleepDataUrl = fitBitBaseUrl.concat("sleep/"+sourcePath+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(daysJson);
					}
					
					dataResponse = restTemplate.exchange(sleepDataUrl, HttpMethod.GET, entity, String.class);
					JSONObject jsonObject = new JSONObject(dataResponse.getBody());
					JSONArray data = jsonObject.getJSONArray(jsonObjectKey);
					
					for (int i = 0; i < data.length(); i++) {
	                   HealthDataDTO healthDataDTO = new HealthDataDTO();
	                   healthDataDTO.setHealthTime(data.getJSONObject(i).getString("dateTime"));
	                   value  = data.getJSONObject(i).getDouble("value");
	                   healthDataDTO.setHealthValue(value);
	                   healthDataDTO.setGoalValue(goalValue);
	                   healthDataDTO.setWidget(widgetDTO);
	                   healthDataDtoList.add(healthDataDTO);
					}
				}
			
			}catch(Exception exception)
			{  
				logger.error(exception.getMessage());
				throw exception;
			}
			return healthDataDtoList;

	 	}
	
public List<HealthDataDTO> getActivityData(List<WidgetDTO> widgetDtoList,FitBitAccountDTO fitBitAccountDTO) throws ParseException
	{
	    String activityDataUrl;
	    String jsonObjectKey = null;
	    String lastAccessDate ; 
	    HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer  " +fitBitAccountDTO.getFitbitAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> dataResponse;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
		String currentDate = sdf.format(cal.getTime());
		
		Double value = (double) 0;
		Double goalValue = (double) 0;

		List<HealthDataDTO> healthDataDtoList = new ArrayList<>();
		try
		{
			RestTemplate restTemplate = new RestTemplate();
			String activityGoalUrl = fitBitBaseUrl.concat("activities/goals/daily.json");
			ResponseEntity<String> goalResponse = restTemplate.exchange(activityGoalUrl, HttpMethod.GET, entity, String.class);
			JSONObject jsonObject1 = new JSONObject(goalResponse.getBody());
			
			for (WidgetDTO widgetDTO : widgetDtoList) {
				
				Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));

				ArrayList<WidgetDTO> widgetDTOList = new ArrayList<>();
				if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				 {
					jsonObjectKey = "activities-activityCalories";
					
					
				 }
				else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				{
					jsonObjectKey = "activities-calories";
					if(jsonObject1.getJSONObject("goals").has("caloriesOut"))
					{
						goalValue = jsonObject1.getJSONObject("goals").getDouble("caloriesOut");
					}
				}
				else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				{
					jsonObjectKey = "activities-caloriesBMR";
				}
				else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_DISTANCE)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				{
					jsonObjectKey = "activities-distance";
					if(jsonObject1.getJSONObject("goals").has("distance"))
					{
						goalValue = jsonObject1.getJSONObject("goals").getDouble("distance");
					}
				}
				else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_STEP)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY)))
				{
					jsonObjectKey = "activities-steps";
					if(jsonObject1.getJSONObject("goals").has("steps"))
					{
						goalValue = jsonObject1.getJSONObject("goals").getDouble("steps");
					}
				}
				
				HealthDataDTO healthDataDTO1 = fitbitDao.getFitBitLastAccessDate(widgetDTO,fitBitAccountDTO);
				if(healthDataDTO1!=null){
					lastAccessDate = healthDataDTO1.getHealthTime();
						Date todaysDate = sdf.parse(currentDate);
						Date date = sdf.parse(lastAccessDate);
						long diffInMilies= todaysDate.getTime()-date.getTime();
						long days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);

						if(days<=6)
						{
							widgetDTOList.add(widgetDTO);
							fitbitDao.removeHealthKitDataByDate(fitBitAccountDTO, widgetDTOList, lastAccessDate);
							activityDataUrl = fitBitBaseUrl.concat("activities/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(ResourceManager.getProperty(JSONFORMAT_EXTENSION));	
						}else{
							cal1.add(Calendar.DATE, -6);
							lastAccessDate = sdf.format(cal1.getTime());
							activityDataUrl = fitBitBaseUrl.concat("activities/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(ResourceManager.getProperty(JSONFORMAT_EXTENSION));
						}
				}else{
					cal1.add(Calendar.DATE, -6);
					lastAccessDate = sdf.format(cal1.getTime());
					activityDataUrl = fitBitBaseUrl.concat("activities/"+widgetDTO.getType()+"/date/").concat(lastAccessDate).concat("/"+currentDate).concat(ResourceManager.getProperty(JSONFORMAT_EXTENSION));
				}
				
				dataResponse = restTemplate.exchange(activityDataUrl, HttpMethod.GET, entity, String.class);
				JSONObject jsonObject = new JSONObject(dataResponse.getBody());
				JSONArray data = jsonObject.getJSONArray(jsonObjectKey);
				
				for (int i = 0; i < data.length(); i++) {
                HealthDataDTO healthDataDTO = new HealthDataDTO();
                healthDataDTO.setHealthTime(data.getJSONObject(i).getString("dateTime"));
                value  = data.getJSONObject(i).getDouble("value");
                healthDataDTO.setHealthValue(value);
                healthDataDTO.setGoalValue(goalValue);
                healthDataDTO.setWidget(widgetDTO);
                healthDataDtoList.add(healthDataDTO);
				}
			}
		}catch(Exception exception)
		{  
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthDataDtoList;
	}


public List<HealthDataDTO> getNeutritionData(List<WidgetDTO> widgetDtoList,FitBitAccountDTO fitBitAccountDTO) throws ParseException
	{
		String neutritionDataUrl = "" ; 
	    HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer  " +fitBitAccountDTO.getFitbitAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		List<HealthDataDTO> healthDataDtoList = new ArrayList<>();
		ResponseEntity<String> dataResponse;
		RestTemplate restTemplate = new RestTemplate();
		
		Double caloriesGoal = (double) 0;
		Double fatGoal = (double) 0;
		Double carbGoal = (double) 0;
		Double fiberGoal = (double) 0;
		Double proteinGoal = (double) 0;
		Double sodiumGoal = (double) 0;
		Double waterGoal = (double) 0;
		String neutritionGoalUrl = fitBitBaseUrl.concat("foods/log/goal.json");
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone(fitBitAccountDTO.getFitbitUserTimeZone()));
		String currentDate = sdf.format(cal.getTime());

		
		if(!widgetDtoList.isEmpty())
		{
			List<String> subCategoryList = new ArrayList<>();
			subCategoryList.add(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY));
			HealthDataDTO healthDataDTO1 = fitbitDao.getFitBitLastAccessDate(widgetDtoList.get(0),fitBitAccountDTO);
			fitbitDao.removeHealthKitDataByDate(fitBitAccountDTO, widgetDtoList, currentDate);
			String lastAccessDate;
			long days;
			try
			{
				if(healthDataDTO1!=null)
				{
					    lastAccessDate = healthDataDTO1.getHealthTime();
						Date todaysDate = sdf.parse(currentDate);
						Date lastDate = sdf.parse(lastAccessDate);
						long diffInMilies= todaysDate.getTime()-lastDate.getTime();
						days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);
				   	   if(days>6)
						{
								days = 7;
						}else
						{
							if(days == 0)
							{
								days = 1;
							}
						}
				}else
				{
					days = 7;
				}
				ResponseEntity<String> goalResponse = restTemplate.exchange(neutritionGoalUrl, HttpMethod.GET, entity, String.class);
				JSONObject jsonObject1 = new JSONObject(goalResponse.getBody());
				if(jsonObject1.length()>0)
				{
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_CALORIES)))
					{
					   caloriesGoal = jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_CALORIES));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_FAT)))
					{
						fatGoal = jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_FAT));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_CARB)))
					{
						carbGoal =jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_CARB));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_FIBER)))
					{
						fiberGoal =jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_FIBER));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_PROTEIN)))
					{
						proteinGoal = jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_PROTEIN));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_SODIUM)))
					{
						sodiumGoal = jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_SODIUM));
					}
					if(jsonObject1.getJSONObject("goals").has(ResourceManager.getProperty(WIDGET_WATER)))
					{
						waterGoal = jsonObject1.getJSONObject("goals").getDouble(ResourceManager.getProperty(WIDGET_WATER));
					}	
				}
				
				for (int i = 0; i < days; i++) {
					String date = sdf.format(cal.getTime());	
					neutritionDataUrl = fitBitBaseUrl.concat("foods/log/date/").concat(date).concat(ResourceManager.getProperty(JSONFORMAT_EXTENSION));
					dataResponse = restTemplate.exchange(neutritionDataUrl, HttpMethod.GET, entity, String.class);
					JSONObject jsonObject = new JSONObject(dataResponse.getBody());
					JSONObject neutritionsData = jsonObject.getJSONObject("summary");
					
					for (WidgetDTO widgetDTO : widgetDtoList) {
						Double value = (double) 0;
						HealthDataDTO healthDataDTO = new HealthDataDTO();
						healthDataDTO.setWidget(widgetDTO);
						healthDataDTO.setHealthTime(date);
				        
						if (widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FAT)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						 {
						  value = neutritionsData.getDouble("fat");
						  healthDataDTO.setGoalValue(fatGoal);
						 }
						else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CALORIES)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("calories");
							healthDataDTO.setGoalValue(caloriesGoal);
						}
						else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_CARB)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("carbs");
							healthDataDTO.setGoalValue(carbGoal);
						}
						else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_FIBER)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("fiber");
							healthDataDTO.setGoalValue(fiberGoal);
						}
						else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_PROTEIN)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("protein");
							healthDataDTO.setGoalValue(proteinGoal);
						}
						else if( widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_SODIUM)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("sodium");
							healthDataDTO.setGoalValue(sodiumGoal);
						}
						else if(widgetDTO.getType().equals(ResourceManager.getProperty(WIDGET_WATER)) && widgetDTO.getSubCategory().equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY)))
						{
							value = neutritionsData.getDouble("water");
							healthDataDTO.setGoalValue(waterGoal);
						}
						healthDataDTO.setHealthValue(value);
						 healthDataDtoList.add(healthDataDTO);
					}	
					cal.add(Calendar.DATE  , -1);
				}
			}catch(Exception exception)
			{
				logger.error(exception);
				throw exception;
			}
		}
			return healthDataDtoList;
	}

@Override
public FitBitAccountModel getFitbitProfileDetail(String fitbitUserId, String version) throws MangoMirrorException {
	
	ResponseEntity<String> dataResponse;
	HttpHeaders headers1 = new HttpHeaders();
	FitBitAccountDTO fitBitAccountDTO = fitbitDao.getFitBitAccountByUserId(fitbitUserId);
	try
	{
		headers1.set("Content-Type", MediaType.APPLICATION_JSON);
		headers1.set("Authorization", "Bearer  " +fitBitAccountDTO.getFitbitAccessToken());
		HttpEntity<String> entity1 = new HttpEntity<>(headers1);
		RestTemplate restTemplate = new RestTemplate();
		String profileUrl = fitBitBaseUrl.concat("profile.json");
		dataResponse = restTemplate.exchange(profileUrl, HttpMethod.GET, entity1, String.class);
		JSONObject jsonObject = new JSONObject(dataResponse.getBody());		   
		
		FitBitAccountModel fitBitAccountModel = new FitBitAccountModel();
		JSONObject userProfileDetail =  jsonObject.getJSONObject("user");
		
		fitBitAccountModel.setFitbitUserTimeZone(userProfileDetail.getString("timezone"));
		fitBitAccountModel.setFitbitUserName(userProfileDetail.getString("fullName"));
		return fitBitAccountModel;
	
	}catch(Exception exception)
	{
		logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION) +" "+exception);
		throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
	}
  }
}
