package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_V1;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.STATUS_ON;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TITLE_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TWITTERCREDENTIALSTATUS_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TWO_DECIMAL_FLOATING_POINT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BETA_USER_TIME_DELAY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_GENERAL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_NOT_REGISTER_TO_MIRROR_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_ROLE_NOT_SPECIFIED_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALENDAR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALORIES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALORIESBMR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CARB;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CLOCK;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DAILY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DIATRY_CHOLESTEROL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DIATRY_FATTOTAL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DIATRY_SUGAR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DISTANCE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_FAT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_FIBER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_FLIGHT_CLIMB;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_HEART_RATE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_HOURLEY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_PROTEIN;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_QUOTES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_SODIUM;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_STEP;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_STICKYNOTES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_TOTAL_SLEEPMINUTE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_TOTAL_TIMEINBED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_TWITTER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WALKINGRUNNING_DISTANCE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WATER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_WEIGHT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VERSION_BETA;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.VITAL_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_24HOUR_WEATHER_FORECAST;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_5DAY_WEATHER_FORECAST;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_ACTIVITY_CALORIES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_BLOOD_GLUCOSE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_BLOOD_PRESSURE_DIASTOLIC;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_BLOOD_PRESSURE_SYSTOLIC;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_BMI;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.ACTIVITY_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.AFTERNOON_WISH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.AUTHOR_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BETA_USER_DEFAULT_EMAILID;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BETA_USER_DEFAULT_MIRRORID;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BETA_USER_FITBIT_ID;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BODY_AND_WEIGHT_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.CALENDARTITLE_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_FILLCOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_POINTCOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_POINTSTROKECOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATA_STROKECOLOR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DATEFORMATID_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_MULTIPLIER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEVICEID_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EVENING_WISH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.EVENTS_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FITBIT_MASTERCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.GOALVALUE_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_DISTANCE_KILOMETER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_DISTANCE_MILES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_WATER_FLOZ;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HEALTH_WEIGHT_POUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.HELLO_WISH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KEY_DATA;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KG_TO_POUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.KM_TO_MILES;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MAXVALUE_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MINVALUE_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_NOT_SPECIFIED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.ML_TO_FLOZ;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MORNING_WISH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NUTRITIONS_SUBCATEGORY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PREVIEWSTATUS_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.QUOTES_CATEGORY_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.QUOTES_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.QUOTES_LENGTH_LABEL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SLEEP_SUBCATEGORY;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.FitBitDao;
import com.mindbowser.homeDisplay.dao.SmartMirrorDao;
import com.mindbowser.homeDisplay.dao.UserDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDaoImpl;
import com.mindbowser.homeDisplay.dao.WidgetDao;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.StickyNotesDTO;
import com.mindbowser.homeDisplay.dto.UserCalendarDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.BetaUserCalendarListModel;
import com.mindbowser.homeDisplay.model.BetaUserMirrorList;
import com.mindbowser.homeDisplay.model.CalendarFormatModel;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthDataTimeZoneModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.MirrorPageModel;
import com.mindbowser.homeDisplay.model.QuotesCategoryModel;
import com.mindbowser.homeDisplay.model.QuotesLengthModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.model.WidgetData;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.util.AsymmetricCryptography;
import com.mindbowser.homeDisplay.util.BetaUserThreadSchedular;
import com.mindbowser.homeDisplay.util.KMSUtil;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.MirrorPageDozerHelper;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.UserCalenderDozerHelper;
import com.mindbowser.homeDisplay.util.WidgetSettingDozerHelper;



public class BetaUserServiceImpl implements BetaUserService {

	private static Logger logger = Logger.getLogger(BetaUserServiceImpl.class);
	private Random randomGenerator;
	public static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(15); // no
	static ScheduledFuture<?> t,t1;
	public static ConcurrentHashMap<String, ScheduledFuture<?>> betaMap = new ConcurrentHashMap<>();
	
	@Autowired
	private FitBitService fitbitService;
	
	@Autowired
	private Mapper dozerMapper;
	
	@Autowired
	private FitBitDao fitbitDao;
	
	@Autowired
	private SmartMirrorDao smartMirrorDao;
	
	@Autowired
	private WidgetService widgetService;
	
	@Autowired
	private UserMirrorDaoImpl userMirrorDao;
	
	@Autowired
	private WidgetDao widgetDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private KMSUtil kmsUtil;
	
	@Value("${aws_access_key_id}")
	private String accessKeyId;

	@Value("${aws_secret_key_id}")
	private String secretAccessKey;
	
	@Value("${s3_betauser_bucket_name}")
	private String betaUserBucketName;

	@Value("${s3_betausermirror_jsonfile}")
	private String betauserFileName;
		
	@Autowired
	private AsymmetricCryptography asymmetricCryptography;
	
	@Autowired
	private SocketService socketService;

	@Override
	public void saveBetaUserData(UserDTO userDTO) throws MangoMirrorException {

		ObjectMapper objectMapper = new ObjectMapper();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int i;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		try {
			
			byte[] jsonHealthKitData = IOUtils.toByteArray(classloader.getResourceAsStream("betaUsersHealthData.json"));
			HealthWidgetModel healthWidgetModel = objectMapper.readValue(jsonHealthKitData, HealthWidgetModel.class);
			Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			cal1.add(Calendar.DAY_OF_YEAR, -6);
			df.setTimeZone(cal1.getTimeZone());
			i=7;
			if(!healthWidgetModel.getHealthDataList().isEmpty())
			{
				for (HealthDataModel healthDataModel : healthWidgetModel.getHealthDataList()) {
					String date = df.format(cal1.getTime());
					healthDataModel.setHealthTime(date);
					
					cal1.add(Calendar.DAY_OF_YEAR, 1);
					 i--;
					if(i == 0)
					{
						cal1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
						cal1.add(Calendar.DAY_OF_YEAR, -6);
						i=7;
					}
				}
		
				HealthDataDTO healthDataDTO = new HealthDataDTO();
				healthDataDTO.setUser(userDTO);
				fitbitService.saveHealthKitData(healthDataDTO,healthWidgetModel,ResourceManager.getProperty(VERSION_BETA));
			}
			
			/*save calendar data*/
			byte[] jsonCalendarData = IOUtils.toByteArray(classloader.getResourceAsStream("betaUsersCalendarData.json"));
			BetaUserCalendarListModel betaUserCalendarListModel = objectMapper.readValue(jsonCalendarData, BetaUserCalendarListModel.class);
			List<UserCalendarModel> userCalendarList = betaUserCalendarListModel.getUserCalendarList();
	
			String oldEventStartDateString,newEventStartDateString ;
			String oldEventEndDateString,newEventEndDateString;
			String oldEventOccuranceDateString,newEventOccuranceDateString ;
			if(!userCalendarList.isEmpty())
			{
				SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				dateTimeFormatter.setTimeZone(cal.getTimeZone());
				df.setTimeZone(cal.getTimeZone());
				String currentDate = df.format(cal.getTime());
				long startDateDiffMilli = 0;
				i=0;
				for (UserCalendarModel userCalendarModel : userCalendarList) {
					
					oldEventStartDateString = userCalendarModel.getStartDate();
					Date oldEventStartDate = df.parse(oldEventStartDateString);
					if(i<1)
					{
						Date todaysDate = df.parse(currentDate);
						startDateDiffMilli= todaysDate.getTime()-oldEventStartDate.getTime();
						i++;
					}
					
					if(userCalendarModel.getCalendarType().equals(ResourceManager.getProperty(WIDGET_CALENDAR)))
					{
						
						oldEventEndDateString = userCalendarModel.getEndDate();
						oldEventOccuranceDateString = userCalendarModel.getOccuranceDate();
						
						Date oldEventEndDate = df.parse(oldEventStartDateString);
						Date oldEventOccuranceDate = df.parse(oldEventStartDateString);
						Long startDateDiff = TimeUnit.DAYS.convert(startDateDiffMilli, TimeUnit.MILLISECONDS);
						
						cal.setTime(oldEventStartDate);
						cal.add(Calendar.DATE, startDateDiff.intValue());
						newEventStartDateString = df.format(cal.getTime())+oldEventStartDateString.substring(10);
						
						cal.setTime(oldEventEndDate);
						cal.add(Calendar.DATE, startDateDiff.intValue());
						newEventEndDateString = df.format(cal.getTime())+oldEventEndDateString.substring(10);
						
						cal.setTime(oldEventOccuranceDate);
						cal.add(Calendar.DATE, startDateDiff.intValue());
						newEventOccuranceDateString = df.format(cal.getTime())+oldEventOccuranceDateString.substring(10);
						
						userCalendarModel.setStartDate(newEventStartDateString);
						userCalendarModel.setEndDate(newEventEndDateString);
						userCalendarModel.setOccuranceDate(newEventOccuranceDateString);
					}else
					{
						oldEventStartDateString = userCalendarModel.getStartDate();
						Date todaysDate = df.parse(currentDate);
						Long startDateDiff = TimeUnit.DAYS.convert(startDateDiffMilli, TimeUnit.MILLISECONDS);

						cal.setTime(oldEventStartDate);
						cal.add(Calendar.DATE, startDateDiff.intValue());
						newEventStartDateString = df.format(cal.getTime())+oldEventStartDateString.substring(10);
				
						userCalendarModel.setStartDate(newEventStartDateString);
					}
				}
				List<UserCalendarDTO> userCalendarDtoList = UserCalenderDozerHelper.map(dozerMapper, userCalendarList,UserCalendarDTO.class);
				widgetDao.saveCalenderData(userDTO.getId(), userCalendarDtoList);
			}
			
		} catch (Exception exception) {
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}
	
	
	
	public Map<String, Object> updateWidgetSetting(HttpServletRequest request,
			List<GenericWidgetSettingModel> genericWidgetSettingModelList, String version)
			throws MangoMirrorException, MangoMirrorException {
		
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		Map<String, Object> updatedStatus = new HashMap<>();
		try {
			if (genericWidgetSettingModelList.get(0).getUserMirror().getMirror() != null) {
				int mirrorId = genericWidgetSettingModelList.get(0).getUserMirror().getMirror().getId();
				if (mirrorId > 0) {
					if (genericWidgetSettingModelList.get(0).getUserMirror().getUserRole() != null) {
						/* Check user relation with mirror is existed or not */
						UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), mirrorId,
								genericWidgetSettingModelList.get(0).getUserMirror().getUserRole(),
								Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
						
						if (userMirrorDTO != null) {

							if(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID).equals(userMirrorDTO.getMirror().getDeviceId()) 
									   && (ResourceManager.getProperty(USER_GENERAL).equals(userMirrorDTO.getUserRole())))
									{
										version = ResourceManager.getProperty(VERSION_V1);
										updatedStatus = widgetService.updateWidgetSetting(request, genericWidgetSettingModelList,version);
									}else
									{
										if (genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus() != null && 
												userMirrorDTO.getClockMessageStatus() != genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus()) 
										{
											userMirrorDTO.setClockMessageStatus(genericWidgetSettingModelList.get(0).getUserMirror().getClockMessageStatus());
										}	
										
										List<WidgetSettingModel> widgetSettingModelList = new ArrayList<>();
										List<MirrorPageModel> mirrorPageList = new ArrayList<>(); 
										int i=0;
										for (GenericWidgetSettingModel genericWidgetSettingModel : genericWidgetSettingModelList) {
											i++;
											MirrorPageModel mirrorPageModel = genericWidgetSettingModel.getMirrorPage();
											mirrorPageModel.setId(i);
											mirrorPageList.add(mirrorPageModel);
											
											if (!genericWidgetSettingModel.getWidgets().isEmpty()) {
												for (WidgetSettingModel widgetSettingModel : genericWidgetSettingModel.getWidgets()) {
												widgetSettingModel.setMirrorPage(mirrorPageModel);
												widgetSettingModelList.add(widgetSettingModel);
												}
											}
										}
										
										List<WidgetSettingDTO> widgetSettingDTOs = WidgetSettingDozerHelper.MapModelToDto(dozerMapper, widgetSettingModelList, WidgetSettingDTO.class);
										int randomId=0;
										for (WidgetSettingDTO widgetSettingDTO : widgetSettingDTOs) {
											WidgetDTO widgetDTO = widgetDao.getWidgetById(widgetSettingDTO.getWidget().getId());
											if(widgetDTO.getId() == 49 && widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
											{
												StickyNotesDTO notesDTO = new StickyNotesDTO();
												notesDTO.setId(randomId);
												widgetSettingDTO.setId(randomId);
												randomId ++;
											}
											widgetSettingDTO.setWidget(widgetDTO);	
										}

											if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
												JSONObject jsonObject = new JSONObject();
											    String iconFormat = "svg";
												List<WidgetData> widgetData = addSocketWidgetData(widgetSettingDTOs, 480, 800, userMirrorDTO, iconFormat, version,mirrorPageList);
												SocketResponce socketResponce = new SocketResponce();
												socketResponce.setType("widgetList");
												socketResponce.setMessage("widgetList");
												
												ObjectMapper objectMapper = new ObjectMapper();
												String widgets = objectMapper.writeValueAsString(widgetData);
												socketResponce.setData(widgets);
												jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
												jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
												JSONArray array = new JSONArray();	
												array.put(jsonObject);
												socketService.broadcast(array.toString());
											}
											
										if (userMirrorDTO.getTwitter() != null) {
											updatedStatus.put(ResourceManager.getProperty(TWITTERCREDENTIALSTATUS_LABEL),
													userMirrorDTO.getTwitter().getTwitterCredentialStatus());
										}
										
										synchronized (BetaUserServiceImpl.betaMap) {
											
											int timeDelay = Integer.parseInt(ResourceManager.getProperty(BETA_USER_TIME_DELAY));
											
											if(betaMap.containsKey(userMirrorDTO.getMirror().getDeviceId()))
											{
												t = betaMap.get(userMirrorDTO.getMirror().getDeviceId());
												t.cancel(false);
											    Iterator it = betaMap.entrySet().iterator();
												while (it.hasNext())
												{
												   Entry item = (Entry) it.next();
												   if(item.getKey().equals(userMirrorDTO.getMirror().getDeviceId()))
												   {
													   System.out.println(item.getKey());
													   betaMap.remove(item.getKey());   
												   }
												}
												t1 = executor.schedule(new BetaUserThreadSchedular(userMirrorDTO.getMirror().getDeviceId()), timeDelay, TimeUnit.SECONDS);
												betaMap.put(userMirrorDTO.getMirror().getDeviceId(), t1);
										}
										else
										{
											t = executor.schedule(new BetaUserThreadSchedular(userMirrorDTO.getMirror().getDeviceId()), timeDelay , TimeUnit.SECONDS);
											betaMap.put(userMirrorDTO.getMirror().getDeviceId(), t);
										}
									}
									}
						} else {
							throw new MangoMirrorException(ResourceManager.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION,
									null, NOT_FOUND, locale));
						}
					} else {
						throw new MangoMirrorException(
								ResourceManager.getMessage(USER_ROLE_NOT_SPECIFIED_EXCEPTION, null, NOT_FOUND, locale));
					}
				} else {
					throw new MangoMirrorException(
							ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, locale));
				}
			} else {
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_SPECIFIED, null, NOT_FOUND, locale));
			}

		} 
		catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return updatedStatus;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<WidgetData> addSocketWidgetData(List<WidgetSettingDTO> widgetSettingData, int deviceWidth, int deviceHeight,
			UserMirrorDTO userMirrorDTO, String iconFormat, String version,List<MirrorPageModel> mirrorPageList)throws MangoMirrorException {
		int userId = userMirrorDTO.getUser().getId();
		double oldDeviceHeight;
		double oldDeviceWidth;
		double minHeight;
		double xPos;
		double yPos;
		double widgetWidth;
		double widgetHeight;
		double newXPos;
		double newYPos;
		double newWidgetWidth;
		double newWidgetHeight;
		double newMinHeight;
		double newMinWidth;
		Boolean pinnedWidgetStatus = false;
		Boolean checkPinnedStatus = false;
		
		WidgetData pinnedWidget = new WidgetData();
		ArrayList groups = new ArrayList();
	//	List<MirrorPageDTO> mirrorPageDtoList = widgetDao.getMirrorPage(userMirrorDTO.getId());
		LocationDTO locationDTO = userMirrorDTO.getLocation();
		FitBitAccountModel fitBitAccountModel = null;
		if (userMirrorDTO.getFitbitAccount() != null) {
			fitBitAccountModel = dozerMapper.map(userMirrorDTO.getFitbitAccount(), FitBitAccountModel.class);
		}
		UserModel userModel = dozerMapper.map(userMirrorDTO.getUser(), UserModel.class);
		try
		{
			if (locationDTO != null) {
				widgetService.checkAndUpdateWeatherData(locationDTO);
			}

			for (MirrorPageModel mirrorPageModel : mirrorPageList) {
				Map<String, Object> pages = new HashMap<>();
				pages.put("pageNumber", mirrorPageModel.getPageNumber());
				pages.put("pageId", mirrorPageModel.getId());
				ArrayList widgetList = new ArrayList();

				for (WidgetSettingDTO widgetSettingDTO : widgetSettingData) {
					widgetHeight = widgetSettingDTO.getHeight();
					widgetWidth = widgetSettingDTO.getWidth();
					xPos = 1;
					yPos = widgetSettingDTO.getyPos();
					minHeight = widgetSettingDTO.getMinHeight();
					oldDeviceHeight = widgetSettingDTO.getDeviceHeight();
					oldDeviceWidth = widgetSettingDTO.getDeviceWidth();
					newXPos = (xPos * deviceWidth) / oldDeviceWidth;
					newYPos = (yPos * deviceHeight) / oldDeviceHeight;
					newWidgetWidth = (widgetWidth * deviceWidth) / oldDeviceWidth;
					newWidgetHeight = (widgetHeight * deviceHeight) / oldDeviceHeight;
					newMinHeight = (minHeight * deviceHeight) / oldDeviceHeight;
					newMinWidth = deviceWidth;
					newMinWidth = deviceWidth;
					WidgetData widgetData = new WidgetData();

					if (widgetSettingDTO.getMirrorPage().getId() == mirrorPageModel.getId()) {

						widgetData.setWidgetSettingId(widgetSettingDTO.getId());
						widgetData.setDeviceId(userMirrorDTO.getMirror().getDeviceId());
						widgetData.setContentId(widgetSettingDTO.getWidget().getId());
						widgetData.setContentType(widgetSettingDTO.getWidget().getType());
						widgetData.setHeight(newWidgetHeight);
						widgetData.setWidth(newWidgetWidth);
						widgetData.setxPos(newXPos);
						widgetData.setyPos(newYPos);
						widgetData.setStatus(widgetSettingDTO.getStatus());
						widgetData.setMinHeight(newMinHeight);
						widgetData.setMinWidth(newMinWidth);
						widgetData.setViewType(widgetSettingDTO.getViewType());
						widgetData.setWidgetMasterCategory(widgetSettingDTO.getWidget().getMasterCategory());
						widgetData.setWidgetSubCategory(widgetSettingDTO.getWidget().getSubCategory());
						
						if (!widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
							widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName());	
						}

						widgetData.setGraphType(widgetSettingDTO.getWidget().getGraphType());
						if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CLOCK))) {
							int minutes = userMirrorDTO.getTimeZone();
							int offMilisecond = minutes * 60 * 1000;
							Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
							long time = cal.getTimeInMillis() + offMilisecond;
							SimpleDateFormat sdf = new SimpleDateFormat("HH");
							cal.add(Calendar.MINUTE, minutes);
							sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
							String Hours = sdf.format(cal.getTime());
							int hours = Integer.parseInt(Hours);
							String greeting = "";
							String userName = "";
							Map<String, Object> data = new HashMap<>();
							
							if (hours >= 4 && hours < 12) {
								greeting = greeting.concat(ResourceManager.getProperty(MORNING_WISH));
							}
							if (hours >= 12 && hours < 16) {
								greeting = greeting.concat(ResourceManager.getProperty(AFTERNOON_WISH));
							}
							if (hours >= 16 && hours < 22) {
								greeting = greeting.concat(ResourceManager.getProperty(EVENING_WISH));
							}
							if (hours >= 22 || hours < 4) {
								greeting = greeting.concat(ResourceManager.getProperty(HELLO_WISH));
							}
							if (!userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
								if (userMirrorDTO.getUser().getName() != null) {
									
										File publicKeyFile = new File(System.getProperty("java.io.tmpdir")+"KeyPair/publicKey");
										PublicKey publicKey = asymmetricCryptography.getPublic(publicKeyFile);
										String emailId = asymmetricCryptography.decryptText(userMirrorDTO.getUser().getEmailId(), publicKey);
										String plainTextKey = kmsUtil.decryptKey(userMirrorDTO.getUser().getEncryptedPlainText(), emailId);
										userName  = kmsUtil.decryptData(plainTextKey, userMirrorDTO.getUser().getName());
										data.put("userName", userName);
								}
							}
							
							data.put("utcMiliSecond", Long.toString(time));
							data.put("greeting", greeting);
							data.put("clockMessageStatus", userMirrorDTO.getClockMessageStatus().toString());
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_TWITTER))) {
							Map<String, Object> data = new HashMap<String, Object>();
							if (userMirrorDTO.getTwitter() != null) {
								data.put("id", Integer.toString(userMirrorDTO.getTwitter().getId()));
								data.put("twitterOauthToken", userMirrorDTO.getTwitter().getTwitterOauthToken());
								data.put("twitterOauthTokenSecret",
										userMirrorDTO.getTwitter().getTwitterOauthTokenSecret());
								data.put(ResourceManager.getProperty(TWITTERCREDENTIALSTATUS_LABEL),
										userMirrorDTO.getTwitter().getTwitterOauthTokenSecret());
								data.put("timeLine", userMirrorDTO.getTwitter().getTimeLine());
								data.put("twitterFollowingUser", userMirrorDTO.getTwitter().getTwitterFollowingUser());
								data.put("screenName", userMirrorDTO.getTwitter().getScreenName());
							}
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_WEATHER))) {
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = widgetService.getCurrentWeatherData(userMirrorDTO, iconFormat);	
							}
							widgetData.setData(data);
						} else if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STEP))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_FAT))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_CALORIES))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DISTANCE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_CALORIESBMR))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CARB))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_FIBER))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_PROTEIN))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_SODIUM))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_BMI))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_WATER))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WALKINGRUNNING_DISTANCE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_SUGAR))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_GLUCOSE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_DIASTOLIC))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_BLOOD_PRESSURE_SYSTOLIC))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_HEART_RATE))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_FATTOTAL))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_FLIGHT_CLIMB))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DIATRY_CHOLESTEROL))
								|| widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
							
							
							List healthData = new ArrayList();
							Object goalValue = null;
							Object maxValue = null;
							Object minValue = null;
							Double multiplier = Double.parseDouble(ResourceManager.getProperty(DEFAULT_MULTIPLIER));
							String unit = null;
							String unitName = null;
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								HealthDataModel healthDataModel = new HealthDataModel();
								HealthDataTimeZoneModel timeZoneModel = new HealthDataTimeZoneModel();

								if (widgetSettingDTO.getWidget().getMasterCategory().equals(ResourceManager.getProperty(FITBIT_MASTERCATEGORY))) {
									if (fitBitAccountModel != null) {
										healthDataModel.setFitbit(fitBitAccountModel);
									} else {
										healthDataModel.setUser(userModel);
									}
								} else {
									healthDataModel.setUser(userModel);
								}

								timeZoneModel.setUseratimeZone(userMirrorDTO.getUser().getUserCurrentTimeZone());
								if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_DISTANCE))) {
									if (userMirrorDTO.getUser().getDistanceUnit()
											.equals(ResourceManager.getProperty(HEALTH_DISTANCE_MILES))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(KM_TO_MILES));
										unitName = userMirrorDTO.getUser().getDistanceUnit();
									} else if (userMirrorDTO.getUser().getDistanceUnit()
											.equals(ResourceManager.getProperty(HEALTH_DISTANCE_KILOMETER))) {
										unitName = userMirrorDTO.getUser().getDistanceUnit();
									}
								} else if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WEIGHT))) {
									unitName = userMirrorDTO.getUser().getWeightUnit();
									if (userMirrorDTO.getUser().getWeightUnit()
											.equals(ResourceManager.getProperty(HEALTH_WEIGHT_POUND))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(KG_TO_POUND));
									}
								} else if (widgetSettingDTO.getWidget().getType()
										.equals(ResourceManager.getProperty(WIDGET_WATER))) {
									unitName = userMirrorDTO.getUser().getWaterUnit();
									if (userMirrorDTO.getUser().getWaterUnit()
											.equals(ResourceManager.getProperty(HEALTH_WATER_FLOZ))) {
										multiplier = Double.parseDouble(ResourceManager.getProperty(ML_TO_FLOZ));
									}
								} 
								List<String> labelArray = new ArrayList<>();
								try {

									Map<String, Object> healthDataValues = widgetService.getHealthData(healthDataModel, multiplier,
											timeZoneModel, widgetSettingDTO.getWidget(), unitName);
									if (!healthDataValues.isEmpty()) {
										healthData = (List) healthDataValues.get(ResourceManager.getProperty(DATA_LABEL));
										labelArray = (List<String>) healthDataValues.get(ResourceManager.getProperty(LABEL));
										goalValue = healthDataValues.get(ResourceManager.getProperty(GOALVALUE_LABEL));
										maxValue = healthDataValues.get(ResourceManager.getProperty(MAXVALUE_LABEL));
										minValue = healthDataValues.get(ResourceManager.getProperty(MINVALUE_LABEL));
										unit = (String) healthDataValues.get("unit");
									}
								} catch (Exception exception) {
									logger.info(exception);
								}
								
								HashMap datasetObject = new HashMap();
								// barGraph property

								datasetObject.put(ResourceManager.getProperty(KEY_DATA), healthData);
								datasetObject.put(ResourceManager.getProperty(LABEL), "step data");

								if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(ACTIVITY_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(46,204,113,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(46,204,113,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(46,204,113,1)");

								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(SLEEP_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(52,152,219,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(52,152,219,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(52,152,219,1)");

								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(BODY_AND_WEIGHT_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(231,76,60,0)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(231,76,60,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(231,76,60,1)");
								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(NUTRITIONS_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(155,89,182,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(155,89,182,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(155,89,182,1)");
								} else if (widgetSettingDTO.getWidget().getSubCategory()
										.equals(ResourceManager.getProperty(VITAL_SUBCATEGORY))) {
									datasetObject.put(ResourceManager.getProperty(DATA_POINTSTROKECOLOR), "#fff");
									datasetObject.put(ResourceManager.getProperty(DATA_FILLCOLOR), "rgba(192,57,43,0)");
									datasetObject.put(ResourceManager.getProperty(DATA_POINTCOLOR), "rgba(192,57,43,1)");
									datasetObject.put(ResourceManager.getProperty(DATA_STROKECOLOR), "rgba(192,57,43,1)");
								}
								List datasetObjectValue = new ArrayList();
								datasetObjectValue.add(datasetObject);

								/* new code as per new design */

								int divider = 1000;
								String currentDateData = null;

								if(widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_TIMEINBED))
								|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_TOTAL_SLEEPMINUTE)))
								{
	 								   String totalTime = String.valueOf(healthData.get(6));
									   String hours = totalTime.substring(0,totalTime.indexOf('.'));
								       String minutes = totalTime.substring(totalTime.indexOf('.')+1);
								        if(hours.length() ==1)
								        { 
								        	StringBuilder hourStringBuilder = new StringBuilder();
								        	hours = hourStringBuilder.append("0").append(hours).toString(); 
								        }
								        if(minutes.length() ==1)
								        {  
								        	StringBuilder minStringBuilder = new StringBuilder();
								        	minutes = minStringBuilder.append(minutes).append("0").toString(); 
								        }
								        currentDateData = hours+"h : "+minutes+"m";
								}else
								{
									int index = healthData.size();
									
									if(index > 0)
									{
										if (healthData.get(index-1) instanceof Double) 
										{
											double currentDateValue =  (double) healthData.get(index-1);
											if (currentDateValue >= divider && (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CALORIES))
													|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_ACTIVITY_CALORIES))
													|| widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CALORIESBMR)))) 
												{
												double newCaloriesValue = currentDateValue / 1000;
												int newCaloriesValueInteger = Double.valueOf(newCaloriesValue).intValue();
												if (newCaloriesValue == newCaloriesValueInteger) {
													currentDateData =  Integer.toString(newCaloriesValueInteger);
											    } else {
											    	currentDateData =  Double.valueOf(String.format(ResourceManager.getProperty(TWO_DECIMAL_FLOATING_POINT), (newCaloriesValue))).toString();
											    }
												unit = "k".concat(unit);
											} else {
												int currentDateDataInInteger = Double.valueOf(currentDateValue).intValue();
												if (currentDateValue == currentDateDataInInteger) {
													currentDateData = Integer.toString(currentDateDataInInteger);
											    } else {
											    	currentDateData =  Double.toString(currentDateValue);
											    }
												
											}
										}
									}else
									{
										int currentDateDataInDouble = 0;
									    currentDateData = Double.toString(currentDateDataInDouble);
									}
								}
								
								HashMap<String, Object> data = new HashMap();
								data.put("datasets", datasetObjectValue);
								data.put("labels", labelArray);
								data.put("unit", unit);
								data.put("todaysData", currentDateData);
								data.put(ResourceManager.getProperty(GOALVALUE_LABEL), goalValue);
								data.put(ResourceManager.getProperty(MAXVALUE_LABEL), maxValue);
								data.put(ResourceManager.getProperty(MINVALUE_LABEL), minValue);
								widgetData.setData(data);	
							}
							
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_CALENDAR))) {

							HashMap event = new HashMap();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								Map<String, List<Map<String, Object>>> events = widgetService.getCalenderData(userId, userMirrorDTO);
								List<CalendarFormatModel> calendarFormatList = widgetService.calendarFormatList();
								event.put(ResourceManager.getProperty(EVENTS_LABEL), events);
								event.put("calendarFormatList", calendarFormatList);
								Map<String, String> calendarTitle = new HashMap<>();
								calendarTitle.put(ResourceManager.getProperty(DATEFORMATID_LABEL), Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
								calendarTitle.put(ResourceManager.getProperty(CALENDARTITLE_LABEL), userMirrorDTO.getCalendarFormat().getLabel());
								List<Map<String, String>> Title = new ArrayList<>();
								Title.add(calendarTitle);
								event.put(ResourceManager.getProperty(TITLE_LABEL), Title);	
							}
							widgetData.setData(event);
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_5DAY_WEATHER_FORECAST))) {
							
							Map<String, Object> data = new HashMap<>();
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = widgetService.getDailyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
									
							}
							widgetData.setData(data);
							
						}else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_24HOUR_WEATHER_FORECAST))) {
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data = widgetService.getHourlyWeatherData(userMirrorDTO, iconFormat,
										ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
									
							}
							widgetData.setData(data);
							
						} else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_QUOTES))) {
							Map<String, Object> data = new HashMap<>();
							HashMap quotesData = new HashMap();
							
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								List<QuotesCategoryModel> quotesCategoryDtoList = widgetService.getQuotesCategoryList();
								List<QuotesLengthModel> quotesLengthDtoList = widgetService.getQuotesCategoryLengthList();
								quotesData.put(ResourceManager.getProperty(QUOTES_CATEGORY_LABEL), quotesCategoryDtoList);
								quotesData.put(ResourceManager.getProperty(QUOTES_LENGTH_LABEL), quotesLengthDtoList);
								
								List<QuotesDTO> quotesList = widgetDao.getQuotesByUserMrrrorId(userMirrorDTO.getId());
								if(!quotesList.isEmpty())
								{
									randomGenerator = new Random();
									int index = randomGenerator.nextInt(quotesList.size());
							        QuotesDTO quotesDTO = quotesList.get(index);
									if (quotesDTO != null) {
										data.put(ResourceManager.getProperty(AUTHOR_LABEL), quotesDTO.getAuthor());
										data.put(ResourceManager.getProperty(QUOTES_LABEL), quotesDTO.getQuotes());
										data.put(ResourceManager.getProperty(QUOTES_LENGTH_LABEL), quotesDTO.getQuotesLength().getId());
										data.put(ResourceManager.getProperty(QUOTES_CATEGORY_LABEL), quotesDTO.getQuotesCategory().getId());
									}	
								}
							}
							quotesData.put(ResourceManager.getProperty(QUOTES_LABEL), data);
							widgetData.setData(quotesData);
						}
						
						else if (widgetSettingDTO.getWidget().getType()
								.equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))) {
							
							Map<String, Object> data = new HashMap<>();
							if(widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON)))
							{
								data.put("name", widgetSettingDTO.getWidget().getDisplayName()+ " : " +widgetSettingDTO.getStickyNotes().get(0).getName());
								data.put("htmlContent", widgetSettingDTO.getStickyNotes().get(0).getData());
								widgetData.setDisplayName(widgetSettingDTO.getWidget().getDisplayName()+ " : " +widgetSettingDTO.getStickyNotes().get(0).getName());
								widgetData.setData(data);	
							}
						}
						
						if(widgetSettingDTO.getPinned())
						{
							pinnedWidget = widgetData;
							pinnedWidgetStatus = true;
							checkPinnedStatus = true;
						}
						widgetList.add(widgetData);
					}
				}
				pages.put("widgets", widgetList);
				pages.put("delay", mirrorPageModel.getDelay());
				pages.put(ResourceManager.getProperty(PREVIEWSTATUS_LABEL), userMirrorDTO.getMirror().getMirrorPreview());
				pages.put("pinned", pinnedWidgetStatus);
				groups.add(pages);
				pinnedWidgetStatus = false;
			}			
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		
		if(checkPinnedStatus)
		{
			groups = widgetService.addPinnedWidget(pinnedWidget,groups);
		}
		
		return groups;
	}
	
	
	@Override
	public void saveBetaUserDataByThread() throws MangoMirrorException {
		
		AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
		AmazonS3Client s3Client = new AmazonS3Client(credentials);

		try {
		
			S3Object s3object = s3Client.getObject(new GetObjectRequest(betaUserBucketName, betauserFileName));
			BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
			String line;

			String jsonString="";
			while ((line = reader.readLine()) != null) {
				jsonString += line;
			}
			ObjectMapper mapper = new ObjectMapper();
			byte[] jsonBetaMirrorData = jsonString.getBytes();
			BetaUserMirrorList betaMirrorList = mapper.readValue(jsonBetaMirrorData, BetaUserMirrorList.class);
			List<String> mirrorList = betaMirrorList.getBetaUserMirrorList();
			
			if(mirrorList.contains(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID)))
			{
				mirrorList.remove(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID));
			}
			
			
			List<UserMirrorDTO> userMirrorDtoList = userMirrorDao.getUserMirrorDetailByMirrorList(mirrorList);
			FitBitAccountDTO fitBitAccountDTO = userMirrorDtoList.get(0).getFitbitAccount();
			WidgetDTO widgetDTO  = new WidgetDTO();
			widgetDTO.setId(9);
			HealthDataDTO healthDataDTO = fitbitDao.getFitBitLastAccessDate(widgetDTO, fitBitAccountDTO);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(cal.getTimeZone());
			String currentDate = sdf.format(cal.getTime());
			
			String lastAccessDate = healthDataDTO.getHealthTime();
			Date todaysDate = sdf.parse(currentDate);
			Date lastDate = sdf.parse(lastAccessDate);
			long diffInMilies= todaysDate.getTime()-lastDate.getTime();
			Long days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);
			//int daysDifference = days.intValue() + 1;
			if(days.intValue() > 0)
			{
				widgetDao.updateBetaUsersFitbitData(fitBitAccountDTO, days.intValue());
				List<Integer> userDTOList = new ArrayList<>();
				
				WidgetDTO widgetDTO2 = new WidgetDTO();
				widgetDTO2.setId(2);
				for (UserMirrorDTO userMirrorDTO : userMirrorDtoList) {
					
					if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
					{
						userDTOList.add(userMirrorDTO.getUser().getId());
						HealthDataDTO userHealthKitDTO = widgetDao.getHealthKitLastAccessDate(userMirrorDTO.getUser(),widgetDTO2);
						lastAccessDate = userHealthKitDTO.getHealthTime();
						lastDate = sdf.parse(lastAccessDate);
						diffInMilies= todaysDate.getTime()-lastDate.getTime();
						days = TimeUnit.DAYS.convert(diffInMilies, TimeUnit.MILLISECONDS);
						widgetDao.updateBetaUsersHealthKitData(userMirrorDTO.getUser(),days.intValue());
						widgetDao.updateBetaUsersCalendarData(userMirrorDTO.getUser(), 1);
					}
				}
			}
		} catch (Exception exception) {
			logger.info(exception);
		}
	}

	@Override
	public void betaUserDefaultSetting(String deviceId) throws MangoMirrorException {
		
		try
		{
			SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(deviceId);
			userMirrorDao.setOutOfRange(smartMirrorDTO.getId());
			UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorDTO.getId());
					
			List<WidgetDTO> widgetDtoList = widgetService.widgetListByVersion(ResourceManager.getProperty(VERSION_BETA));
			List<WidgetSettingDTO> widgetSettingDTOs = widgetDao.getWidgetSetting(userMirrorDTO, widgetDtoList);
			List<MirrorPageDTO> mirrorPageDtoList = widgetDao.getMirrorPage(userMirrorDTO.getId());
			List<MirrorPageModel> mirrorPageList = MirrorPageDozerHelper.mapDtotoModel(dozerMapper, mirrorPageDtoList, MirrorPageModel.class);
			
					if (userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL))) {
						JSONObject jsonObject = new JSONObject();
					    String iconFormat = "svg";
					    List<WidgetData> widgetData = addSocketWidgetData(widgetSettingDTOs, 480, 800, userMirrorDTO, iconFormat, ResourceManager.getProperty(VERSION_BETA) ,mirrorPageList);
						SocketResponce socketResponce = new SocketResponce();
						socketResponce.setType("widgetList");
						socketResponce.setMessage("widgetList");
						
						ObjectMapper objectMapper = new ObjectMapper();
						String widgets = objectMapper.writeValueAsString(widgetData);
						socketResponce.setData(widgets);
						jsonObject.put(ResourceManager.getProperty(DEVICEID_LABEL), userMirrorDTO.getMirror().getDeviceId());
						jsonObject.put(ResourceManager.getProperty(DATA_LABEL), objectMapper.writeValueAsString(socketResponce));
						JSONArray array = new JSONArray();	
						array.put(jsonObject);
						socketService.broadcast(array.toString());
					}
		}catch(Exception exception)
		{
			logger.info(exception);
		}
	}

	@Override
	public void updateFitbitDataFirstTime() throws MangoMirrorException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int i=1;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		try
		{
			
			/*save fitbit data*/
			byte[] jsonFitbitData = IOUtils.toByteArray(classloader.getResourceAsStream("betaUsersFitBitData.json"));
			HealthWidgetModel healthWidgetModel1 = objectMapper.readValue(jsonFitbitData, HealthWidgetModel.class);

			Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			df.setTimeZone(cal1.getTimeZone());
			cal1.add(Calendar.DAY_OF_YEAR, -6);
			
			i=7;
			if(!healthWidgetModel1.getHealthDataList().isEmpty())
			{
				for (HealthDataModel healthDataModel : healthWidgetModel1.getHealthDataList()) {
					String date = df.format(cal1.getTime());
					healthDataModel.setHealthTime(date);
					
					cal1.add(Calendar.DAY_OF_YEAR, 1);
					 i--;
					if(i == 0)
					{
						cal1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
						cal1.add(Calendar.DAY_OF_YEAR, -6);
						i=7;
					}
	               
				}
				
				FitBitAccountDTO fitBitAccountDTO = fitbitDao.getFitBitAccountByUserId(ResourceManager.getProperty(BETA_USER_FITBIT_ID));
				fitbitDao.deleteFitbitDataByFitBitUserId(fitBitAccountDTO.getId());
				HealthDataDTO healthDataDTO1 = new HealthDataDTO();
				healthDataDTO1.setFitbit(fitBitAccountDTO);
				fitbitService.saveHealthKitData(healthDataDTO1,healthWidgetModel1,ResourceManager.getProperty(VERSION_BETA));
			}
	
		}catch(Exception exception)
		{
			logger.info(exception);
		}
	}

	@Override
	public void updateAllBetaMirrorLayout(String version) throws MangoMirrorException {
		
		try
		{
			File privatefilepath = new File(System.getProperty("java.io.tmpdir") + "KeyPair/privateKey");
			PrivateKey privateKey = asymmetricCryptography.getPrivate(privatefilepath);
			String encrypted_EmailId =asymmetricCryptography.encryptText(ResourceManager.getProperty(BETA_USER_DEFAULT_EMAILID),privateKey);
			if (version.equals(ResourceManager.getProperty(VERSION_BETA))) {
				
				SmartMirrorDTO mirrorDTO2 = smartMirrorDao.getMirrorByDeviceId(ResourceManager.getProperty(BETA_USER_DEFAULT_MIRRORID));
				UserDTO userDTO2 = userDao.checkUser(encrypted_EmailId);
				UserMirrorDTO defaultUserMirrorDto = userMirrorDao.getUserMirror(userDTO2.getId(), mirrorDTO2.getId(),
						ResourceManager.getProperty(USER_GENERAL), 0);
				
				AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
				AmazonS3Client s3Client = new AmazonS3Client(credentials);
				S3Object s3object = s3Client.getObject(new GetObjectRequest(betaUserBucketName, betauserFileName));
				BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
				String line;
				String jsonString="";

				while ((line = reader.readLine()) != null) {
						jsonString += line;
					}
				ObjectMapper mapper = new ObjectMapper();
				byte[] jsonBetaMirrorData = jsonString.getBytes();
				BetaUserMirrorList betaMirrorList = mapper.readValue(jsonBetaMirrorData, BetaUserMirrorList.class);
				List<String> mirrorList = betaMirrorList.getBetaUserMirrorList();
				List<UserMirrorDTO> userMirrorDtoList = userMirrorDao.getUserMirrorDetailByMirrorList(mirrorList);
				widgetDao.deleteWidgetSetting(userMirrorDtoList);
				
				
				List<WidgetDTO> widgetDTOList = widgetService.widgetListByVersion(ResourceManager.getProperty(VERSION_BETA));
				List<WidgetSettingDTO> widgetSettingDTOs = widgetDao.getWidgetSetting(defaultUserMirrorDto, widgetDTOList);
				
				List<QuotesDTO> quotesList = widgetDao.getQuotesByUserMrrrorId(defaultUserMirrorDto.getId());
				for (UserMirrorDTO userMirrorDTO : userMirrorDtoList) {
				  if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
					{
					  widgetDao.deleteMirrorPage(userMirrorDTO.getId());
					  Map<Integer, MirrorPageDTO> pagelist = new HashMap<>();
				
						List<WidgetSettingDTO> updatedWidgetSettingDto = new ArrayList<>();
						for (WidgetSettingDTO widgetSettingDTO : widgetSettingDTOs) {
						
							WidgetSettingDTO widgetSettingDTO2 = new WidgetSettingDTO();
							
							widgetSettingDTO2.setHeight(widgetSettingDTO.getHeight());
							widgetSettingDTO2.setWidth(widgetSettingDTO.getWidth());
							widgetSettingDTO2.setxPos(widgetSettingDTO.getxPos());
							widgetSettingDTO2.setyPos(widgetSettingDTO.getyPos());
							widgetSettingDTO2.setStatus(widgetSettingDTO.getStatus());
							widgetSettingDTO2.setMinHeight(widgetSettingDTO.getMinHeight());
							widgetSettingDTO2.setMinWidth(widgetSettingDTO.getMinWidth());
							widgetSettingDTO2.setViewType(widgetSettingDTO.getViewType());
							widgetSettingDTO2.setPinned(widgetSettingDTO.getPinned());
							widgetSettingDTO2.setDeviceHeight(widgetSettingDTO.getDeviceHeight());
							widgetSettingDTO2.setDeviceWidth(widgetSettingDTO.getDeviceWidth());
							widgetSettingDTO2.setWidget(widgetSettingDTO.getWidget());
							widgetSettingDTO2.setUserMirror(userMirrorDTO);
							
							if(widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_CALENDAR)))
							{
								Boolean calendarFlag = defaultUserMirrorDto.getCalendarFlag();
								Boolean remindarFlag = defaultUserMirrorDto.getReminderFlag();
								userMirrorDao.updatecalendar(userMirrorDTO.getId(),calendarFlag,remindarFlag,defaultUserMirrorDto.getCalendarFormat().getId());
							}
							
							if (pagelist.containsKey(widgetSettingDTO.getMirrorPage().getPageNumber())) {
								MirrorPageDTO mirrorPageDTO = pagelist.get(widgetSettingDTO.getMirrorPage().getPageNumber());
								widgetSettingDTO2.setMirrorPage(mirrorPageDTO);
							} else {
								MirrorPageDTO mirrorPageDTO = new MirrorPageDTO();
								mirrorPageDTO.setPageNumber(widgetSettingDTO.getMirrorPage().getPageNumber());
								mirrorPageDTO.setDelay(widgetSettingDTO.getMirrorPage().getDelay());
								mirrorPageDTO.setUserMirror(userMirrorDTO);
								mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
								pagelist.put(widgetSettingDTO.getMirrorPage().getPageNumber(), mirrorPageDTO);
								widgetSettingDTO2.setMirrorPage(mirrorPageDTO);
							}

							if (widgetSettingDTO.getWidget().getType().equals(ResourceManager.getProperty(WIDGET_STICKYNOTES))
									&& widgetSettingDTO.getStatus().equals(ResourceManager.getProperty(STATUS_ON))) {
								
								List<StickyNotesDTO> stickyNotesDTOs = new ArrayList<>();
								StickyNotesDTO  stickyNotesDTO = new StickyNotesDTO();
								stickyNotesDTO.setData(widgetSettingDTO.getStickyNotes().get(0).getData());
								stickyNotesDTO.setName(widgetSettingDTO.getStickyNotes().get(0).getName());
								stickyNotesDTOs.add(stickyNotesDTO);
								widgetSettingDTO2.setStickyNotes(stickyNotesDTOs);
							}
							updatedWidgetSettingDto.add(widgetSettingDTO2);
						}
						widgetDao.addQuotesForUser(userMirrorDTO,quotesList);
						widgetDao.saveWidgetSetting(updatedWidgetSettingDto);
					}
				}
			}			
		}catch(Exception exception)
		{
			logger.info(exception);
		}
		
	}
			
}
