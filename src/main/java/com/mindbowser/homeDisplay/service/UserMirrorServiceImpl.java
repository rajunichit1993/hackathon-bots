package com.mindbowser.homeDisplay.service;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.BLE_OUT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_PAGENUMBER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_PAGE_TRANSITION_DELAY;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.DEFAULT_QUOTES_VALUE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ONE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_MIRROR_DETAIL_UPDATE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.LOG_USER_JOIN_MIRROR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_NOT_REGISTERED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.MIRROR_OWNER_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.NOT_FOUND;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TWITTER_CREDENTIAL_UPDATE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_ALREADY_EXIST_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_GENERAL;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_LINKED;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_MIRROR_NOT_REGISTERED_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_NOT_REGISTER_TO_MIRROR_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DAILY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_HOURLEY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_REFRESH;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PRIVATE_MIRROR_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.SOMETHING_WENTWRONG_EXCEPTION;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.PARSING_EXCEPTION;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dvdme.ForecastIOLib.ForecastIO;
import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dao.LogDao;
import com.mindbowser.homeDisplay.dao.SmartMirrorDao;
import com.mindbowser.homeDisplay.dao.UserMirrorDaoImpl;
import com.mindbowser.homeDisplay.dao.WidgetDao;
import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.dto.LogDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.QuotesCategoryDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.QuotesLengthDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.QuotesCategoryModel;
import com.mindbowser.homeDisplay.model.QuotesLengthModel;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.SocketResponce;
import com.mindbowser.homeDisplay.model.UpdateQuotesModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.util.CalendarUtcDateTime;
import com.mindbowser.homeDisplay.util.LocaleConverter;
import com.mindbowser.homeDisplay.util.QuotesCategoryDozerHelper;
import com.mindbowser.homeDisplay.util.ResourceManager;
import com.mindbowser.homeDisplay.util.UserMirrorDozerHelper;

public class UserMirrorServiceImpl  implements UserMirrorService{

private static Logger logger = Logger.getLogger(UserMirrorServiceImpl.class);
	
	
	@Autowired
	private UserMirrorDaoImpl userMirrorDao;
	
	@Autowired
	private SmartMirrorDao smartMirrorDao;
	
	@Autowired
	private WidgetDao widgetDao;
	
	@Autowired
	private LogDao logDao;
	
	@Autowired
	private WidgetService widgetService;
	
	@Autowired
	private SmartMirrorService mirrorService;

	@Autowired
	private SocketService socketService;
	
	@Autowired
	private FitBitService fitbitService;
	
	@Autowired
	private Mapper dozerMapper;
	
	private Random randomGenerator;
	
	
	@Override
	public Map<String, Object> getUserMirrorList(HttpServletRequest request,String version)throws MangoMirrorException {
		
		UserModel userModel = (UserModel) request.getAttribute(USER);
		List<UserMirrorDTO> mirrorList;
		List<UserMirrorModel> userMirrorModelList;
		Map<String, Object> data = new HashMap<>();
		try
		{
			String generalUser = ResourceManager.getProperty(USER_GENERAL);
			String linkedUser  = ResourceManager.getProperty(USER_LINKED);
			/*check if requested user anyhow connected with mirror or not*/
			int mirrorStatus = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO));
			mirrorList = userMirrorDao.getUserMirrorList(userModel.getId(),generalUser,linkedUser,mirrorStatus,userStatusFlag);
			if(mirrorList.isEmpty()){
				throw new MangoMirrorException(ResourceManager.getMessage(USER_NOT_REGISTER_TO_MIRROR_EXCEPTION, null, NOT_FOUND, null));	
			}
			else
			{
				userMirrorModelList = UserMirrorDozerHelper.map(dozerMapper, mirrorList, UserMirrorModel.class);	
			}
			
			/*this is used by app side to show helth widget list*/
			List<Map<String, Object>> healthWidgetList = fitbitService.getHealthKitWidgetList(version);
			
			data.put("MirrorList", userMirrorModelList);
			data.put("healthWidget", healthWidgetList);
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
			return data;
	}


	@Override
	public void updateMirrorDetail(HttpServletRequest request,
			UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {
		
		UserModel userModel = (UserModel)request.getAttribute(USER);
		
		try
		{
			/*check requested user is owner or not if not then it shows exception*/
			UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorFileds(userModel.getId(),userMirrorModel.getMirror().getId(),userMirrorModel.getUserRole(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
			
			String logTime = CalendarUtcDateTime.getUtcDateTime();
			LogDTO logDTO = new LogDTO();
			logDTO.setUserId(userModel.getId());
			logDTO.setTime(logTime);
			
			if(userMirrorDTO != null)
			{
				if(userMirrorModel.getMirrorName()!=null && !userMirrorModel.getMirrorName().isEmpty())
				{
					userMirrorDao.updateMirrorName(userModel.getId(), userMirrorModel.getMirror().getId(), userMirrorModel.getMirrorName());
				}
				if(userMirrorModel.getMirror().getWifissid()!=null && !userMirrorModel.getMirror().getWifissid().isEmpty() ||
						userMirrorModel.getMirror().getDelay()!=null || userMirrorModel.getMirror().getAutoUpdate()!=null)
				{
				  smartMirrorDao.updateMirrorDetail(userMirrorModel.getMirror());
					
				}
				logDTO.setMirrorId(userMirrorModel.getMirror().getId());
				logDTO.setEvent(ResourceManager.getProperty(LOG_MIRROR_DETAIL_UPDATE));
				logDao.saveLog(logDTO);
			}else
			{
				throw new MangoMirrorException(
					      ResourceManager.getMessage(MIRROR_OWNER_EXCEPTION, null, NOT_FOUND, null));
			}	
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}
	
	@Override
	public List<UserMirrorModel> getUserListByMajorMinor(HttpServletRequest request, List<SmartMirrorModel> mirroList,String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		List<UserMirrorModel> userMirrorList = new ArrayList<UserMirrorModel>();
		UserModel userModel = (UserModel)request.getAttribute(USER);
		UserMirrorDTO userMirrorDTO = new UserMirrorDTO();
		try
		{
			for (SmartMirrorModel smartModel  : mirroList)
			{
			 /*check one by one all the mirrors whether the mirror major minor is registered or not*/ 	
		      SmartMirrorDTO smartMirrorDTO1 =  smartMirrorDao.checkMirror(smartModel.getMajor(), smartModel.getMinor(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		      if(smartMirrorDTO1!=null)
		      {
		    	
		    		  UserMirrorModel userMirrorModel;
			    	  SmartMirrorModel smartMirrorModel = dozerMapper.map(smartMirrorDTO1, SmartMirrorModel.class);
			    	  
			    	  /*check requested user is any how connected with the mirror or not and if not then access the owner of that mirror to return name of that mirror*/
			    	  userMirrorDTO = userMirrorDao.getUserMirrorData(userModel.getId(),smartMirrorModel.getId(), ResourceManager.getProperty(USER_GENERAL), ResourceManager.getProperty(USER_LINKED),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO))); 
					   if(userMirrorDTO!=null)
						{
							 userMirrorModel = dozerMapper.map(userMirrorDTO, UserMirrorModel.class);
							 userMirrorList.add(userMirrorModel);		
						}else
						{
							userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorModel.getId());
							userMirrorModel = dozerMapper.map(userMirrorDTO, UserMirrorModel.class);
							userMirrorModel.setUserRole(null);
							userMirrorList.add(userMirrorModel);
						}
		        }
			}
			if(userMirrorList.size()<1)
			{
				throw new MangoMirrorException(
						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
			}	
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return userMirrorList;
	}

	@Override
	public void removeUserMirror(HttpServletRequest request,
				UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {

		UserModel userModel = (UserModel)request.getAttribute(USER);
		try
		{
			userMirrorModel.setUser(userModel);
			UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
		    int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ONE));
		    String bleRangeStatus=ResourceManager.getProperty(BLE_OUT);
		    userMirrorModel.setBleRangeStatus(bleRangeStatus);
			userMirrorModel.setUserStatusFlag(userStatusFlag);
			userMirrorDao.removeUserMirror(userMirrorModel);
			
			if(userMirrorDTO!=null)
			{
				if(userMirrorDTO.getUser().getId() == userModel.getId())
				{
					UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
					Map<String, String> userMap = new HashMap<String, String>();
					SocketResponce socketResponce = new SocketResponce();
					ObjectMapper objectMapper = new ObjectMapper();
					JSONObject jsonObject = new JSONObject();
					JSONArray array = new JSONArray();
					socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
					if(userMirrorDTO1!=null)
					{
						if(userMirrorDTO1.getMirror().getMirrorPreview())
						{
							userMap.put("userId", "");
							userMap.put("userRole", ResourceManager.getProperty(USER_GENERAL));
						}else
						{
							userMap.put("userId", Integer.toString(userMirrorDTO1.getUser().getId()));
							userMap.put("userRole", userMirrorDTO1.getUserRole());	
						}
						socketResponce.setData(objectMapper.writeValueAsString(userMap));
					}else
					{
						userMap.put("userId", "");
						userMap.put("userRole", ResourceManager.getProperty(USER_GENERAL));
						socketResponce.setData(objectMapper.writeValueAsString(userMap));
					}
					jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
					jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
					array.put(jsonObject);
					socketService.broadcast(array.toString());
				}
			}
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public HashMap updateCalenderFormatSetting(HttpServletRequest request,
			UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {
		UserModel userModel = (UserModel) request.getAttribute(USER);
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), userMirrorModel.getMirror().getId(), userMirrorModel.getUserRole(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		CalendarFormatDTO calendarFormatDTO = widgetDao.getCalendarFormatById(userMirrorModel.getCalendarFormat().getId());
		userMirrorDTO.setCalendarFormat(calendarFormatDTO);
		HashMap event = new HashMap();
		
		try
		{
			if(userMirrorModel.getCalendarFlag()!=null || userMirrorModel.getReminderFlag()!=null)
			{
				if(userMirrorModel.getCalendarFlag()!=null)
				{
					userMirrorDTO.setCalendarFlag(userMirrorModel.getCalendarFlag());	
				}
				if(userMirrorModel.getReminderFlag()!=null)
				{
					userMirrorDTO.setReminderFlag(userMirrorModel.getReminderFlag());	
				}
			}
			
			SocketResponce socketResponce = new SocketResponce();
			socketResponce.setType("refreshCalenderData");
			socketResponce.setMessage("refreshCalenderData");
			
			if(userMirrorDTO.getCalendarFlag() || userMirrorDTO.getReminderFlag())
			{
				Map<String, List<Map<String, Object>>> events =widgetService.getCalenderData(userModel.getId(),userMirrorDTO);
				event.put("events", events);
			}
	  		
			Map<String,String> calendarTitle =  new HashMap<String, String>();
			calendarTitle.put("dateFormatId", Integer.toString(userMirrorDTO.getCalendarFormat().getId()));
			calendarTitle.put("calendarTitle",userMirrorDTO.getCalendarFormat().getLabel());
			
			List<Map<String, String>> Title = new ArrayList<>(); 
			Title.add(calendarTitle);
			event.put("title",Title);
			
			ObjectMapper objectMapper= new ObjectMapper();
			socketResponce.setData(objectMapper.writeValueAsString(event));
		    UserMirrorDTO userMirrorDTO2 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
			
			JSONArray array = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
		    
		    if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
			{
				if (userMirrorDTO2 == null) 
			    {
		    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
		    		array.put(jsonObject);
		    		socketService.broadcast(array.toString());
			    }
				
			}else
			{
				if (userMirrorDTO2 != null)
				{
					if (userMirrorDTO2.getUser().getId() == userModel.getId()) 
					{
						jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
			    		array.put(jsonObject);
			    		socketService.broadcast(array.toString());    
					}
				}
			}	
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	    return event;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public Map<String, Object> updateWeatherLocation(HttpServletRequest request,
			UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {
		
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel)request.getAttribute(USER);
		/*check requested user is owner or not if not then it shows exception*/
		Map<String, Object> weather =  new HashMap<>();
		ObjectMapper objectMapper= new ObjectMapper();
		ArrayList<Map<String, Object>> weatherData = new ArrayList<>();
		Map<String, Object> updatedWeatherData = new HashMap<>();
		
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(),userMirrorModel.getMirror().getId(),userMirrorModel.getUserRole(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
		try
		{
		if(userMirrorDTO != null)
		{
			if(userMirrorModel.getLocation().getLocationName()!=null && !userMirrorModel.getLocation().getLocationName().isEmpty())
			{
				LocationDTO locationDTO = widgetDao.checkLocation(userMirrorModel.getLocation().getLatitude(), userMirrorModel.getLocation().getLongitude());
                if(locationDTO == null)
                {
                	locationDTO = widgetService.addWeatherData(userMirrorModel.getLocation().getLatitude(), userMirrorModel.getLocation().getLongitude(),userMirrorModel.getLocation().getLocationName()); 
                	userMirrorDao.updateLocation(userMirrorDTO, locationDTO);
                }else
                {
                	/*userMirrorDTO.setLocation(locationDTO );*/
                	userMirrorDao.updateLocation(userMirrorDTO, locationDTO);
                	Date date = new Date();
            		long currentTime = date.getTime();
                	Timestamp LastAccessTime = locationDTO.getLastAccessTime();
        			SimpleDateFormat sdf = new SimpleDateFormat("dd");
        			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        			
        			if (currentTime - LastAccessTime.getTime() > 300000
        					|| Integer.parseInt(sdf.format(date)) > LastAccessTime.getDate()) {

        				Timestamp updatedCurrentTime = new Timestamp(currentTime);
        				ForecastIO weatherAPIData = widgetService.getWeatherInfo(locationDTO.getLatitude(), locationDTO.getLongitude());
        				JSONObject dailyTemperature = new JSONObject(weatherAPIData.getDaily().toString());
        				JSONObject hourleyTemperature = new JSONObject(weatherAPIData.getHourly().toString());
        				widgetDao.deleteWeatherData(locationDTO.getId());
        				locationDTO.setLastAccessTime(updatedCurrentTime);
        				widgetService.saveWeatherData(dailyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
        				widgetService.saveWeatherData(hourleyTemperature, locationDTO, ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
                     }
                }
                String iconFormat = "svg";
                UserMirrorDTO userMirrorDTO1 = new UserMirrorDTO();
                userMirrorDTO1.setLocation(locationDTO);
                userMirrorDTO1.setUser(userMirrorDTO.getUser());
                
                /*UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirror(userModel.getId(),userMirrorModel.getMirror().getId(),userMirrorModel.getUserRole(),Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));*/
                Map<String, Object> dailyWeatherdata ;
        		dailyWeatherdata =  widgetService.getDailyWeatherData(userMirrorDTO1, iconFormat, ResourceManager.getProperty(WIDGET_DAILY_WEATHER));
        		weather.put("dailyWeatherdata", dailyWeatherdata);
        		weatherData.add(dailyWeatherdata);
        		
        		Map<String, Object> hourleyWeatherdata;
        		hourleyWeatherdata =  widgetService.getHourlyWeatherData(userMirrorDTO1, iconFormat, ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER));
        		weather.put("hourleyWeatherdata", hourleyWeatherdata);
        		weatherData.add(hourleyWeatherdata);
                
        		Map<String, Object> currentWeatherdata;
        		currentWeatherdata =  widgetService.getCurrentWeatherData(userMirrorDTO1, iconFormat);
        		weather.put("currentWeatherdata", currentWeatherdata);
        		weatherData.add(currentWeatherdata);
        		
        		SocketResponce socketResponce = new SocketResponce();
        		socketResponce.setType("refreshWeather");
        		socketResponce.setMessage("refreshWeather");
        		socketResponce.setData(objectMapper.writeValueAsString(weatherData));
        		UserMirrorDTO userMirrorDTO3 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
        		
        		if(userMirrorDTO3 == null)
        		{
        			if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
        			{
        				JSONArray array = new JSONArray();
        	    		JSONObject jsonObject = new JSONObject();
        	    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
        	    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
        	    		array.put(jsonObject);
        	    		socketService.broadcast(array.toString());
        			}
        		}else
        		{
        			if(userMirrorDTO.getMirror().getMirrorPreview() && userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
            			{
            				JSONArray array = new JSONArray();
            	    		JSONObject jsonObject = new JSONObject();
            	    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
            	    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
            	    		array.put(jsonObject);
            	    		socketService.broadcast(array.toString());
        			}else
        			{
        				if(userMirrorDTO3.getId() == userMirrorDTO.getId())
                		{
        					JSONArray array = new JSONArray();
            	    		JSONObject jsonObject = new JSONObject();
            	    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
            	    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
            	    		array.put(jsonObject);
            	    		socketService.broadcast(array.toString());
                		}
        			}
        		}
			}
		}else
		{
			throw new MangoMirrorException(ResourceManager.getMessage(
					USER_MIRROR_NOT_REGISTERED_EXCEPTION, null, NOT_FOUND, locale));
		}
		}catch(MangoMirrorException exception)
		{
			logger.info(exception);
			throw exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		updatedWeatherData.put("updatedWeather", weather);
        return updatedWeatherData;
	}


	@Override
	public Map<String, String> updateClockTimezone(HttpServletRequest request,
			UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {
		    
		    Locale locale = LocaleConverter.getLocaleFromRequest(request);
		    UserModel userModel = (UserModel)request.getAttribute(USER);
		    userMirrorModel.setUser(userModel);
		    userMirrorDao.updateClockTimezone(userMirrorModel);
		    SocketResponce socketResponce = new SocketResponce();
			socketResponce.setType("refreshClock");
			socketResponce.setMessage("refreshClock");
		 	ObjectMapper objectMapper= new ObjectMapper();
		 	JSONArray array = new JSONArray();
		 	Map<String, String> data = new HashMap<String, String>();
		 	
		 	
		 	try
		 	{
		 		long offMilisecond =  userMirrorModel.getTimeZone()*60*1000;
				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				long time = cal.getTimeInMillis() + offMilisecond;
				
				data.put("utcMiliSecond", Long.toString(time));
				socketResponce.setData(objectMapper.writeValueAsString(data));
	             
				SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorById(userMirrorModel.getMirror().getId());
				UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
				
	    		JSONObject jsonObject = new JSONObject();
	    		if(smartMirrorDTO!=null)
	        		{
	        			jsonObject.put("deviceId", smartMirrorDTO.getDeviceId());
	        			if(userMirrorDTO!=null)
	            		{
	            			if(userMirrorDTO.getUser().getId() ==  userModel.getId() && userMirrorDTO.getUserRole().equals(userMirrorModel.getUserRole()))
	        		    	{
	        					jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
	        		    		array.put(jsonObject);
	        		    	}
	            		}else
	            		{
	            			if(userMirrorModel.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
	            			{
	        					jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
	        		    		array.put(jsonObject);
	            			}
	            		}
	        		}else
	        		{
	        			throw new MangoMirrorException(
	    						ResourceManager.getMessage(MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));
	        		}
		 	}catch (MangoMirrorException exception) {
				logger.info(exception);
				throw  exception;
			}catch (JSONException exception) {
				logger.info(exception);
				throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
			} catch (JsonProcessingException exception) {
				logger.info(exception);
				throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
			}
			catch(Exception exception)
			{
				logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
				throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
			}
    		socketService.broadcast(array.toString());
    		return data;
	}
	
	@Override
	public void addLinkedUser(HttpServletRequest request,
			UserMirrorModel userMirrorModel,String version) throws MangoMirrorException {

		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		SmartMirrorDTO smartMirrorDTO = smartMirrorDao.getMirrorByDeviceId(userMirrorModel.getMirror().getDeviceId());
		UserDTO userDTO = new UserDTO();
		List<WidgetSettingDTO> widgetSettingList = new ArrayList<WidgetSettingDTO>();
		try {
			if (smartMirrorDTO != null && smartMirrorDTO.getMultiUser()) {
				UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirrorFileds(
						userModel.getId(), smartMirrorDTO.getId(),
						ResourceManager.getProperty(USER_LINKED),
						Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)));
				
				if (userMirrorDTO != null) {
					throw new MangoMirrorException(ResourceManager.getMessage(
							USER_ALREADY_EXIST_EXCEPTION, null, NOT_FOUND,
							locale));
				} else {

					UserMirrorDTO userMirrorDTO1 = dozerMapper.map(
							userMirrorModel, UserMirrorDTO.class);
					userDTO.setId(userModel.getId());
					userMirrorDTO1.setMirror(smartMirrorDTO);
					userMirrorDTO1.setUser(userDTO);
					userMirrorDTO1.setClockMessageStatus(true);
					userMirrorDTO1.setUserStatusFlag(Integer.parseInt(ResourceManager
							.getProperty(FLAG_ZERO)));
					userMirrorDTO1.setUserRole(ResourceManager
							.getProperty(USER_LINKED));
					
					userMirrorDTO1.setCalendarFlag(false);
					userMirrorDTO1.setReminderFlag(false);
					
					LocationDTO locationDTO = widgetDao.checkLocation(userMirrorModel.getLocation().getLatitude(), userMirrorModel.getLocation().getLongitude());
					if(locationDTO!=null)
					{
						userMirrorDTO1.setLocation(locationDTO);
					}else
					{
						locationDTO = widgetService.addWeatherData(userMirrorModel.getLocation().getLatitude(), userMirrorModel.getLocation().getLongitude(), userMirrorModel.getLocation().getLocationName());
					}
					userMirrorDTO1.setLocation(locationDTO);
					userMirrorDTO = userMirrorDao.setUserMirror(userMirrorDTO1);
					
					QuotesDTO quote = new QuotesDTO();
					quote.setId(Integer.parseInt(ResourceManager.getProperty(DEFAULT_QUOTES_VALUE)));
					List<QuotesDTO> quotes = new ArrayList<>();
					quotes.add(quote);
					widgetDao.addQuotesForUser(userMirrorDTO1, quotes);
					MirrorPageDTO mirrorPageDTO = new MirrorPageDTO();
					mirrorPageDTO.setPageNumber(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGENUMBER)));
					mirrorPageDTO.setDelay(Integer.parseInt(ResourceManager.getProperty(DEFAULT_PAGE_TRANSITION_DELAY)));
					mirrorPageDTO.setUserMirror(userMirrorDTO);
					mirrorPageDTO = widgetDao.saveMirrorPage(mirrorPageDTO);
					widgetSettingList = mirrorService.InitializeWidgets(userMirrorDTO,mirrorPageDTO);
					widgetDao.saveWidgetSetting(widgetSettingList);
					
					String logTime = CalendarUtcDateTime.getUtcDateTime();
					LogDTO logDTO = new LogDTO();
					logDTO.setUserId(userModel.getId());
					logDTO.setTime(logTime);
					logDTO.setMirrorId(smartMirrorDTO.getId());
					logDTO.setEvent(ResourceManager.getProperty(LOG_USER_JOIN_MIRROR));
					logDao.saveLog(logDTO);
				}
			} else {
				if(smartMirrorDTO!=null)
				{
					if(!smartMirrorDTO.getMultiUser())
					{
						throw new MangoMirrorException(ResourceManager.getMessage(
								PRIVATE_MIRROR_EXCEPTION, null, NOT_FOUND, locale));	
					}
				}else
				{
					throw new MangoMirrorException(ResourceManager.getMessage(
							MIRROR_NOT_REGISTERED, null, NOT_FOUND, locale));	
				}
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}	
	
	@Override
	public void updateTwitterCredentials(HttpServletRequest request,
			String version, UserMirrorModel userMirrorModel) throws MangoMirrorException {
	
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)); 
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), userMirrorModel.getMirror().getId(), userMirrorModel.getUserRole(), userStatusFlag);
		SocketResponce socketResponce = new SocketResponce();
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, String> userMap = new HashMap<>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray array = new JSONArray();
		Boolean refreshPageFlag = true;
		Boolean sendDataToSocketFlag = false;
		
		try 
		{
		if(userMirrorDTO!=null)
		{
			if(userMirrorDTO.getTwitter()!=null)
			{
				socketResponce.setType(ResourceManager.getProperty(TWITTER_CREDENTIAL_UPDATE));
				userMirrorDao.updateTwitterCredentials(userMirrorModel,userMirrorDTO.getTwitter().getId());
				refreshPageFlag = false;
				
				if(!userMirrorDTO.getTwitter().getTwitterOauthToken().isEmpty())
				{
					if(!userMirrorModel.getTwitter().getTwitterOauthToken().isEmpty() && !userMirrorModel.getTwitter().getTwitterOauthTokenSecret().isEmpty())
						{
							data.put("id", Integer.toString(userMirrorDTO.getTwitter().getId()));
							data.put("twitterOauthToken", userMirrorModel.getTwitter().getTwitterOauthToken());
							data.put("twitterOauthTokenSecret",userMirrorModel.getTwitter().getTwitterOauthTokenSecret());
							data.put("twitterCredentialStatus",userMirrorModel.getTwitter().getTwitterOauthTokenSecret());
							data.put("timeLine", userMirrorModel.getTwitter().getTimeLine());
							data.put("twitterFollowingUser", userMirrorModel.getTwitter().getTwitterFollowingUser());
							data.put("screenName", userMirrorModel.getTwitter().getScreenName());
							sendDataToSocketFlag = true;
						}					
				}
			}
			else
			{
				if(userMirrorModel.getTwitter().getTwitterOauthToken()!=null  && !userMirrorModel.getTwitter().getTwitterOauthToken().isEmpty() 
						&& userMirrorModel.getTwitter().getTwitterOauthTokenSecret()!=null && !userMirrorModel.getTwitter().getTwitterOauthTokenSecret().isEmpty())
				{
					TwitterDTO twitterDTO =  dozerMapper.map(userMirrorModel.getTwitter(), TwitterDTO.class);
					twitterDTO = userMirrorDao.saveTwitter(twitterDTO);
					userMirrorDTO.setTwitter(twitterDTO);
					socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
					sendDataToSocketFlag = true;
				}
			}

			if(sendDataToSocketFlag)
			{
				UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorDTO.getMirror().getId());
				if(userMirrorDTO.getMirror().getMirrorPreview() == true)
				{
					if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
					{
						if(refreshPageFlag)
							{
							userMap.put("userId",Integer.toString(userModel.getId()));
							userMap.put("userRole", userMirrorDTO.getUserRole());
							socketResponce.setData(objectMapper.writeValueAsString(userMap));
							}
						else
						{
							socketResponce.setData(objectMapper.writeValueAsString(data));
						}
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
						jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
						array.put(jsonObject);
					    socketService.broadcast(array.toString());
					}
				}else
				{
					if(userMirrorDTO1!=null)
				    {
				    	if(userMirrorDTO1.getUser().getId() ==  userModel.getId() && userMirrorDTO1.getUserRole().equals(userMirrorDTO.getUserRole()))
				    	{
				    		
				    		if(refreshPageFlag)
							{
				    			userMap.put("userId",Integer.toString(userModel.getId()));
								userMap.put("userRole", userMirrorDTO1.getUserRole());
								socketResponce.setData(objectMapper.writeValueAsString(userMap));
							}
							else
							{
								socketResponce.setData(objectMapper.writeValueAsString(data));
							}
				    	
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("deviceId", userMirrorDTO1.getMirror().getDeviceId());
							jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
						    socketService.broadcast(array.toString());
				    	}
				    }else
				    {
				    	if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
				    	{
				    		if(refreshPageFlag)
							{
							userMap.put("userId",Integer.toString(userModel.getId()));
							userMap.put("userRole", userMirrorDTO.getUserRole());
							socketResponce.setData(objectMapper.writeValueAsString(userMap));
							}
						else
						{
							socketResponce.setData(objectMapper.writeValueAsString(data));
						}
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
						    socketService.broadcast(array.toString());	
				    	}
				    }
				}				
			}
		}else
		{
			throw new MangoMirrorException(ResourceManager.getMessage(
					USER_MIRROR_NOT_REGISTERED_EXCEPTION, null, NOT_FOUND, locale));
		}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SmartMirrorModel updatePreview(HttpServletRequest request, String version, SmartMirrorModel smartMirrorModel)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);

		UserMirrorDTO userMirrorDTO = userMirrorDao.getOwnerDetail(smartMirrorModel.getId());
		SocketResponce socketResponce = new SocketResponce();
		socketResponce.setType(ResourceManager.getProperty(WIDGET_REFRESH));
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, String> userMap = new HashMap<>();
		JSONArray array = new JSONArray();
		ArrayList<String> socketStatus;
		SmartMirrorDTO smartMirrorDTO = null;
	
		try
		{
			if(userMirrorDTO!=null)
			{
				if(userMirrorDTO.getUser().getId() == userModel.getId())
				{
					smartMirrorDTO = userMirrorDTO.getMirror();
					smartMirrorDTO.setMirrorPreview(smartMirrorModel.getMirrorPreview());
					UserMirrorDTO userMirrorDTO1 = userMirrorDao.getUserMirrorByBleRangeStatusTime(smartMirrorDTO.getId());
					if(smartMirrorModel.getMirrorPreview() == true)
					{
							if (userMirrorDTO1 != null) {
								userMap.put("userId",Integer.toString(userModel.getId()));
								userMap.put("userRole", userMirrorDTO.getUserRole());
								socketResponce.setData(objectMapper.writeValueAsString(userMap));
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
								jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
								array.put(jsonObject);
							    socketStatus = socketService.broadcast(array.toString());	
							    logger.info(socketStatus);
						    }
					}else
					{
						if (userMirrorDTO1 != null) {
							userMap.put("userId",
									Integer.toString(userMirrorDTO1.getUser().getId()));
							userMap.put("userRole", userMirrorDTO1.getUserRole());
							socketResponce.setData(objectMapper.writeValueAsString(userMap));
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
							jsonObject.put("data",objectMapper.writeValueAsString(socketResponce));
							array.put(jsonObject);
						    socketStatus = socketService.broadcast(array.toString());
						    logger.info(socketStatus);
						} 
					}
				}
				else
				{
					throw new MangoMirrorException(ResourceManager.getMessage(
							MIRROR_OWNER_EXCEPTION, null, NOT_FOUND, locale));
				}
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		return dozerMapper.map(smartMirrorDTO, SmartMirrorModel.class);
	}


/*	@Override
	public Map<String, Object> updateQuotes(HttpServletRequest request, UserMirrorModel userMirrorModel, String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)); 
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), userMirrorModel.getMirror().getId(), userMirrorModel.getUserRole(), userStatusFlag);
		ObjectMapper objectMapper= new ObjectMapper();
		Map<String, Object> updatedQuotesData = new  HashMap<>();
		try 
		{
			if(userMirrorDTO!=null)
			{
				QuotesDTO categoryLengthDTO = widgetDao.getQuotesDataByLengthCategory(userMirrorModel.getQuotes().getQuotesCategory().getId(),userMirrorModel.getQuotes().getQuotesLength().getId());
				userMirrorDTO.setQuotes(categoryLengthDTO);
				Map<String, String> data = new HashMap<>();
          		
          		HashMap<String, Object> quotesData = new HashMap<>();
          		quotesData.put("quotesCategory", categoryLengthDTO.getQuotesCategory().getId());
          		quotesData.put("quotesLength", categoryLengthDTO.getQuotesLength().getId());
          		if(userMirrorDTO.getQuotes()!=null)
          		{
          			data.put("author", categoryLengthDTO.getAuthor());
	          		data.put("quotes", categoryLengthDTO.getQuotes());	
          		}
          		quotesData.put("quotes", data);
				updatedQuotesData.put("updatedQuotes", quotesData);
				SocketResponce socketResponce = new SocketResponce();
				socketResponce.setType("refreshQuotes");
				socketResponce.setMessage("refreshQuotes");
				socketResponce.setData(objectMapper.writeValueAsString(updatedQuotesData));
			    
				UserMirrorDTO userMirrorDTO3 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
				
				if(userMirrorDTO.getMirror().getMirrorPreview() == true)
				{
					if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
					{
						JSONArray array = new JSONArray();
			    		JSONObject jsonObject = new JSONObject();
			    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
			    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
			    		array.put(jsonObject);
			    		socketService.broadcast(array.toString());
					}
				}else
				{
					if(userMirrorDTO3!=null)
				    {
				    	if(userMirrorDTO3.getUser().getId() ==  userModel.getId() && userMirrorDTO3.getUserRole().equals(userMirrorModel.getUserRole()))
				    	{
				    		JSONArray array = new JSONArray();
				    		JSONObject jsonObject = new JSONObject();
				    		jsonObject.put("deviceId", userMirrorDTO3.getMirror().getDeviceId());
				    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
				    		array.put(jsonObject);
				    		socketService.broadcast(array.toString());		
				    	}
				    }else
				    {
				    	if(userMirrorModel.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
				    	{
				    		JSONArray array = new JSONArray();
				    		JSONObject jsonObject = new JSONObject();
				    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
				    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
				    		array.put(jsonObject);
				    		socketService.broadcast(array.toString());		
				    	}
				    }
				}	
			}
			else
			{
				throw new MangoMirrorException(ResourceManager.getMessage(
						USER_MIRROR_NOT_REGISTERED_EXCEPTION, null, NOT_FOUND, locale));
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		 
	     return updatedQuotesData;
		
	}*/
	
	
	@Override
	public Map<String, Object> updateQuotes(HttpServletRequest request, UpdateQuotesModel updateQuotesModel, String version)
			throws MangoMirrorException {
		Locale locale = LocaleConverter.getLocaleFromRequest(request);
		UserModel userModel = (UserModel) request.getAttribute(USER);
		int userStatusFlag = Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO)); 
		UserMirrorModel userMirrorModel = updateQuotesModel.getUserMirrorModel();
		UserMirrorDTO userMirrorDTO = userMirrorDao.getUserMirror(userModel.getId(), userMirrorModel.getMirror().getId(), userMirrorModel.getUserRole(), userStatusFlag);
		ObjectMapper objectMapper= new ObjectMapper();
		Map<String, Object> updatedQuotesData = new  HashMap<>();
		try 
		{
			if(userMirrorDTO!=null)
			{
				List<QuotesCategoryModel> categoryModel = updateQuotesModel.getQuotesCategories();
				QuotesLengthModel quotesLengthModel = updateQuotesModel.getQuotesLength();
				widgetDao.deleteQuotesByUserMirrorId(userMirrorDTO.getId());
				List<QuotesCategoryDTO> quotesCategories = 	QuotesCategoryDozerHelper.QuotesListModelToDto(dozerMapper, categoryModel, QuotesCategoryDTO.class);
				QuotesLengthDTO quotesLength = dozerMapper.map(quotesLengthModel, QuotesLengthDTO.class);	
				List<QuotesDTO> quotesDTOs = widgetDao.getQuotesByCategoryAndLength(quotesCategories,quotesLength);
				widgetDao.addQuotesForUser(userMirrorDTO,quotesDTOs);
				List<QuotesDTO> quotesList = widgetDao.getQuotesByUserMrrrorId(userMirrorDTO.getId());
				randomGenerator = new Random();
				System.out.println(randomGenerator.nextInt(quotesList.size()));
				int index = randomGenerator.nextInt(quotesList.size());
		        QuotesDTO quotesDTO = quotesList.get(index);
				userMirrorDTO.setQuotes(quotesDTO);
				Map<String, String> data = new HashMap<>();
          		
          		HashMap<String, Object> quotesData = new HashMap<>();
          		quotesData.put("quotesCategory", quotesDTO.getQuotesCategory().getId());
          		quotesData.put("quotesLength", quotesDTO.getQuotesLength().getId());
          		if(userMirrorDTO.getQuotes()!=null)
          		{
          			data.put("author", quotesDTO.getAuthor());
	          		data.put("quotes", quotesDTO.getQuotes());	
          		}
          		quotesData.put("quotes", data);
				updatedQuotesData.put("updatedQuotes", quotesData);
				SocketResponce socketResponce = new SocketResponce();
				socketResponce.setType("refreshQuotes");
				socketResponce.setMessage("refreshQuotes");
				socketResponce.setData(objectMapper.writeValueAsString(updatedQuotesData));
			    
				UserMirrorDTO userMirrorDTO3 = userMirrorDao.getUserMirrorByBleRangeStatusTime(userMirrorModel.getMirror().getId());
				
				if(userMirrorDTO.getMirror().getMirrorPreview() == true)
				{
					if(userMirrorDTO.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
					{
						JSONArray array = new JSONArray();
			    		JSONObject jsonObject = new JSONObject();
			    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
			    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
			    		array.put(jsonObject);
			    		socketService.broadcast(array.toString());
					}
				}else
				{
					if(userMirrorDTO3!=null)
				    {
				    	if(userMirrorDTO3.getUser().getId() ==  userModel.getId() && userMirrorDTO3.getUserRole().equals(userMirrorModel.getUserRole()))
				    	{
				    		JSONArray array = new JSONArray();
				    		JSONObject jsonObject = new JSONObject();
				    		jsonObject.put("deviceId", userMirrorDTO3.getMirror().getDeviceId());
				    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
				    		array.put(jsonObject);
				    		socketService.broadcast(array.toString());		
				    	}
				    }else
				    {
				    	if(userMirrorModel.getUserRole().equals(ResourceManager.getProperty(USER_GENERAL)))
				    	{
				    		JSONArray array = new JSONArray();
				    		JSONObject jsonObject = new JSONObject();
				    		jsonObject.put("deviceId", userMirrorDTO.getMirror().getDeviceId());
				    		jsonObject.put("data", objectMapper.writeValueAsString(socketResponce));
				    		array.put(jsonObject);
				    		socketService.broadcast(array.toString());		
				    	}
				    }
				}	
			}
			else
			{
				throw new MangoMirrorException(ResourceManager.getMessage(
						USER_MIRROR_NOT_REGISTERED_EXCEPTION, null, NOT_FOUND, locale));
			}
		}catch (MangoMirrorException exception) {
			logger.info(exception);
			throw  exception;
		}catch (JSONException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		} catch (JsonProcessingException exception) {
			logger.info(exception);
			throw new MangoMirrorException(ResourceManager.getProperty(PARSING_EXCEPTION));
		}
		catch(Exception exception)
		{
			logger.info(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION)+""+exception);
			throw new MangoMirrorException(ResourceManager.getProperty(SOMETHING_WENTWRONG_EXCEPTION));
		}
		 
		
	     return updatedQuotesData;
		
	}
	

}
