
package com.mindbowser.homeDisplay.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;


public interface SmartMirrorService {

	public void updateDelayTime(HttpServletRequest request,int mirrorId,int delay,String version) throws MangoMirrorException;
	public void resetMirror(HttpServletRequest request,String deviceId,String version)throws MangoMirrorException;
	public void restartMirror(HttpServletRequest request,String deviceId,String version)throws MangoMirrorException;
	public void mirrorPairingMode(HttpServletRequest request,String deviceId,String version)throws MangoMirrorException;
	public SmartMirrorModel checkMirrorByMajorMinor(HttpServletRequest request,int major,int minor) throws MangoMirrorException;
	public SmartMirrorModel saveMirror(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	public List<WidgetSettingDTO> InitializeWidgets(UserMirrorDTO userMirrorDTO,MirrorPageDTO mirrorPageDTO) ;
	public void updateBleBroadcastRange(HttpServletRequest request, String deviceId, int broadcastRange, String version)throws MangoMirrorException;
	public void updateBluetoothSetting(HttpServletRequest request, String deviceId, String bluetoothStatus, String version)throws MangoMirrorException;

	/*this method is used for auto update*/
	public void checkForFirmwareUpdate() throws MangoMirrorException ;
	
	/*this method is used to check*/
	public SmartMirrorModel firmwareUpdateCheck(String deviceId) throws MangoMirrorException ;
	
	/*this method is used to send sqs message for firmware update*/
	public void firmwareUpdateSQSMessage(String deviceId,String version)throws MangoMirrorException;
    
	/*this method is used to check version on s3 server*/		
	public double checkCurrentFirmwareVersionS3() throws MangoMirrorException;
	
	/*this method is used to reset all mirrors on s3 server*/		
	public void resetAllMirror(HttpServletRequest request,String version)throws MangoMirrorException;
}
