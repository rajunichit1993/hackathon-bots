package com.mindbowser.homeDisplay.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.model.BleInDataModel;
import com.mindbowser.homeDisplay.model.EmailLogDataModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.UserModel;

public interface UserService {
	
	
	public UserModel getUserByAuthToken(String authToken)throws MangoMirrorException;

	public UserModel saveUser(HttpServletRequest request,UserModel userModel, String version) throws MangoMirrorException;
	
	public UserModel logIn( HttpServletRequest request, UserModel userModel, String version) throws MangoMirrorException;

	public void forgotPassword(HttpServletRequest request, UserModel userModel,String version) throws MangoMirrorException;

	public UserModel editProfile(HttpServletRequest request, UserModel userModel,String version) throws MangoMirrorException;
	
	public void changePassword(HttpServletRequest request,UserModel userModel,String version) throws MangoMirrorException;
	
	public void updateUserTimezone(HttpServletRequest request,int timeZone,String version) throws MangoMirrorException;
	
	public void updateUnit(HttpServletRequest request,UserModel userModel,String version) throws MangoMirrorException;
	
	
	@SuppressWarnings("rawtypes")
	public ArrayList publishWidgetSetting(HttpServletRequest request,List<UserMirrorModel> userMirrorList,String rangeType,String version) throws MangoMirrorException;
	
	public void sendUserLog(EmailLogDataModel logData,String version) throws MangoMirrorException;
	
	
	/*update data with ble in*/
	public void updateData(HttpServletRequest request,BleInDataModel bleInDataModel,String rangeType,int timeZone,String version) throws MangoMirrorException ;
	
	public void updateAllFitbitUserName(HttpServletRequest request,String version) throws MangoMirrorException ;
}
