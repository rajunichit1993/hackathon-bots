package com.mindbowser.homeDisplay.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.GenericWidgetSettingModel;
public interface BetaUserService {
	
	/**
	 * this method is used to save health and calendar records for 7 days.
	 * 
	 * @param authToken
	 * @return
	 */
	void saveBetaUserData(UserDTO userDTO) throws MangoMirrorException;
	
	/**
	 * this method is used by thread to insert user health and calendar records.
	 * 
	 * @param authToken
	 * @return
	 */
	void saveBetaUserDataByThread() throws MangoMirrorException;
	
	/**
	 * this method is used by thread to send the default setting on mirror after 5 minutes interval from last update.
	 * 
	 * @param authToken
	 * @return
	 */	
	void betaUserDefaultSetting(String deviceId) throws MangoMirrorException;
	
	
	/**
	 * this method is used to update layout by b8ta user.
	 * 
	 * @param authToken
	 * @return
	 */
	public Map<String, Object> updateWidgetSetting(HttpServletRequest request,List<GenericWidgetSettingModel> mirrorPageList,String version) throws MangoMirrorException;
	
	
	/**
	 * this method is used to update fitbit data on initial level because on initial level we need data for beta account.
	 * 
	 * @param authToken
	 * @return
	 */
	public void updateFitbitDataFirstTime()throws MangoMirrorException;
	
	
	/**
	 * update all beta mirror layout by master mirror.
	 * 
	 * @param authToken
	 * @return
	 */
	public void updateAllBetaMirrorLayout(String version)throws MangoMirrorException;
	
	

}
