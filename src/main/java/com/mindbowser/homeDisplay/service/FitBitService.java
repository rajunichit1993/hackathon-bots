package com.mindbowser.homeDisplay.service;


import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.mindbowser.homeDisplay.Exception.MangoMirrorException;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.FitBitNotificationModel;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.HealthWidgetModel;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.model.WidgetHealthDataModel;
import com.mindbowser.homeDisplay.model.WidgetModel;

public interface FitBitService {
	public List<Map<String, Object>> getHealthKitWidgetList(String version) throws MangoMirrorException; 
	public List<WidgetModel> healthWidgetList(String healthWidgetMasterCategory,String version) throws MangoMirrorException;
	
	public void saveHealthKit(UserDTO userDTO,HealthWidgetModel healthWidgetModel,int timeZone, String version) throws MangoMirrorException;
	public void saveHealthKitData(HealthDataDTO healthDataDTO,HealthWidgetModel healthWidgetModel,String version) throws MangoMirrorException;
	public void removeHealthKitData(HttpServletRequest request,List<HealthDataModel> healthDataList, String version) throws MangoMirrorException;
	
	public void fitBitUpdateCredential(HttpServletRequest request,UserMirrorModel userMirrorModel,String version) throws MangoMirrorException;
	public void getUpdatedFitbitAccessToken(FitBitAccountDTO fitBitAccountDTO) throws MangoMirrorException;
	public List<HealthDataDTO> getFitBitApiData(FitBitAccountDTO fitBitAccountDTO)throws MangoMirrorException;
	public List<HealthDataDTO> saveNotificationFitbitData(List<FitBitNotificationModel> fitbitNotificationData)throws MangoMirrorException;
	
	public FitBitAccountModel getFitbitProfileDetail(String fitbitUserId, String version) throws MangoMirrorException;
	public List<WidgetHealthDataModel> getHealthDataByUserMirror(UserMirrorDTO userMirrorDTO,List<WidgetModel> widgetModelList);

	

}
