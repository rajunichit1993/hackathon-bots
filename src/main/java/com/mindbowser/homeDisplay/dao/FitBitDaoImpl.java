package com.mindbowser.homeDisplay.dao;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ONE;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.util.ResourceManager;


public class FitBitDaoImpl extends BaseDAO implements FitBitDao {

	Logger logger = Logger.getLogger(FitBitDaoImpl.class);

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)*/	
	@Override
	public void saveHealthData(HealthDataDTO requestHealthDataDTO,List<HealthDataDTO> healthDataDtoList) {
		try
		{
			Session session = getCurrentSession();
			/*Transaction transaction = session.beginTransaction();*/
			 for (HealthDataDTO healthDataDTO : healthDataDtoList) {
				  if(requestHealthDataDTO.getUser()!=null)
				  {
					  healthDataDTO.setUser(requestHealthDataDTO.getUser());  
				  }else if(requestHealthDataDTO.getFitbit()!=null)
				  {
					  healthDataDTO.setFitbit(requestHealthDataDTO.getFitbit());
				  }
				 session.save(healthDataDTO);
			}
			 /*transaction.commit();*/
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)	*/
	@Override
	public void removeHealthKitData(UserModel userModel,List<Integer> widgetListIds) {
		try
		{
			Session session = getCurrentSession();
			Query query = session.createQuery("DELETE FROM HealthDataDTO WHERE widget.id IN (:list) and user.id = :userId");
			query.setParameterList("list", widgetListIds);
			query.setParameter("userId", userModel.getId());
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)*/
	@Override
	public void updateFitBitCredential(FitBitAccountModel fitBitAccountModel, int fitBitId) {
		Session session = getCurrentSession();
		StringBuilder queryString = new StringBuilder();
		queryString.append("update fitbitaccount set ");
		
		try
		{
			if(fitBitAccountModel!=null)
			 {
				if(fitBitAccountModel.getFitbitAccessToken() !=null && !fitBitAccountModel.getFitbitAccessToken().isEmpty())
				{
					queryString.append("fitbitAccessToken = :fitbitAccessToken,");
				}
				if( fitBitAccountModel.getFitbitRefreshToken()!=null && !fitBitAccountModel.getFitbitRefreshToken().isEmpty())
				{
					queryString.append("fitbitRefreshToken = :fitbitRefreshToken,");
				}
				if(fitBitAccountModel.getFitbitUserId()!=null && !fitBitAccountModel.getFitbitUserId().isEmpty())
				{
					queryString.append("fitbitUserId = :fitbitUserId,");	
				}
				if(fitBitAccountModel.getFitbitUserTimeZone()!=null && !fitBitAccountModel.getFitbitUserTimeZone().isEmpty())
				{
					queryString.append("fitbitUserTimeZone = :fitbitUserTimeZone,");
				}
		      }	
			
			queryString.deleteCharAt(queryString.length()-Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
			queryString.append(" where id= :fitBitId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("fitBitId", fitBitId);
			
			if(fitBitAccountModel!=null)
			{
				if(fitBitAccountModel.getFitbitAccessToken() !=null && !fitBitAccountModel.getFitbitAccessToken().isEmpty())
				{
					query.setParameter("fitbitAccessToken", fitBitAccountModel.getFitbitAccessToken());	
				}
				if( fitBitAccountModel.getFitbitRefreshToken()!=null && !fitBitAccountModel.getFitbitRefreshToken().isEmpty())
				{
					query.setParameter("fitbitRefreshToken", fitBitAccountModel.getFitbitRefreshToken());	
				}
				if(fitBitAccountModel.getFitbitUserId()!=null && !fitBitAccountModel.getFitbitUserId().isEmpty())
				{
					query.setParameter("fitbitUserId",fitBitAccountModel.getFitbitUserId());
				}
				if(fitBitAccountModel.getFitbitUserTimeZone()!=null && !fitBitAccountModel.getFitbitUserTimeZone().isEmpty())
				{
					query.setParameter("fitbitUserTimeZone",fitBitAccountModel.getFitbitUserTimeZone());
				}
			}
			query.executeUpdate();
			session.flush();
			session.clear();	
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@Override
	public FitBitAccountDTO saveFitBitAccount(FitBitAccountDTO fitBitAccountDTO) {
		try
		{
			Session session = getCurrentSession();
			session.save(fitBitAccountDTO);
			session.flush();
			session.clear();	
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return fitBitAccountDTO;
	}

	@Override
	public HealthDataDTO getFitBitLastAccessDate(WidgetDTO widgetDTO, FitBitAccountDTO fitBitAccountDTO) {
		
		HealthDataDTO healthDataDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(HealthDataDTO.class);
			criteria.add(Restrictions.eq("widget", widgetDTO));
			criteria.add(Restrictions.eq("fitbit",fitBitAccountDTO));
			criteria.addOrder(Order.desc("healthTime"));
			criteria.setMaxResults(1);
			healthDataDTO = (HealthDataDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthDataDTO;
	}

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)*/
	@Override
	public void removeHealthKitDataByDate(FitBitAccountDTO fitBitAccountDTO, List<WidgetDTO> widgetDTOList, String currentDate) {
		
		try
		{
			StringBuilder queryString = new StringBuilder();
			queryString.append("delete from healthdata where widgetId in :widget and fitbitId = :fitbit and healthTime = :healthTime");
			   Query query = getCurrentSession().createSQLQuery(queryString.toString()).addEntity(HealthDataDTO.class);
			   query.setParameterList("widget", widgetDTOList);
			   query.setParameter("fitbit", fitBitAccountDTO);
			  query.setParameter("healthTime", currentDate);
			  query.executeUpdate();
			  getCurrentSession().flush();
			  getCurrentSession().clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)*/
	@Override
	public FitBitAccountDTO getFitBitAccountByUserId(String fitBitUserId) {
		FitBitAccountDTO fitBitAccountDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(FitBitAccountDTO.class);
			criteria.add(Restrictions.eq("fitbitUserId", fitBitUserId));
			fitBitAccountDTO = (FitBitAccountDTO) criteria.uniqueResult();
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return fitBitAccountDTO;
	}

	/*@Transactional(propagation = Propagation.REQUIRES_NEW)*/
	@Override
	public void removeHealthKitDataByFitBitId(int fitBitId) {
		try
		{
			Session session = getCurrentSession();
			Query query = session.createQuery("DELETE FROM HealthDataDTO WHERE fitbit.id = :fitBitId");
			query.setParameter("fitBitId",fitBitId);
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@Override
	public void deleteFitbitDataByFitBitUserId(int fitBitId) {
		try
		{
			Session session = getCurrentSession();
			Query query = session.createQuery("DELETE FROM HealthDataDTO WHERE fitbit.id = :fitBitId");
			query.setParameter("fitBitId",fitBitId);
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

}
