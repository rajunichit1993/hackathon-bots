package com.mindbowser.homeDisplay.dao;


import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.UserModel;

import org.hibernate.HibernateException;

public interface UserDao {

	UserDTO getUserByAuthToken(String authToken);
	
	/**
	 * This method saves user details in database
	 * 
	 * @param user
	 *            object
	 * @return user object containing all user details.
	 * @throws HibernateException
	 */
	UserDTO saveUser(UserDTO user) throws HibernateException;
	UserDTO checkUser(String email);
	UserDTO getUserUsingAuthToken(String authToken);
	UserDTO getUserById(int userId) ;
	void updateUserTimezone(int userId,int timeZone);
	void updateClockTimezone(int userId,int timeZone);
	void updateUnit(UserModel userModel);
}
