
package com.mindbowser.homeDisplay.dao;


import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ONE;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ZERO;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.USER_GENERAL;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.model.UserMirrorModel;
import com.mindbowser.homeDisplay.util.ResourceManager;

public class UserMirrorDaoImpl extends BaseDAO implements UserMirrorDao {

	Logger logger = Logger.getLogger(UserMirrorDaoImpl.class);

	@Override
	public UserMirrorDTO getUserMirror(int userId, int mirrorId,String userRole,int userStatusFlag) {
		UserMirrorDTO userMirrorDTO;
		try
		{
			Session session =getCurrentSession();
			Criteria criteria = session.createCriteria(UserMirrorDTO.class);
			criteria.add(Restrictions.eq("user.id", userId));
			criteria.add(Restrictions.eq("mirror.id", mirrorId));
			criteria.add(Restrictions.eq("userRole", userRole));
			criteria.add(Restrictions.eq("userStatusFlag", userStatusFlag));
			userMirrorDTO = (UserMirrorDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorDTO;
	}
	
	@Override
	public UserMirrorDTO getUserMirrorFileds(int userId, int mirrorId,
			String userRole, int userStatusFlag) {
		UserMirrorDTO userMirrorDTO;
		try
		{
			Session session =getCurrentSession();
			ProjectionList projections = Projections.projectionList()
					    .add(Projections.property("id"),"id")
			            .add(Projections.property("us.id"),"user.id")  
			            .add(Projections.property("userRole"),"userRole") 
			            .add(Projections.property("mr.id"),"mirror.id");
				  Criteria criteria = session.createCriteria(UserMirrorDTO.class)
						  .add(Restrictions.eq("user.id", userId))
						  .add(Restrictions.eq("mirror.id", mirrorId))
						  .add(Restrictions.eq("userRole", userRole))
						  .add(Restrictions.eq("userStatusFlag", userStatusFlag))
				    .createAlias("user", "us")
				    .createAlias("mirror", "mr")
				    .setProjection(projections)
				    .setResultTransformer(new AliasToBeanNestedResultTransformer(UserMirrorDTO.class));
					
				  userMirrorDTO = (UserMirrorDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorDTO;
	}
	
	
	@Override
	public void updateMirrorName(int userId, int mirrorId,String mirrorName) {
		
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString
			.append("update userMirrors um set um.mirrorName = :mirrorName"
					+ " where um.mirrorId = :mirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("mirrorName", mirrorName);
			query.setParameter("mirrorId", mirrorId);
			query.executeUpdate();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
	}
	
	@Override
	public UserMirrorDTO getOwnerDetail(int mirrorId) {
		
		UserMirrorDTO userMirrorDTO;
		try
		{
			Session session =getCurrentSession();
			Criteria criteria = session.createCriteria(UserMirrorDTO.class);
			criteria.add(Restrictions.eq("mirror.id", mirrorId));
			criteria.add(Restrictions.eq("userRole", ResourceManager.getProperty(USER_GENERAL)));
			criteria.add(Restrictions.eq("userStatusFlag",Integer.parseInt(ResourceManager.getProperty(FLAG_ZERO))));
			userMirrorDTO = (UserMirrorDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserMirrorDTO> getUserMirrorList(int userId,String generalUser,String linkedUser,int mirrorStatus,int userStatusFlag) {
		List<UserMirrorDTO> userMirrorList;
		try
		{
			Session session =getCurrentSession();
			String sql = "SELECT * FROM userMirrors WHERE (userRole= :linkeduser or userRole = :generaluser) and userId = :id  "
					+ "and userStatusFlag = :userStatusFlag and mirrorId in (select id from smartMirror where mirrorStatus= :mirrorStatus)";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(UserMirrorDTO.class);
			query.setParameter("id", userId);
			query.setParameter("userStatusFlag", userStatusFlag);
			query.setParameter("generaluser", generalUser);
			query.setParameter("linkeduser", linkedUser);
			query.setParameter("mirrorStatus", mirrorStatus);
			userMirrorList = query.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorList;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserMirrorDTO> getUserMirrorListByFitBitId(int fitbitAccountId,String generalUser,String linkedUser,int mirrorStatus,int userStatusFlag) {
		List<UserMirrorDTO> userMirrorList;
		try
		{
			Session session =getCurrentSession();
			String sql = "SELECT * FROM userMirrors WHERE fitbitAccountId = :fitbitAccountId and userStatusFlag = :userStatusFlag";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(UserMirrorDTO.class);
			query.setParameter("fitbitAccountId", fitbitAccountId);
			query.setParameter("userStatusFlag", userStatusFlag);
			userMirrorList = query.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorList;
	}
	
	@Override
	public UserMirrorDTO setUserMirror(UserMirrorDTO userMirrorDTO) {
		try
		{
			Session session= getCurrentSession();
			session.save(userMirrorDTO);
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorDTO;
	}

	@Override
	public UserMirrorDTO getUserMirrorData(int userId,int mirrorId, String generalUser,
			String linkedUser,int userStatusFlag) {
		UserMirrorDTO userMirror;
		try
		{
			Session session =getCurrentSession();
	        Criteria criteria = session.createCriteria(UserMirrorDTO.class);
	        Criterion rest1= Restrictions.or(Restrictions.eq("userRole", generalUser), 
			                 Restrictions.eq("userRole", linkedUser));
	        Criterion rest2= Restrictions.and(rest1,
			         Restrictions.eq("userStatusFlag",userStatusFlag));
			Criterion rest3= Restrictions.and(Restrictions.eq("user.id", userId),
					         Restrictions.eq("mirror.id",mirrorId));
			
			criteria.add(Restrictions.and(rest2, rest3));
			userMirror = (UserMirrorDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirror;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<UserMirrorDTO> getUserMirrorByMirrorId(int mirrorId) {
		List<UserMirrorDTO> userMirrorList;
		try
		{
			Session session =getCurrentSession();
			Criteria criteria = session.createCriteria(UserMirrorDTO.class);
			criteria.add(Restrictions.eq("mirror.id", mirrorId));
			userMirrorList =criteria.list();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return userMirrorList;
	}

	
	@Override
	public UserMirrorDTO getUserMirrorByBleRangeStatusTime(int mirrorId) {
		UserMirrorDTO userMirror;	
	    try
	    {
	    	Session session =getCurrentSession();
			String sql = "SELECT * FROM userMirrors where mirrorId="+mirrorId+" and bleRangeStatus = 'in' and userStatusFlag = 0 "
					     + " ORDER BY bleRangeStatusTime ASC limit 1";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(UserMirrorDTO.class);
			userMirror = (UserMirrorDTO) query.uniqueResult();
	    }catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
		return userMirror;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserMirrorDTO> getConnectedUser(int userId) {
		List<UserMirrorDTO> userMirrorList;	
	    try
	    {
	    	Session session =getCurrentSession();
	    	String sql = "select * from (select * from"
	    			+ "(SELECT * FROM userMirrors where bleRangeStatus='in' order by bleRangeStatusTime asc) data group by mirrorId )data2 where userId="+userId+" and userStatusFlag = 0";
	    	SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(UserMirrorDTO.class);
			userMirrorList = query.list();
	    }catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
		return userMirrorList;
	}

	@Override
	public void setOutOfRange(int mirrorId) {
		try
		{
		Session session=getCurrentSession();
		String sql = "update userMirrors set bleRangeStatus='out' where mirrorId = "+mirrorId;
		SQLQuery query = session.createSQLQuery(sql);
		query.executeUpdate();
		
	}catch(HibernateException exception)
	{
		logger.error(exception.getStackTrace(), exception);
		throw exception;
	}
	}

	@Override
	public void updateClockTimezone(UserMirrorModel userMirrorModel) {
		try
		{
		Session session=getCurrentSession();
		StringBuilder queryString = new StringBuilder();
		queryString
				.append("update userMirrors um set um.timeZone = :timeZone "
						+ " where um.mirrorId = :mirrorId and um.userId = :userId and um.userRole = :userRole");
		Query query = session.createSQLQuery(queryString.toString());
		query.setParameter("timeZone", userMirrorModel.getTimeZone());
		query.setParameter("userRole", userMirrorModel.getUserRole());
		query.setParameter("mirrorId", userMirrorModel.getMirror().getId());
		query.setParameter("userId", userMirrorModel.getUser().getId());
		query.executeUpdate();
	
		
	}catch(HibernateException exception)
	  {
		logger.error(exception.getStackTrace(), exception);
		throw exception;
  	  }
	}
	

	@Override
	public void removeUserMirror(UserMirrorModel useMirrorModel) {
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString
					.append("update userMirrors um set um.userStatusFlag = :userStatusFlag,um.bleRangeStatus = :bleRangeStatus "
							+ " where um.mirrorId = :mirrorId and um.userId = :userId and um.userRole = :userRole");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("userStatusFlag", useMirrorModel.getUserStatusFlag());
			query.setParameter("userRole", useMirrorModel.getUserRole());
			query.setParameter("mirrorId", useMirrorModel.getMirror().getId());
			query.setParameter("bleRangeStatus", useMirrorModel.getBleRangeStatus());
			query.setParameter("userId", useMirrorModel.getUser().getId());
			query.executeUpdate();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		
	}

	@Override
	public void updateUserMirrorStatus(int mirrorId, int userStatusFlag) {
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString
					.append("update userMirrors um set um.userStatusFlag = :userStatusFlag where um.mirrorId = :mirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("userStatusFlag", userStatusFlag);
			query.setParameter("mirrorId", mirrorId);
			query.executeUpdate();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
	}


	@Override
	public void updateTwitterCredentials(UserMirrorModel userMirrorModel,int twitterId) {
		try
		{
			Session session = getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update twitter set ");
			
			if(userMirrorModel.getTwitter()!=null)
			{
				if(userMirrorModel.getTwitter().getTwitterOauthToken()!=null )
				{
					queryString.append("twitterOauthToken = :twitterOauthToken,");
				}
				if(userMirrorModel.getTwitter().getTwitterOauthTokenSecret()!=null )
				{
					queryString.append("twitterOauthTokenSecret = :twitterOauthTokenSecret,");
				}
				if(userMirrorModel.getTwitter().getTwitterCredentialStatus()!=null )
				{
					queryString.append("twitterCredentialStatus = :twitterCredentialStatus,");
				}
				if(userMirrorModel.getTwitter().getScreenName()!=null)
				{
					queryString.append("screenName = :screenName,");	
				}
				if(userMirrorModel.getTwitter().getTwitterFollowingUser()!=null )
				{
					queryString.append("twitterFollowingUser = :twitterFollowingUser,");
				}
				if(userMirrorModel.getTwitter().getTimeLine()!=null )
				{
					queryString.append("timeLine = :timeLine,");
				}
			}
			
			queryString.deleteCharAt(queryString.length()-Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
			queryString.append(" where id= :twitterId");
			Query query = session.createSQLQuery(queryString.toString());
    		query.setParameter("twitterId", twitterId);
    		
    		if(userMirrorModel.getTwitter()!=null)
			{
    			if(userMirrorModel.getTwitter().getTwitterOauthToken()!=null)
				{
					query.setParameter("twitterOauthToken", userMirrorModel.getTwitter().getTwitterOauthToken());	
				}
    			if(userMirrorModel.getTwitter().getTwitterOauthTokenSecret()!=null)
				{
					query.setParameter("twitterOauthTokenSecret", userMirrorModel.getTwitter().getTwitterOauthTokenSecret());	
				}
    			if(userMirrorModel.getTwitter().getTwitterCredentialStatus()!=null)
				{
					query.setParameter("twitterCredentialStatus",userMirrorModel.getTwitter().getTwitterCredentialStatus());	
				}
				if(userMirrorModel.getTwitter().getScreenName()!=null)
				{
					query.setParameter("screenName",userMirrorModel.getTwitter().getScreenName());
				}
				if(userMirrorModel.getTwitter().getTwitterFollowingUser()!=null )
				{
					query.setParameter("twitterFollowingUser",userMirrorModel.getTwitter().getTwitterFollowingUser());
				}
				if(userMirrorModel.getTwitter().getTimeLine()!=null )
				{
					query.setParameter("timeLine",userMirrorModel.getTwitter().getTimeLine());
				}
			}
   		query.executeUpdate();
		
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@Override
	public TwitterDTO saveTwitter(TwitterDTO twitterDTO) {
		try
		{
			Session session = getCurrentSession();
			session.persist(twitterDTO);
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return twitterDTO;
	}

	@Override
	public UserMirrorDTO getUserMirrorByFitBitId(FitBitAccountDTO fitBitAccountDTO,int userStatusFlag) {
		UserMirrorDTO userMirrorDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(UserMirrorDTO.class);
			criteria.add(Restrictions.eq("fitbitAccount", fitBitAccountDTO));
			criteria.add(Restrictions.eq("userStatusFlag", userStatusFlag));
			userMirrorDTO = (UserMirrorDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return userMirrorDTO;
	}

	@Override
	public void updateLocation(UserMirrorDTO userMirrorDTO, LocationDTO locationDTO) {
		
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update userMirrors um set um.locationId = :locationId where um.id = :userMirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("locationId", locationDTO.getId());
			query.setParameter("userMirrorId", userMirrorDTO.getId());
			query.executeUpdate();
			session.flush();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@Override
	public void updateFitbitAccount(FitBitAccountDTO fitBitAccountDTO, UserMirrorDTO userMirrorDTO) {
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update userMirrors um set um.fitbitAccountId = :fitbitAccount where um.id = :userMirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("fitbitAccount", fitBitAccountDTO.getId());
			query.setParameter("userMirrorId", userMirrorDTO.getId());
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FitBitAccountDTO> getFitbitAccounts() {
		
		List<FitBitAccountDTO> fitBitAccounts  = new ArrayList<>();
	    try
		    {
		    	Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(FitBitAccountDTO.class);
		        fitBitAccounts = criteria.list();
				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return fitBitAccounts;
	}

	
	@Override
	public void updateBleStatus(int usermirrorId, String bleRangeStatus, String bleRangeStatusTime) {
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update userMirrors um set um.bleRangeStatus = :bleRangeStatus,um.bleRangeStatusTime = :bleRangeStatusTime where um.id = :userMirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("bleRangeStatus", bleRangeStatus);
			query.setParameter("bleRangeStatusTime", bleRangeStatusTime);
			query.setParameter("userMirrorId", usermirrorId);
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserMirrorDTO> getUserMirrorDetailByMirrorList(List<String> deviceIdList) {
		
		List<UserMirrorDTO> userList = new ArrayList<>();
		try
		{
			Session session=getCurrentSession();
			Query query = session.createQuery("FROM UserMirrorDTO WHERE mirror IN (FROM SmartMirrorDTO mirror WHERE mirror.deviceId IN (:deviceIdList) and mirror.mirrorStatus = :mirrorStatus) and userStatusFlag  = :userStatus");
			query.setParameterList("deviceIdList", deviceIdList);
			query.setParameter("mirrorStatus", 0);
			query.setParameter("userStatus", 0);
			userList = query.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return userList;
		
	}

	@Override
	public void updatecalendar(int userMirrorId, Boolean calendar, Boolean remindar,int calendarFormatId) {
		try
		{
			Session session=getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update userMirrors um set um.calendarFormatId = :calendarFormatId,um.calendarFlag = :calendarFlag,um.reminderFlag = :reminderFlag where um.id = :userMirrorId");
			Query query = session.createSQLQuery(queryString.toString());
			query.setParameter("calendarFlag", calendar);
			query.setParameter("reminderFlag", remindar);
			query.setParameter("calendarFormatId", calendarFormatId);
			query.setParameter("userMirrorId", userMirrorId);
			query.executeUpdate();
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

}
