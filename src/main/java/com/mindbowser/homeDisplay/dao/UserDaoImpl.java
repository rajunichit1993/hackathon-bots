package com.mindbowser.homeDisplay.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ONE;

import org.apache.log4j.Logger;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.model.UserModel;
import com.mindbowser.homeDisplay.util.ResourceManager;

/**
 * 
 * @author MB User : Gaurav Tripathi.
 *
 */


public class UserDaoImpl extends BaseDAO implements UserDao{

	Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	@Override
	public UserDTO getUserByAuthToken(String token){
		UserDTO userDetail = null;
		try {
			Session session = getCurrentSession();
			userDetail = (UserDTO) session.createCriteria(UserDTO.class).add(Restrictions.eq("authToken", token)).uniqueResult();
			
		} catch (HibernateException ex) {
			logger.error(ex.getStackTrace(), ex);
			throw ex;
		}
		return userDetail;
	}
	
	
	/**
	 * This method saves user details in database
	 * 
	 * @param user
	 *            object
	 * @return user object containing all user details.
	 * @throws HibernateException
	 */
	@Override
	public UserDTO saveUser(UserDTO userDTO) {
		try {
			Session session = getCurrentSession();
			session.persist(userDTO);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException.getMessage(), hibernateException);
			throw hibernateException;
		}
		return userDTO;
	}

	/**
	 * This method checks user already signed up to application 
	 * 
	 * @param phoneNumber contains user phone number
	 * @param countryCode
	 * @return list containing user objects.
	 * @throws HibernateException
	 */
	@Override
	public UserDTO checkUser(String email)
	{
		UserDTO user=null;
		try
		{
			Criteria criteria = getCurrentSession().createCriteria(UserDTO.class);
			criteria.add(Restrictions.eq("emailId", email));
			user = (UserDTO) criteria.uniqueResult();
		} catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return user;
	}
	
	
	/**
	 * This method checks user already signed up to application 
	 * 
	 * @param phoneNumber contains user phone number
	 * @param countryCode
	 * @return list containing user objects.
	 * @throws HibernateException
	 */
	
	@Override
	public UserDTO getUserUsingAuthToken(String authToken)
	{
		UserDTO user=null;
		try
		{
			Criteria criteria = getCurrentSession().createCriteria(UserDTO.class);
			criteria.add(Restrictions.eq("authToken", authToken));
			user = (UserDTO) criteria.uniqueResult();
		} catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return user;
	}

	
	/**
	 * This API will return user record using userId.
	 * 
	 *  @author MB User : Gaurav Tripathi.
	 *  @param int userId
	 *  @return UserDTO object
	 */
	@Override
	public UserDTO getUserById(int userId)
	{
		UserDTO user=null;
		try
		{
			Criteria criteria = getCurrentSession().createCriteria(UserDTO.class);
			criteria.add(Restrictions.eq("id", userId));
			user = (UserDTO) criteria.uniqueResult();
		} catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return user;
	}


	@Override
	public void updateUserTimezone(int userId, int timeZone) {
		try
		{
			String sqlquery = "update UserDTO  set userCurrentTimeZone = :userCurrentTimeZone where id = :userId";
			Query query = getCurrentSession().createQuery(sqlquery);
			query.setParameter("userCurrentTimeZone", timeZone);
			query.setParameter("userId", userId);
			query.executeUpdate();
		}catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
	}


	@Override
	public void updateClockTimezone(int userId, int timeZone) {
		try
		{
			String sqlquery = "update UserMirrorDTO  set timeZone = :timeZone where userId = :userId";
			Query query = getCurrentSession().createQuery(sqlquery);
			query.setParameter("timeZone", timeZone);
			query.setParameter("userId", userId);
			query.executeUpdate();
		}catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		
	}

	@Override
	public void updateUnit(UserModel userModel) {
		Session session = getCurrentSession();
		StringBuilder queryString = new StringBuilder();
		queryString.append("update user set ");
	
		if(userModel!=null)
		 {
			if(userModel.getDistanceUnit() !=null && !userModel.getDistanceUnit().isEmpty())
			{
				queryString.append("distanceUnit = :distanceUnit,");
			}
			if(userModel.getWaterUnit()!=null && !userModel.getWaterUnit().isEmpty())
			{
				queryString.append("waterUnit = :waterUnit,");
			}
			if(userModel.getWeightUnit()!=null && !userModel.getWeightUnit().isEmpty())
			{
				queryString.append("weightUnit = :weightUnit,");
			}if(userModel.getTemperatureUnit()!=null && !userModel.getTemperatureUnit().isEmpty())
			{
				queryString.append("temperatureUnit = :temperatureUnit,");
			}
	      }	
		
		queryString.deleteCharAt(queryString.length()-Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
		queryString.append(" where id= :userId");
		Query query = session.createSQLQuery(queryString.toString());
		query.setParameter("userId", userModel.getId());
		
		if(userModel!=null)
		{
			if(userModel.getDistanceUnit() !=null && !userModel.getDistanceUnit().isEmpty())
			{
				query.setParameter("distanceUnit", userModel.getDistanceUnit());	
			}
			if(userModel.getWaterUnit()!=null && !userModel.getWaterUnit().isEmpty())
			{
				query.setParameter("waterUnit", userModel.getWaterUnit());	
			}
			if(userModel.getWeightUnit()!=null && !userModel.getWeightUnit().isEmpty())
			{
				query.setParameter("weightUnit",userModel.getWeightUnit());
			}
			if(userModel.getTemperatureUnit()!=null && !userModel.getTemperatureUnit().isEmpty())
			{
				query.setParameter("temperatureUnit",userModel.getTemperatureUnit());
			}
			
		}
		query.executeUpdate();
		session.flush();
		
	}

}
