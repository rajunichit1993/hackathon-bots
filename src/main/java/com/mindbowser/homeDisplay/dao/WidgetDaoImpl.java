package com.mindbowser.homeDisplay.dao;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.STATUS_OFF;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.STATUS_ON;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TEMPERATURE_CELSIUS;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.TEMPERATURE_FAHRENHEIT;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_CALENDAR;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_DAILY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_HOURLEY_WEATHER;
import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.WIDGET_REMINDER;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.QuotesCategoryDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.QuotesLengthDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserCalendarDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorQuoteDTO;
import com.mindbowser.homeDisplay.dto.UserStepDTO;
import com.mindbowser.homeDisplay.dto.WeatherDataDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.StickyNotesModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserStepModel;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;
import com.mindbowser.homeDisplay.util.ResourceManager;


public class WidgetDaoImpl extends BaseDAO implements WidgetDao {

	Logger logger = Logger.getLogger(WidgetDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getAllWidget(List<String> widgetList) {
		
		List<WidgetDTO> widgets = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.in("type", widgetList));
			widgets =criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return widgets;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getWidgetListByWidgetId(List<Integer> widgetList) {
		
		List<WidgetDTO> widgets = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.in("id", widgetList));
			widgets =criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return widgets;
	}
	
	@Override
	public void saveWidgetSetting( List<WidgetSettingDTO>  widgetSettingDtos) {
		try
		{
			Session session=getCurrentSession();
			for (WidgetSettingDTO widgetSettingDTO  : widgetSettingDtos)
			{
				session.save(widgetSettingDTO);
		    }
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
   }
    
	@Override
	public WidgetSettingDTO checkWidgetSetting(WidgetSettingDTO widgetSettingDTO) {
		WidgetSettingDTO widgetSettingData;
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(WidgetSettingDTO.class);
			criteria.add(Restrictions.eq("userMirror.id", widgetSettingDTO.getUserMirror().getId()));
			criteria.add(Restrictions.eq("widget.id",widgetSettingDTO.getWidget().getId()));
			widgetSettingData = (WidgetSettingDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return widgetSettingData;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void socketUpdateWidgetSetting(List<WidgetSettingModel> widgetSettingList,UserMirrorDTO userMirrorDTO) {
		try
		{
			Session session = getCurrentSession();
			for (WidgetSettingModel widgetSettingModel : widgetSettingList) {
				StringBuilder queryString = new StringBuilder();
				queryString
						.append("update widgetSetting ws set ws.status = :status,"
								+ "ws.xPos = :xPos,ws.yPos = :yPos,ws.height = :height,ws.width = :width,"
								+ "ws.deviceWidth = :deviceWidth,ws.deviceHeight = :deviceHeight,"
								+ "ws.minWidth = :minWidth,ws.minHeight = :minHeight "
								+ " where ws.id = :id");
				Query query = session.createSQLQuery(queryString.toString());
				query.setParameter("status", widgetSettingModel.getStatus());
				query.setParameter("id", widgetSettingModel.getId());
				query.setParameter("xPos", widgetSettingModel.getxPos());
				query.setParameter("yPos", widgetSettingModel.getyPos());
				query.setParameter("height", widgetSettingModel.getHeight());
				query.setParameter("minWidth", widgetSettingModel.getMinWidth());
				query.setParameter("minHeight", widgetSettingModel.getMinHeight());
				query.setParameter("width", widgetSettingModel.getWidth());
				query.setParameter("deviceWidth", widgetSettingModel.getDeviceWidth());
				query.setParameter("deviceHeight", widgetSettingModel.getDeviceHeight());
                query.executeUpdate();
			}
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}
	
	
	
	@Override
	public void newUpdateWidgetSetting(List<WidgetSettingDTO> widgetSettingList,UserMirrorDTO userMirrorDTO,MirrorPageDTO mirrorPageDTO) {
		try
		{
			Session session = getCurrentSession();
			for (WidgetSettingDTO widgetSettingDTO : widgetSettingList) {
				
				if(widgetSettingDTO.getHeight()<=0)
				{
					widgetSettingDTO.setHeight(widgetSettingDTO.getMinHeight());
				}
				if(widgetSettingDTO.getWidth()<=0)
				{
					widgetSettingDTO.setWidth(widgetSettingDTO.getMinWidth());
				}
				if(widgetSettingDTO.getxPos()<=0)
				{
					widgetSettingDTO.setxPos(5);
				}
				if(widgetSettingDTO.getyPos()<=0)
				{
					widgetSettingDTO.setyPos(5);
				}
				widgetSettingDTO.setUserMirror(userMirrorDTO);
				widgetSettingDTO.setMirrorPage(mirrorPageDTO);
			    session.saveOrUpdate(widgetSettingDTO);
			   }
	    	session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetSettingDTO> getWidgetSetting(UserMirrorDTO userMirrorDTO,List<WidgetDTO> widgetDTOList) {
		List<WidgetSettingDTO> widgetSettingData = new ArrayList<>();
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(WidgetSettingDTO.class);
			criteria.add(Restrictions.eq("userMirror.id", userMirrorDTO.getId()));
			criteria.addOrder(Order.asc("mirrorPage.id"));
			criteria.add(Restrictions.in("widget", widgetDTOList));
			widgetSettingData = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return widgetSettingData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getWidgetSettingByStatusAndWidgetList(UserMirrorDTO userMirrorDTO,List<WidgetDTO> widgetDTOList) {
		List<WidgetDTO> WidgetList = new ArrayList<>();
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(WidgetSettingDTO.class);
			criteria.add(Restrictions.eq("userMirror.id", userMirrorDTO.getId()));
			criteria.add(Restrictions.in("widget", widgetDTOList));
			criteria.add(Restrictions.eq("status", ResourceManager.getProperty(STATUS_ON)));
			criteria.setProjection(Projections.property("widget"));
			WidgetList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return WidgetList;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<UserCalendarModel>  getCalendarDataByLimit(int userId,String fromDate,int timezone,int limit,Boolean calendarFlag,Boolean reminderFlag) {
		List<UserCalendarModel> calendarList = new ArrayList<>();
	    try
		    {
	    	    List<String> widgetType = new ArrayList<>();
	    	    if(calendarFlag == true)
		    	{
		    		widgetType.add(ResourceManager.getProperty(WIDGET_CALENDAR));
		    	}
		    	if(reminderFlag == true)
		    	{
		    		widgetType.add(ResourceManager.getProperty(WIDGET_REMINDER));		    		
		    	}
		    	StringBuilder queryString = new StringBuilder();
				queryString.append("SELECT id,event,date_format(date_add(timestamp(startDate),interval "+timezone+ " minute),'%Y-%m-%d %H:%i:%s') as startDate, "
                        + "date_format(date_add(timestamp(endDate),interval "+timezone+" minute),'%Y-%m-%d %H:%i:%s') as endDate,allDay,location "
                        +"FROM userCalendar where userId = :userId and calendarType in (:calendarType) having startDate >= timestamp('"+fromDate +"')"
                        		+ " or endDate >= timestamp('"+fromDate +"') ORDER BY startDate ASC limit :limit"); 
				Query query = getCurrentSession().createSQLQuery(queryString.toString()).setResultTransformer(Transformers.aliasToBean(UserCalendarModel.class));
				query.setParameterList("calendarType", widgetType);
				query.setParameter("userId",userId);
				query.setParameter("limit",limit);
				calendarList = query.list();
			    				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return calendarList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserStepModel> getStepData(int userId,String startDate,int timezone) {
	    List<UserStepModel> userStepList;	
	    try
	    {
	    	Session session =getCurrentSession();
	    	String sql = "SELECT round(value) as value, date_format(date_add(timestamp(time),interval " +timezone+ " minute),'%Y-%m-%d') as steptime"
					+ "  FROM userSteps where userId = "+userId+" having steptime >= DATE_SUB('"+startDate +"',INTERVAL 6 DAY) ORDER BY steptime DESC";
			SQLQuery query = (SQLQuery) session.createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(UserStepModel.class));
		    userStepList = query.list();
	    }catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
		return userStepList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HealthDataModel> getHealthData(HealthDataModel healthDataModel,Double conversionValue,String startDate,WidgetDTO widgetDTO) {
	    List<HealthDataModel> healthDataList;	
	    try
		{
	    	Session session = getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("SELECT round((healthValue*"+conversionValue+"),2) as healthValue, round((goalValue*"+conversionValue+"),2) as goalValue,date_format(healthTime,'%Y-%m-%d') as healthTime FROM healthdata where ");
			
			if(healthDataModel.getUser()!=null)
			{
				if(healthDataModel.getUser().getId()!= 0 )
				{
					queryString.append("userId = "+healthDataModel.getUser().getId());
				}
			}else
			{
				if(healthDataModel.getFitbit().getId()!= 0 )
				{
					queryString.append("fitBitId = "+healthDataModel.getFitbit().getId());
				}
			}
			queryString.append(" and widgetId = "+widgetDTO.getId()+"  having healthTime >= DATE_SUB('"+startDate +"',INTERVAL 6 DAY) ORDER BY healthTime ASC");
			Query query = session.createSQLQuery(queryString.toString()).setResultTransformer(Transformers.aliasToBean(HealthDataModel.class));
    		healthDataList = query.list();
    		session.flush();
    		session.clear();
    		session.flush();
    		session.clear();
		}
	    catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
		    return healthDataList;
	}
	
		
	@Override
	public void deleteStepData(int userId,String startDate) {
	    try
	    {
	    	Session session = getCurrentSession();
	    	String sql = "delete  FROM userSteps where userId = "+userId+" and time >= DATE_SUB('"+startDate +"',INTERVAL 6 DAY) "
					+ "ORDER BY time DESC";
			SQLQuery query = session.createSQLQuery(sql);
		    query.executeUpdate();
		    session.flush();
		    session.clear();
		    
	    }catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
	}
		
	@Override
	public void saveHealthKitData(int userId,List<UserStepDTO> userStepDtoList) {
		try
		{
			Session session = getCurrentSession();
			for (UserStepDTO userStepDto : userStepDtoList) {
				  UserDTO userDTO = new UserDTO();
				  userDTO.setId(userId);
				  userStepDto.setUser(userDTO);
				 session.save(userStepDto);
			}
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public LocationDTO checkLocation(String latitude, String longitude) {
		
		LocationDTO locationDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(LocationDTO.class);
			criteria.add(Restrictions.eq("latitude", latitude));
			criteria.add(Restrictions.eq("longitude", longitude));
			locationDTO=(LocationDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return locationDTO;
	}

	@Override
	public void saveCalenderData(int userId,List<UserCalendarDTO> userCalendarList) {
		try
		{
			Session session = getCurrentSession();
			for (UserCalendarDTO userCalendarDTO : userCalendarList) {
				  UserDTO userDTO = new UserDTO();
				  userDTO.setId(userId);
				  userCalendarDTO.setUser(userDTO);
				 session.save(userCalendarDTO);
			}
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@Override
	public void deleteCalenderData(int userId) {
		try
		{
			Session session = getCurrentSession();
			String sql = "delete FROM userCalendar where userId= "+userId;
	    	SQLQuery query = session.createSQLQuery(sql);
		    query.executeUpdate();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalendarFormatDTO> calendarFormatList() {
		List<CalendarFormatDTO> calendarFormatList = new ArrayList<>();
	    try
		    {
		    	Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(CalendarFormatDTO.class);
		    	calendarFormatList = criteria.list();
				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return calendarFormatList;
	}

	@Override
	public CalendarFormatDTO getCalendarFormatById(int id) {
		
		CalendarFormatDTO calendarFormatDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(CalendarFormatDTO.class);
			criteria.add(Restrictions.eq("id", id));
			calendarFormatDTO=(CalendarFormatDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return calendarFormatDTO;
	}

	
	@Override
	public void deleteMirrorPage(int userMirrorId) {
		try
		{
			Session session = getCurrentSession();
			String sql = "delete FROM mirrorpage where userMirrorId= "+userMirrorId;
	    	SQLQuery query = session.createSQLQuery(sql);
		    query.executeUpdate();
		    session.flush();
		    session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
	}

	
	@Override
	public MirrorPageDTO saveMirrorPage(MirrorPageDTO mirrorPageDTO) {
		try
		{
			Session session = getCurrentSession();
		    session.save(mirrorPageDTO);
		    session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return mirrorPageDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MirrorPageDTO> getMirrorPage(int userMirrorId) {
		List<MirrorPageDTO> mirrorPageDtoList = new ArrayList<>();
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(MirrorPageDTO.class);
			criteria.add(Restrictions.eq("userMirror.id", userMirrorId));
			mirrorPageDtoList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return mirrorPageDtoList;
	}

	@Override
	public TwitterDTO getTwitter(int twitterId) {
		
		TwitterDTO twitterDTO;
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(TwitterDTO.class);
			criteria.add(Restrictions.eq("id", twitterId));
			twitterDTO = (TwitterDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return twitterDTO;
	}

	
	@Override
	public LocationDTO saveLocationData(LocationDTO locationDTO) {
			try
			{
				Session session = getSessionFactory().getCurrentSession();
				session.save(locationDTO);
				session.flush();
			}catch(HibernateException exception)
			{
				logger.error(exception.getMessage(), exception);
				throw exception;
			}
			return locationDTO;
	}

	
	@Override
	public void saveWeatherDataList(List<WeatherDataDTO> weatherDataDTOList) {
		try
		{
			Session session = getCurrentSession();
			for (WeatherDataDTO weatherDataDTO : weatherDataDTOList) {
				session.save(weatherDataDTO);
			}
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WeatherDataDTO> getWeatherData(int locationId,String weatherType,String temperatureUnit) {
		List<WeatherDataDTO> dailyWeatherDTOList = new ArrayList<>();
		try
		{
			StringBuilder queryString = new StringBuilder();
			if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_FAHRENHEIT)))
			{
				queryString.append("SELECT id,weatherTime, round((minTemperature *9/5)+32) as minTemperature,round((currentTemperature *9/5)+32) as currentTemperature,round((maxTemperature *9/5)+32) as maxTemperature,icon,description,locationId,weatherType from weatherdata where locationId = "+locationId+" and  "
				   		+ "weatherType = '"+weatherType+ "'");
			}else if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_CELSIUS)))
			{
				queryString.append("SELECT * from weatherdata where locationId = "+locationId+" and  "
				   		+ "weatherType = '"+weatherType+ "'");	
			}
			 Query query = getCurrentSession().createSQLQuery(queryString.toString()).addEntity(WeatherDataDTO.class);
	    	dailyWeatherDTOList = query.list();
	    	
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return dailyWeatherDTOList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WeatherDataDTO> getHourleyWeatherData(int locationId, List<String> dates,String temperatureUnit) {
		List<WeatherDataDTO> dailyWeatherDTOList = new ArrayList<>();
		try
		{
			StringBuilder queryString = new StringBuilder();
			if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_FAHRENHEIT)))
			{
				queryString.append("SELECT id,weatherTime, round((minTemperature *9/5)+32) as minTemperature,round((currentTemperature *9/5)+32) as currentTemperature,round((maxTemperature *9/5)+32) as maxTemperature,icon,description,locationId,weatherType from weatherdata where locationId = "+locationId+" and  "
				   		+ "weatherType = '"+ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)+ "' having date_format(weatherTime,'%Y-%m-%d %H') in (:dates)");
			}else if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_CELSIUS)))
			{
				queryString.append("SELECT * from weatherdata where locationId = "+locationId+" and  "
				   		+ "weatherType = '"+ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)+ "' having date_format(weatherTime,'%Y-%m-%d %H') in (:dates)");	
			}
			Query query = getCurrentSession().createSQLQuery(queryString.toString()).addEntity(WeatherDataDTO.class);
			query.setParameterList("dates", dates);
	    	dailyWeatherDTOList = query.list();
	    	
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return dailyWeatherDTOList;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<WeatherDataDTO> getCurrentWeatherData(int locationId,String dailyDate,String hourleyDate,String temperatureUnit) {
		List<WeatherDataDTO> dailyWeatherDTOList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			String sql = null;
			if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_FAHRENHEIT)))
			{
				sql = "SELECT id,weatherTime, round((minTemperature *9/5)+32) as minTemperature,round((currentTemperature *9/5)+32) as currentTemperature,round((maxTemperature *9/5)+32) as maxTemperature,icon,description,locationId,weatherType from weatherdata where locationId  ="+locationId+" having"
		    			+ " (weatherType = '"+ResourceManager.getProperty(WIDGET_DAILY_WEATHER)+"' and date_format(weatherTime,'%Y-%m-%d') = '"+dailyDate+"') or (weatherType = '"+ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)+"' and  date_format(weatherTime,'%Y-%m-%d %H')='"+hourleyDate+"')";
			}else if(temperatureUnit.equals(ResourceManager.getProperty(TEMPERATURE_CELSIUS)))
			{
				sql = "SELECT * from weatherdata where locationId  ="+locationId+" having"
		    			+ " (weatherType = '"+ResourceManager.getProperty(WIDGET_DAILY_WEATHER)+"' and date_format(weatherTime,'%Y-%m-%d') = '"+dailyDate+"') or (weatherType = '"+ResourceManager.getProperty(WIDGET_HOURLEY_WEATHER)+"' and  date_format(weatherTime,'%Y-%m-%d %H')='"+hourleyDate+"')";	
			}
	    	SQLQuery query = session.createSQLQuery(sql).addEntity(WeatherDataDTO.class);
	    	dailyWeatherDTOList = query.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return dailyWeatherDTOList;
	}

/*	@Transactional(propagation = Propagation.REQUIRES_NEW)*/
	@Override
	public void deleteWeatherData(int locationId) {
		try
		{
			Session session = getCurrentSession();
			String sql = "delete FROM weatherdata where locationId= "+locationId;
	    	SQLQuery query = session.createSQLQuery(sql);
		    query.executeUpdate();
		    session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuotesCategoryDTO> quotesCategoryList() {
		List<QuotesCategoryDTO> quotesCategoryList = new ArrayList<>();
	    try
		    {
	    	    Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(QuotesCategoryDTO.class);
		        criteria.add(Restrictions.eq("categoryStatus", true));
		        quotesCategoryList = criteria.list();
		        session.flush();
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return quotesCategoryList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuotesLengthDTO> quotesCategoryLengthList() {
		List<QuotesLengthDTO> quotesLengthList = new ArrayList<>();
	    try
		    {
		    	Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(QuotesLengthDTO.class);
		        quotesLengthList = criteria.list();
				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return quotesLengthList;
	}


	@Override
	public QuotesDTO getQuotesData(int quotesId) {
		QuotesDTO quotes;
	    try
		    {
		    	Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(QuotesDTO.class);
		        criteria.add(Restrictions.eq("id", quotesId));
		        quotes = (QuotesDTO) criteria.uniqueResult();
				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return quotes;
	}

	@Override
	public QuotesDTO getQuotesDataByLengthCategory(int categoryId, int lengthId) {
		
		QuotesDTO categoryLengthDTO;
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(QuotesDTO.class);
			criteria.add(Restrictions.eq("quotesCategory.id", categoryId));
			criteria.add(Restrictions.eq("quotesLength.id", lengthId));
			categoryLengthDTO = (QuotesDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return categoryLengthDTO;
	}

	@Override
	public void saveQuotesCategoryList(List<QuotesCategoryDTO> categoryDTOList) {
		try
		{
			Session session = getCurrentSession();
			for (QuotesCategoryDTO categoryDTO : categoryDTOList) {
				 session.save(categoryDTO);
			}
			session.flush();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@Override
	public void saveQuotesData(List<QuotesDTO> quotesList) {
		try
		{
			Session session = getCurrentSession();
			for (QuotesDTO quotesDTO : quotesList) {
				 session.save(quotesDTO);
			}
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuotesDTO> getQuotesData() {
		
		List<QuotesDTO> quotes = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
            Criteria criteria = session.createCriteria(QuotesDTO.class);
            quotes = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return quotes;
	}

	@Override
	public void updateQuotesData(List<QuotesDTO> quotesList) {
		try
		{
			Session session = getCurrentSession();
			for (QuotesDTO quotesDTO : quotesList) {
				Query query = session.createQuery("update QuotesDTO set quotes = :quotes , author = :author" +
						" where quotesLength.id = :quotesLengthId and quotesCategory.id = :quotesCategoryId");
		     query.setParameter("quotes", quotesDTO.getQuotes());
		     query.setParameter("author", quotesDTO.getAuthor());
		     query.setParameter("quotesLengthId", quotesDTO.getQuotesLength().getId());
		     query.setParameter("quotesCategoryId", quotesDTO.getQuotesCategory().getId());
		     query.executeUpdate();
			 }
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
		public List<WidgetDTO> getHealthKitWidgetListByMasterCategory(List<String> categoryList) {
		
		List<WidgetDTO> healthKitWidgetList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.in("masterCategory", categoryList));
			healthKitWidgetList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthKitWidgetList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getWidgetByStatus(WidgetModel widgetModel) {
		List<WidgetDTO> widgetList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(WidgetSettingDTO.class);
			if(widgetModel.getMasterCategory()!=null)
			{
				criteria.add(Restrictions.eq("widget.masterCategory", widgetModel.getMasterCategory()));	
			}
			if(widgetModel.getSubCategory()!=null)
			{
				criteria.add(Restrictions.eq("widget.subCategory", widgetModel.getSubCategory()));
			}
			
			criteria.add(Restrictions.eq("status",ResourceManager.getProperty(STATUS_ON) ));
			criteria.setProjection(Projections.property("widget"));
			widgetList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return widgetList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getWidgetBySubCategory(List<String> categoryList,String masterCategory) {
		List<WidgetDTO> healthKitWidgetList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.in("subCategory", categoryList));
			criteria.add(Restrictions.eq("masterCategory", masterCategory));
			healthKitWidgetList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthKitWidgetList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getSubcategoryWidgetByStatus(List<String> subCategoryList,String masterCategory,int userMirrorId) {
		List<WidgetDTO> healthKitWidgetList = new ArrayList<>();
		
		try
		{
			Query query = getCurrentSession().createQuery("select wd from WidgetSettingDTO as ws join ws.widget as wd join ws.userMirror as um "
					+ "where um.id ="+userMirrorId+" and wd.masterCategory = '"+masterCategory+"' and ws.status = 'on' and wd.subCategory in (:subCategory)");
			 query.setParameterList("subCategory",subCategoryList);
			 healthKitWidgetList = query.list();
				 
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
	return healthKitWidgetList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getNewWidgetList(UserMirrorDTO userMirrorDTO) {
		List<WidgetDTO> widgetList;	
	    try
	    {
	    	Session session =getCurrentSession();
	    	String sql = "select * from  widgets where id not in (select widgetId from widgetSetting where userMirrorId="+userMirrorDTO.getId()+")";
	    	SQLQuery query = (SQLQuery) session.createSQLQuery(sql);
			query.addEntity(WidgetDTO.class);
			widgetList = query.list();
	    }catch(HibernateException exception)
	    {
	    	logger.error(exception.getMessage());
	    	throw exception;
	    }
		return widgetList;
	}

	@Override
	public void UpdatAllWidgetsStatusByWidgetList(UserMirrorDTO userMirrorDTO, List<Integer> widgetDTOList,MirrorPageDTO mirrorPageDTO) {
		try
		{
			StringBuilder queryString = new StringBuilder();
			queryString.append("update widgetSetting ws set ws.status = :status, ws.mirrorPageId = :mirrorPageId  where ws.userMirrorId = :userMirrorId and ws.widgetId not in (:widget)"); 
			Query query = getCurrentSession().createSQLQuery(queryString.toString()).addEntity(WeatherDataDTO.class);
			query.setParameterList("widget", widgetDTOList);
			query.setParameter("status",ResourceManager.getProperty(STATUS_OFF));
			query.setParameter("mirrorPageId",mirrorPageDTO.getId());
			query.setParameter("userMirrorId",userMirrorDTO.getId());
			query.executeUpdate();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		getCurrentSession().flush();
		getCurrentSession().clear();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetDTO> getWidgetByTypeAndMasterCategory(List<String> widgetList, String masterCategory) {
		List<WidgetDTO> healthKitWidgetList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.in("type", widgetList));
			criteria.add(Restrictions.eq("masterCategory", masterCategory));
			healthKitWidgetList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthKitWidgetList;
	}

	@Override
	public MirrorPageDTO getMirrorPageByPageNumber(UserMirrorDTO userMirrorDTO, int pageNumber) {
		MirrorPageDTO mirrorPageDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(MirrorPageDTO.class);
			criteria.add(Restrictions.eq("userMirror", userMirrorDTO));
			criteria.add(Restrictions.eq("pageNumber", pageNumber));
			mirrorPageDTO=(MirrorPageDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return mirrorPageDTO;
	}

	@Override
	public void updateStickyNotesContent(List<StickyNotesModel> stickyNotesList) {
		try
		{
			Session session = getCurrentSession();
			for (StickyNotesModel stickyNotesModel : stickyNotesList) {
				
				StringBuilder queryString = new StringBuilder();
				queryString
						.append("update stickynotes sn set sn.data = :data,"
								+ "sn.name = :name where sn.id = :id");
				Query query = session.createSQLQuery(queryString.toString());
				query.setParameter("data", stickyNotesModel.getData());
				query.setParameter("name", stickyNotesModel.getName());
				query.setParameter("id", stickyNotesModel.getId());
				query.executeUpdate();
			}
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@Override
	public void removeStickyNotes(List<WidgetSettingDTO> widgetSettingList){
		
		try
		{
			Session session = getCurrentSession();
			for (WidgetSettingDTO widgetSettingDTO : widgetSettingList) {
				session.delete(widgetSettingDTO);
			}
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
	}

	@Override
	public void removeTest(List<WidgetSettingDTO> widgetSettingList) {
		try
		{
			Session session = getCurrentSession();
			for (WidgetSettingDTO widgetSettingDTO : widgetSettingList) {
				session.delete(widgetSettingDTO);
			}
			session.flush();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HealthDataModel> getHealthDataByLimit(HealthDataModel healthDataModel, Double conversionValue,
			WidgetDTO widgetDTO) {
		 List<HealthDataModel> healthDataList;	
		    try
			{
		    	Session session = getCurrentSession();
				StringBuilder queryString = new StringBuilder();
				queryString.append("SELECT * FROM (SELECT id,round((healthValue*"+conversionValue+"),2) as healthValue, round((goalValue*"+conversionValue+"),2) as goalValue,date_format(healthTime,'%Y-%m-%d') as healthTime FROM healthdata where ");
				if(healthDataModel.getUser()!=null)
				{
					if(healthDataModel.getUser().getId()!= 0 )
					{
						queryString.append("userId = "+healthDataModel.getUser().getId());
					}
				}else
				{
					if(healthDataModel.getFitbit().getId()!= 0 )
					{
						queryString.append("fitBitId = "+healthDataModel.getFitbit().getId());
					}
				}
				queryString.append(" and widgetId = "+widgetDTO.getId()+" ORDER BY healthTime DESC limit 7)sub ORDER BY id ASC");
				Query query = session.createSQLQuery(queryString.toString()).setResultTransformer(Transformers.aliasToBean(HealthDataModel.class));
	    		healthDataList = query.list();
	    		session.flush();
	    		session.clear();
			}
		    catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			    return healthDataList;
	}

	@Override
	public void deleteQuotesByUserMirrorId(int userMirrorId) {
		try
		{
			Session session = getCurrentSession();
			String sql = "delete FROM usermirrorquotes where userMirrorId= "+userMirrorId;
	    	SQLQuery query = session.createSQLQuery(sql);
		    query.executeUpdate();
		    session.flush();
		    session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		
	}

	@Override
	public void addQuotesForUser(UserMirrorDTO userMirrors,List<QuotesDTO> quotes) {
		try
		{
			Session session=getCurrentSession();
			for (QuotesDTO quotesDTO  : quotes)
			{
				UserMirrorQuoteDTO userMirrorQuoteDTO = new UserMirrorQuoteDTO();
				userMirrorQuoteDTO.setUserMirrors(userMirrors);
				userMirrorQuoteDTO.setQuotes(quotesDTO);
				session.save(userMirrorQuoteDTO);
		    }
			session.flush();
			session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuotesDTO> getQuotesByCategoryAndLength(List<QuotesCategoryDTO> quotesCategories,
			QuotesLengthDTO quotesLength) {
		List<QuotesDTO> quotesList = new ArrayList<>();
		try
		{
			Session session = getCurrentSession();
			Criteria criteria  = session.createCriteria(QuotesDTO.class);
			criteria.add(Restrictions.in("quotesCategory", quotesCategories));
			criteria.add(Restrictions.eq("quotesLength", quotesLength));
			quotesList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return quotesList;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<QuotesDTO> getQuotesByUserMrrrorId(int userMirrorId) {
		List<QuotesDTO> quotesList = new ArrayList<>();
		try
		{
			Session session=getCurrentSession();
			Criteria criteria = session.createCriteria(UserMirrorQuoteDTO.class);
			criteria.add(Restrictions.eq("userMirrors.id", userMirrorId));
			criteria.setProjection(Projections.property("quotes"));
			quotesList = criteria.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return quotesList;
	}

	@Override
	public void deleteStickyNotesByUserMirror(int userMirrorId) {
		try
		{
			Session session = getCurrentSession();
			Query query = session.createSQLQuery("delete from widgetSetting where userMirrorId = :userMirrorId and status = :status and widgetId=49");
			query.setParameter("userMirrorId", userMirrorId);
			query.setParameter("status", ResourceManager.getProperty(STATUS_ON));
			query.executeUpdate();
			session.flush();
		    session.clear();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
	}

	@Override
	public WidgetDTO getWidgetById(int widgetId) {
		WidgetDTO widget;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria= session.createCriteria(WidgetDTO.class);
			criteria.add(Restrictions.eq("id", widgetId));
			widget = (WidgetDTO) criteria.uniqueResult();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		return widget;
	}

	@Override
	public void updateBetaUsersFitbitData(FitBitAccountDTO fitBitAccountDTO, int dateDifference) {
		 try
		    {
		    	Session session = getCurrentSession();
		    	String sql = "update healthdata set healthTime = date_format(date_add(timestamp(healthTime),interval "+dateDifference+" day),'%Y-%m-%d') where fitBitId = :fitBitId";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("fitBitId", fitBitAccountDTO.getId());
			    query.executeUpdate();
			    session.flush();
			    session.clear();
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
	}

	@Override
	public void updateBetaUsersHealthKitData(UserDTO userDTO, int dateDifference) {
		 try
		    {
		    	Session session = getCurrentSession();
		    	String sql = "update healthdata set healthTime = date_format(date_add(timestamp(healthTime),interval "+dateDifference+" day),'%Y-%m-%d') where userId = :userId";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("userId", userDTO.getId());
				query.executeUpdate();
			    session.flush();
			    session.clear();
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
	}

	@Override
	public void updateBetaUsersCalendarData(UserDTO userDTO, int dateDifference) {
		 try
		    {
		    	Session session = getCurrentSession();
		    	String sql = "update userCalendar set "
		    				+ "startDate = date_format(date_add(timestamp(startDate),interval "+dateDifference+" day),'%Y-%m-%d  %H:%i:%s'),"
		    				+ "endDate = date_format(date_add(timestamp(endDate),interval "+dateDifference+" day),'%Y-%m-%d  %H:%i:%s'),"
		    				+ "occuranceDate = date_format(date_add(timestamp(occuranceDate),interval "+dateDifference+" day),'%Y-%m-%d  %H:%i:%s') "
		    				+ "where userId = :userId";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("userId", userDTO.getId());
				query.executeUpdate();
			    session.flush();
			    session.clear();
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
	}

	@Override
	public HealthDataDTO getHealthKitLastAccessDate(UserDTO userDTO, WidgetDTO widgetDTO) {
		HealthDataDTO healthDataDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(HealthDataDTO.class);
			criteria.add(Restrictions.eq("widget", widgetDTO));
			criteria.add(Restrictions.eq("user",userDTO));
			criteria.addOrder(Order.desc("healthTime"));
			criteria.setMaxResults(1);
			healthDataDTO = (HealthDataDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return healthDataDTO;
	}

	@Override
	public UserCalendarDTO getCalendarLastAccessDate(UserDTO userDTO) {
		UserCalendarDTO userCalendarDTO;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(UserCalendarDTO.class);
			criteria.add(Restrictions.eq("user",userDTO));
			criteria.add(Restrictions.eq("calendarType",ResourceManager.getProperty(WIDGET_CALENDAR)));
			criteria.addOrder(Order.desc("startDate"));
			criteria.setMaxResults(1);
			userCalendarDTO = (UserCalendarDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		return userCalendarDTO;
	}

	@Override
	public void deleteWidgetSetting(List<UserMirrorDTO> userMirrorList) {
		
		try
		{
			Session session = getCurrentSession();
			Query q = session.createQuery("DELETE FROM WidgetSettingDTO widgetsetting WHERE userMirror IN (:userMirrorList)");
			q.setParameterList("userMirrorList", userMirrorList);
			q.executeUpdate();
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
		
	}
}
