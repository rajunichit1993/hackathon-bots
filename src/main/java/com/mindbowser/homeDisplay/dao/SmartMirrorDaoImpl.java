package com.mindbowser.homeDisplay.dao;

import static com.mindbowser.homeDisplay.constant.HomeDisplayConstants.FLAG_ONE;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mindbowser.homeDisplay.dto.BetaUserSmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;
import com.mindbowser.homeDisplay.util.ResourceManager;

public class SmartMirrorDaoImpl extends BaseDAO implements SmartMirrorDao{

	Logger logger = Logger.getLogger(SmartMirrorDaoImpl.class);
	
	
	@Override
	public SmartMirrorDTO saveMirror(SmartMirrorDTO smartMirrorDTO) {
	
		try
		{
			Session session = getCurrentSession();
			session.persist(smartMirrorDTO);
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return smartMirrorDTO;
	}

	
	@Override
	public BetaUserSmartMirrorDTO saveBetaMirror(BetaUserSmartMirrorDTO betaUserSmartMirrorDTO) {
	
		try
		{
			Session session = getCurrentSession();
			session.persist(betaUserSmartMirrorDTO);
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return betaUserSmartMirrorDTO;
	}

	
	@Override
	public SmartMirrorDTO getMirrorByDeviceId(String deviceId) {
		
		SmartMirrorDTO mirrorDTO = null;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria =  session.createCriteria(SmartMirrorDTO.class);
			criteria.add(Restrictions.eq("deviceId", deviceId));
			criteria.addOrder(Order.desc("id"));
			criteria.setMaxResults(1);
			mirrorDTO = (SmartMirrorDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return mirrorDTO;
	}

	
	@Override
	public SmartMirrorDTO checkMirror(int major, int minor,int mirrorStatus) {
		
		SmartMirrorDTO mirrorDTO = null;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria =  session.createCriteria(SmartMirrorDTO.class);
			criteria.add(Restrictions.eq("major", major));
			criteria.add(Restrictions.eq("minor", minor));
			criteria.add(Restrictions.eq("mirrorStatus", mirrorStatus));
			mirrorDTO = (SmartMirrorDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return mirrorDTO;
	}
	
	@Override
	public SmartMirrorDTO getMajorMinor() {
		SmartMirrorDTO smartMirrorDTO = null;
		try {
			Session session = getCurrentSession();
			Criteria c = session.createCriteria(SmartMirrorDTO.class);
			c.addOrder(Order.desc("id"));
			c.setMaxResults(1);
			smartMirrorDTO =(SmartMirrorDTO)c.uniqueResult();
    		} 
		catch (HibernateException ex) {
			logger.error(ex.getStackTrace(), ex);
			throw ex;
		   }
		return smartMirrorDTO;
	}

	@Override
	public SmartMirrorDTO getMirrorById(int mirrorId) {
		SmartMirrorDTO mirrorDTO = null;
		try
		{
			Session session = getCurrentSession();
			Criteria criteria =  session.createCriteria(SmartMirrorDTO.class);
			criteria.add(Restrictions.eq("id", mirrorId));
			mirrorDTO = (SmartMirrorDTO) criteria.uniqueResult();
			
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return mirrorDTO;
	}

	@Override
	public void updateMirrorstatus(String deviceId, int mirrorStatus) {
		try
		{
			String sqlquery = "update SmartMirrorDTO  set mirrorStatus = :mirrorStatus where deviceId = :deviceId";
			Query query = getCurrentSession().createQuery(sqlquery);
			query.setParameter("mirrorStatus", mirrorStatus);
			query.setParameter("deviceId", deviceId);
			query.executeUpdate();
		}catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		
	}
	
	@Override
	public void updateBetaMirrorstatus(String deviceId, int mirrorStatus) {
		try
		{
			String sqlquery = "update BetaUserSmartMirrorDTO  set mirrorStatus = :mirrorStatus where deviceId = :deviceId";
			Query query = getCurrentSession().createQuery(sqlquery);
			query.setParameter("mirrorStatus", mirrorStatus);
			query.setParameter("deviceId", deviceId);
			query.executeUpdate();
		}catch (HibernateException exception)
		{
			logger.error(exception.getMessage(), exception);
			throw exception;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SmartMirrorDTO> getMirrorListByMirrorUpdateDateTime(String startDateTime,String endDateTime) {
		
	   List<SmartMirrorDTO> smartMirrorList = new ArrayList<>();
		try
		{
			Session session =getCurrentSession();
			String sql = "SELECT * FROM smartMirror where date_format(firmwareUpdateTime,'%H:%i') >= '"+startDateTime+"' and date_format(firmwareUpdateTime,'%H:%i') < '"+endDateTime+"'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(SmartMirrorDTO.class);
			smartMirrorList = query.list();
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		return smartMirrorList;
		
	}

	@Override
	public void saveMirrorSetting(List<SmartMirrorDTO> smartMirrorDTOList) {
		try
		{
			Session session =getCurrentSession();
			for (SmartMirrorDTO smartMirrorDTO : smartMirrorDTOList) {
				session.saveOrUpdate(smartMirrorDTO);
			}
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
	}

	@Override
	public void updateMirrorDetail(SmartMirrorModel smartMirrorModel) {
		try
		{
			Session session = getCurrentSession();
			StringBuilder queryString = new StringBuilder();
			queryString.append("update smartMirror set ");
			
			if(smartMirrorModel!=null)
			{
				if(smartMirrorModel.getWifissid()!=null && !smartMirrorModel.getWifissid().isEmpty())
				{
					queryString.append("wifissid = :wifissid,");
				}
				if(smartMirrorModel.getDelay()!=null)
				{
					queryString.append("delay = :delay,");
				}
				if(smartMirrorModel.getAutoUpdate()!=null)
				{
					queryString.append("firmwareUpdateTime = :firmwareUpdateTime,");
					queryString.append("autoUpdate = :autoUpdate,");
				}
				if(smartMirrorModel.getDeviceMode()!=null && !smartMirrorModel.getDeviceMode().isEmpty())
				{
					queryString.append("deviceMode = :deviceMode,");
				}
			}
			
			queryString.deleteCharAt(queryString.length()-Integer.parseInt(ResourceManager.getProperty(FLAG_ONE)));
			queryString.append(" where id= :mirrorId");
			Query query = session.createSQLQuery(queryString.toString());
    		query.setParameter("mirrorId", smartMirrorModel.getId());
    		
			if(smartMirrorModel.getWifissid()!=null && !smartMirrorModel.getWifissid().isEmpty())
			{
				query.setParameter("wifissid", smartMirrorModel.getWifissid());
			}
			if(smartMirrorModel.getDelay()!=null)
			{
				query.setParameter("delay", smartMirrorModel.getDelay());
			}
			if(smartMirrorModel.getAutoUpdate()!=null)
			{
				query.setParameter("firmwareUpdateTime", smartMirrorModel.getFirmwareUpdateTime());
				query.setParameter("autoUpdate", smartMirrorModel.getAutoUpdate());
			}
			if(smartMirrorModel.getDeviceMode()!=null && !smartMirrorModel.getDeviceMode().isEmpty())
			{
				query.setParameter("deviceMode", smartMirrorModel.getDeviceMode());
			}
   		query.executeUpdate();
		
		}catch(HibernateException exception)
		{
			logger.error(exception.getMessage());
			throw exception;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SmartMirrorDTO> getallmirrors() {
		List<SmartMirrorDTO> smartMirrorList  = new ArrayList<>();
	    try
		    {
		    	Session session = getCurrentSession();
		        Criteria criteria = session.createCriteria(SmartMirrorDTO.class);
		        criteria.add(Restrictions.eq("mirrorStatus", 0));
		        smartMirrorList = criteria.list();
				
		    }catch(HibernateException exception)
		    {
		    	logger.error(exception.getMessage());
		    	throw exception;
		    }
			return smartMirrorList;
	}

}
