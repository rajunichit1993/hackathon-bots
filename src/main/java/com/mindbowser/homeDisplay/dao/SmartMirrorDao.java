package com.mindbowser.homeDisplay.dao;


import java.util.List;

import com.mindbowser.homeDisplay.dto.BetaUserSmartMirrorDTO;
import com.mindbowser.homeDisplay.dto.SmartMirrorDTO;
import com.mindbowser.homeDisplay.model.SmartMirrorModel;

public interface SmartMirrorDao {
	
	public SmartMirrorDTO saveMirror(SmartMirrorDTO smartMirrorDto);
	public SmartMirrorDTO checkMirror(int major,int minor,int mirrorStatus);
	public SmartMirrorDTO getMirrorByDeviceId(String deviceId);
	public SmartMirrorDTO getMirrorById(int mirrorId);
	public SmartMirrorDTO getMajorMinor();
	public void updateMirrorstatus(String deviceId,int mirrorStatus);
	public List<SmartMirrorDTO> getMirrorListByMirrorUpdateDateTime(String currentDateTime,String endDateTime);
	public void updateMirrorDetail(SmartMirrorModel smartMirrorModel);
	public void saveMirrorSetting(List<SmartMirrorDTO> smartMirrorDTOList);
	public List<SmartMirrorDTO> getallmirrors();
	

	/*beta users operations*/
	public void updateBetaMirrorstatus(String deviceId, int mirrorStatus);
	public BetaUserSmartMirrorDTO saveBetaMirror(BetaUserSmartMirrorDTO betaUserSmartMirrorDTO);
}