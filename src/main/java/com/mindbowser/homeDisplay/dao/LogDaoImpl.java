package com.mindbowser.homeDisplay.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import com.mindbowser.homeDisplay.dto.LogDTO;

public class LogDaoImpl extends BaseDAO implements LogDao{

	Logger logger = Logger.getLogger(LogDaoImpl.class);
	
	@Override
	public void saveLog(LogDTO logDto) {
		
		try
		{
			Session session = getCurrentSession();
			session.save(logDto);
		}catch(HibernateException exception)
		{
			logger.error(exception.getStackTrace(), exception);
			throw exception;
		}
		
	}

}
