package com.mindbowser.homeDisplay.dao;

import java.util.List;

import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.model.FitBitAccountModel;
import com.mindbowser.homeDisplay.model.UserModel;

public interface FitBitDao {

	public void saveHealthData(HealthDataDTO healthDataDTO,List<HealthDataDTO> healthDataDtoList);
	
	public void removeHealthKitData(UserModel userModel,List<Integer> widgetListIds);
	public void updateFitBitCredential(FitBitAccountModel bitAccountModel, int fitBitId);
	public FitBitAccountDTO saveFitBitAccount(FitBitAccountDTO fitBitAccountDTO);
	public HealthDataDTO getFitBitLastAccessDate(WidgetDTO widgetDTO,FitBitAccountDTO fitBitAccountDTO);
	public void removeHealthKitDataByDate(FitBitAccountDTO fitBitAccountDTO,List<WidgetDTO> widgetDTOList,String currentDate);
	public void removeHealthKitDataByFitBitId(int fitBitId);
	public FitBitAccountDTO getFitBitAccountByUserId(String fitBitUserId);
	
	
	public void deleteFitbitDataByFitBitUserId(int fitBitId);
	
}
