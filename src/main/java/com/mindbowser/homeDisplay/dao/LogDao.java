package com.mindbowser.homeDisplay.dao;


import com.mindbowser.homeDisplay.dto.LogDTO;

public interface LogDao {
	
	public void saveLog(LogDTO logDto);
}