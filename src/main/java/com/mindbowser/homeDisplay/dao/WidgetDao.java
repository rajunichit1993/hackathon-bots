package com.mindbowser.homeDisplay.dao;

import java.util.List;
import com.mindbowser.homeDisplay.dto.CalendarFormatDTO;
import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.HealthDataDTO;
import com.mindbowser.homeDisplay.dto.MirrorPageDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.QuotesCategoryDTO;
import com.mindbowser.homeDisplay.dto.QuotesDTO;
import com.mindbowser.homeDisplay.dto.QuotesLengthDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserCalendarDTO;
import com.mindbowser.homeDisplay.dto.UserDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.dto.UserStepDTO;
import com.mindbowser.homeDisplay.dto.WeatherDataDTO;
import com.mindbowser.homeDisplay.dto.WidgetDTO;
import com.mindbowser.homeDisplay.dto.WidgetSettingDTO;
import com.mindbowser.homeDisplay.model.HealthDataModel;
import com.mindbowser.homeDisplay.model.StickyNotesModel;
import com.mindbowser.homeDisplay.model.UserCalendarModel;
import com.mindbowser.homeDisplay.model.UserStepModel;
import com.mindbowser.homeDisplay.model.WidgetModel;
import com.mindbowser.homeDisplay.model.WidgetSettingModel;

public interface WidgetDao {

	List<WidgetDTO> getAllWidget(List<String> widgetList);
	
	List<WidgetDTO> getWidgetListByWidgetId(List<Integer> widgetList);
	public void saveWidgetSetting(List<WidgetSettingDTO>  widgetSettingDtos);
	public WidgetSettingDTO checkWidgetSetting(WidgetSettingDTO widgetSettingDTO);
	
	public void socketUpdateWidgetSetting(List<WidgetSettingModel> widgetSettingList,UserMirrorDTO userMirrorDTO);
	
	public void newUpdateWidgetSetting(List<WidgetSettingDTO> widgetSettingList,UserMirrorDTO userMirrorDTO,MirrorPageDTO mirrorPageDTO);
	
	public void UpdatAllWidgetsStatusByWidgetList(UserMirrorDTO userMirrorDTO,List<Integer> widgetDTOList,MirrorPageDTO mirrorPageDTO);
	public void deleteMirrorPage(int userMirrorId);
	public MirrorPageDTO saveMirrorPage(MirrorPageDTO mirrorPageDTO);
	public List<WidgetSettingDTO> getWidgetSetting(UserMirrorDTO userMirrorDTO,List<WidgetDTO> widgetDTOList);
	
	public List<UserStepModel> getStepData(int userId,String startDate,int timezone);
	public List<HealthDataModel> getHealthData(HealthDataModel healthDataModel,Double conversionValue,String startDate,WidgetDTO widgetDTO);
	public List<HealthDataModel> getHealthDataByLimit(HealthDataModel healthDataModel,Double conversionValue,WidgetDTO widgetDTO);
	
	
	public void deleteStepData(int userId,String startDate);
	public void saveHealthKitData(int userId,List<UserStepDTO> userStepDtoList);
	
	public LocationDTO checkLocation(String Latitude,String Longitude);
	public LocationDTO saveLocationData(LocationDTO weatherDTO);
	public void saveWeatherDataList(List<WeatherDataDTO> weatherDataDTOList);
	public List<WeatherDataDTO> getWeatherData(int locationId,String weatherType,String temperatureUnit);
	public List<WeatherDataDTO>  getHourleyWeatherData(int locationId,List<String> dates,String temperatureUnit);
	public List<WeatherDataDTO>  getCurrentWeatherData(int locationId,String dailyDate,String hourleyDate,String temperatureUnit);
	public void deleteWeatherData(int locationId);
	
	public List<UserCalendarModel> getCalendarDataByLimit(int userId,String startDate,int timezone,int limit,Boolean calendarFlag,Boolean reminderFlag);
	public void saveCalenderData(int userId,List<UserCalendarDTO> userCelendarList);
	public void deleteCalenderData(int userId);
	public List<CalendarFormatDTO> calendarFormatList();
	public CalendarFormatDTO getCalendarFormatById(int id);
	
	public List<MirrorPageDTO> getMirrorPage(int userMirrorId);
	public TwitterDTO getTwitter(int twitterId);
	
	public List<QuotesCategoryDTO> quotesCategoryList();
	public List<QuotesLengthDTO> quotesCategoryLengthList();
	public List<QuotesDTO> getQuotesData();
	public QuotesDTO getQuotesData(int quotesCategoryLengthId);
	public QuotesDTO getQuotesDataByLengthCategory(int categoryId,int lengthId);
	
	public void saveQuotesCategoryList(List<QuotesCategoryDTO> categoryDTOList);
	public void saveQuotesData(List<QuotesDTO> QuotesList);
	public void updateQuotesData(List<QuotesDTO> quotesList);
	
	/*health kit related functions*/
	
	public List<WidgetDTO> getHealthKitWidgetListByMasterCategory(List<String> categoryList);
	public List<WidgetDTO> getWidgetByStatus(WidgetModel widgetModel);
	public List<WidgetDTO> getWidgetSettingByStatusAndWidgetList(UserMirrorDTO userMirrorDTO,List<WidgetDTO> widgetDTOList);
	public List<WidgetDTO> getWidgetBySubCategory(List<String> categoryList,String masterCategory);
	public List<WidgetDTO> getWidgetByTypeAndMasterCategory(List<String> widgetList,String masterCategory);
	public List<WidgetDTO> getSubcategoryWidgetByStatus(List<String> subCategoryList,String masterCategory,int userMirrorId);
	
	public List<WidgetDTO> getNewWidgetList(UserMirrorDTO userMirrorDTO); 
	
	public MirrorPageDTO getMirrorPageByPageNumber(UserMirrorDTO userMirrorDTO,int pageNumber);
	public void updateStickyNotesContent(List<StickyNotesModel> stickyNotesList);
	public void removeStickyNotes(List<WidgetSettingDTO> widgetSettingList);
	public void removeTest(List<WidgetSettingDTO> widgetSettingList);
	
	public void deleteQuotesByUserMirrorId(int userMirrorId);
	public void addQuotesForUser(UserMirrorDTO userMirrorDTO,List<QuotesDTO> quotes);
	public List<QuotesDTO> getQuotesByCategoryAndLength(List<QuotesCategoryDTO> quotesCategories,QuotesLengthDTO quotesLength);
	public List<QuotesDTO> getQuotesByUserMrrrorId(int userMirrorId);
	
	public void deleteStickyNotesByUserMirror(int userMirrorId);
	
	/*beta user functions*/
	WidgetDTO getWidgetById(int widgetId);
	public void updateBetaUsersFitbitData(FitBitAccountDTO fitBitAccountDTO,int dateDifference);
	public void updateBetaUsersHealthKitData(UserDTO userDTO,int dateDifference);
	public void updateBetaUsersCalendarData(UserDTO userDTO,int dateDifference);
	public HealthDataDTO getHealthKitLastAccessDate(UserDTO userDTO  ,WidgetDTO widgetDTO);
	public UserCalendarDTO getCalendarLastAccessDate(UserDTO userDTO);
	public void deleteWidgetSetting( List<UserMirrorDTO> userMirrorList);
	
	
}
