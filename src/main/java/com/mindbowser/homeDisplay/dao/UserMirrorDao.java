package com.mindbowser.homeDisplay.dao;



import java.util.List;

import com.mindbowser.homeDisplay.dto.FitBitAccountDTO;
import com.mindbowser.homeDisplay.dto.LocationDTO;
import com.mindbowser.homeDisplay.dto.TwitterDTO;
import com.mindbowser.homeDisplay.dto.UserMirrorDTO;
import com.mindbowser.homeDisplay.model.UserMirrorModel;

public interface UserMirrorDao {
	public List<UserMirrorDTO> getUserMirrorByMirrorId(int mirrorId);
	public UserMirrorDTO getUserMirror(int userId,int mirrorId,String userRole,int userStatusFlag);
	public UserMirrorDTO getUserMirrorFileds(int userId,int mirrorId,String userRole,int userStatusFlag);
	public UserMirrorDTO getOwnerDetail(int mirrorId);
	public UserMirrorDTO setUserMirror(UserMirrorDTO userMirrorDTO);
	public List<UserMirrorDTO> getUserMirrorList(int userId,String generalUser,String linkedUser,int mirrorStatus,int userStatusFlag);
	public List<UserMirrorDTO> getUserMirrorListByFitBitId(int fitBitId,String generalUser,String linkedUser,int mirrorStatus,int userStatusFlag);
	public UserMirrorDTO getUserMirrorData(int userId,int mirrorId,String generalUser,String linkedUser,int userStatusFlag);
	public void updateMirrorName(int userId, int mirrorId,String mirrorName);
	public void updateUserMirrorStatus(int mirrorId,int userStatus);
	public UserMirrorDTO getUserMirrorByBleRangeStatusTime(int mirrorId);
	public List<UserMirrorDTO> getConnectedUser(int userId);
	public void setOutOfRange(int mirrorId);
	public void updateClockTimezone(UserMirrorModel userMirrorModel);
	public void removeUserMirror(UserMirrorModel userMirrorModel);
	public TwitterDTO saveTwitter(TwitterDTO twitterDTO);
	public void updateTwitterCredentials(UserMirrorModel userMirrorModel,int twitterId);
	public UserMirrorDTO getUserMirrorByFitBitId(FitBitAccountDTO fitBitAccountDTO,int userStatusFlag);
	public void updateLocation(UserMirrorDTO userMirrorDTO , LocationDTO locationDTO);
	public void updateFitbitAccount(FitBitAccountDTO fitBitAccountDTO,UserMirrorDTO userMirrorDTO);
	public void updateBleStatus(int usermirrorId,String status,String time);
	
	
	//=================temp functions===================
	public List<FitBitAccountDTO> getFitbitAccounts();
	public List<UserMirrorDTO> getUserMirrorDetailByMirrorList(List<String> deviceIdList);
	public void updatecalendar(int userMirrorId, Boolean calendar,Boolean remindar,int calendarFormatId);
	
	
}
