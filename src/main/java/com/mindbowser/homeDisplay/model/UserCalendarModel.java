package com.mindbowser.homeDisplay.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



public class UserCalendarModel {
	
	private int id;
	private UserModel user;
	private String eventIdentifier;
	private String startDate;
	private String endDate;
	private String event;
	private String occuranceDate;
	private String availability;
	private String allDay;
	private String location;
	private String calendarType;
	
	
	
	public String getCalendarType() {
		return calendarType;
	}
	public void setCalendarType(String calendarType) {
		this.calendarType = calendarType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}

	@JsonIgnore
	public UserModel getUser() {
		return user;
	}
	
	@JsonProperty
	public void setUser(UserModel user) {
		this.user = user;
	}
	public String getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(String eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public String getAllDay() {
		return allDay;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	public String getOccuranceDate() {
		return occuranceDate;
	}

	public void setOccuranceDate(String occuranceDate) {
		this.occuranceDate = occuranceDate;
	}

	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public void setAllDay(String allDay) {
		this.allDay = allDay;
	}
	
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	private String organizer;
	private String status;

	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
