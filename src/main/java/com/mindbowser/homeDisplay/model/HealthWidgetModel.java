package com.mindbowser.homeDisplay.model;

import java.util.List;

public class HealthWidgetModel {

	private List<HealthDataModel> healthDataList;
	private List<WidgetModel> widgetList;
	private String date;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<HealthDataModel> getHealthDataList() {
		return healthDataList;
	}
	public void setHealthDataList(List<HealthDataModel> healthDataList) {
		this.healthDataList = healthDataList;
	}
	public List<WidgetModel> getWidgetList() {
		return widgetList;
	}
	public void setWidgetList(List<WidgetModel> widgetList) {
		this.widgetList = widgetList;
	}
	
	
}
