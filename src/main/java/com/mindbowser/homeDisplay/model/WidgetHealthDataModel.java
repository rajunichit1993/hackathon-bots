
package com.mindbowser.homeDisplay.model;

import java.util.Map;

public class WidgetHealthDataModel {

	
	private WidgetModel widget;
	private Map<String, Object> data;
	
	
	public WidgetModel getWidget() {
		return widget;
	}
	public void setWidget(WidgetModel widget) {
		this.widget = widget;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
		
}