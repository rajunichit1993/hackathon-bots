package com.mindbowser.homeDisplay.model;


public class QuotesCategoryModel {
	
	private int id;
	private String categoryType;
    private String categoryDesscription;
   /* private Set<QuotesCategoryLengthModel> quotes = new HashSet<QuotesCategoryLengthModel>();
	
	
	public Set<QuotesCategoryLengthModel> getQuotes() {
		return quotes;
	}
	public void setQuotes(Set<QuotesCategoryLengthModel> quotes) {
		this.quotes = quotes;
	}*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getCategoryDesscription() {
		return categoryDesscription;
	}
	public void setCategoryDesscription(String categoryDesscription) {
		this.categoryDesscription = categoryDesscription;
	}
	
    

}
