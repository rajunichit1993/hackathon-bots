package com.mindbowser.homeDisplay.model;

import java.util.Map;

public class WidgetData {
	
	private int widgetSettingId;
	private int contentId;
	private String deviceId;
	private String widgetMasterCategory;
	private String widgetSubCategory;
	private String contentType;
	private double height;
	private double width;
	private int defaultHeight;
	private int defaultMinHeight;
	private double xPos;
	private double yPos;
	private String status;
	private double minHeight;
	private double minWidth;
	private String viewType;
	private String displayName;
	private String graphType;
	private Boolean pinned;
	
	public Boolean getPinned() {
		return pinned;
	}
	public void setPinned(Boolean pinned) {
		this.pinned = pinned;
	}
	private Map<String, Object> data;

	
	public int getWidgetSettingId() {
		return widgetSettingId;
	}
	public void setWidgetSettingId(int widgetSettingId) {
		this.widgetSettingId = widgetSettingId;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getWidgetMasterCategory() {
		return widgetMasterCategory;
	}
	public void setWidgetMasterCategory(String widgetMasterCategory) {
		this.widgetMasterCategory = widgetMasterCategory;
	}
	public String getWidgetSubCategory() {
		return widgetSubCategory;
	}
	public void setWidgetSubCategory(String widgetSubCategory) {
		this.widgetSubCategory = widgetSubCategory;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public int getDefaultHeight() {
		return defaultHeight;
	}
	public void setDefaultHeight(int defaultHeight) {
		this.defaultHeight = defaultHeight;
	}
	public int getDefaultMinHeight() {
		return defaultMinHeight;
	}
	public void setDefaultMinHeight(int defaultMinHeight) {
		this.defaultMinHeight = defaultMinHeight;
	}
	public double getxPos() {
		return xPos;
	}
	public void setxPos(double xPos) {
		this.xPos = xPos;
	}
	public double getyPos() {
		return yPos;
	}
	public void setyPos(double yPos) {
		this.yPos = yPos;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getMinHeight() {
		return minHeight;
	}
	public void setMinHeight(double minHeight) {
		this.minHeight = minHeight;
	}
	public double getMinWidth() {
		return minWidth;
	}
	public void setMinWidth(double minWidth) {
		this.minWidth = minWidth;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getGraphType() {
		return graphType;
	}
	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}	
	
}
