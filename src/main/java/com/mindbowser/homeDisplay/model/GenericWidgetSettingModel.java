package com.mindbowser.homeDisplay.model;

import java.util.List;


public class GenericWidgetSettingModel {
	
	private List<WidgetSettingModel> widgets;
	private MirrorPageModel mirrorPage;
	private UserMirrorModel userMirror;
	
	public UserMirrorModel getUserMirror() {
		return userMirror;
	}
	public void setUserMirror(UserMirrorModel userMirror) {
		this.userMirror = userMirror;
	}
	public MirrorPageModel getMirrorPage() {
		return mirrorPage;
	}
	public void setMirrorPage(MirrorPageModel mirrorPage) {
		this.mirrorPage = mirrorPage;
	}
	public List<WidgetSettingModel> getWidgets() {
		return widgets;
	}
	public void setWidgets(List<WidgetSettingModel> widgets) {
		this.widgets = widgets;
	}
		
}
