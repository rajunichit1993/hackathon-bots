package com.mindbowser.homeDisplay.model;

import java.util.List;

public class BetaUserMirrorList {

	private List<String> betaUserMirrorList;

	public List<String> getBetaUserMirrorList() {
		return betaUserMirrorList;
	}

	public void setBetaUserMirrorList(List<String> betaUserMirrorList) {
		this.betaUserMirrorList = betaUserMirrorList;
	}
	
	
}
