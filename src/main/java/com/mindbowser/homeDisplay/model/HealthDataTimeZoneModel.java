package com.mindbowser.homeDisplay.model;

public class HealthDataTimeZoneModel {
	private int useratimeZone;
	private String fitBittimeZone;	
	
	public int getUseratimeZone() {
		return useratimeZone;
	}
	public void setUseratimeZone(int useratimeZone) {
		this.useratimeZone = useratimeZone;
	}
	public String getFitBittimeZone() {
		return fitBittimeZone;
	}
	public void setFitBittimeZone(String fitBittimeZone) {
		this.fitBittimeZone = fitBittimeZone;
	}
	

}
