package com.mindbowser.homeDisplay.model;


public class FitBitAccountModel {
	
	private int id;
    private String fitbitAccessToken;
    private String fitbitRefreshToken;
	private String fitbitUserId;
    private String fitbitUserTimeZone;
    private String fitbitUserName;
    
    
    
	public String getFitbitUserTimeZone() {
		return fitbitUserTimeZone;
	}
	public void setFitbitUserTimeZone(String fitbitUserTimeZone) {
		this.fitbitUserTimeZone = fitbitUserTimeZone;
	}
	public String getFitbitUserName() {
		return fitbitUserName;
	}
	public void setFitbitUserName(String fitbitUserName) {
		this.fitbitUserName = fitbitUserName;
	}
	public int getId() {
		return id;
	}
	public String getFitbitAccessToken() {
		return fitbitAccessToken;
	}
	public void setFitbitAccessToken(String fitbitAccessToken) {
		this.fitbitAccessToken = fitbitAccessToken;
	}
	public String getFitbitRefreshToken() {
		return fitbitRefreshToken;
	}
	public void setFitbitRefreshToken(String fitbitRefreshToken) {
		this.fitbitRefreshToken = fitbitRefreshToken;
	}
	public String getFitbitUserId() {
		return fitbitUserId;
	}
	public void setFitbitUserId(String fitbitUserId) {
		this.fitbitUserId = fitbitUserId;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
