package com.mindbowser.homeDisplay.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



public class UserStepModel {
	
	private int id;
	private UserModel user;
	private String time;
	private Double value;
	private Double totalSteps;
	private String	steptime;



	public String getSteptime() {
		return steptime;
	}

	public Double getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(Double totalSteps) {
		this.totalSteps = totalSteps;
	}

	public void setSteptime(String steptime) {
		this.steptime = steptime;
	}

	@JsonIgnore
	public UserModel getUser() {
		return user;
	}
	
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@JsonProperty
	public void setUser(UserModel user) {
		this.user = user;
	}
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
