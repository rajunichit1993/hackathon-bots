package com.mindbowser.homeDisplay.model;

import java.sql.Timestamp;


public class LogModel {
	
	private int id;
	private int userId;
	private int widgetId;
	private int mirrorId;
	private String event;
    private Timestamp time;
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(int widgetId) {
		this.widgetId = widgetId;
	}
	public int getMirrorId() {
		return mirrorId;
	}
	public void setMirrorId(int mirrorId) {
		this.mirrorId = mirrorId;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	

}
