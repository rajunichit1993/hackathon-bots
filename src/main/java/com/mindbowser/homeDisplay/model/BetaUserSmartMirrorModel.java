
package com.mindbowser.homeDisplay.model;


public class BetaUserSmartMirrorModel {
	
	private int id;
	private String deviceId;
	private int mirrorStatus;
	private Boolean multiUser;

	public Boolean getMultiUser() {
		return multiUser;
	}
	public void setMultiUser(Boolean multiUser) {
		this.multiUser = multiUser;
	}
		public int getMirrorStatus() {
		return mirrorStatus;
	}
	public void setMirrorStatus(int mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
