
package com.mindbowser.homeDisplay.model;


public class SmartMirrorModel {
	
	private int id;
	private int major;
	private int minor;
	private String wifissid;
	private String deviceMode;
	private String deviceId;
	private Integer delay;
	private String motionStopTime;
	private Boolean autoUpdate;
	private Integer bluetoothRange;
	private Boolean multiUser;
	

	public Boolean getMultiUser() {
		return multiUser;
	}
	public void setMultiUser(Boolean multiUser) {
		this.multiUser = multiUser;
	}
	public Integer getBluetoothRange() {
		return bluetoothRange;
	}
	public void setBluetoothRange(Integer bluetoothRange) {
		this.bluetoothRange = bluetoothRange;
	}
	public Boolean getAutoUpdate() {
		return autoUpdate;
	}
	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}
	private int mirrorStatus;
	private Boolean mirrorPreview;
	private String bluetoothStatus;
	
	private Boolean mirrorSource;
	private Double currentFirmwareVersion;
	private String firmwareUpdateTime;
	
	public Boolean getMirrorSource() {
		return mirrorSource;
	}
	public void setMirrorSource(Boolean mirrorSource) {
		this.mirrorSource = mirrorSource;
	}
	
	public Double getCurrentFirmwareVersion() {
		return currentFirmwareVersion;
	}
	public void setCurrentFirmwareVersion(Double currentFirmwareVersion) {
		this.currentFirmwareVersion = currentFirmwareVersion;
	}


	public String getFirmwareUpdateTime() {
		return firmwareUpdateTime;
	}
	public void setFirmwareUpdateTime(String firmwareUpdateTime) {
		this.firmwareUpdateTime = firmwareUpdateTime;
	}
	public String getBluetoothStatus() {
		return bluetoothStatus;
	}
	public void setBluetoothStatus(String bluetoothStatus) {
		this.bluetoothStatus = bluetoothStatus;
	}
	public Boolean getMirrorPreview() {
		return mirrorPreview;
	}
	public void setMirrorPreview(Boolean mirrorPreview) {
		this.mirrorPreview = mirrorPreview;
	}
	public String getMotionStopTime() {
		return motionStopTime;
	}
	public void setMotionStopTime(String motionStopTime) {
		this.motionStopTime = motionStopTime;
	}
	public int getMirrorStatus() {
		return mirrorStatus;
	}
	public void setMirrorStatus(int mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDeviceMode() {
		return deviceMode;
	}
	public void setDeviceMode(String deviceMode) {
		this.deviceMode = deviceMode;
	}
	public int getMajor() {
		return major;
	}
	public void setMajor(int major) {
		this.major = major;
	}
	public int getMinor() {
		return minor;
	}
	public void setMinor(int minor) {
		this.minor = minor;
	}
	public String getWifissid() {
		return wifissid;
	}
	public void setWifissid(String wifissid) {
		this.wifissid = wifissid;
	}
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
   

}
