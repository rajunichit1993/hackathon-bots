package com.mindbowser.homeDisplay.model;

import java.util.List;

public class BetaUserCalendarListModel {
	
	private List<UserCalendarModel> userCalendarList;

	public List<UserCalendarModel> getUserCalendarList() {
		return userCalendarList;
	}

	public void setUserCalendarList(List<UserCalendarModel> userCalendarList) {
		this.userCalendarList = userCalendarList;
	}

}
