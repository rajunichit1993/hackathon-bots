package com.mindbowser.homeDisplay.model;

public class TwitterModel {
	
	private int id;
	private String twitterFollowingUser;
	private String timeLine;
	private String twitterOauthToken;
	private String twitterOauthTokenSecret;
	private String twitterCredentialStatus;
	private String screenName;
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getTwitterCredentialStatus() {
		return twitterCredentialStatus;
	}
	public void setTwitterCredentialStatus(String twitterCredentialStatus) {
		this.twitterCredentialStatus = twitterCredentialStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTwitterFollowingUser() {
		return twitterFollowingUser;
	}
	public void setTwitterFollowingUser(String twitterFollowingUser) {
		this.twitterFollowingUser = twitterFollowingUser;
	}
	public String getTimeLine() {
		return timeLine;
	}
	public void setTimeLine(String timeLine) {
		this.timeLine = timeLine;
	}
	public String getTwitterOauthToken() {
		return twitterOauthToken;
	}
	public void setTwitterOauthToken(String twitterOauthToken) {
		this.twitterOauthToken = twitterOauthToken;
	}
	public String getTwitterOauthTokenSecret() {
		return twitterOauthTokenSecret;
	}
	public void setTwitterOauthTokenSecret(String twitterOauthTokenSecret) {
		this.twitterOauthTokenSecret = twitterOauthTokenSecret;
	}
	
	
	
	
	

}
