package com.mindbowser.homeDisplay.model;

import java.util.List;

public class BleInDataModel {

	List<UserMirrorModel> userMirrorList;
	List<UserCalendarModel> userCalendarModelList;
	HealthWidgetModel healthWidgetModel;
	
	
	public List<UserMirrorModel> getUserMirrorList() {
		return userMirrorList;
	}
	public void setUserMirrorList(List<UserMirrorModel> userMirrorList) {
		this.userMirrorList = userMirrorList;
	}
	public List<UserCalendarModel> getUserCalendarModelList() {
		return userCalendarModelList;
	}
	public void setUserCalendarModelList(List<UserCalendarModel> userCalendarModelList) {
		this.userCalendarModelList = userCalendarModelList;
	}
	public HealthWidgetModel getHealthWidgetModel() {
		return healthWidgetModel;
	}
	public void setHealthWidgetModel(HealthWidgetModel healthWidgetModel) {
		this.healthWidgetModel = healthWidgetModel;
	}
	
	
}
