package com.mindbowser.homeDisplay.model;

import java.util.List;

public class StickyNotesRemoveModel {
	
	private UserMirrorModel userMirror;
	private List<WidgetSettingModel>  widgetSetting;
	
	
	public UserMirrorModel getUserMirror() {
		return userMirror;
	}
	public void setUserMirror(UserMirrorModel userMirror) {
		this.userMirror = userMirror;
	}
	public List<WidgetSettingModel> getWidgetSetting() {
		return widgetSetting;
	}
	public void setWidgetSetting(List<WidgetSettingModel> widgetSetting) {
		this.widgetSetting = widgetSetting;
	}

}
