package com.mindbowser.homeDisplay.model;


public class UserModel {
	
	private int id;
	private String name;
	private String gender;
	private String emailId;
	private String password;
	private String authToken;
	private String createdAt;
	private String updatedAt;
	private String oldPassword;
	private String confirmPassword;
	private String newPassword;
	private String stepsLastUpdatedDate;
	private int userCurrentTimeZone;
    private String distanceUnit;
    private String waterUnit;
    private String weightUnit;
    private String temperatureUnit;
    private String encryptedPlainText;
    
	
	
	public String getEncryptedPlainText() {
		return encryptedPlainText;
	}
	public void setEncryptedPlainText(String encryptedPlainText) {
		this.encryptedPlainText = encryptedPlainText;
	}
	public String getTemperatureUnit() {
		return temperatureUnit;
	}
	public void setTemperatureUnit(String temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}
	public String getDistanceUnit() {
		return distanceUnit;
	}
	public void setDistanceUnit(String distanceUnit) {
		this.distanceUnit = distanceUnit;
	}
	public String getWaterUnit() {
		return waterUnit;
	}
	public void setWaterUnit(String waterUnit) {
		this.waterUnit = waterUnit;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	
	public String getStepsLastUpdatedDate() {
		return stepsLastUpdatedDate;
	}
	public void setStepsLastUpdatedDate(String stepsLastUpdatedDate) {
		this.stepsLastUpdatedDate = stepsLastUpdatedDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public int getUserCurrentTimeZone() {
		return userCurrentTimeZone;
	}
	public void setUserCurrentTimeZone(int userCurrentTimeZone) {
		this.userCurrentTimeZone = userCurrentTimeZone;
	}



}
