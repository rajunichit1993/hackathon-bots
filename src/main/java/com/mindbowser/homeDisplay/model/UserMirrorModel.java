package com.mindbowser.homeDisplay.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class UserMirrorModel {
	
	private int id;
	private UserModel user;
	private SmartMirrorModel mirror;
	private CalendarFormatModel calendarFormat;
	private String userRole;
	private int userStatusFlag;
	private int timeZone;
	private String mirrorName;
	private String bleRangeStatus;
	private String bleRangeStatusTime;
	private TwitterModel twitter;
	private LocationModel location;
	private QuotesModel quotes;
	private Boolean clockMessageStatus;
	private FitBitAccountModel fitbitAccount;
	private Boolean calendarFlag;
	
	public Boolean getCalendarFlag() {
		return calendarFlag;
	}
	public void setCalendarFlag(Boolean calendarFlag) {
		this.calendarFlag = calendarFlag;
	}
	public Boolean getReminderFlag() {
		return reminderFlag;
	}
	public void setReminderFlag(Boolean reminderFlag) {
		this.reminderFlag = reminderFlag;
	}
	private Boolean reminderFlag;
	
	public FitBitAccountModel getFitbitAccount() {
		return fitbitAccount;
	}
	public void setFitbitAccount(FitBitAccountModel fitbitAccount) {
		this.fitbitAccount = fitbitAccount;
	}
	public Boolean getClockMessageStatus() {
		return clockMessageStatus;
	}
	public void setClockMessageStatus(Boolean clockMessageStatus) {
		this.clockMessageStatus = clockMessageStatus;
	}
	public QuotesModel getQuotes() {
		return quotes;
	}
	public void setQuotes(QuotesModel quotes) {
		this.quotes = quotes;
	}
	/*public LocationModel getWeather() {
		return weather;
	}
	public void setWeather(LocationModel weather) {
		this.weather = weather;
	}*/
	public TwitterModel getTwitter() {
		return twitter;
	}
	public LocationModel getLocation() {
		return location;
	}
	public void setLocation(LocationModel location) {
		this.location = location;
	}
	public void setTwitter(TwitterModel twitter) {
		this.twitter = twitter;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	/*public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}*/
	
	@JsonIgnoreProperties({"authToken", "id", "password", "emailId","gender","name","createdAt","updatedAt"})
	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	public SmartMirrorModel getMirror() {
		return mirror;
	}
	public void setMirror(SmartMirrorModel mirror) {
		this.mirror = mirror;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public CalendarFormatModel getCalendarFormat() {
		return calendarFormat;
	}
	public void setCalendarFormat(CalendarFormatModel calendarFormat) {
		this.calendarFormat = calendarFormat;
	}

	public int getTimeZone() {
		return timeZone;
	}
	public int getUserStatusFlag() {
		return userStatusFlag;
	}
	public void setUserStatusFlag(int userStatusFlag) {
		this.userStatusFlag = userStatusFlag;
	}
	public void setTimeZone(int timeZone) {
		this.timeZone = timeZone;
	}
	public String getMirrorName() {
		return mirrorName;
	}
	public void setMirrorName(String mirrorName) {
		this.mirrorName = mirrorName;
	}
	
	/*public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}*/
	public String getBleRangeStatus() {
		return bleRangeStatus;
	}
	public void setBleRangeStatus(String bleRangeStatus) {
		this.bleRangeStatus = bleRangeStatus;
	}
	public String getBleRangeStatusTime() {
		return bleRangeStatusTime;
	}
	public void setBleRangeStatusTime(String bleRangeStatusTime) {
		this.bleRangeStatusTime = bleRangeStatusTime;
	}
	


}
