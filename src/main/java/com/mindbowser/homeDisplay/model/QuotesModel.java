package com.mindbowser.homeDisplay.model;



public class QuotesModel {
	
	private int id;
	private QuotesLengthModel quotesLength;
	private QuotesCategoryModel quotesCategory;
	private String quotes;
	private String author;
	
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public QuotesLengthModel getQuotesLength() {
		return quotesLength;
	}
	public void setQuotesLength(QuotesLengthModel quotesLength) {
		this.quotesLength = quotesLength;
	}
	public QuotesCategoryModel getQuotesCategory() {
		return quotesCategory;
	}
	public void setQuotesCategory(QuotesCategoryModel quotesCategory) {
		this.quotesCategory = quotesCategory;
	}
	public String getQuotes() {
		return quotes;
	}
	public void setQuotes(String quotes) {
		this.quotes = quotes;
	}

	
	
	
}
