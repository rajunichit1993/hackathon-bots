package com.mindbowser.homeDisplay.model;


public class MirrorPageModel {

	private int id;
	private int pageNumber;
	private int delay;
	private UserMirrorModel userMirror;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	public UserMirrorModel getUserMirror() {
		return userMirror;
	}
	public void setUserMirror(UserMirrorModel userMirror) {
		this.userMirror = userMirror;
	}
		
}
