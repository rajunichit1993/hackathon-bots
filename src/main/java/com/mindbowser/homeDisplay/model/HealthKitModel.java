package com.mindbowser.homeDisplay.model;

import java.util.List;

public class HealthKitModel {
	
	private List<WidgetModel> Activity;
	private List<WidgetModel> Nutrition;
	private List<WidgetModel> Sleep;
	private List<WidgetModel> BodyAndWeight;
	private List<WidgetModel> Vital;
	
	public List<WidgetModel> getVital() {
		return Vital;
	}
	public void setVital(List<WidgetModel> vital) {
		Vital = vital;
	}
	public List<WidgetModel> getActivity() {
		return Activity;
	}
	public void setActivity(List<WidgetModel> activity) {
		Activity = activity;
	}
	public List<WidgetModel> getNutrition() {
		return Nutrition;
	}
	public void setNutrition(List<WidgetModel> nutrition) {
		Nutrition = nutrition;
	}
	public List<WidgetModel> getSleep() {
		return Sleep;
	}
	public void setSleep(List<WidgetModel> sleep) {
		Sleep = sleep;
	}
	public List<WidgetModel> getBodyAndWeight() {
		return BodyAndWeight;
	}
	public void setBodyAndWeight(List<WidgetModel> bodyAndWeight) {
		BodyAndWeight = bodyAndWeight;
	}
	
}
