package com.mindbowser.homeDisplay.model;

import java.util.List;

public class WidgetSettingModel {
	
	private int id;
	private UserMirrorModel userMirror;
	private WidgetModel widget;
	private String status;
	private int xPos;
	private int yPos;
	private int width;
	private int height;
	private int deviceWidth;
	private int deviceHeight;
	private int minHeight;
	private int minWidth;
	private int pageSequence;
	private String iconFormat;
	private MirrorPageModel mirrorPage;
	private String viewType;
	private List<StickyNotesModel> stickyNotes;
	private Boolean pinned;
	
	public Boolean getPinned() {
		return pinned;
	}
	public void setPinned(Boolean pinned) {
		this.pinned = pinned;
	}
	public List<StickyNotesModel> getStickyNotes() {
		return stickyNotes;
	}
	public void setStickyNotes(List<StickyNotesModel> stickyNotes) {
		this.stickyNotes = stickyNotes;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public int getPageSequence() {
		return pageSequence;
	}
	public MirrorPageModel getMirrorPage() {
		return mirrorPage;
	}
	public void setMirrorPage(MirrorPageModel mirrorPage) {
		this.mirrorPage = mirrorPage;
	}
	public void setPageSequence(int pageSequence) {
		this.pageSequence = pageSequence;
	}
	public String getIconFormat() {
		return iconFormat;
	}
	public void setIconFormat(String iconFormat) {
		this.iconFormat = iconFormat;
	}
	public WidgetModel getWidget() {
		return widget;
	}
	public void setWidget(WidgetModel widget) {
		this.widget = widget;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public UserMirrorModel getUserMirror() {
		return userMirror;
	}
	public void setUserMirror(UserMirrorModel userMirror) {
		this.userMirror = userMirror;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public int getxPos() {
		return xPos;
	}
	public void setxPos(int xPos) {
		this.xPos = xPos;
	}
	public int getyPos() {
		return yPos;
	}
	public void setyPos(int yPos) {
		this.yPos = yPos;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getDeviceWidth() {
		return deviceWidth;
	}
	public void setDeviceWidth(int deviceWidth) {
		this.deviceWidth = deviceWidth;
	}
	public int getDeviceHeight() {
		return deviceHeight;
	}
	public void setDeviceHeight(int deviceHeight) {
		this.deviceHeight = deviceHeight;
	}
	public int getMinHeight() {
		return minHeight;
	}
	public void setMinHeight(int minHeight) {
		this.minHeight = minHeight;
	}
	public int getMinWidth() {
		return minWidth;
	}
	public void setMinWidth(int minWidth) {
		this.minWidth = minWidth;
	}

}
