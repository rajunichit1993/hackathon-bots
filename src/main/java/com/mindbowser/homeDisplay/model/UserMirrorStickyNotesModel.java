package com.mindbowser.homeDisplay.model;

import java.util.List;

public class UserMirrorStickyNotesModel {
	private UserMirrorModel userMirror;
	private  List<StickyNotesModel> stickyNotesModelList;
	
	
	
	public UserMirrorModel getUserMirror() {
		return userMirror;
	}
	public void setUserMirror(UserMirrorModel userMirror) {
		this.userMirror = userMirror;
	}
	public List<StickyNotesModel> getStickyNotesModelList() {
		return stickyNotesModelList;
	}
	public void setStickyNotesModelList(List<StickyNotesModel> stickyNotesModelList) {
		this.stickyNotesModelList = stickyNotesModelList;
	}
	
	
	

}
