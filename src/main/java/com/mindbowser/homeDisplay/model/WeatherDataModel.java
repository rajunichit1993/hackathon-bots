package com.mindbowser.homeDisplay.model;


public class WeatherDataModel {

	private int id;
	private String weatherTime;
	private int minTemperature;
	private int maxTemperature;
	private int currentTemperature;
	private String description;
	private String icon;
	private String weatherType;
	public String getWeatherType() {
		return weatherType;
	}

	public void setWeatherType(String weatherType) {
		this.weatherType = weatherType;
	}

	private LocationModel weather;
	
	
	public String getWeatherTime() {
		return weatherTime;
	}

	public void setWeatherTime(String weatherTime) {
		this.weatherTime = weatherTime;
	}

	public LocationModel getWeather() {
		return weather;
	}

	public void setWeather(LocationModel weather) {
		this.weather = weather;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(int minTemperature) {
		this.minTemperature = minTemperature;
	}

	public int getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public int getCurrentTemperature() {
		return currentTemperature;
	}

	public void setCurrentTemperature(int currentTemperature) {
		this.currentTemperature = currentTemperature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
}
