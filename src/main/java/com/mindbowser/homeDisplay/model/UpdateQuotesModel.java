package com.mindbowser.homeDisplay.model;

import java.util.List;

public class UpdateQuotesModel {

	List<QuotesCategoryModel> quotesCategories;
	QuotesLengthModel quotesLength;
	UserMirrorModel userMirrorModel;
	
	public List<QuotesCategoryModel> getQuotesCategories() {
		return quotesCategories;
	}
	public void setQuotesCategories(List<QuotesCategoryModel> quotesCategories) {
		this.quotesCategories = quotesCategories;
	}
	public QuotesLengthModel getQuotesLength() {
		return quotesLength;
	}
	public void setQuotesLength(QuotesLengthModel quotesLength) {
		this.quotesLength = quotesLength;
	}
	public UserMirrorModel getUserMirrorModel() {
		return userMirrorModel;
	}
	public void setUserMirrorModel(UserMirrorModel userMirrorModel) {
		this.userMirrorModel = userMirrorModel;
	}
	
}
