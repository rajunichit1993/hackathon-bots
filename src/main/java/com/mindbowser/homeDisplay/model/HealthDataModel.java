package com.mindbowser.homeDisplay.model;


public class HealthDataModel {

	private int id;
	private UserModel user;
	private WidgetModel widget;
	private String healthTime;
	private Double healthValue;
	private Double goalValue;
	private FitBitAccountModel fitbit;
	
	
	public FitBitAccountModel getFitbit() {
		return fitbit;
	}
	public void setFitbit(FitBitAccountModel fitbit) {
		this.fitbit = fitbit;
	}
	public Double getGoalValue() {
		return goalValue;
	}
	public void setGoalValue(Double goalValue) {
		this.goalValue = goalValue;
	}
	public String getHealthTime() {
		return healthTime;
	}
	public void setHealthTime(String healthTime) {
		this.healthTime = healthTime;
	}
	public Double getHealthValue() {
		return healthValue;
	}
	public void setHealthValue(Double healthValue) {
		this.healthValue = healthValue;
	}

	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	public WidgetModel getWidget() {
		return widget;
	}
	public void setWidget(WidgetModel widget) {
		this.widget = widget;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
