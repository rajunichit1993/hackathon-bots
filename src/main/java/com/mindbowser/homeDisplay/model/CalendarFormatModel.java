package com.mindbowser.homeDisplay.model;



public class CalendarFormatModel {

    private int id;
	private String label;
	private String durationFormat;
	private int count;
	/*Set<UserMirrorModel> userMirror;*/
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	/*@JsonIgnore
	public Set<UserMirrorModel> getUserMirror() {
		return userMirror;
	}
	@JsonProperty
	public void setUserMirror(Set<UserMirrorModel> userMirror) {
		this.userMirror = userMirror;
	}*/
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDurationFormat() {
		return durationFormat;
	}
	public void setDurationFormat(String durationFormat) {
		this.durationFormat = durationFormat;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	

}
