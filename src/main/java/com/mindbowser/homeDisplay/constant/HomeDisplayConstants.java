package com.mindbowser.homeDisplay.constant;


public class HomeDisplayConstants {
	public static final String POST = "POST";
	public static final String AUTHTOKEN = "authToken";
	public static final String SIGN_UP_USER_URL = "/users/signUp";
	public static final String SWAGGER_DOC = "/sw/api-docs";
	public static final String SQS_URL = "/sqs/sqsMessage";
	public static final String FITBIT_NOTIFICATION_URL ="/v2/healthData/notifications";
	public static final String SOCKET_URL = "/socket";
	public static final String VERSION_URL = "/v2";
	public static final String REST_URL_SOCKET = "base.url";
	public static final String DEFAULT_FIRMWARE_UPDATE_TIME = "default.firmware.update.time";
	public static final String AES = "AES_256";
	
	
	/*beta user*/
	public static final String BETA_USER_DEFAULT_EMAILID = "beta.user.default.emailId";
	public static final String BETA_USER_DEFAULT_MIRRORID = "beta.user.default.mirrorid";
	public static final String BETA_USER_TIME_DELAY = "beta.user.time.delay";
	
	
	/*api versioning*/
	public static final String ANDROID_OS = "android.os";
	public static final String IOS_OS = "ios.os";
	public static final String CURRENT_ANDROID_VERSION = "current.android.version";
	public static final String VERSION_V1 = "version.v1";
	public static final String VERSION_BETA = "version.beta";
	public static final String BETA_USER_FITBIT_ID = "beta.user.fitbit.id";
	
	/*public static final String VERSION_V3 = "version.v3";*/
	public static final String FIRMWARE_VERSION = "firmware.version";
	
	
	public static final String LOGIN_URL = "/users/logIn";
	public static final String DEFAULT_URL = "/";
	public static final String LOADBALANCER_URL = "/checkHealth";
	public static final String SERVER_HEALTH_GOOD = "server.health.good";
	
	public static final String DEFAULT_URL_ADDRESS = "default.url.address";
	public static final String FORGOT_PASSWORD = "/users/forgotPassword";
	public static final String AUTHTOKEN_KEYWORD = "authToken= ";
	public static final String EMAIL_SUBJECT = "email.subject";
	public static final String EMAIL_LOG_SUBJECT = "email.log.subject";
	
	public static final String FAIL = "fail";
	public static final String USER = "user";
	public static final String USERID = "userId";
	
	/*some constant integer value*/
	public static final String MAJORLIMIT = "message.major.limit";
	public static final String MINORLIMIT = "message.minor.limit";
	public static final String FLAG_ZERO = "message.active.fleg.zero";
	public static final String FLAG_ONE = "message.active.flag.one";
	public static final String DEFAULT_QUOTES_VALUE = "message.default.quotes.value";
	public static final String MAX_BLEIN_STATUS_TIME_LIMIT = "message.max.blein.time.limit";
	
	/*socket message*/
	public static final String USER_OWNER = "user.type.personal";
	public static final String USER_GENERAL ="user.type.general";
	public static final String USER_LINKED = "user.type.linkedUser";
	public static final String RETURN_SUCCESS = "return.success";
	public static final String RETURN_ERROR = "return.error";
	public static final String UpdateMirror = "update.mirror";
	public static final String WITHINRANGE = "within.range";
	public static final String WIDGET_REFRESH = "widget.refresh";
	public static final String REFRESH_STICKYNOTES = "refresh.stickynotes";
	public static final String LOAD_GENERAL_SETTING = "load.general.setting";
	public static final String STICKYNOTES_UPDATED = "stickynotes.updated";
	public static final String STICKYNOTES_REMOVED = "removed.removed";
	public static final String TWITTER_CREDENTIAL_UPDATE = "twitter.credential.update";
	public static final String REFRESH_HEALTHKIT_SOCKET_MESSAGE = "refresh.healthkit.socket.message";
	public static final String REFRESH_WEATHER_SOCKET_MESSAGE = "refresh.weather.socket.message";
	
	public static final String DEVICEID_LABEL = "deviceid.label";
	public static final String USERID_LABEL = "userid.label";
	public static final String USERROLE_LABEL = "userrole.label";
	public static final String DATA_LABEL = "data.label";
	public static final String TWITTERCREDENTIALSTATUS_LABEL = "twitterCredentialStatus.label";
	public static final String TITLE_LABEL = "title.label";
	public static final String EVENTS_LABEL = "events.label";
	public static final String DATEFORMATID_LABEL = "dateFormatId.label";
	public static final String CALENDARTITLE_LABEL = "calendarTitle.label";

	public static final String QUOTES_CATEGORY_LABEL = "quotesCategory.label";
	public static final String QUOTES_LENGTH_LABEL = "quotesLength.label";
	public static final String AUTHOR_LABEL = "author.label";
	public static final String QUOTES_LABEL = "quotes.label";
	public static final String PREVIEWSTATUS_LABEL = "previewStatus.label";
	public static final String GOALVALUE_LABEL = "goalValue.label";
	public static final String MAXVALUE_LABEL = "maxValue.label";
	public static final String LABEL = "label";
	public static final String MINVALUE_LABEL = "minValue.label";
	public static final String TEMPERATURE_LABEL = "temperature.label";
	public static final String TWO_DECIMAL_FLOATING_POINT = "two.decimal.float.point";
	public static final String JSONFORMAT_EXTENSION = "jsonformat.extension";
	public static final String GOALS_LABEL = "goals.label";
		
	
	public static final String USER_MAX_ACTIVE_SESSION_TIME = "user.max.active.session.time";
	public static final String BLE_IN = "ble.in";
	public static final String BLE_OUT = "ble.out";
	
	public static final String SOCKET_STATUS = "socket.status";
	public static final String SOCKET_DISCONNECT_STATUS = "socket.disconnect.status";
	
	public static final String CALENDAR_HOUR_FORMAT = "calendar.hour.format";
	public static final String CALENDAR_NUMBER_OF_EVENTS_FORMAT = "calendar.number.of.events.format";
	
	
	/* health data units */
	public static final String HEALTH_WEIGHT_KILOGRAM = "health.weight.kilogram";
	public static final String HEALTH_WEIGHT_POUND = "health.weight.pound";
	public static final String HEALTH_WEIGHT_STONE = "health.weight.stone";
	
	public static final String HEALTH_DISTANCE_KILOMETER = "health.distance.kilometer";
	public static final String HEALTH_DISTANCE_METER = "health.distance.meter";
	public static final String HEALTH_DISTANCE_MILES = "health.distance.miles";
	
	public static final String HEALTH_WATER_FLOZ = "health.water.floz";
	public static final String HEALTH_WATER_MILLILITER = "health.water.milliliter";
	
	
	public static final String MILES_TO_KM = "miles.to.km";
	public static final String KM_TO_MILES = "km.to.miles";
	public static final String ML_TO_FLOZ = "ml.to.floz";
	public static final String KG_TO_POUND = "kg.to.pound";
	public static final String KG_TO_STONE = "kg.to.stone";
	public static final String DEFAULT_MULTIPLIER = "default.multiplier";
	
	public static final String KILOMETER_UNIT = "kilometer.unit";
	public static final String METER_UNIT = "meter.unit";
	
	public static final String MILES_UNIT = "miles.unit";
	public static final String POUND_UNIT = "pound.unit";
	public static final String KILOGRAM_UNIT = "kilogram.unit";
	public static final String STONE_UNIT = "stone.unit";
	public static final String MILILITER_UNIT = "mililiter.unit";
	public static final String LITER_UNIT = "liter.unit";
	public static final String FLOZ_UNIT = "floz.unit";
	public static final String GALLONS_UNIT = "gallons.unit";
	public static final String SLEEP_UNIT = "sleep.unit";
	public static final String YARDS_UNIT = "yards.unit";
	public static final String OUNCES_UNIT = "ounces.unit";
	public static final String BMI_UNIT = "bmi.unit";
	
	public static final String STEPS_UNIT = "step.unit";
	public static final String FLIGHTCLIMB_UNIT = "flightclimb.unit";
	public static final String CALORIES_UNIT = "calories.unit";
	
	public static final String BODYWEIGHT_FAT_UNIT = "bodyweight.fat.unit";
	public static final String BODYWEIGHT_WEIGHT_UNIT = "bodyweight.weight.unit";
	public static final String NUTRITION_DIETARY_CHOLESTEROL_UNIT = "neutrition.dietary.cholesterol.unit";
	public static final String NUTRITION_DIETARY_SUGAR_UNIT = "neutrition.dietary.sugar.unit";
	public static final String NUTRITION_DIETARY_FAT_UNIT = "neutrition.dietary.fat.unit";
	public static final String NUTRITION_CARBS_UNIT = "neutrition.carbs.unit";
	public static final String NUTRITION_FIBER_UNIT = "neutrition.fiber.unit";
	public static final String NUTRITION_PROTEIN_UNIT = "neutrition.protein.unit";
	public static final String NUTRITION_SODIUM_UNIT = "neutrition.sodium.unit";
	public static final String VITAL_BLOOD_PRESSURE_SYSTOLIC_UNIT = "vital.bloodpressure.systolic.unit";
	public static final String VITAL_BLOOD_GLUCOSE_UNIT = "vital.blood.glucose.unit";
	public static final String VITAL_BLOOD_PRESSURE_DIASTOLIC_UNIT = "vital.bloodpressure.diastolic.unit";
	public static final String VITAL_HEART_RATE_UNIT = "vital.heartrate.unit";
	
	
	public static final String GRAM_UNIT = "gram.unit";
	public static final String MG_UNIT = "mg.unit";
	public static final String MMHG_UNIT = "mmHg.unit";
	
	public static final String TEMPERATURE_FAHRENHEIT = "temperature.fahrenheit";
	public static final String TEMPERATURE_CELSIUS = "temperature.celsius";
	
	
	

	/* health data messages */
	public static final String FITBIT_MASTERCATEGORY = "fitbit.mastercategory";
	public static final String HEALTHKIT_MASTERCATEGORY = "healthkit.mastercategory";
	public static final String ACTIVITY_SUBCATEGORY = "activity.category";
	public static final String NUTRITIONS_SUBCATEGORY = "nutrition.category";
	public static final String BODY_AND_WEIGHT_SUBCATEGORY = "body.and.weight.subcategory";
	public static final String VITAL_SUBCATEGORY = "vital.subcategory";
	public static final String SLEEP_SUBCATEGORY = "sleep.subcategory";
	public static final String LINEGRAPH = "graphtype.linegraph";
	
	
	
	/*awsiot labels */
	public static final String DISPLAY_SLEEP_TIME = "display_sleep_timeout";
	public static final String IBEACON = "ibeacon";
	public static final String DESIRED = "desired";
	public static final String REPORTED = "reported";
	public static final String STATE = "state";
	public static final String BEACON_ON = "on";
	public static final String BEACON_OFF = "off";
	
	/* clock messages */
	public static final String MORNING_WISH = "morning.wish.message";
	public static final String EVENING_WISH = "evening.wish.message";
	public static final String AFTERNOON_WISH = "afternoon.wish.message";
	public static final String HELLO_WISH = "hello.wish.message";
	
	
	/*sqs messages */
	public static final String MOTION_STOP = "motion.stop";
	public static final String MOTION_START = "motion.start";
	public static final String KEY_MOTION_TYPE = "key.motion.type";
	public static final String KEY_MAC_ADDRESS = "key.mac.address";
	public static final String KEY_DATA = "key.data";
	public static final String KEY_DEVICE_ID = "key.device.id";
	public static final String KEY_USER_ROLE = "key.user.role";
	public static final String KEY_USER_ID = "key.user.id";
	public static final String KEY_MAJOR = "key.major";
	public static final String KEY_MINOR = "key.minor";
	public static final String KEY_HEIGHT = "key.height";
	public static final String KEY_WIDTH = "key.width";
	public static final String CURRENT_FW_VERSION = "current.fw.version";
	public static final String FW_VERSION = "fw.version";
	
	
	/*name key*/
	public static final String KEY_IDENTIFIER = "key.identifier";
	public static final String KEY_UNIT = "key.unit";
	public static final String KEY_NAME = "key.name";
	
	/*mirror status */
	public static final String MIRROR_STATUS_DISABLE = "mirror.status.disable";
	public static final String MIRROR_STATUS_ACTIVE = "mirror.status.active";
	
	
	/*widget list*/
	public static final String WIDGET_VIEW_TYPE_GRAPH = "widget.view.type.graph";
	public static final String WIDGET_VIEW_TYPE_TEXT = "widget.view.type.text";
	
	public static final String WIDGET_STICKYNOTES = "widget.stickynotes";
	public static final String WIDGET_CLOCK = "widget.clock";
	public static final String WIDGET_WEATHER = "widget.weather";
	public static final String WIDGET_CALENDAR = "widget.calendar";
	public static final String WIDGET_REMINDER = "widget.reminder";
	public static final String WIDGET_TWITTER = "widget.twitter";
	public static final String WIDGET_5DAY_WEATHER_FORECAST = "widget.5day.weather.forecast";
	public static final String WIDGET_24HOUR_WEATHER_FORECAST = "widget.24hour.weather.forecast";
	public static final String WIDGET_DAILY_WEATHER = "widget.daily.weather";
	public static final String WIDGET_HOURLEY_WEATHER = "widget.hourley.weather";
	public static final String WIDGET_QUOTES = "widget.quotes";
	
	/*COMMON HEALTH widgets*/
	public static final String WIDGET_FAT = "widget.fat";
	public static final String WIDGET_CALORIES = "widget.calories";
	
	/*healthkit widget */
	public static final String WIDGET_WALKINGRUNNING_DISTANCE = "widget.walkingRunning.distance";
	public static final String WIDGET_FLIGHT_CLIMB = "widget.flight.climb";
	public static final String WIDGET_DIATRY_CHOLESTEROL = "widget.dietary.cholesterol";
	public static final String WIDGET_DIATRY_SUGAR = "widget.dietary.sugar";
	public static final String WIDGET_DIATRY_FATTOTAL = "widget.dietary.fatTotal";
	public static final String WIDGET_BLOOD_GLUCOSE = "widget.blood.glucose";
	public static final String WIDGET_BLOOD_PRESSURE_DIASTOLIC = "widget.blood.pressure.diastolic";
	public static final String WIDGET_BLOOD_PRESSURE_SYSTOLIC = "widget.blood.pressure.systolic";
	public static final String WIDGET_HEART_RATE = "widget.heart.rate";
	
	
    /*fitbit notification labels*/	
	public static final String FOODS_NOTIFICATION_KEY = "foods.notification.key";
	public static final String BODY_NOTIFICATION_KEY = "body.notification.key";
	public static final String ACTIVITIES_NOTIFICATION_KEY = "activities.notification.key";
	public static final String SLEEP_NOTIFICATION_KEY = "sleep.notification.key";
	public static final String PROFILE_NOTIFICATION_KEY = "profile.notification.key";
	
	/*activity widgets*/
	public static final String WIDGET_STEP = "widget.step";
	public static final String WIDGET_DISTANCE = "widget.distance";
	public static final String WIDGET_ACTIVITY_CALORIES = "widget.activity.calories";
	public static final String WIDGET_CALORIESBMR = "widget.caloriesbmr";

	/*neutritions widget*/
	public static final String WIDGET_CARB = "widget.carb";
	public static final String WIDGET_FIBER = "widget.fiber";
	public static final String WIDGET_PROTEIN = "widget.protein";
	public static final String WIDGET_SODIUM = "widget.sodium";
	public static final String WIDGET_WATER = "widget.water";
	
	/*sleep widget*/
	public static final String WIDGET_TOTAL_SLEEPMINUTE = "widget.total.sleepminute";
	public static final String WIDGET_TOTAL_TIMEINBED = "widget.total.timeinbed";
	
	/*BODY AND WIIGHT*/
	public static final String WIDGET_WEIGHT = "widget.weight";	
	public static final String WIDGET_BMI = "widget.bmi";
	
	public static final String MINUTEASSLEEP = "minute.asSleep";
	public static final String TIMEINBED = "time.inBed";
	
	/*widget related constant file data*/
	public static final String DEFAULT_MIRROR_NAME = "default.mirror.name";
	public static final String DATA_POINTSTROKECOLOR = "json.data.pointStrokeColor.key";
	public static final String DATA_FILLCOLOR = "json.data.fillColor.key";
	public static final String DATA_POINTCOLOR = "json.data.pointColor.key";
	public static final String DATA_STROKECOLOR = "json.data.strokeColor.key";
	public static final String LANDSCAPE_MODE   = "landscape.mode.device";
	public static final String PORTRAIT_MODE  = "portrait.mode.device";

	/*Icons name*/
	public static final String ICON_RAIN               = "icon.rain";
	public static final String ICON_CLEAR_DAY          = "icon.clear.day";
	public static final String ICON_CLEAR_NIGHT        = "icon.clear.night";
	public static final String ICON_CLOUDY             = "icon.cloudy";
	public static final String ICON_PARTLY_CLOUDY_DAY  = "icon.partly.cloudy.day";
	public static final String ICON_PARTLY_CLOUDY_NIGHT= "icon.partly.cloudy.night";
	public static final String ICON_SLEET              = "icon.sleet";
	public static final String ICON_SNOW               = "icon.snow";
	public static final String ICON_WIND               = "icon.wind";
	public static final String ICON_DEFAULT            = "icon.default";
	public static final String ICON_FOG                = "icon.fog";
	
	
	
	
	/*Widget portrait mode default value*/
	public static final String PORTRAIT_DEFAULT_DEVICE_HEIGHT      = "portrait.default.device.height";
	public static final String PORTRAIT_DEFAULT_DEVICE_WIDTH       = "portrait.default.device.width";
	public static final String PORTRAIT_DEFAULT_CLOCK_WIDGET_HEIGHT= "portrait.default.clock.widget.height";
	public static final String PORTRAIT_DEFAULT_CLOCK_WIDGET_WIDTH = "portrait.default.clock.widget.width";
	public static final String PORTRAIT_DEFAULT_CLOCK_WIDGET_XPOS  = "portrait.default.clock.widget.xpos";
	public static final String PORTRAIT_DEFAULT_CLOCK_WIDGET_YPOS  = "portrait.default.clock.widget.ypos";
	public static final String PORTRAIT_DEFAULT_CLOCK_MINHEIGHT    = "portrait.default.clock.minheight";
	public static final String PORTRAIT_DEFAULT_CLOCK_MINWIDTH     = "portrait.default.clock.minwidth";
	
	public static final String PORTRAIT_DEFAULT_HEIGHT    = "portrait.default.height";
	public static final String PORTRAIT_DEFAULT_MINHEIGHT     = "portrait.default.minHeight";
	public static final String PORTRAIT_DEFAULT_GRAPHHEIGHT    = "portrait.default.graph.height";
	public static final String PORTRAIT_DEFAULT_GRAPHMINHEIGHT     = "portrait.default.graph.minHeight";
	public static final String PORTRAIT_DEFAULT_XPOS    = "portrait.default.xpos";
	public static final String PORTRAIT_DEFAULT_YPOS     = "portrait.default.ypos";
	
	public static final String PORTRAIT_DEFAULT_CELENDAR_WIDGET_HEIGHT= "portrait.default.celendar.widget.height";
	public static final String PORTRAIT_DEFAULT_CELENDAR_WIDGET_WIDTH = "portrait.default.celendar.widget.width";
	public static final String PORTRAIT_DEFAULT_CELENDAR_WIDGET_XPOS  = "portrait.default.celendar.widget.xpos";
	public static final String PORTRAIT_DEFAULT_CELENDAR_WIDGET_YPOS  = "portrait.default.celendar.widget.ypos";
	public static final String PORTRAIT_DEFAULT_CELENDAR_MINHEIGHT    = "portrait.default.celendar.minheight";
	public static final String PORTRAIT_DEFAULT_CELENDAR_MINWIDTH     = "portrait.default.celendar.minwidth";
	
	public static final String PORTRAIT_DEFAULT_WEATHER_WIDGET_HEIGHT= "portrait.default.weather.widget.height";
	public static final String PORTRAIT_DEFAULT_WEATHER_WIDGET_WIDTH = "portrait.default.weather.widget.width";
	public static final String PORTRAIT_DEFAULT_WEATHER_WIDGET_XPOS  = "portrait.default.weather.widget.xpos";
	public static final String PORTRAIT_DEFAULT_WEATHER_WIDGET_YPOS  = "portrait.default.weather.widget.ypos";
	public static final String PORTRAIT_DEFAULT_WEATHER_MINHEIGHT  = "portrait.default.weather.minheight";
	public static final String PORTRAIT_DEFAULT_WEATHER_MINWIDTH   = "portrait.default.weather.minwidth";
	
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_HEIGHT= "portrait.default.weather.daily.widget.height";
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_WIDTH = "portrait.default.weather.daily.widget.width";
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_XPOS  = "portrait.default.weather.daily.widget.xpos";
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_WIDGET_YPOS  = "portrait.default.weather.daily.widget.ypos";
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_MINHEIGHT  = "portrait.default.weather.daily.minheight";
	public static final String PORTRAIT_DEFAULT_WEATHER_DAILY_MINWIDTH   = "portrait.default.weather.daily.minwidth";

	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_HEIGHT= "portrait.default.weather.hourley.widget.height";
	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_WIDTH = "portrait.default.weather.hourley.widget.width";
	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_XPOS  = "portrait.default.weather.hourley.widget.xpos";
	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_WIDGET_YPOS  = "portrait.default.weather.hourley.widget.ypos";
	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_MINHEIGHT  = "portrait.default.weather.hourley.minheight";
	public static final String PORTRAIT_DEFAULT_WEATHER_HOURLEY_MINWIDTH   = "portrait.default.weather.hourley.minwidth";
	
	public static final String PORTRAIT_DEFAULT_QUOTES_WIDGET_HEIGHT= "portrait.default.quotes.widget.height";
	public static final String PORTRAIT_DEFAULT_QUOTES_WIDGET_WIDTH = "portrait.default.quotes.widget.width";
	public static final String PORTRAIT_DEFAULT_QUOTES_WIDGET_XPOS  = "portrait.default.quotes.widget.xpos";
	public static final String PORTRAIT_DEFAULT_QUOTES_WIDGET_YPOS  = "portrait.default.quotes.widget.ypos";
	public static final String PORTRAIT_DEFAULT_QUOTES_MINHEIGHT    = "portrait.default.quotes.minheight";
	public static final String PORTRAIT_DEFAULT_QUOTES_MINWIDTH     = "portrait.default.quotes.minwidth";
	
	public static final String PORTRAIT_DEFAULT_STEPS_WIDGET_HEIGHT= "portrait.default.steps.widget.height";
	public static final String PORTRAIT_DEFAULT_STEPS_WIDGET_WIDTH = "portrait.default.steps.widget.width";
	public static final String PORTRAIT_DEFAULT_STEPS_WIDGET_XPOS  = "portrait.default.steps.widget.xpos";
	public static final String PORTRAIT_DEFAULT_STEPS_WIDGET_YPOS  = "portrait.default.steps.widget.ypos";
	public static final String PORTRAIT_DEFAULT_STEPS_MINHEIGHT    = "portrait.default.steps.minheight";
	public static final String PORTRAIT_DEFAULT_STEPS_MINWIDTH     = "portrait.default.steps.minwidth";
	
	public static final String PORTRAIT_DEFAULT_TWITTER_WIDGET_HEIGHT= "portrait.default.twitter.widget.height";
	public static final String PORTRAIT_DEFAULT_TWITTER_WIDGET_WIDTH = "portrait.default.twitter.widget.width";
	public static final String PORTRAIT_DEFAULT_TWITTER_WIDGET_XPOS  = "portrait.default.twitter.widget.xpos";
	public static final String PORTRAIT_DEFAULT_TWITTER_WIDGET_YPOS  = "portrait.default.twitter.widget.ypos";
	public static final String PORTRAIT_DEFAULT_TWITTER_MINHEIGHT    = "portrait.default.twitter.minheight";
	public static final String PORTRAIT_DEFAULT_TWITTER_MINWIDTH     = "portrait.default.twitter.minwidth";
	
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_HEIGHT= "portrait.default.stickynotes.widget.height";
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_WIDTH = "portrait.default.stickynotes.widget.width";
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_XPOS  = "portrait.default.stickynotes.widget.xpos";
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_WIDGET_YPOS  = "portrait.default.stickynotes.widget.ypos";
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_MINHEIGHT    = "portrait.default.stickynotes.minheight";
	public static final String PORTRAIT_DEFAULT_STICKYNOTES_MINWIDTH     = "portrait.default.stickynotes.minwidth";
	

	public static final String DEFAULT_PAGENUMBER   = "default.pagenumber";
	public static final String DEFAULT_PAGE_TRANSITION_DELAY   = "default.page.transition.delay";
	public static final String DEFAULT_BLUETOOTH_RANGE   = "default.bluetoothrange";
	
	public static final String STATUS_ON   = "status.on";
	public static final String STATUS_OFF = "status.off";

	// Paths
	public static final String REGISTRATION_CONFIRMATION_EMAIL_PATH = "message.registration.confirmation.email.path";
	public static final String USER_LOG_EMAIL_PATH = "message.user.log.email.path";
	public static final String FORGOT_PASSWORD_EMAIL_PATH = "message.forgot.password.email.path";
	
	// Success Messages
	public static final String REGISTRATION_CONFIRMATION_EMAIL_BODY = "message.registration.confirmation.email.body";
	public static final String FORGOT_PASSWORD_EMAIL_BODY = "message.forgot.password.email.body";
	public static final String USER_REGISTERED_SUCCESSFULLY_MESSAGE = "message.user.successfully.registered";
	public static final String USER_DETAILS_MESSAGE = "message.user.details.sent";
	public static final String USER_UPDATE_SUCCESS_MESSAGE = "message.user.details.updated";
	public static final String MIRROR_REGISTER_SUCCESS_MESSAGE = "message.mirror.success.register";
	public static final String WIDGET_LIST_SENT_SUCCESS = "message.widget.list.success";
	public static final String WIDGET_GENERAL_SETTING_SUCCESS_MESSAGE = "message.widget.generalSetting.success";
	public static final String WIDGET_PERSONAL_SETTING_SUCCESS_MESSAGE = "message.widget.personalSetting.success";
	public static final String UPDATE_WIDGET_SETTING_SUCCESS_MESSAGE = "message.update.widget.success";
	public static final String MIRROR_LIST_SENT_SUCCESS = "message.mirror.list.sent.success";
	public static final String CELENDAR_LIST_SENT_SUCCESS = "message.celendar.list.success";
	public static final String MIRROR_DETAILS_UPDATE_SUCCESS = "message.mirror.detail.update.success";
	public static final String MIRROR_ADDED_SUCCESS_MESSAGE = "message.mirror.added.success";
	public static final String USER_ADDED_SUCCESSFULLY_MESSAGE = "message.user.added.success";
	public static final String HEALTH_KIT_DATA_SAVE_SUCCESS = "message.health.kit.data.save.success";
	public static final String CALENDER_DATA_SAVE_SUCCESS = "message.calender.data.save.success";
	public static final String USER_TIMEZONE_SUCCESSFULLY_UPDATED = "message.timezone.update.success";
	public static final String MIRROR_DELAY_UPDATE_SUCCESS = "message.delay.update.success";
	public static final String USER_DATA_SENT_SUCCESSFULLY_ON_SOCKET = "message.user.data.send.on.socket.success";
	public static final String USER_PASSWORD_CHANGE_SUCCESS_MESSAGE = "message.user.password.update.success";
	public static final String SQS_PROCESSED_SUCCESS = "message.sqs.send.success";
	public static final String CALENDER_FORMAT_LIST_SENT_SUCCESS = "message.calendar.format.list.sent.success";
	public static final String MIRROR_DETAIL_SENT_SUCCESS = "message.mirror.detail.sent.success";
	public static final String MIRROR_USER_REMOVE_SUCCESS = "message.mirror.user.remove.sent.success";
	public static final String SOCKET_DISCONNECT_PUBLISH_SUCCESS_MESSAGE = "message.socket.disconnect.publish.success";
	public static final String UPDATE_CLOCK_TIMEZONE_SUCCESS = "message.update.clock.timezone.success";
	public static final String UPDATE_TWITTER_CREDENTIAL_SUCCESS_MESSAGE = "message.update.twitter.credential.success";
	
	public static final String UPDATE_WEATHER_TOGGLE_SUCCESS_MESSAGE = "message.weather.toggle.success";
	public static final String WEATHER_DATA_UPDATE_SUCCESS = "message.weather.data.update.success";
	public static final String PREVIEW_START_SUCCESS_MESSAGE = "message.preview.start.success";
	public static final String PREVIEW_STOP_SUCCESS_MESSAGE = "message.preview.stop.success";
	public static final String UPDATE_QUOTES_SUCCESS_MESSAGE = "message.update.quotes.success";
	public static final String QUOTES_DATA_SAVE_SUCCESS_MESSAGE = "message.quotes.data.save.success";
	public static final String USER_PROFILE_UNIT_UPDATE_SUCCESS_MESSAGE = "message.user.profile.unit.update.success";
	
	
	public static final String STICKYNOTES_SAVED_SUCCESS_MESSAGE = "message.stickynotes.save.success";
	public static final String STICKYNOTES_UPDATE_SUCCESS_MESSAGE = "message.stickynotes.update.success";
	
	public static final String USER_LOG_SEND_SUCCESS_MESSAGE = "message.log.send.success";
	
	
	

	/*fitbit success message*/
	public static final String FITBIT_CREDENTIAL_UPDATE_SUCCESS = "message.fitbit.credential.update.success";
	public static final String FITBIT_CORRECT_VERIFICATION_CODE = "message.fitbit.correct.verification.code";
	public static final String FITBIT_INCORRECT_VERIFICATION_CODE = "message.fitbit.incorrect.verification.code";
	
	//log data
	public static final String LOG_USER_SIGNUP = "log.user.signup.message";
	public static final String LOG_USER_LOGIN = "log.user.login.message";
	public static final String LOG_USER_RECOVER_PASSWORD = "log.user.recover.password.message";
	public static final String LOG_USER_EDIT_PROFILE = "log.user.edit.profile.message";
	public static final String LOG_USER_CHANGE_PASSWORD = "log.user.change.password.message";
	public static final String LOG_USER_CONFIGURE_NEWMIRROR = "log.user.configure.newMirror.message";
	public static final String LOG_USER_JOIN_MIRROR = "log.user.join.mirror.message";
	public static final String LOG_USER_BLEIN = "log.user.bleIn.message";
	public static final String LOG_USER_BLEOUT = "log.user.bleOut.message";
	public static final String LOG_SOCKET_CONNECT = "log.user.socketOpen.message";
	public static final String LOG_MIRROR_DETAIL_UPDATE = "log.mirror.detail.updated.message";
	
	
	
	// Exception Messages
	public static final String EXCEPTION = "exception";
	public static final String NOT_FOUND = "not.found";
	public static final String CONTACT_ADMIN = "exception.message.contact.admin";
	
	public static final String MIRROR_NOT_REGISTER= "mirror.not.configured";
	public static final String REGISTER_FAILED_EXCEPTION = "exception.message.register.fail";
	public static final String INVALID_USER_TOKEN = "exception.message.invalid.user.token";
	public static final String USER_NOT_REGISTERED = "exception.message.user.not.registered";
	public static final String LOG_IN_FAILED = "exception.message.user.login.fail";
	public static final String PASSWORD_MISMATCH = "exception.message.password.mismatch";
	public static final String USER_LOGGED_IN_SUCCESSFULLY = "message.user.login.successfully";
	public static final String PASSWORD_SENT_SUCCESSFULLY = "message.password.sent.successfully";
	public static final String USER_ALREADY_EXIST_EXCEPTION = "exception.message.email.already.exists";
	public static final String DAILY_GAME_DATA_FETCH_EXCEPTION = "exception.message.daily.game.data.fetch";
	public static final String PASSWORD_AND_CONFIRMATION_PASSWORD_DOES_NOT_MATCH_EXCEPTION = "exception.password.confirmation.password.does.not.match";
	public static final String WRONG_OLD_PASSWORD_EXCEPTION = "exception.wrong.old.password";
	public static final String DATA_MISSING_EXCEPTION = "exception.data.missing";
	public static final String BLE_LIMIT_EXCEPTION = "exception.ble.limit";
	public static final String MIRROR_NOT_SPECIFIED = "exception.mirror.not.specified";
	public static final String MIRROR_NOT_REGISTERED = "exception.mirror.not.registered";
	public static final String MIRROR_OWNER_EXCEPTION = "exception.mirror.owner";
	public static final String USER_MIRROR_NOT_REGISTERED_EXCEPTION = "exception.user.mirror.not.registered";
	public static final String MODIFICATION_ACCESS_DENIED_EXCEPTION = "exception.modification.access.denied";
	public static final String USER_ROLE_NOT_SPECIFIED_EXCEPTION = "exception.user.role.not.specified";
	public static final String EMAIL_IS_NOT_VALID_EXCEPTION = "exception.email.id.not.valid";
	public static final String USER_NOT_REGISTER_TO_MIRROR_EXCEPTION = "exception.user.not.register.to.mirror";
	public static final String PASSWORD_LENGTH_EXCEPTION = "exception.password.length";
	public static final String USER_ROLE_REGISTRATION_EXCEPTION = "exception.user.role";
	public static final String NAME_IS_NOT_VALID_EXCEPTION = "exception.user.name.format";
	public static final String NAME_NO_CHARACTER_EXCEPTION = "exception.user.name.character";
	public static final String MIRROR_REGISTER_EXIST_EXCEPTION = "exception.mirror.already.register";
	public static final String NO_WIDGET_AVAILABLE_EXCEPTION = "exception.no.widget.available";
	public static final String MIRROR_DISCONNECTED_ABNORMALLY = "exception.mirror.disconnect.abnormally";
	public static final String USER_NOT_REGISTER = "exception.message.user.not.register";
	public static final String APP_VERSION_EXCEPTION = "exception.api.version.not.supported";
	public static final String INVALID_TWITTER_TOKEN = "exception.invalid.twitter.token";
	public static final String INVALID_FITBIT_TOKEN = "exception.invalid.fitbit.token";
	public static final String MIRROR_UPDATE_UNSUCCESSFUL = "exception.mirror.update.unsuccessful";
	public static final String PRIVATE_MIRROR_EXCEPTION = "exception.private.mirror";
	public static final String SOMETHING_WENTWRONG_EXCEPTION = "exception.something.wentwrong";
	public static final String PARSING_EXCEPTION = "exception.parsing";
	public static final String FIRMWARE_UPDATE_EXCEPTION = "exception.firmware.update";
	public static final String BLERANGE_UPDATE_EXCEPTION = "exception.blerange.update";
	
	
	
	
}
