package com.mindbowser.homeDisplay.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "quotescategory")
public class QuotesCategoryDTO {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="categoryType")
	private String categoryType;
	
	public Boolean getCategoryStatus() {
		return categoryStatus;
	}

	public void setCategoryStatus(Boolean categoryStatus) {
		this.categoryStatus = categoryStatus;
	}

	@Column(name="categoryDesscription")
    private String categoryDesscription;
	
	@Column(name="categoryStatus")
    private Boolean categoryStatus;
	
	
/*	@OneToMany(mappedBy="quotesCategory")
	private Set<QuotesCategoryLengthDTO> quotes = new HashSet<QuotesCategoryLengthDTO>();
	
	
	public Set<QuotesCategoryLengthDTO> getQuotes() {
		return quotes;
	}

	public void setQuotes(Set<QuotesCategoryLengthDTO> quotes) {
		this.quotes = quotes;
	}
*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getCategoryDesscription() {
		return categoryDesscription;
	}

	public void setCategoryDesscription(String categoryDesscription) {
		this.categoryDesscription = categoryDesscription;
	}

}
