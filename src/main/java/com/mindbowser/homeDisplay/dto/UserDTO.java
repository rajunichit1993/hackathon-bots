package com.mindbowser.homeDisplay.dto;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserDTO {
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	
	@Column(name="name")
	String name;
	
	@Column(name="gender")
	String gender;
	
	@Column(name="emailId")
	String emailId;
	
	@Column(name="password")
	String password;

	@Column(name="authToken")
	String authToken;
	
	@Column(name="createdAt")
	String createdAt;
	
	@Column(name="updatedAt")
	String updatedAt;
	
	@Column(name="userCurrentTimeZone")
	int userCurrentTimeZone;
	
	@Column(name="distanceUnit")
    private String distanceUnit;
    
    @Column(name="waterUnit")
    private String waterUnit;
    
    @Column(name="weightUnit")
    private String weightUnit;
    
    @Column(name="temperatureUnit")
    private String temperatureUnit;
    
    @Column(name="encryptedPlainText")
    private String encryptedPlainText;
    

    
	public String getEncryptedPlainText() {
		return encryptedPlainText;
	}

	public void setEncryptedPlainText(String encryptedPlainText) {
		this.encryptedPlainText = encryptedPlainText;
	}

	public String getTemperatureUnit() {
		return temperatureUnit;
	}


	public void setTemperatureUnit(String temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}


	public int getUserCurrentTimeZone() {
		return userCurrentTimeZone;
	}


	public String getDistanceUnit() {
		return distanceUnit;
	}


	public void setDistanceUnit(String distanceUnit) {
		this.distanceUnit = distanceUnit;
	}


	public String getWaterUnit() {
		return waterUnit;
	}


	public void setWaterUnit(String waterUnit) {
		this.waterUnit = waterUnit;
	}


	public String getWeightUnit() {
		return weightUnit;
	}


	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}


	public void setUserCurrentTimeZone(int userCurrentTimeZone) {
		this.userCurrentTimeZone = userCurrentTimeZone;
	}

	public int getId() {
		return id;
	}
	

	public void setId(int id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

		
}
