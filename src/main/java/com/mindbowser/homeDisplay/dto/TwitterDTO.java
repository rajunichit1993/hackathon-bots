package com.mindbowser.homeDisplay.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="twitter")
public class TwitterDTO {
	
    @Id
    @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
    
    @Column(name="twitterFollowingUser")
    private String twitterFollowingUser;
    
    @Column(name="screenName")
    private String screenName;
	
	@Column(name="timeLine")
	private String timeLine;
	
	@Column(name="twitterOauthToken")
	private String twitterOauthToken;
	
	@Column(name="twitterCredentialStatus")
	private String twitterCredentialStatus;
	
	@Column(name="twitterOauthTokenSecret")
	private String twitterOauthTokenSecret;
	
	public String getTwitterCredentialStatus() {
		return twitterCredentialStatus;
	}
	public void setTwitterCredentialStatus(String twitterCredentialStatus) {
		this.twitterCredentialStatus = twitterCredentialStatus;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTwitterFollowingUser() {
		return twitterFollowingUser;
	}
	public void setTwitterFollowingUser(String twitterFollowingUser) {
		this.twitterFollowingUser = twitterFollowingUser;
	}
	public String getTimeLine() {
		return timeLine;
	}
	public void setTimeLine(String timeLine) {
		this.timeLine = timeLine;
	}
	public String getTwitterOauthToken() {
		return twitterOauthToken;
	}
	public void setTwitterOauthToken(String twitterOauthToken) {
		this.twitterOauthToken = twitterOauthToken;
	}
	public String getTwitterOauthTokenSecret() {
		return twitterOauthTokenSecret;
	}
	public void setTwitterOauthTokenSecret(String twitterOauthTokenSecret) {
		this.twitterOauthTokenSecret = twitterOauthTokenSecret;
	}
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	
}
