package com.mindbowser.homeDisplay.dto;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="userMirrors")
public class UserMirrorDTO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="userRole")
	private String userRole;
	
	@Column(name="userStatusFlag")
	private int userStatusFlag;
	
	@Column(name="bleRangeStatus")
	private String bleRangeStatus;
	
	@Column(name="bleRangeStatusTime")
	private String bleRangeStatusTime;
	
	@Column(name="timeZone")
	private int timeZone;
	
	@Column(name="mirrorName")
	private String mirrorName;
	
	@Column(name="clockMessageStatus")
	private Boolean clockMessageStatus;
	
	@Column(name="calendarFlag")
	private Boolean calendarFlag;
	
	@Column(name="reminderFlag")
	private Boolean reminderFlag;
		
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name="twitterId")
	private TwitterDTO twitter;
	
	public Boolean getCalendarFlag() {
		return calendarFlag;
	}
	public void setCalendarFlag(Boolean calendarFlag) {
		this.calendarFlag = calendarFlag;
	}
	public Boolean getReminderFlag() {
		return reminderFlag;
	}
	public void setReminderFlag(Boolean reminderFlag) {
		this.reminderFlag = reminderFlag;
	}
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name="fitbitAccountId")
	private FitBitAccountDTO fitbitAccount;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "userId")
	private UserDTO user;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "mirrorId")
	private SmartMirrorDTO mirror;
	
	@ManyToOne(cascade = CascadeType.REMOVE)  
	@JoinColumn(name = "calendarFormatId")  
	private CalendarFormatDTO calendarFormat;
	
	/*@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "weatherId")
	private LocationDTO weather;*/
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "locationId")
	private LocationDTO location;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "quotesId")
	private QuotesDTO quotes;

		
	public QuotesDTO getQuotes() {
		return quotes;
	}
	public void setQuotes(QuotesDTO quotes) {
		this.quotes = quotes;
	}
	public TwitterDTO getTwitter() {
		return twitter;
	}
	public void setTwitter(TwitterDTO twitter) {
		this.twitter = twitter;
	}
	
	/*public LocationDTO getWeather() {
		return weather;
	}
	public void setWeather(LocationDTO weather) {
		this.weather = weather;
	}*/
	public String getMirrorName() {
		return mirrorName;
	}
	public FitBitAccountDTO getFitbitAccount() {
		return fitbitAccount;
	}
	public void setFitbitAccount(FitBitAccountDTO fitbitAccount) {
		this.fitbitAccount = fitbitAccount;
	}
	public LocationDTO getLocation() {
		return location;
	}
	public void setLocation(LocationDTO location) {
		this.location = location;
	}
	public int getUserStatusFlag() {
		return userStatusFlag;
	}
	public void setUserStatusFlag(int userStatusFlag) {
		this.userStatusFlag = userStatusFlag;
	}
	public void setMirrorName(String mirrorName) {
		this.mirrorName = mirrorName;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public SmartMirrorDTO getMirror() {
		return mirror;
	}
	public void setMirror(SmartMirrorDTO mirror) {
		this.mirror = mirror;
	}
	
	public String getBleRangeStatus() {
		return bleRangeStatus;
	}
	public void setBleRangeStatus(String bleRangeStatus) {
		this.bleRangeStatus = bleRangeStatus;
	}
	public String getBleRangeStatusTime() {
		return bleRangeStatusTime;
	}
	public void setBleRangeStatusTime(String bleRangeStatusTime) {
		this.bleRangeStatusTime = bleRangeStatusTime;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public int getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(int timeZone) {
		this.timeZone = timeZone;
	}
	
	public CalendarFormatDTO getCalendarFormat() {
		return calendarFormat;
	}
	public void setCalendarFormat(CalendarFormatDTO calendarFormat) {
		this.calendarFormat = calendarFormat;
	}
	public Boolean getClockMessageStatus() {
		return clockMessageStatus;
	}
	public void setClockMessageStatus(Boolean clockMessageStatus) {
		this.clockMessageStatus = clockMessageStatus;
	}
		
}
