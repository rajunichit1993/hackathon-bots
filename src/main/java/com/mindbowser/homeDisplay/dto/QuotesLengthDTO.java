package com.mindbowser.homeDisplay.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "quoteslength")
public class QuotesLengthDTO {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="quotesLength")
	private String quoteslength;
	
	@Column(name="minLength")
	private int minLength;
	
	@Column(name="maxLength")
	private int maxLength;
	
	public int getMaxLength() {
		return maxLength;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuoteslength() {
		return quoteslength;
	}

	public void setQuoteslength(String quoteslength) {
		this.quoteslength = quoteslength;
	}


		
}
