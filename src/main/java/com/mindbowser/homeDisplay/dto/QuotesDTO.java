package com.mindbowser.homeDisplay.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "quotes")
public class QuotesDTO {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="quotesLengthId")
	private QuotesLengthDTO quotesLength;
	
	@ManyToOne
	@JoinColumn(name="quotesCategoryId")
	private QuotesCategoryDTO quotesCategory;
	
	@Column(name="quotes")
	private String quotes;

	@Column(name="author")
	private String author;
	
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

		public QuotesLengthDTO getQuotesLength() {
		return quotesLength;
	}

	public void setQuotesLength(QuotesLengthDTO quotesLength) {
		this.quotesLength = quotesLength;
	}

	public QuotesCategoryDTO getQuotesCategory() {
		return quotesCategory;
	}

	public void setQuotesCategory(QuotesCategoryDTO quotesCategory) {
		this.quotesCategory = quotesCategory;
	}

		public String getQuotes() {
		return quotes;
	}

	public void setQuotes(String quotes) {
		this.quotes = quotes;
	}
	
}
