package com.mindbowser.homeDisplay.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="usermirrorquotes")
public class UserMirrorQuoteDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "userMirrorId")
	private UserMirrorDTO userMirrors;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "quotesId")
	private QuotesDTO quotes;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserMirrorDTO getUserMirrors() {
		return userMirrors;
	}

	public void setUserMirrors(UserMirrorDTO userMirrors) {
		this.userMirrors = userMirrors;
	}

	public QuotesDTO getQuotes() {
		return quotes;
	}

	public void setQuotes(QuotesDTO quotes) {
		this.quotes = quotes;
	}

}
