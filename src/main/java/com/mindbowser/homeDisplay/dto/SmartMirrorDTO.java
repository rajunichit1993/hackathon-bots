package com.mindbowser.homeDisplay.dto;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="smartMirror")
public class SmartMirrorDTO {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="major")
	private int major;
	
	@Column(name="autoUpdate")
	private Boolean autoUpdate;
	
	@Column(name="mirrorPreview")
	private Boolean mirrorPreview;
	
	@Column(name="motionStopTime")
	private String motionStopTime;
	
	@Column(name="bluetoothStatus")
	private String bluetoothStatus;
	
	@Column(name="mirrorSource")
	private Boolean mirrorSource;
	
	@Column(name="mirrorStatus")
	private int mirrorStatus;
	
	@Column(name="minor")
	private int minor;
	
	@Column(name="deviceId")
	private String deviceId;
	
	@Column(name="delay")
	private Integer delay;

	@Column(name="wifissid")
	private String wifissid;
	
	@Column(name="deviceMode")
	private String deviceMode;

	@Column(name="currentFirmwareVersion")
	private Double currentFirmwareVersion;
	
	
	@Column(name="firmwareUpdateTime")
	private String firmwareUpdateTime;

	@Column(name="bluetoothRange")
	private Integer bluetoothRange;

	@Column(name="multiUser")
	private Boolean multiUser;

	public Boolean getMultiUser() {
		return multiUser;
	}
	public void setMultiUser(Boolean multiUser) {
		this.multiUser = multiUser;
	}	
	public Integer getBluetoothRange() {
		return bluetoothRange;
	}
	public void setBluetoothRange(Integer bluetoothRange) {
		this.bluetoothRange = bluetoothRange;
	}
	public Boolean getAutoUpdate() {
		return autoUpdate;
	}
	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}
	public Boolean getMirrorSource() {
		return mirrorSource;
	}
	public void setMirrorSource(Boolean mirrorSource) {
		this.mirrorSource = mirrorSource;
	}
	
	
	public String getFirmwareUpdateTime() {
		return firmwareUpdateTime;
	}
	public void setFirmwareUpdateTime(String firmwareUpdateTime) {
		this.firmwareUpdateTime = firmwareUpdateTime;
	}
	public Double getCurrentFirmwareVersion() {
		return currentFirmwareVersion;
	}
	public void setCurrentFirmwareVersion(Double currentFirmwareVersion) {
		this.currentFirmwareVersion = currentFirmwareVersion;
	}
	
	public String getBluetoothStatus() {
		return bluetoothStatus;
	}
	public void setBluetoothStatus(String bluetoothStatus) {
		this.bluetoothStatus = bluetoothStatus;
	}
	public Boolean getMirrorPreview() {
		return mirrorPreview;
	}
	public void setMirrorPreview(Boolean mirrorPreview) {
		this.mirrorPreview = mirrorPreview;
	}
	public String getMotionStopTime() {
		return motionStopTime;
	}
	public void setMotionStopTime(String motionStopTime) {
		this.motionStopTime = motionStopTime;
	}
	public int getMirrorStatus() {
		return mirrorStatus;
	}
	public void setMirrorStatus(int mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	public String getWifissid() {
		return wifissid;
	}
	public void setWifissid(String wifissid) {
		this.wifissid = wifissid;
	}
	
	public String getDeviceMode() {
		return deviceMode;
	}
	public void setDeviceMode(String deviceMode) {
		this.deviceMode = deviceMode;
	}
	public int getMajor() {
		return major;
	}
	public void setMajor(int major) {
		this.major = major;
	}
	public int getMinor() {
		return minor;
	}
	public void setMinor(int minor) {
		this.minor = minor;
	}

}
