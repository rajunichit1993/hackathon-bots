package com.mindbowser.homeDisplay.dto;

import java.io.File;
import java.io.Serializable;


public class EmailLogDTO implements Serializable {

	private static final long serialVersionUID = -7645517832019406737L;
	private String[] to;
	private String[] cc;
	private String from;
	private String subject;
	private File file;
	private String fileName;
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	

	
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
	
}
