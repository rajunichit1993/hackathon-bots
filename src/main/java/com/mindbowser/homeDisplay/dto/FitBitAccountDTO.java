package com.mindbowser.homeDisplay.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fitbitaccount")
public class FitBitAccountDTO {
	
    @Id
    @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
    
    @Column(name="fitbitAccessToken")
    private String fitbitAccessToken;

    @Column(name="fitbitRefreshToken")
    private String fitbitRefreshToken;
	
	@Column(name="fitbitUserId")
	private String fitbitUserId;
    
    @Column(name="fitbitUserTimeZone")
    private String fitbitUserTimeZone;
    
    @Column(name="fitbitUserName")
    private String fitbitUserName;
   
    
    public String getFitbitUserName() {
		return fitbitUserName;
	}
	public void setFitbitUserName(String fitbitUserName) {
		this.fitbitUserName = fitbitUserName;
	}
	public String getFitbitUserTimeZone() {
		return fitbitUserTimeZone;
	}
	public void setFitbitUserTimeZone(String fitbitUserTimeZone) {
		this.fitbitUserTimeZone = fitbitUserTimeZone;
	}
	public int getId() {
		return id;
	}
	public String getFitbitAccessToken() {
		return fitbitAccessToken;
	}
	public void setFitbitAccessToken(String fitbitAccessToken) {
		this.fitbitAccessToken = fitbitAccessToken;
	}
	public String getFitbitRefreshToken() {
		return fitbitRefreshToken;
	}
	public void setFitbitRefreshToken(String fitbitRefreshToken) {
		this.fitbitRefreshToken = fitbitRefreshToken;
	}
	public String getFitbitUserId() {
		return fitbitUserId;
	}
	public void setFitbitUserId(String fitbitUserId) {
		this.fitbitUserId = fitbitUserId;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
