package com.mindbowser.homeDisplay.dto;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="betausermirror")
public class BetaUserSmartMirrorDTO {
	
	@Id
	@Column(name="id")
	private int id;

	@Column(name="mirrorStatus")
	private int mirrorStatus;
	
	@Column(name="deviceId")
	private String deviceId;
	
	@Column(name="multiUser")
	private Boolean multiUser;

	public Boolean getMultiUser() {
		return multiUser;
	}
	public void setMultiUser(Boolean multiUser) {
		this.multiUser = multiUser;
	}	
	public int getMirrorStatus() {
		return mirrorStatus;
	}
	public void setMirrorStatus(int mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
