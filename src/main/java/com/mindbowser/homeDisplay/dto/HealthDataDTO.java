package com.mindbowser.homeDisplay.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "healthdata")
public class HealthDataDTO {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@ManyToOne  
	@JoinColumn(name = "userId")  
	private UserDTO user;
	
	@ManyToOne  
	@JoinColumn(name = "fitbitId")  
	private FitBitAccountDTO fitbit;
	
	
	public FitBitAccountDTO getFitbit() {
		return fitbit;
	}
	public void setFitbit(FitBitAccountDTO fitbit) {
		this.fitbit = fitbit;
	}

	@ManyToOne  
	@JoinColumn(name = "widgetId")  
	private WidgetDTO widget;
	
	@Column(name="healthTime")
	private String healthTime;
	
	@Column(name="healthValue")
	private Double healthValue;
	
	@Column(name="goalValue")
	private Double goalValue;
	
	public Double getGoalValue() {
		return goalValue;
	}
	public void setGoalValue(Double goalValue) {
		this.goalValue = goalValue;
	}
	public WidgetDTO getWidget() {
		return widget;
	}
	public void setWidget(WidgetDTO widget) {
		this.widget = widget;
	}
	public String getHealthTime() {
		return healthTime;
	}
	public void setHealthTime(String healthTime) {
		this.healthTime = healthTime;
	}
	public Double getHealthValue() {
		return healthValue;
	}
	public void setHealthValue(Double healthValue) {
		this.healthValue = healthValue;
	}

	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
