package com.mindbowser.homeDisplay.dto;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="widgetSetting")
public class WidgetSettingDTO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@ManyToOne(cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
	@JoinColumn(name = "userMirrorId")
	private UserMirrorDTO userMirror;
	
	@ManyToOne(cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
	@JoinColumn(name = "widgetId")
	private WidgetDTO widget;
	
	@ManyToOne
	@JoinColumn(name = "mirrorPageId")
	private MirrorPageDTO mirrorPage;
	
	@Column(name="status")
	private String status;
	
	@Column(name="viewType")
	private String viewType;
	
	@Column(name="xPos")
	private double xPos;
	
	@Column(name="yPos")
	private double yPos;
	
	@Column(name="width")
	private double width;
	
	@Column(name="height")
	private double height;
	
	@Column(name="deviceWidth")
	private double deviceWidth;
	
	@Column(name="deviceHeight")
	private double deviceHeight;

	@Column(name="minHeight")
	private double minHeight;
	
	@Column(name="minWidth")
	private double minWidth;
	
	@Column(name="pinned")
	private Boolean pinned;
	
	@OneToMany(cascade = CascadeType.ALL,orphanRemoval=true,fetch = FetchType.EAGER)
    @JoinColumn(name="widgetSettingId",nullable=false) 
	private List<StickyNotesDTO> stickyNotes;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserMirrorDTO getUserMirror() {
		return userMirror;
	}

	public void setUserMirror(UserMirrorDTO userMirror) {
		this.userMirror = userMirror;
	}

	public WidgetDTO getWidget() {
		return widget;
	}

	public void setWidget(WidgetDTO widget) {
		this.widget = widget;
	}

	public MirrorPageDTO getMirrorPage() {
		return mirrorPage;
	}

	public void setMirrorPage(MirrorPageDTO mirrorPage) {
		this.mirrorPage = mirrorPage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public double getxPos() {
		return xPos;
	}

	public void setxPos(double xPos) {
		this.xPos = xPos;
	}

	public double getyPos() {
		return yPos;
	}

	public void setyPos(double yPos) {
		this.yPos = yPos;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getDeviceWidth() {
		return deviceWidth;
	}

	public void setDeviceWidth(double deviceWidth) {
		this.deviceWidth = deviceWidth;
	}

	public double getDeviceHeight() {
		return deviceHeight;
	}

	public void setDeviceHeight(double deviceHeight) {
		this.deviceHeight = deviceHeight;
	}

	public double getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(double minHeight) {
		this.minHeight = minHeight;
	}

	public double getMinWidth() {
		return minWidth;
	}

	public void setMinWidth(double minWidth) {
		this.minWidth = minWidth;
	}

	public Boolean getPinned() {
		return pinned;
	}

	public void setPinned(Boolean pinned) {
		this.pinned = pinned;
	}

	public List<StickyNotesDTO> getStickyNotes() {
		return stickyNotes;
	}

	public void setStickyNotes(List<StickyNotesDTO> stickyNotes) {
		this.stickyNotes = stickyNotes;
	}

}
