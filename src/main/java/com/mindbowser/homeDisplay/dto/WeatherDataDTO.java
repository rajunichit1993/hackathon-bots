package com.mindbowser.homeDisplay.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="weatherdata")
public class WeatherDataDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="weatherTime")
	private String weatherTime;
	
	@Column(name="minTemperature")
	private int minTemperature;

	@Column(name="maxTemperature")
	private int maxTemperature;
	
	@Column(name="currentTemperature")
	private int currentTemperature;
	
	@Column(name="description")
	private String description;
	
	@Column(name="weatherType")
	private String weatherType;
	
	@Column(name="icon")
	private String icon;
	
	@ManyToOne
	@JoinColumn(name = "locationId")
	private LocationDTO location;
	
	
	public String getWeatherTime() {
		return weatherTime;
	}

	public void setWeatherTime(String weatherTime) {
		this.weatherTime = weatherTime;
	}
	
	public String getWeatherType() {
		return weatherType;
	}

	public void setWeatherType(String weatherType) {
		this.weatherType = weatherType;
	}

	public LocationDTO getLocation() {
		return location;
	}

	public void setLocation(LocationDTO location) {
		this.location = location;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(int minTemperature) {
		this.minTemperature = minTemperature;
	}

	public int getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public int getCurrentTemperature() {
		return currentTemperature;
	}

	public void setCurrentTemperature(int currentTemperature) {
		this.currentTemperature = currentTemperature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
}
