package com.mindbowser.homeDisplay.dto;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mirrorpage")
public class MirrorPageDTO {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="pageNumber")
	private int pageNumber;
	
	@Column(name="delay")
	private int delay;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "userMirrorId")
	private UserMirrorDTO userMirror;


	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public UserMirrorDTO getUserMirror() {
		return userMirror;
	}

	public void setUserMirror(UserMirrorDTO userMirror) {
		this.userMirror = userMirror;
	}
		
}
